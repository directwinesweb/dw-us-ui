#!/usr/bin/env node
const rollup = require('rollup');
const path = require('path');
const resolve = require('@rollup/plugin-node-resolve').default;
const babel = require('@rollup/plugin-babel').default;
const postcss = require('rollup-plugin-postcss');
const filesize = require('rollup-plugin-filesize');
const del = require('rollup-plugin-delete');
const currentWorkingPath = process.cwd();

const image = require('@rollup/plugin-image');

let isDev = false;
process.argv.forEach(function (val, index, array) {
	if (val.includes('builder-watch')) {
		isDev = true;
	}
});
// Little refactor from where we get the code
const { src, name } = require(path.join(currentWorkingPath, 'package.json'));

// build input path using the src
const inputPath = path.join(currentWorkingPath, src);

// Little hack to just get the file name
const fileName = name.replace('@dw-us-ui/', '');

// see below for details on the options
const inputOptions = {
	input: inputPath,
	external: [
		'react',
		'emotion-theming',
		'@emotion/core',
		'prop-types',
		'@emotion/styled',
		'react-dom',
		'axios',
		'react-helmet',
		'dateformat',
		'card-validator',
		'react-router-dom',
	],
	plugins: [
		filesize(),
		resolve(),
		postcss({
			// Key configuration
			modules: true,
		}),
		babel({
			presets: ['@babel/preset-env', '@babel/preset-react'],
			babelHelpers: 'bundled',
			exclude: '/node_modules/',
		}),
		del({ targets: 'dist/*' }),
		image(),
	],
};

const outputOptions = [
	{
		file: `dist/${fileName}.cjs.js`,
		format: 'cjs',
	},
	{
		file: `dist/${fileName}.esm.js`,
		format: 'esm',
	},
];

async function build() {
	// create bundle
	const bundle = await rollup.rollup(inputOptions);
	// loop through the options and write individual bundles
	outputOptions.forEach(async (options) => {
		await bundle.write(options);
	});
}

const watchOptions = {
	...inputOptions,
	output: outputOptions,
	watch: {
		buildDelay: 1000,
		clearScreen: true,
		exclude: '/node_modules/',
	},
};

async function watchFiles() {
	// create bundle
	const watcher = rollup.watch(watchOptions);
	watcher.on('event', async (e) => {
		switch (e.code) {
			case 'BUNDLE_START':
				console.log('🚀  Watch mode started');
				break;
			case 'ERROR':
			case 'FATAL':
				console.error(e.error);
				break;
			case 'BUNDLE_END':
				process.exitCode = 0;
				console.log('Finished bundling, waiting for file change...');
				break;
		}
	});

	watcher.on('event', ({ result }) => {
		if (result) {
			result.close();
		}
	});

	// stop watching
	watcher.close();
}

if (isDev) {
	watchFiles();
} else {
	build();
}
