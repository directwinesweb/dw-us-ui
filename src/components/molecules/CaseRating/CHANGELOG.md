# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/case-rating@1.0.2...@dw-us-ui/case-rating@1.0.3) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/case-rating






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/case-rating@1.0.1...@dw-us-ui/case-rating@1.0.2) (2021-07-02)

**Note:** Version bump only for package @dw-us-ui/case-rating





## 1.0.1 (2021-06-30)


### Bug Fixes

* fixed style and refactoring ([2e6b84f](https://bitbucket.org/directwinesweb/dw-us-ui/commits/2e6b84f80046b28e4ac7d1bb93c2bbaf221c46e2))
* refactored components ([2d1125c](https://bitbucket.org/directwinesweb/dw-us-ui/commits/2d1125c03f06afaebc58b647213b4139a4fe1f42))
* test case fixes ([e1d8d94](https://bitbucket.org/directwinesweb/dw-us-ui/commits/e1d8d9428644435578c7603dc2d431627f5f941a))
