import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import { ReviewStars } from '@dw-us-ui/review-stars';
import { Tooltip } from '@dw-us-ui/tooltip';

/* Logic for Ratings
For each wine in the case where the review count > 0, multiply the number of bottles of that
wine that are included in the case by the wine’s average overall rating.  Then, sum all these
values and divide it by the total number of bottles of wines that have a review count > 0 that are in the case.

For example, for a case where wine A has a 3.0 average overall rating and has 6 bottles in the
case; wine B has a 4.5 average overall rating and 4 bottles in the case; and wine C doesn’t have any reviews and has 2 bottles in the case, the case’s average overall rating would be calculated as follows:

(((6*3.0)+(4*4.5))/ 10) = 3.6

To display the average overall rating for the case, at least 50% of the unique wines in the case should have at least 1 rating.  If this requirement isn’t satisfied, we would like to not display the Average Bottle Rating for the case.

*/

const SytledCaseRating = styled.div`
  overflow: auto;
  .stars-container {
    display: inline-block;
    margin: 0 5px 0 0px;
    .star-alignment {
      vertical-align: inherit;
      margin: 0 2px;
    }
    .info-alignment {
      margin-left: -10px;
    }
  }
  label {
    cursor: default;
    font-weight: normal;
    font-style: italic;
    font-size: 0.9rem;
    padding: 0 2px 0 0;
    display: inline-block;
    ${EmotionBreakpoint("mobile")} {
      .wsj & {
        font-size: 0.8rem;
      }
      .npr & {
        font-size: 0.8rem;
      }
    }
    span {
      font-style: normal;
      margin-right: 2px;
    }
  }
  ${EmotionBreakpoint("mobile")} {
    margin-bottom: 10px;
  }
  .popover-container {
    vertical-align: inherit;
  }
}`;


const CaseRating = ({
    caseContents,
    gaAction,
    gaCategory,
    productDetails
}) => {
    if (!caseContents.length) {
        return null;
      }
      const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	  const brand = BrandUtil.getBrand(brandTag);
      const tag = BrandUtil.content(brand).tag;
    
      let caseContentsResponse = caseContents;
      let caseContentsArray = [];
    
      let totalRatings = 0;
      let overallCaseRating;
      let caseSize = 0;
    
      let { setEventTracking } = GoogleAnalytics;
    
      let popover = (
          `Indicates average (combined) bottle rating of each wine in this
          collection.`
      );
      /*
        Loop through products and create an array with just the data that we need
        Add calculatedRating which takes averageRating * quantity
    */
      caseContentsResponse.forEach(function (product, index) {
        if (product.referencedProduct.ratingDetails) {
          caseContentsArray.push({
            itemCode: product.skuItemCode,
            quantity: product.quantity,
            averageRating:
              product.referencedProduct.ratingDetails.productRating.avgRating,
            numberOfReviews:
              product.referencedProduct.ratingDetails.productRating.numberOfReviews,
            calculatedRating:
              product.quantity *
              product.referencedProduct.ratingDetails.productRating.avgRating,
            hasRating: true,
          });
        } else {
          // Add items without ratings, so we can get totals
          caseContentsArray.push({
            itemCode: product.skuItemCode,
            quantity: product.quantity,
            hasRating: false,
          });
        }
      });
    
      // Set Total in Case
      let totalinCase = caseContentsArray.length;
      // Filter out only those that have a rating
      let filteredItems = caseContentsArray.filter((obj) => {
        return obj.hasRating === true;
      });
      // Set Total with Ratings in Case
      let totalwithRatings = filteredItems.length;
      // Calculate percentage that have ratings
      let percentageRated =
        ((totalwithRatings - totalinCase) / totalinCase) * 100 + 100;
      // Loop through products with ratings and add up case size and calculated rating
      filteredItems.forEach((obj) => {
        totalRatings += obj.calculatedRating;
        caseSize += obj.quantity;
      });
    
      // Show if 50% of wines have at least 1 wine rating
      if (
        caseContentsArray.length > 0 &&
        percentageRated >= 50 &&
        productDetails.subProductType !== "Z098"
      ) {
        overallCaseRating = (totalRatings / caseSize).toFixed(1);
      } else {
        return null;
      }
    
      let gaEvent = () => {
        setEventTracking({
          category: gaCategory,
          action: gaAction,
          label: "Rating_Info_Icon_Click",
        });
    };
    
    return (
        <SytledCaseRating>
          <div className="col-md-12 no-pad">
            <ReviewStars rating={overallCaseRating} />
            <label className="rating">
              <span>{overallCaseRating}</span> Average bottle rating
            </label>
            &nbsp;
            <Tooltip
                tooltipContent={popover}
                tooltipPosition='bottom'
            >
                <span aria-hidden='true'>
                    <img className='info-alignment'src={`/images/us/common/icons/${tag}/info_circle_solid_4x12.svg`} width="40px" height="40px" />
                </span>
            </Tooltip>
          </div>
        </SytledCaseRating>
    );
};

CaseRating.propTypes= {
    caseContents: PropTypes.array.isRequired,
    productDetails: PropTypes.object,
    gaCategory: PropTypes.string,
    gaAction: PropTypes.string,
};

CaseRating.defaultProps= {};

export default CaseRating;
