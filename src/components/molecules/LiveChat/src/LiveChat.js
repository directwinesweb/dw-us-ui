import React, { useState } from 'react';
import PropTypes from 'prop-types';

const LiveChat = ({ brand, trigger }) => {
	const [mounted, setMounted] = useState(false);
	const token = {
		law: '240561AC638B18710DF0E7DAABEE5052',
		wsj: '6E176418607D066C38A1430971B167E6',
		vir: '1F4889EE4BD30582F634C74DB3FE4899',
	};

	const cssString =
		'.webchatStartButtonContainer{ transform: rotate(90deg); right: -50px; left: inherit!important; top: inherit!important; bottom: 175px; width: 150px!important;} .webchatContainer { right: 0px!important;}';

	const renderLiveChat = () => {
		setMounted(true);
		let proto = document.location.protocol || 'http:';
		let node = document.createElement('script');
		node.type = 'text/javascript';
		node.async = true;
		node.src =
			proto +
				'//webchat-s2g.i6.inconcertcc.com/v3/click_to_chat?token=' +
				token[brand] || token['law'];

		let s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(node, s);

		let style = document.createElement('style');
		style.appendChild(document.createTextNode(cssString));
		document.head.append(style);
	};
	return trigger && brand && !mounted ? (
		<script>{renderLiveChat()}</script>
	) : null;
};

LiveChat.propTypes = {
	/**
	 * Determines access token consumed by liveChat
	 */
	brand: PropTypes.string,
	/**
	 * Triggers live chat
	 */
	trigger: PropTypes.bool,
};

LiveChat.defaultProps = {
	trigger: false,
};

export default LiveChat;
