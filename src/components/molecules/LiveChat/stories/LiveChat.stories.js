import React from 'react';
import LiveChat from '../src/LiveChat';

export default {
	title: 'Utils/LiveChat',
	parameters: {
		component: LiveChat,
	},
};

export const Default = () => <LiveChat trigger={true} brand={'law'} />;
