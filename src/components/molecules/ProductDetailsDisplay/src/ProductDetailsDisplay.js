import React from 'react';
import PropTypes from 'prop-types';

import { BottleZoom } from '@dw-us-ui/bottle-zoom';
import { CaseRating } from '@dw-us-ui/case-rating';
import { defaultImageUrls } from '@dw-us-ui/constants';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import { ProductAttributes } from '@dw-us-ui/product-attributes';
import { ProductDescription } from '@dw-us-ui/product-description';
import { ProductHeadline } from '@dw-us-ui/product-headline';
import { ProductRatingStars } from '@dw-us-ui/product-rating-stars';

const ProductDetailsDisplay = ({
    caseContents,
    componentName,
    gaAction,
    gaCategory,
    learnMoreGaLabel,
    product,
    quantity,
    qvEnabled,
    ratingsGaLabel
}) => {
    let productDetails = product;
    let productType = productDetails.productType;
    let bottleType = productDetails.bottleType?.toLowerCase();
    let { setEventTracking } = GoogleAnalytics;
    let { DEFAULT_MIXED_IMG, DEFAULT_SINGLE_S_IMG } = defaultImageUrls;
    
    let onProductClick = (e) => {
      e.preventDefault();
      setEventTracking({
        category: gaCategory,
        action: gaAction,
        label: learnMoreGaLabel,
      });
      let type = productDetails.mixed ? "mixed" : "single";
      //Function from us_tests js to launch Quickview
  
      if (productDetails.mixed) {
        if (
          window.pageLayer[0].brand === "law" &&
          window.pageLayer[0].brandDomain !== "npr"
        ) {
          document
            .getElementById("quick-view-modal-input")
            .setAttribute("data-type", type);
          document
            .getElementById("quick-view-modal-input")
            .setAttribute("value", productDetails.itemCode);
          document.getElementById("quick-view-modal-submit").click();
        } else {
          quickview.getProduct(productDetails.itemCode, type);
        }
      } else {
        if (
          window.pageLayer[0].brand === "law" &&
          window.pageLayer[0].brandDomain !== "npr"
        ) {
          document
            .getElementById("quick-view-modal-input")
            .setAttribute("data-type", type);
          document
            .getElementById("quick-view-modal-input")
            .setAttribute("value", productDetails.itemCode);
          document.getElementById("quick-view-modal-submit").click();
        } else {
          quickview.getProductTab(productDetails.itemCode, type);
        }
      }
      quickview.addToRecentlyBrowsed(productDetails.itemCode);
    };
  
    let getBottle = () => {
      let quantityText = productType === "wine" ? bottleType === 'can' ? 'Packs' : "Bottles" : "Items";
      return (
        <div className="row single-product-details">
          <div className="item-image bottle-zoom col-sm-3">
            <div className="zoom-image">
              {productType === "wine" ? (
                <BottleZoom
                  image={productDetails.largeImage}
                  imgWidth="39"
                  imgHeight="230"
                  itemCode={productDetails.itemCode}
                  componentName={componentName}
                  gaCategory={gaCategory}
                  gaAction={gaAction}
                />
              ) : (
                <img
                  className="media-object"
                  src={productDetails.largeImage}
                  alt={productDetails.name}
                  onError={(e) => {
                    e.target.src = DEFAULT_SINGLE_S_IMG;
                  }}
                />
              )}
            </div>
          </div>
          <div className="item-details col-sm-9">
            <ProductHeadline
              productDetails={productDetails}
              qvEnabled={true}
              onProductClick={onProductClick}
              saleTagEnabled={false}
            />
            {productType === "wine" ? (
              <div>
                <ProductRatingStars
                  ratingDetails={productDetails.ratingDetails}
                  itemCode={productDetails.itemCode}
                  showRating={true}
                  gaCategory={gaCategory}
                  gaAction={gaAction}
                  ratingsGaLabel={ratingsGaLabel}
                />
                <ProductAttributes
                  product={productDetails}
                  attributeLabelClass="col-md-4 col-xs-5"
                  attributeValueClass="col-md-8 col-xs-7"
                />
              </div>
            ) : (
              false
            )}
            <p className="item-description">
              <ProductDescription description={productDetails.description} />
            </p>
            {quantity > 0 ? (
              <p className="number-of-bottles">
                {quantityText} in case: {quantity}
              </p>
            ) : (
              false
            )}
            {qvEnabled ? (
              <div className="col-xs-12 no-pad learn-more-link">
                <a href="#" onClick={(e) => onProductClick(e)}>
                  <strong>Learn More</strong>
                </a>
              </div>
            ) : (
              false
            )}
          </div>
        </div>
      );
    };
  
    let getCase = () => {
      return (
        <div className="row mixed-case-details">
          <div className="item-image col-xs-12 text-center">
            <img
              className="img-responsive"
              src={productDetails.largeImage}
              alt={productDetails.name}
              onError={(e) => {
                e.target.src = DEFAULT_MIXED_IMG;
              }}
            />
          </div>
          <div className="item-details col-xs-12 no-pad">
            <div className="col-xs-12 no-pad">
              <ProductHeadline
                productDetails={productDetails}
                qvEnabled={true}
                onProductClick={onProductClick}
              />
            </div>
            <div className="col-xs-12 no-pad">
              <CaseRating
                caseContents={caseContents}
                productDetails={productDetails}
                gaCategory=""
                gaAction=""
              />
            </div>
            <div className="col-xs-12 no-pad">
              <ProductDescription description={productDetails.description} />
            </div>
            <div className="col-xs-12 no-pad learn-more-link">
              <a href="#" onClick={(e) => onProductClick(e)}>
                <strong>Learn More</strong>
              </a>
            </div>
          </div>
        </div>
      );
    };
  
    return productDetails.mixed ? getCase() : getBottle();
};

ProductDetailsDisplay.propTypes= {
    product: PropTypes.object.isRequired,
    quantity: PropTypes.number,
    qvEnabled: PropTypes.bool,
    caseContents: PropTypes.array,
    gaCategory: PropTypes.string,
    gaAction: PropTypes.string,
    ratingsGaLabel: PropTypes.string,
    componentName: PropTypes.string,
};

ProductDetailsDisplay.defaultProps= {
    quantity: 0,
    caseContents: [],
    qvEnabled: false,
};

export default ProductDetailsDisplay;
