# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/product-details-display@1.0.2...@dw-us-ui/product-details-display@1.0.3) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/product-details-display






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/product-details-display@1.0.1...@dw-us-ui/product-details-display@1.0.2) (2021-07-02)


### Bug Fixes

* font change, texas winery change and skudata bug fix ([4c3b6a5](https://bitbucket.org/directwinesweb/dw-us-ui/commits/4c3b6a5ad624222d79abe00406038cd10234ac83))





## 1.0.1 (2021-06-30)


### Bug Fixes

* down merge with develop ([4ebda58](https://bitbucket.org/directwinesweb/dw-us-ui/commits/4ebda5886377131d7c78d8b27db1af5960282401))
* optimized code and ui fixes ([069b043](https://bitbucket.org/directwinesweb/dw-us-ui/commits/069b043299c9a184246ceff62ea4121a4170c922))
