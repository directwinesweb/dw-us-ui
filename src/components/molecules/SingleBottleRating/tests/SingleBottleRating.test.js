import SingleBottleRating from '../src/SingleBottleRating';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('SingleBottleRating test', () => {
    let itemCode = 'M12145';
    let triggerTab = function() { return true };
    let ratingDetails ={};

        it('Creates snapshot test for <SingleBottleRating />', () => {
            const wrapper = shallow(
                <SingleBottleRating
                    itemCode={itemCode}
                    triggerTab={triggerTab}
                    ratingDetails={ratingDetails}
                ></SingleBottleRating>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });