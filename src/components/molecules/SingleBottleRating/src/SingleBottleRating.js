import React, { useEffect, useState } from 'react';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { GetEnvironment } from '@dw-us-ui/get-environment';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import PropTypes from 'prop-types';
import { ReviewStars } from '@dw-us-ui/review-stars';
import axios from 'axios';
import { bvApiConfig } from '@dw-us-ui/constants';
import styled from '@emotion/styled';

const SytledSingleBottleRating = styled.div`
	text-align: left;
	.rating-stars {
		display: inline-block;
		.stars-container {
			display: inline-block;
			margin: 0 5px 0 -2px;
			.fa {
				font-size: 1.2em;
				color: #e09d00 !important;
				margin: 0 2px;
				.law & {
					color: ${({ theme }) => theme.colors.body} !important;
				}
				.mcy & {
					color: ${({ theme }) => theme.colors.headerRed} !important;
				}
			}
		}
	}
	font-size: 1.2em;
	.law & {
		font-size: 11px;
	}
	.wsj & {
		font-size: 1.1em;
	}
	.preview-total-reviews {
		cursor: default;
		font-weight: normal;
		padding: 0 2px 0 0;
		display: inline-block;
		.wsj & {
			font-size: 1em;
		}
		.vir & {
			font-size: 1em;
		}
		.law & {
			margin-left: 5px;
		}
		${EmotionBreakpoint('mobile')} {
			padding: 0;
			margin-bottom: 10px;
		}
	}
	.rating-total-reviews {
		${EmotionBreakpoint('desktop')} {
			display: block;
			margin-top: 5px;
			margin-bottom: 10px;
		}
	}
	.product-reviews-link {
		${EmotionBreakpoint('mobile')} {
			display: block;
			margin-bottom: 10px;
		}
	}
	.write-review-link {
		margin: 0px 0px 0px 10px;
		border-left: 2px solid #d9d9d9;
		padding: 0px 10px;
		${EmotionBreakpoint('mobile')} {
			display: block;
			margin: 0 0 10px;
			padding: 0px;
			border-left: none;
		}
	}
`;

const SingleBottleRating = ({
	gaAction,
	gaCategory,
	itemCode,
	ratingDetails,
	triggerTab,
}) => {
	const [userReview, setUserReview] = useState(false);
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const tag = BrandUtil.content(brand).tag;
	const country = BrandUtil.getCountry();
	const { setEventTracking } = GoogleAnalytics;

	ratingDetails =
		ratingDetails.productRating === undefined ? false : ratingDetails;

	useEffect(() => {
		let environment = GetEnvironment();
		let authorId = pageLayer[0].vid;

		let url =
			bvApiConfig[environment].url +
			'/data/reviews.json?apiversion=5.4&passkey=' +
			bvApiConfig[environment][country][tag].key +
			'&Filter=ProductId:' +
			itemCode +
			'&Filter=AuthorId:' +
			authorId;

		axios.get(url).then((review) => {
			let response = review.data;
			let userReview = response.Results[0] !== undefined;

			setUserReview(userReview);
		});
	}, []);

	const setGaEvent = (label) => {
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: label,
		});
	};

	const setStars = () => {
		let avgRating = ratingDetails ? ratingDetails.productRating.avgRating : 0;
		return <ReviewStars rating={avgRating} />;
	};

	const setLabel = () => {
		if (!ratingDetails) {
			// Wine has not yet been reviewed.
			return (
				<a
					className='rating-total-reviews'
					href={'/jsp/product/common/createReview.jsp?itemCode=' + itemCode}
					onClick={() => setGaEvent('Write_Review')}
				>
					Write the first review
				</a>
			);
		} else {
			let numberOfReviews =
				ratingDetails.productRating.numberOfReviews.toLocaleString();
			let avgReview =
				ratingDetails.productRating.avgRating.toFixed(1) + ' out of 5';
			let readReviewCopy;

			// Need to update the readRatingURL once the Customer Reviews tab has been created
			let readRatingUrl = '#';
			// let readRatingUrl = "/product/" + this.state.itemCode;
			if (numberOfReviews === '1') {
				readReviewCopy = 'Read review';
			} else {
				readReviewCopy = 'Read all ' + numberOfReviews + ' reviews';
			}
			let writeReviewCopy, writeRatingUrl;
			if (userReview) {
				writeReviewCopy = 'See Your Review Here';
				// Need to update the writeRatingUrl once the Customer Reviews tab has been created
				writeRatingUrl = '#';
			} else {
				writeReviewCopy = 'Write a review';
				writeRatingUrl =
					'/jsp/product/common/createReview.jsp?itemCode=' + itemCode;
			}

			let customerReviewsClick = (e, gaLabel) => {
				setGaEvent(gaLabel);
				if (gaLabel !== 'Write_Review') {
                    if (gaLabel === 'Read_Reviews') {
                        triggerTab(e, 'Customer Reviews');
                        let element = document.getElementById('reviews-app');
						let headerOffset = window.innerWidth >= 500 ? 300 : 500;
						let elementPosition = element.offsetTop;
                          let offsetPosition = elementPosition - headerOffset;
                          window.scrollTo({
                            top: elementPosition + 100,
                            behavior: "smooth"
                          });
                    } 
                    else {
                    triggerTab(e, 'Customer Reviews');
                    }
                }
			};

			return (
				<span>
					<label className='preview-total-reviews'>{avgReview}</label>
					<label>
						<a
							className='product-reviews-link'
							href={readRatingUrl}
							onClick={(e) => customerReviewsClick(e, 'Read_Reviews')}
						>
							{readReviewCopy}
						</a>
						{userReview ? (
							<a
								className='write-review-link'
								href={writeRatingUrl}
								onClick={(e) => customerReviewsClick(e, 'Read_My_Review')}
							>
								{writeReviewCopy}
							</a>
						) : (
							<a
								className='write-review-link'
								href={writeRatingUrl}
								onClick={(e) => customerReviewsClick(e, 'Write_Review')}
							>
								{writeReviewCopy}
							</a>
						)}
					</label>
				</span>
			);
		}
	};

	return (
		<SytledSingleBottleRating>
			<ul className='rating-stars'>{setStars()}</ul>
			{setLabel()}
		</SytledSingleBottleRating>
	);
};

SingleBottleRating.propTypes = {
	ratingDetails: PropTypes.object.isRequired,
	itemCode: PropTypes.string.isRequired,
	triggerTab: PropTypes.func.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
};

SingleBottleRating.defaultProps = {
	ratingDetails: {},
	itemCode: '',
};

export default SingleBottleRating;
