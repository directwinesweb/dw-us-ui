import React from 'react';
import SearchbarInput from '../src/SearchbarInput';

export default {
	title: 'Molecules/SearchbarInput',
	parameters: {
		component: SearchbarInput,
	},
};

export const Default = () => <SearchbarInput />;
