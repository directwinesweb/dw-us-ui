import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
const StyledSearchBarInput = styled.div`
	position: relative;
	input {
		height: 52px;
		width: 100%;
		padding: 15px 15px 15px 65px;
		border: 1px solid #ccc;
		transition: border-color 0.2s;
		color: rgba(0, 0, 0, 0.76);
		:focus {
			outline: none;
			border-color: #8e8e8e;
		}
	}
	img {
		position: absolute;
		font-size: 18px;
		color: #9e9e9e;
		top: 50%;
		left: 22px;
		transform: translateY(-50%);
	}
`;

const SearchBarInput = ({
	placeholder,
	className,
	handleOnChange,
	handleOnFocus,
	cypressClass,
	brandTag,
	...props
}) => {
	return (
		<StyledSearchBarInput className={className}>
			<input
				data-cy={cypressClass}
				placeholder={placeholder}
				onChange={handleOnChange}
				onFocus={handleOnFocus}
				{...props}
			/>
			<img
				src={`/images/us/common/icons/${brandTag}/search_solid_16x19.svg`}
				alt='search'
				width='20px'
				height='25px'
			/>
		</StyledSearchBarInput>
	);
};

SearchBarInput.propTypes = {
	placeholder: PropTypes.string,
	className: PropTypes.string,
	handleOnFocus: PropTypes.func,
	handleOnChange: PropTypes.func,
	cypressClass: PropTypes.string,
	brandTag: PropTypes.string,
};

SearchBarInput.defaultProps = {
	placeholder: 'Search',
	className: null,
	handleOnChange: () => {},
	handleOnFocus: () => {},
	cypressClass: 'search-bar',
	brandTag: 'wsj',
};
export default SearchBarInput;
