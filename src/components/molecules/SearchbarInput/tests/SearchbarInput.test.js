import React from 'react';
import SearchbarInput from '../src/SearchbarInput';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('SearchbarInput test', () => {
	it('Creates snapshot test for <SearchbarInput />', () => {
		const wrapper = shallow(<SearchbarInput></SearchbarInput>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
