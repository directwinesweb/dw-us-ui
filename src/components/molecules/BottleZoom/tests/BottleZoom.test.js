import BottleZoom from '../src/BottleZoom';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('BottleZoom test', () => {
        it('Creates snapshot test for <BottleZoom />', () => {
            const wrapper = shallow(<BottleZoom></BottleZoom>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });