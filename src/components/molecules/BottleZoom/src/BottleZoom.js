import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { defaultImageUrls } from '@dw-us-ui/constants';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';

const BottleZoom = (props) => {
    const [state, setState] = useState({
		imageError: false,
		imageClass: '',
		activeZoom: false,
		index: props.index,
		itemCode: props.itemCode,
		sourceImg: {
			width: props.imgWidth,
			height: props.imgHeight,
			url: props.image
		},
		overlayStyle: {
			display: 'none',
			background: `white url(${props.image}) no-repeat`,
            border: '1px solid #000',
            width: '200px',
            height: '175px',
            position: 'absolute',
            top: 0,
            left: '150px',
            zIndex: 1000
		}
	});
    const imageZoom = useRef();
    const overlay = useRef();
    const zoomImage = useRef();
    let { setEventTracking } = GoogleAnalytics;
    let { DEFAULT_SINGLE_S_IMG, DEFAULT_SINGLE_L_IMG } = defaultImageUrls;
	const brandData = BrandUtil.content(BrandUtil.getBrand());
	const brandTag = brandData.tag;

	useEffect(() => {
		let el = document.getElementsByTagName('body')[0];
		el.addEventListener('touchstart', () => {
			if (state.activeZoom === true) {
				closeZoom();
			}
		});
	}, []);

	useEffect(() => {
		setState((prevState) => {
			return {
				...prevState,
				imageError: false,
				activeZoom: false
			}
		});
	}, [props.index, props.itemCode]);

	const setGaEvent = (e) => {
		let componentName = props.componentName !== undefined ? props.componentName : false;

		if (componentName.length > 0) {
			setEventTracking({
				category: props.gaCategory,
				action: props.gaAction,
				label: componentName + '_Zoom_Launch'
			});
		} else {
			setEventTracking({
				category: 'Casebuilder_LP',
				action: 'Product_Details',
				label: 'Zoom'
			});
		}
	}

	const openZoom = (e) => {
		/*
		Find the absolute page position of the source image to be able to map to the zoomimage
	 */
		let cumulativeOffset = function(element) {
			let top = 0,
				left = 0;
			do {
				top += element.offsetTop || 0;
				left += element.offsetLeft || 0;
				element = element.offsetParent;
			} while (element);

			return {
				top: top,
				left: left
			};
		};
		let offset = cumulativeOffset(e.target);

		/*
		Find where the mouse is compared to the source image borders
	 */
		let relativeX = (e.pageX - offset.left) / e.target.width;
		let relativeY = (e.pageY - offset.top) / e.target.height;

		let element = zoomImage.current;

		/*
	 	 Get the width and height of the overlay box. Removes "px" prefix
	  */
		let overlayWidthString = window.getComputedStyle(overlay.current).width;
		let overlayWidth = overlayWidthString.substring(0, overlayWidthString.length - 2);
		let overlayHeightString = window.getComputedStyle(overlay.current).height;
		let overlayHeight = overlayHeightString.substring(0, overlayHeightString.length - 2);

		/*
		 Impose the mouse coordinates on the bigger image, corrected for the overlay
		 coordinates (0, 0) being in the top left corner, not the middle
	  */
		let posX = -(relativeX * element.width - overlayWidth / 2);
		let posY = -(relativeY * element.height - overlayHeight / 2);

		/*
		 Set the state with the new coordinates
	  */
		if (state.imageError === false) {
			setState((prevState) => {
				return {
					...prevState,
					activeZoom: true,
					overlayStyle: {
						...state.overlayStyle,
						display: 'block',
						background: `white url(${props.image}) no-repeat`,
						backgroundPosition: posX + 'px ' + posY + 'px'
					}
				}
			});
		}
	}

	const closeZoom = (e) => {
		if (state.activeZoom) {
			setState((prevState) => {
				return {
					...prevState,
					overlayStyle: {
						...state.overlayStyle,
						display: 'none'
					}
				}
			});
		}
	}

	const imageError = (e) => {
		e.target.src = props.imgHeight < 150 ? DEFAULT_SINGLE_S_IMG : DEFAULT_SINGLE_L_IMG;
		setState((prevState) => {
			return {
				...prevState,
				imageError: true,
				imageClass: 'empty-image'
			}
		});
	}

	return (
		<div className={props.customClass}>
			<div id='overlay' style={state.overlayStyle} ref={overlay} className='zoom-overlay' />
				<img
					src={props.image}
					onMouseEnter={setGaEvent}
					onMouseMove={openZoom}
					onMouseOut={closeZoom}
					ref={imageZoom}
					height={props.imgHeight}
					onError={imageError}
					className={'source-image ' + state.imageClass}
				/>
			{props.componentName !== undefined && !state.imageError ? (
				<span className='text-zoom col-xs-12 no-pad'>
					<span aria-hidden='true'>
						<img src={`/images/us/common/icons/${brandTag}/search_plus_solid_11x12.svg`} alt="search" height="11px" width="16px" />
					</span>
					ZOOM
				</span>
			) : (
				false
			)}
			<img className='hidden' ref={zoomImage} src={props.image} />
		</div>
	);
};

BottleZoom.propTypes= {
    image: PropTypes.string.isRequired,
	imgWidth: PropTypes.string.isRequired,
	imgHeight: PropTypes.string.isRequired,
	itemCode: PropTypes.string.isRequired,
	index: PropTypes.number,
	componentName: PropTypes.string
};

BottleZoom.defaultProps= {
    image: '',
	imgWidth: '',
	imgHeight: '',
	itemCode: '',
	index: 0,
	componentName: ''
};

export default BottleZoom;
