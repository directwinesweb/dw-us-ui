import React from 'react';
import PropTypes from 'prop-types';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';

const FlavorProfiles = ({
	activeSection,
	flavorProfile,
	gaAction,
	gaCategory,
	title
}) => {
    let isActive = false;
	let tabName = 'Flavor Profile';
	let { setEventTracking } = GoogleAnalytics;

	/*
		Check if Tab is Active
	 */
	if (activeSection === tabName) {
		isActive = true;
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: 'Flavor_Profiles_Open'
		});
	}

	/*
			Check if Flavor Profiles
			is False
		 */
	if (flavorProfile === false) {
		// setEventTracking({
		// 	'category': props.gaCategory,
		// 	'action': props.gaAction,
		// 	'label': 'Flavor_Profiles_No_Copy'
		// });
		return null;
	}

	/*
		Set Tab Content
	 */
	let getContent = () => {
		return <div className='content-container' dangerouslySetInnerHTML={{__html: flavorProfile}} />;
	};

	/*
		Return Flavor Profiles Tab
	 */
	return (
		<div className='flavor-profiles tab-component'>
			<h2><strong>{title}</strong></h2>
			{getContent()}
		</div>
	);
};

FlavorProfiles.propTypes= {
    productConfig: PropTypes.object,
	setActiveSection: PropTypes.func.isRequired,
	activeSection: PropTypes.string.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string
};

FlavorProfiles.defaultProps= {};

export default FlavorProfiles;
