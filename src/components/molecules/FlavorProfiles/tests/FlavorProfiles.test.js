import FlavorProfiles from '../src/FlavorProfiles';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('FlavorProfiles test', () => {
    let setActiveSection = function() { return true };
    let activeSection = 'Flavor profile';
        it('Creates snapshot test for <FlavorProfiles />', () => {
            const wrapper = shallow(<FlavorProfiles 
                setActiveSection={setActiveSection} 
                activeSection={activeSection}
                ></FlavorProfiles>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });