import { select, text } from '@storybook/addon-knobs';

import React from 'react';
import Tooltip from '../src/Tooltip';

export default {
	title: 'Molecules/Tooltip',
	parameters: {
		component: Tooltip,
		componentSubtitle: 'Displays a branded tooltip component',
	},
};

const positions = {
	top: 'top',
	left: 'left',
	right: 'right',
	bottom: 'bottom',
};
export const Default = () => (
	<Tooltip
		tooltipContent={text('Content', 'I am tooltip')}
		tooltipPosition={select('Position', positions, 'bottom')}
	>
		Click me
	</Tooltip>
);
