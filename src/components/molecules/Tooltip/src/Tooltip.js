import React, { useCallback, useEffect, useRef, useState } from 'react';

import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';
import { getPosition } from './PositionMethods';
import styled from '@emotion/styled';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';
import { useEventListener } from '@dw-us-ui/use-event-listener';
import { useOnClickOutside } from '@dw-us-ui/use-onclick-outside';

const TooltipContainer = styled.div`
	display: inline-block;
	vertical-align: text-bottom;
`;

const Tooltip = ({ children, tooltipPosition, tooltipContent }) => {
	const theme = useCustomTheme();
	const [showPopover, setShowPopover] = useState(false);
	const [position, setPosition] = useState({});
	let clientDOMs;
	// Create a ref that we add to the element for which we want to detect outside clicks
	const containerRef = useRef();
	const contentRef = useRef();

	// Call hook passing in the ref and a function to call on outside click
	useOnClickOutside(containerRef, () => setShowPopover(false));

	const popoverStyle = {
		...position,
		fontFamily: theme.fonts.primary,
		lineHeight: theme.fonts.lineHeight,
		visibility: showPopover ? 'visible' : 'hidden',
		opacity: showPopover ? 1 : 0,
		padding: '9px 14px',
		display: 'block',
		position: 'fixed',
		marginTop: '12px',
		transition: 'opacity 0.4s, visibility 0.4s',
		backgroundColor: '#fff',
		border: '1px solid #ccc',
		fontSize: '14px',
		zIndex: 1051,
		maxWidth: '276px',
		letterSpacing: '0.7px',
		color: '#444444',
	};

	// Event handler utilizing useCallback ...
	// ... so that reference never changes.
	const handler = useCallback(() => {
		setShowPopover(false);
	}, []);

	// Add event listener using our hook
	// useEventListener('scroll', handler);

	// const [scrollPosition, setSrollPosition] = useState(0);
	// const handleScroll = () => {
	// 	const position = window.pageYOffset;
	// 	setSrollPosition(position);
	// };

	useEffect(() => {
		window.addEventListener('scroll', handler, { passive: true });

		return () => {
			window.removeEventListener('scroll', handler);
		};
	}, []);

	function togglePopover(e) {
		e.preventDefault();

		setShowPopover(!showPopover);

		setPositions();
	}

	function setPositions() {
		if (!clientDOMs) {
			clientDOMs = {
				contentReact: contentRef.current.getBoundingClientRect(),
				targetRect: containerRef.current.getBoundingClientRect(),
			};
		}
		const newPosition = getPosition(
			tooltipPosition,
			clientDOMs.contentReact,
			clientDOMs.targetRect
		);
		setPosition(newPosition);
	}

	const renderPopover = () => {
		return createPortal(
			<div
				className={`popover fade in ${tooltipPosition}`}
				style={popoverStyle}
				ref={contentRef}
			>
				<div className='arrow' />
				<div className='popover-content'>{tooltipContent}</div>
			</div>,
			document.body
		);
	};

	return (
		<TooltipContainer ref={containerRef} className='popover-container'>
			<a href='#' onClick={(e) => togglePopover(e)}>
				{children}
			</a>
			{renderPopover()}
		</TooltipContainer>
	);
};

Tooltip.propTypes = {
	children: PropTypes.node,
	tooltipPosition: PropTypes.oneOf(['bottom', 'top', 'left', 'right']),
	tooltipContent: PropTypes.node,
};

Tooltip.defaultProps = {
	children: null,
	tooltipPosition: 'top',
	tooltipContent: 'Hello',
};

export default Tooltip;
