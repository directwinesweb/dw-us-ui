export const getPosition = (placement, elRect, targetRect) => {
	switch (placement) {
		case 'top':
			return getTopPosition(elRect, targetRect);
		case 'right':
			return getRightPosition(elRect, targetRect);
		case 'bottom':
			return getBottomPosition(elRect, targetRect);
		case 'left':
			return getLeftPosition(elRect, targetRect);
		case 'auto':
			return getAutoPosition(elRect, targetRect);
		default:
			return getBottomPosition(elRect, targetRect);
	}
};

const getTopPosition = (elRect, targetRect) => {
	const popupLeftPosition = targetRect.left + targetRect.width / 2 - elRect.width / 2;
	return {
		left: Math.sign(popupLeftPosition) === -1 ? '4px' :  popupLeftPosition,
		top: targetRect.top - elRect.height,
	};
};

const getRightPosition = (elRect, targetRect) => {
	return {
		left: targetRect.left + targetRect.width,
		top: targetRect.top + targetRect.height / 2 - elRect.height / 2,
	};
};

const getBottomPosition = (elRect, targetRect) => {
	return {
		left: targetRect.left + targetRect.width / 2 - elRect.width / 2,
		top: targetRect.top + targetRect.height,
	};
};

const getAutoPosition = (elRect, targetRect) => {
	if (targetRect.left > elRect.width) {
		return {
			left: targetRect.left - elRect.width,
			top: targetRect.top + targetRect.height - elRect.height / 2,
		};
	} else if (window.innerWidth - targetRect.right > elRect.width / 2) {
		return {
			left: targetRect.right - targetRect.width,
			top: targetRect.top + targetRect.height - elRect.height / 2,
		};
	} else if (targetRect.top > elRect.height) {
		return {
			left: targetRect.left + targetRect.width - elRect.width / 2,
			top: targetRect.top - elRect.height,
		};
	} else {
		return {
			left: targetRect.left + targetRect.width - elRect.width / 2,
			top: targetRect.top + targetRect.height,
		};
	}
};

const getLeftPosition = (elRect, targetRect) => {
	return {
		left: targetRect.left - elRect.width,
		top: targetRect.top + targetRect.height / 2 - elRect.height / 2,
	};
};
