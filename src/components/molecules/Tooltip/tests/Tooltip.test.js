import React from 'react';
import Tooltip from '../src/Tooltip';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Tooltip test', () => {
	it('Creates snapshot test for <Tooltip />', () => {
		const wrapper = shallow(<Tooltip>Test</Tooltip>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
