import React, { useEffect, useRef, useState } from 'react';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import { Modal } from '@dw-us-ui/modals';
import PropTypes from 'prop-types';
import { StyledImageZoomStyled } from './ImageZoomStyled';
import { defaultImageUrls } from '@dw-us-ui/constants';
import { getAltTagValue } from './getAltTagValue';
import { useWindowWidth } from '@dw-us-ui/use-window-width';

const ImageZoom = ({
	className,
	disableZoom,
	gaAction,
	gaCategory,
	imageClassName,
	productDetails,
	type,
	caseContents,
}) => {
	const [imageZoom, setImageZoom] = useState({
		showModal: false,
		mouseDown: false,
		allowClose: true,
		zoomStyle: {
			top: 0,
			left: 0,
		},
		zoomImageLoaded: false,
		modalWindowWidth: null,
		modalWindowHeight: null,
	});
	let productZoom = useRef();

	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	const { caseImageUrl, tag } = data;
	let isTouchDevice = false,
		imagePathShort = type === 'mixed' ? caseImageUrl : '/images/us/en/product/',
		imagePath = imagePathShort + productDetails?.itemCode,
		numberOfBottles = productDetails?.skus[0]?.numberOfBottles,
		isMystery = productDetails?.subProductType === 'Z098';
	let { setEventTracking } = GoogleAnalytics;
	let { DEFAULT_SINGLE_T_IMG, DEFAULT_MIXED_IMG } = defaultImageUrls;
	let [sourceImage, setSourceImage] = useState(null);
	let [sourceClick, setSourceClick] = useState(null);
	let [zoomImage, setZoomImage] = useState(null);

	useEffect(() => {
		imageClassName = className || imageClassName;
		let sourceImage = document.getElementsByClassName(
			`source-image ${imageClassName}`
		)[0];

		console.log('Source image', sourceImage, className, imageClassName);
		setSourceImage(sourceImage);

		// If the window detects a touch, it will set isTouchDevice to true
		window.addEventListener('touchstart', () => {
			isTouchDevice = true;
		});
	}, []);

	useEffect(() => {
		if (imageZoom.zoomImageLoaded) {
			setZoomLocation();
		}
	}, [imageZoom.zoomImageLoaded]);

	const setZoomLocation = () => {
		let cumulativeOffset = function (element) {
			var top = 0,
				left = 0;
			do {
				top += element.offsetTop || 0;
				left += element.offsetLeft || 0;
				element = element.offsetParent;
			} while (element);

			return {
				top: top,
				left: left,
			};
		};

		let offset = cumulativeOffset(sourceImage);

		/*
			 Find the location of the click compared to the edges of the image, in percents.
			 (clicking the middle of the image gives .5 to these variables)
			*/
		let relativeY = (sourceClick.pageY - offset.top) / sourceImage.height;
		let relativeX = (sourceClick.pageX - offset.left) / sourceImage.width;

		/*
            Set the starting location (startX, startY) based off of the location of the click 
            on the smaller image, respectively.
        */
		let zoomImgWidth = getWidth(zoomImage);
		let zoomImgHeight = getHeight(zoomImage);

		let startX = relativeX * zoomImgWidth;
		let startY = relativeY * zoomImgHeight;

		/*
            Get the width and height of the window to keep the image from being placed, and being moved, past the edges
        */
		let modalWindowWidth = getWidth(zoomImage.parentElement);
		let modalWindowHeight = getHeight(zoomImage.parentElement);

		/*
	 			Find the distance between the click and the middle of the modal window. These values are
	 			used for the new top and left values for the zoom style state
	 		 */
		let newZoomX = -(startX - 0.5 * modalWindowWidth);
		let newZoomY = -(startY - 0.5 * modalWindowHeight);

		/*
	 			Find the farthest the picture can go without showing whitespce. If the new zoom values are
	 			outside of these bounds, set them to the bounds
	 		 */
		// let minLeft = -(zoomImgWidth - modalWindowWidth);
		// let minTop = -(zoomImgHeight- modalWindowHeight);

		/*
	 			Make sure the new zoom values are not out of the bounds
	 		 */
		newZoomX = checkBoundsLeft(newZoomX);
		newZoomY = checkBoundsTop(newZoomY);

		if (isTouchDevice) {
			let zoomImageContainer = document.getElementsByClassName(
				'zoom-image-container'
			)[0];

			let modalBody =
				document.getElementsByClassName('product-zoom-modal')[0].children[0]
					.children[0].children[1];
			modalBody.className += ' touchscreen-scroll';
			zoomImageContainer.className += ' touchscreen-scroll';

			zoomImageContainer.scrollTop = -newZoomY;
			zoomImageContainer.scrollLeft = -newZoomX;
		} else {
			setImageZoom((prevState) => {
				return {
					...prevState,
					zoomStyle: {
						position: 'absolute',
						top: newZoomY,
						left: newZoomX,
					},
					modalWindowHeight: modalWindowHeight,
					modalWindowWidth: modalWindowWidth,
				};
			});
		}
	};

	const triggerImageLoaded = () => {
		let zoomImage = document.getElementsByClassName('zoom-image')[0];
		setZoomImage(zoomImage);

		setImageZoom((prevState) => {
			return {
				...prevState,
				zoomImageLoaded: true,
			};
		});
	};

	/*
	    Sets the sourceClick and sourceImage feilds, sets the zoomStyle to trigger an update
     */
	const openModal = (e) => {
		e.persist();
		setSourceClick(e);
		let product = productDetails;

		setImageZoom((prevState) => {
			return {
				...prevState,
			};
		});

		if (
			product.subProductType !== 'Z098' &&
			sourceImage.getAttribute('class').indexOf('default') === -1
		) {
			setEventTracking({
				category: gaCategory,
				action: gaAction,
				label: 'Zoom_Launch',
			});

			setImageZoom((prevState) => {
				return {
					...prevState,
					showModal: true,
					zoomStyle: {
						top: 1,
						left: 1,
					},
				};
			});
		}
	};

	/*
		If user mouseUp's outside the modal while dragging, the modal won't close. Sets state
		to allow next mouseUp to close the modal
     */
	const closeModal = () => {
		if (imageZoom.allowClose) {
			setImageZoom((prevState) => {
				return {
					...prevState,
					showModal: false,
					zoomImageLoaded: false,
				};
			});
		} else {
			setImageZoom((prevState) => {
				return {
					...prevState,
					allowClose: true,
					zoomImageLoaded: false,
				};
			});
		}
	};

	/*
		Get the first coordinates on mouseDown in order to calculate distance when the mouse moves.
     */
	const setInitial = (e) => {
		e.persist();
		setImageZoom((prevState) => {
			return {
				...prevState,
				stateX: e.pageX,
				stateY: e.pageY,
				mouseDown: true,
			};
		});
	};

	/*
		On mouseUp, stop the image from calculating shift
     */
	const stopShiftMouseUp = (e) => {
		setImageZoom((prevState) => {
			return {
				...prevState,
				mouseDown: false,
			};
		});
	};

	/*
		On mouseOut, stop the image from calculating shift, determines if the modal can close or not
     */
	const stopShiftMouseOut = (e) => {
		let allowClose = imageZoom.mouseDown === false ? true : false;

		setImageZoom((prevState) => {
			return {
				...prevState,
				mouseDown: false,
				allowClose,
			};
		});
	};

	/*
	 	Gets the computated width of an element. Can't directly get style if it isn't inline
     */
	const getWidth = (element) => {
		let widthString = window.getComputedStyle(element).width;
		return widthString.substring(0, widthString.length - 2);
	};

	/*
	 	Gets the computated height of an element. Can't directly get style if it isn't inline
     */
	const getHeight = (element) => {
		let heightString = window.getComputedStyle(element).height;
		return heightString.substring(0, heightString.length - 2);
	};

	/*
		Checks the new zoom left value to make sure they don't go out of bounds.
		i.e. so there is no whitespace on the left
     */
	const checkBoundsLeft = (left) => {
		let minLeft = -(productZoom.current.width - imageZoom.modalWindowWidth);

		if (left > 0) {
			return 0;
		} else if (left < minLeft) {
			return minLeft;
		} else {
			return left;
		}
	};

	/*
		Checks the new zoom top value to make sure they don't go out of bounds.
		i.e. so there is no whitespace on top
     */
	const checkBoundsTop = (top) => {
		let minTop = -(productZoom.current.height - imageZoom.modalWindowHeight);

		if (top > 0) {
			return 0;
		} else if (top < minTop) {
			return minTop;
		} else {
			return top;
		}
	};

	/*
		Caluclates the distance moved every time the mouse moves. If move event happens when
		mouse is down, it will disable the modal from closing on the next mouseUp
     */
	const startShift = (event) => {
		event.persist();
		if (imageZoom.mouseDown === true) {
			/*
		    		Find the distance from current position to last time the state was set
		     */
			let distanceX = event.pageX - imageZoom.stateX;
			let distanceY = event.pageY - imageZoom.stateY;

			/*
		    		Add the distance to the current state coordinates
		     */
			let newZoomX = imageZoom.zoomStyle.left + distanceX;
			let newZoomY = imageZoom.zoomStyle.top + distanceY;

			/*
			    Make sure the new zoom locations aren't outside the bounds
		     */
			newZoomX = checkBoundsLeft(newZoomX);
			newZoomY = checkBoundsTop(newZoomY);

			/*
			    Update the state with the new image position, as well as change the current state coordinates
		     */
			setImageZoom((prevState) => {
				return {
					...prevState,
					allowClose: false,
					zoomStyle: {
						position: 'absolute',
						top: newZoomY,
						left: newZoomX,
					},
					startX: event.pageX,
					stateY: event.pageY,
				};
			});
		}
	};

	const centerCase = () => {
		if (numberOfBottles === 2 || numberOfBottles === 3) {
			return 'center-case';
		}
	};

	const setModalClass = () => {
		//Sets the class foir the modal, depending on number of bottles
		//Widths are set using these classes in the sass code
		let modalWidthClass =
			'product-zoom-modal product-zoom-modal-container ' + type;

		if (numberOfBottles === 2) {
			modalWidthClass += ' f-code';
		} else if (numberOfBottles === 3) {
			modalWidthClass += ' j-code';
		} else if (numberOfBottles >= 6) {
			modalWidthClass += ' full-width';
		} else {
			modalWidthClass += ' bottle-width';
		}

		return modalWidthClass;
	};

	const getDefaultSource = () => {
		let source = '';
		if (tag === 'law') {
			if (type === 'single') {
				if (window.innerWidth < 768) {
					source = '/images/us/law/common/bottle_unavailable_66x240.png';
				} else {
					source = '/images/us/law/common/bottle_unavailable_88x374.png';
				}
			} else {
				if (window.innerWidth < 768) {
					source = '/images/us/law/common/case_unavailable_300x142.png';
				} else {
					source = '/images/us/law/common/case_unavailable.png';
				}
			}
		} else if (tag === 'wsj' || tag === 'vir') {
			if (type === 'single') {
				if (window.innerWidth < 768) {
					source = DEFAULT_SINGLE_T_IMG;
				} else {
					source = `/images/us/${tag}/common/image_unavailable/bottle_unavailable_${
						tag === 'vir' ? 88 : 145
					}x374.png`;
				}
			} else {
				if (window.innerWidth < 768) {
					source = `/images/us/${tag}/common/image_unavailable/case_unavailable_300x142.png`;
				} else {
					source = DEFAULT_MIXED_IMG;
				}
			}
		} else {
			if (type === 'single') {
				source = DEFAULT_SINGLE_T_IMG;
			} else {
				source = DEFAULT_MIXED_IMG;
			}
		}
		return source;
	};

	const handleError = (e) => {
		setEventTracking({
			category: gaCategory,
			action: 'Errors',
			label: 'Old_Cobble_' + productDetails.itemCode,
		});

		let imgEl = e.target;
		imgEl.src = getDefaultSource();
		let currentClassImg = imgEl.getAttribute('class');
		if (!currentClassImg.includes('default')) {
			imgEl.setAttribute('class', currentClassImg + ' default');
		}

		setImageZoom((prevState) => {
			return {
				...prevState,
				imageError: true,
			};
		});
	};

	let zoomImageSource =
		type === 'single' ? imagePath + '_XL.jpg' : imagePath + '_3XL.jpg';
	let enableZoom = isMystery || imageZoom.imageError ? false : true;

	let mysteryClassName = isMystery ? ' mystery' : '';
	let mainImageClass =
		'source-image ' + type + mysteryClassName + ` ${className || ''}`;

	if (disableZoom) {
		mainImageClass += ' default';
	}

	//Single bottle and mixed case have different image sizes for different device types
	let desktopImageSize = type === 'single' ? '_L' : '_XL';
	let desktopImagePath = imagePath + desktopImageSize + '.jpg';
	let tabletImageSize = type === 'single' ? '_L' : '_T';
	let tabletImagePath = imagePath + tabletImageSize + '.jpg';
	let mobileImageSize = type === 'single' ? '_T' : '_T';
	let mobileImagePath = imagePath + mobileImageSize + '.jpg';

	// alt tag

	const [finalImagePath, setFinalImagePath] = useState(desktopImagePath);

	let altTag = getAltTagValue({
		isMixed: type === 'mixed',
		productDetails,
		caseContents,
	});

	const windowWidth = useWindowWidth();

	useEffect(() => {
		if (windowWidth > 991) {
			setFinalImagePath(desktopImagePath);
		} else if (windowWidth > 767 && windowWidth <= 991) {
			setFinalImagePath(tabletImagePath);
		} else {
			setFinalImagePath(mobileImagePath);
		}
	}, [windowWidth]);

	return (
		<StyledImageZoomStyled>
			<div className={centerCase()}>
				<div className={'main-image ' + type}>
					<div>
						<div className='image-block'>
							<img
								className={mainImageClass}
								src={finalImagePath}
								onClick={(e) => (windowWidth > 767 ? openModal(e) : null)}
								onError={(e) => handleError(e)}
								alt={altTag}
							/>
						</div>
					</div>
					{enableZoom && !disableZoom ? (
						<div className='notMobileZoom'>
							<div className='zoom-text'>
							<img src={`/images/us/common/icons/${brandTag}/search_plus_solid_11x12.svg`} alt="search" width="16px" />
							{' '}CLICK IMAGE TO ZOOM
							</div>
						</div>
					) : (
						<div className='notMobileZoom'>
							<div className='zoom-text' />
						</div>
					)}
				</div>

				<div className='notMobileZoom'>
					<Modal
						showModal={imageZoom.showModal}
						disableAnimate={false}
						disableClose={false}
						zoomImageClassName='rc-modal-body-zoom'
						modalHeader={productDetails.name}
						toggleModal={closeModal}
					>
						<div
							className='zoom-image-container'
							onMouseMove={(e) => startShift(e)}
							onMouseDown={(e) => setInitial(e)}
							onMouseUp={(e) => stopShiftMouseUp(e)}
							onMouseOut={() => stopShiftMouseOut()}
						>
							<img
								ref={productZoom}
								style={imageZoom.zoomStyle}
								className='zoom-image'
								src={zoomImageSource}
								onLoad={() => triggerImageLoaded()}
								alt={altTag}
							/>
						</div>
					</Modal>
				</div>
			</div>
		</StyledImageZoomStyled>
	);
};

ImageZoom.propTypes = {
	productDetails: PropTypes.object.isRequired,
	type: PropTypes.string.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
	disableZoom: PropTypes.bool,
	caseContents: PropTypes.node,
};

ImageZoom.defaultProps = {
	disableZoom: false,
	imageClassName: '',
	caseContents: null,
};

export default ImageZoom;
