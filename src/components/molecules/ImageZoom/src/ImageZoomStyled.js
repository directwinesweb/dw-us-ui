import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import styled from '@emotion/styled';

export const StyledImageZoomStyled = styled.div`
	.main-image {
		.source-image {
			cursor: zoom-in;
			max-width: 100%;
			margin-top: 20px;
			&.mixed {
				margin-left: 20px;
				${EmotionBreakpoint('mobile')} {
					margin: 0;
				}
			}
			&.single {
				margin-top: 0;
				${EmotionBreakpoint('notMobile')} {
					max-height: 480px;
				}
			}
			&.mystery {
				cursor: default;
			}
			&.default {
				cursor: default;
				.vir &.mixed {
					margin-left: 0;
				}
				// .wsj &.mixed {
				// 	width: 550px;
				// }
			}
		}
		.zoom-text {
			margin: 20px;
			display: block;
		}
		&.single {
			text-align: center;
		}
		&.mixed {
			${EmotionBreakpoint('mobile')} {
				text-align: center;
				margin: 0 0 30px;
			}
		}
		.image-block {
			display: inline-block;
		}
		.notMobileZoom {
			display: inline-block;
		}
		.mobileZoom {
			display: none;
		}
		.tabletZoom {
			display: none;
		}

		${EmotionBreakpoint('tablet')} {
			.notMobileZoom {
				display: none;
			}
			.mobileZoom {
				display: none;
			}
			.tabletZoom {
				display: inline-block;
			}
		}

		${EmotionBreakpoint('mobile')} {
			.notMobileZoom {
				display: none;
			}
			.tabletZoom {
				display: none;
			}
			.mobileZoom {
				display: inline-block;
			}
		}
	}

	.product-zoom-backdrop.product-zoom-backdrop {
		opacity: 0;
	}

	.product-zoom-modal {
		&.bottle-width {
			.rc-modal-dialog {
				width: 450px;
				left: -245px;
				bottom: -100px;
			}
		}
		&.f-code {
			.rc-modal-dialog {
				width: 600px;
			}
		}
		&.j-code {
			.rc-modal-dialog {
				width: 610px;
			}
		}
		&.full-width {
			.rc-modal-dialog {
				width: 970px;
			}
		}
		.rc-modal-header {
			.close {
				opacity: 1;
				margin-top: 2px;
				font-size: 25px;
			}
			.rc-modal-title {
				font-size: 1.5em;
				.wsj & {
					font-size: 1.4em;
				}
				text-align: left;
			}
		}
		.rc-modal-body {
			width: 100%;
			height: 542px;
			.wsj & {
				height: 540px;
			}
			.npr & {
				height: 542px;
			}
			.mcy & {
				height: 542px;
			}
			overflow: hidden;
			padding: 0;
			position: relative;
			.zoom-image-container {
				width: 100%;
				height: 100%;
				cursor: move;
				.zoom-image {
					pointer-events: none;
					cursor: all-scroll;
				}
			}
		}
		.touchscreen-scroll {
			overflow-x: scroll;
			overflow-y: scroll;
		}
		&.mixed {
			.rc-modal-content {
				width: 100%;
				height: 600px;
			}
		}
	}

	${EmotionBreakpoint('notDesktop')} {
		.product-zoom-modal {
			&.bottle-width {
				.modal-dialog {
					left: -145px;
				}
			}
			&.mixed {
				.modal-dialog {
					max-width: 750px;
				}
			}
			.modal-body {
				max-width: 100%;
			}
		}
	}
`;
