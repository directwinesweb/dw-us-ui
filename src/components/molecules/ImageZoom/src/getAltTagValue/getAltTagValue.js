/**
 *
 *
 * @param ProductDetails - Product details object for single bottle or mixed case
 * @param CaseContents - Case contents for mixed case
 * @param isMixed - determines if the alt tag to be generated should be for mixed or single products
 *
 * @returns alt tag value based on product information
 */
const getAltTagValue = ({
	productDetails = null,
	caseContents = null,
	isMixed = false,
}) => {
	// for mixed cases, a case contents must be supplied
	let altTag = '';
	if (isMixed && caseContents?.length) {
		let grapeCollection = [];
		caseContents.forEach((caseObj) => {
			grapeCollection.push(getGrapeName(caseObj?.referencedProduct));
		});

		if (grapeCollection.length) {
			altTag = grapeCollection.join('-');
		}
	} else if (productDetails && !isMixed) {
		altTag = getGrapeName(productDetails);
	}

	return altTag;
};

const getGrapeName = (productDetails) => {
	if (productDetails) {
		return productDetails.grapeName || '';
	}

	return '';
};
export default getAltTagValue;
