import ImageZoom from '../src/ImageZoom';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ImageZoom test', () => {
    let productDetails = {
        skus: []
    };
    let type = 'mixed';
        it('Creates snapshot test for <ImageZoom />', () => {
            const wrapper = shallow(
            <ImageZoom
                productDetails={productDetails}
                type={type}
            >
            </ImageZoom>
            );
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });