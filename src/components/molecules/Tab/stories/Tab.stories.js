import React, { useState } from 'react';

import Tab from '../src/Tab';
import { text } from '@storybook/addon-knobs';

export default {
	title: 'Molecules/Tab',
	parameters: {
		component: Tab,
	},
};

export const Default = () => {
	const [isActive, setIsActive] = useState(false);
	return (
		<Tab
			tabName={text('Tab Name', 'Click to open')}
			tabContent={text('Content', 'Text content')}
			isActive={isActive}
			onTabClick={() => setIsActive(!isActive)}
		/>
	);
};
