import Tab from '../src/Tab';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('Tab test', () => {
        it('Creates snapshot test for <Tab />', () => {
            const wrapper = shallow(<Tab></Tab>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });