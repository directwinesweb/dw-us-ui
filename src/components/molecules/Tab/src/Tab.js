import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledTabContainer = styled.div`
	width: 100%;
	color: ${({ theme }) => theme.colors.body};
	.tab-header-link {
		display: flex;
		cursor: pointer;
		border-bottom: ${({ isActive }) => (!isActive ? '1px solid #ccc' : 'none')};
		flex-direction: row;
		justify-content: space-between;
		align-items: center;
		height: 95px;
		.tab-indicator img {
			width: 18px;
			height: 18px;
			color: ${({ theme }) => theme.colors.body};
		}
		.tab-image {
			min-width: 50px;
			text-align: center;
		}
	}
	.tab-content {
		border-bottom: 1px solid #ccc;
		padding: 0px 0px 30px 0px;
		margin-top: -10px;
	}
	.tab-name {
		margin-left: ${({ tabImage }) => (tabImage ? '20px' : '0px')};
		padding-right: 10px;
		width: 100%;
		font-size: 18px;
		text-align: left;
		.law & {
			font-family: ${({ theme }) => theme.fonts.productHeadline};
		}
	}
`;
const Tab = ({
	isActive,
	className,
	tabImage,
	tabName,
	tabContent,
	onTabClick,
	tabId,
	cypressClass,
	brandTag,
}) => {
	const finalCypressClass = `${cypressClass}-${tabId ? tabId : 'tab'}`;
	return (
		<StyledTabContainer
			data-cy={finalCypressClass}
			className={`tab${className ? ' ' + className : ''}`}
			tabImage={tabImage}
			isActive={isActive}
			id={tabId}
		>
			<div
				data-cy={`${finalCypressClass}-header`}
				className='tab-header-link'
				onClick={() => {
					onTabClick(tabId);
				}}
			>
				{tabImage ? (
					<div className='tab-image'>
						<img src={tabImage} alt={tabName} />
					</div>
				) : null}
				<div className='tab-name'>{tabName}</div>
				<div className='tab-indicator'>
					{isActive ? (
						<img
							src={`/images/us/common/icons/${brandTag}/minus_solid_11x14.svg`}
							alt='minus'
							width='15px'
							height='20px'
						/>
					) : (
						<img
							src={`/images/us/common/icons/${brandTag}/plus_solid_11x14.svg`}
							alt=''
							width='15px'
							height='20px'
						/>
					)}
				</div>
			</div>
			{isActive ? (
				<div className='tab-content' data-cy={`${finalCypressClass}-content`}>
					{tabContent}
				</div>
			) : null}
		</StyledTabContainer>
	);
};

Tab.propTypes = {
	className: PropTypes.string,
	tabImage: PropTypes.string,
	tabName: PropTypes.string.isRequired,
	tabContent: PropTypes.node,
	isActive: PropTypes.bool,
	onTabClick: PropTypes.func.isRequired,
	tabId: PropTypes.string.isRequired,
	cypressClass: PropTypes.string,
	brandTag: PropTypes.string,
};
Tab.defaultProps = {
	className: null,
	tabImage: null,
	tabName: 'Tab Name',
	tabContent: 'Tab Content',
	isActive: false,
	onTabClick: () => {},
	tabId: '1',
	cypressClass: 'tab-cypress',
	brandTag: 'wsj',
};
export default Tab;
