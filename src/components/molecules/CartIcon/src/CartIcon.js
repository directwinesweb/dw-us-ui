import React, { useEffect, useState } from 'react';

import Axios from 'axios';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const StyledContainer = styled.div`
	display: flex;
	align-content: center;
	justify-content: center;
`;

const StyledItemCount = styled.div`
	position: relative;
	display: flex;
	align-content: center;
	justify-content: center;
	min-width: 13px;
	min-height: 12px;
	max-height: 16px;
	line-height: 16px;
	font-weight: 600;
	padding: 1px 2px;
	top: -6px;
	left: -10px;
	color: #fff;
	font-size: 0.7rem;
	background-color: ${({ theme }) => theme.colors.highlight};
	border-radius: 50%;
	border: 2px solid #fff;
`;

const lawIcon = '/assets/icons/us/law/cart-dark.svg';
const CartIcon = ({ brandTag }) => {
	const url = '/api/cart/list';
	const [itemCount, setItemCount] = useState(null);
	const [error, setError] = useState(null);

	useEffect(() => {
		Axios.get(url)
			.then((res) => {
				let result = res.data.response;
				setItemCount(result.numLineItems);
			})
			.catch((err) => {
				setError(err);
			});
	}, []);

	/**
	 * @todo need to add logic here for other brands and their respective icon based on brandTag
	 *
	 * */

	let iconUrl = lawIcon;

	let icon = <img src={iconUrl} alt='cart icon' width='25px' height='25px' />;

	let ItemCountDisplay =
		itemCount > 0 ? <StyledItemCount>{itemCount}</StyledItemCount> : '';

	let cartIconDisplay = {};

	cartIconDisplay = (
		<StyledContainer>
			{icon}
			{ItemCountDisplay}
		</StyledContainer>
	);

	return cartIconDisplay;
};

CartIcon.propTypes = {
	/**
	 * Brand tag to determine which icon to display
	 */
	brandTag: PropTypes.string,
};

CartIcon.defaultProps = {
	brandTag: 'law',
};

export default CartIcon;
