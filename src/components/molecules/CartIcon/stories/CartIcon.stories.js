import CartIcon from '../src/CartIcon';
import React from 'react';

export default {
	title: 'Molecules/CartIcon',
	parameters: {
		component: CartIcon,
	},
};

export const Default = () => <CartIcon />;
