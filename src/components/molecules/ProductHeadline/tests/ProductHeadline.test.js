import ProductHeadline from '../src/ProductHeadline';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ProductHeadline test', () => {
    let productDetails = {
        mixed: true,
        webHeadline: 'product headline',
        name: 'product 1',
        skus: []
    }
        it('Creates snapshot test for <ProductHeadline />', () => {
            const wrapper = shallow(<ProductHeadline productDetails={productDetails}></ProductHeadline>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });