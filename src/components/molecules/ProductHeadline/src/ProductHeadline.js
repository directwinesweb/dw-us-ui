import { BrandUtil } from '@dw-us-ui/brand-util';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import PropTypes from 'prop-types';
/* eslint-disable no-prototype-builtins */
/* eslint-disable react/prop-types */
import React from 'react';
import styled from '@emotion/styled';

const StyledProductHeader = styled.div`
	h3 {
		font-weight: normal;
	}
	text-align: left;
	margin-bottom: 10px;
	.web-headline {
		font-size: 1.6em;
		margin-bottom: 5px;
		font-family: ${({ theme, brandTag }) =>
			brandTag === 'wsj' ? theme.fonts.special : theme.fonts.primary};
		color: #000;
		.law & {
			font-weight: normal;
			line-height: 28px;
			color: ${({ theme }) => theme.colors.highlight};
			font-family: ${({ theme }) => theme.fonts.productHeadline};
			padding-top: 20px;
		}
		.wsj & {
			font-weight: bold;
			font-size: 1.8em;
			line-height: 28px;
			padding-top: 30px;
		}
		.npr & {
			padding-top: 30px;
		}
		${EmotionBreakpoint('mobile')} {
			margin-bottom: 15px;
			padding: 12px !important;
			text-align: center;
		}
	}
	.on-sale-tag {
		width: 25px;
	}
	.product-name {
		font-weight: normal;
		font-size: 1.2em;
		font-family: ${({ theme }) => theme.fonts.secondary};
		.wsj & {
			font-size: 1.1em;
			color: #616365;
		}
		.npr & {
			color: #6f6f6f;
		}
		.law & {
			font-size: 14px;
		}
		.single-version-one & {
			color: #333;
		}
		.single-version-two & {
			color: #848484;
			font-style: italic;
		}
	}
	.appelleation-name {
		font-weight: normal;
	}
	${EmotionBreakpoint('mobile')} {
		margin-bottom: 15px;
		.single-bottle-page-container & {
			margin-bottom: 30px;
		}
	}
`;

const ProductHeadline = ({
	onProductClick,
	productDetails,
	qvEnabled,
	saleTagEnabled,
}) => {
	const brandData = BrandUtil.content(BrandUtil.getBrand());
	const brandTag = brandData.tag;
	let isMixed = productDetails.mixed;
	let webHeadline = productDetails.webHeadline;
	let hasWebHeadline = webHeadline !== undefined ? true : false;
	let productName = productDetails.name;
	let grapeName = productDetails?.grapeName;
	let regionName = productDetails?.regionName;
	let country = productDetails?.countryName;
	let skus = productDetails.skus;
	let listPrice, salePrice;
	let isOnSale = false;

	if (isMixed && skus.length > 0) {
		listPrice = skus[0].listPrice;
		salePrice = skus[0].salePrice;
		isOnSale = listPrice === salePrice ? false : true;
	} else {
		let hasBCode = false;
		let hasCCode = false;
		let hasTCode = false;

		skus?.forEach((item) => {
			let itemCodePrefix = item.itemCode[0];
			if (itemCodePrefix === 'B') {
				hasBCode = true;
			} else if (itemCodePrefix === 'C') {
				hasCCode = true;
			} else if (itemCodePrefix === 'T') {
				hasTCode = true;
			}
		});

		if (hasBCode || hasCCode || hasTCode) {
			isOnSale = true;
		}
	}

	let onSaleCopy = () => {
		if (isOnSale && saleTagEnabled) {
			return (
				<img
					className='on-sale-tag'
					src='/images/us/common/icon_sale_tag.svg'
					title='On sale'
				/>
			);
		}
	};

	let setVintage = () => {
		let vintage = productDetails.hasOwnProperty('vintage')
			? productDetails.vintage
			: false;

		if (!vintage || !vintage.length || vintage === '1950') {
			return false;
		}

		return vintage;
	};

	let setWebHeadline = () => {
		if (qvEnabled) {
			return (
				<h1 className='web-headline'>
					<a href='#' onClick={(e) => onProductClick(e)}>
						{productName}{' '}
						{setVintage()}{' '}
						{onSaleCopy()}
					</a>
				</h1>
			);
		}

		return (
			<h1 className='web-headline'>
				{productName}{' '}
				{setVintage()}{' '}
				{onSaleCopy()}
			</h1>
		);
	};

	const getAppelleationName = () => {
		let grape = grapeName ? grapeName + ' from ' : '';
		let region = regionName ? regionName + ', ' : '';
		let appelleation = isMixed || grape?.toLowerCase()?.includes('not applicable') || country?.toLowerCase()?.includes('unspecified country') ? hasWebHeadline ? webHeadline : '' : grape + country;
		return (<h3 className='appelleation-name'>{appelleation}</h3>);
	}

	return (
		<StyledProductHeader brandTag={brandTag}>
			{setWebHeadline()}
			{getAppelleationName()}
		</StyledProductHeader>
	);
};

ProductHeadline.propTypes = {
	productDetails: PropTypes.object,
	qvEnabled: PropTypes.bool,
	onProductClick: PropTypes.func,
};

ProductHeadline.defaultProps = {
	qvEnabled: false,
	saleTagEnabled: true,
};

export default ProductHeadline;
