import ProductRatingStars from '../src/ProductRatingStars';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ProductRatingStars test', () => {
    let itemCode = 'M12145';
        it('Creates snapshot test for <ProductRatingStars />', () => {
            const wrapper = shallow(<ProductRatingStars itemCode={itemCode}></ProductRatingStars>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });