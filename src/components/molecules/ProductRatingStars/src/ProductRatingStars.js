import React from 'react';
import PropTypes from 'prop-types';
import styled from "@emotion/styled";

import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import { ReviewStars } from '@dw-us-ui/review-stars';

const SytledProductRatingDetails = styled.div`
	padding: 5px 0px 0px;
	.rating-stars {
		display: inline-block;
		margin-right: 5px;
		.stars-container {
			margin: 1px;
			display: inline-block;
			.rating-stars {
				color: $gil-gold;
				font-size: 1.5em;
				.wsj & {
					font-size: 1.4em;
				}
			}
		}
	}
	.preview-total-reviews {
		cursor: default;
		font-weight: normal;
		padding: 0 2px 0 0;
		display: inline-block;
		.wsj & {
			font-size: 1em;
		}
		.vir & {
			font-size: 1em;
		}
		${EmotionBreakpoint("mobile")} {
			padding: 0;
			margin-bottom: 10px;
		}
	}
	.product-reviews-link {
		border-left: 1px solid #ccc;
		margin-left: 6px;
		padding-left: 6px;
	}
	.rating-total-reviews {
		${EmotionBreakpoint("desktop")} {
			display: block;
			margin-top: 5px;
			margin-bottom: 10px;
		}
	}`;

const ProductRatingStars = ({
	gaAction,
	gaCategory,
	itemCode,
	ratingDetails,
	ratingsGaLabel,
	showRating
}) => {
    let ratingInfo = ratingDetails === undefined ? false : ratingDetails;
	showRating = showRating === undefined ? false : showRating;
	let { setEventTracking } = GoogleAnalytics;

	let setStars = () => {
		let avgRating = ratingInfo ? ratingInfo.productRating.avgRating : 0;
		return <ReviewStars rating={avgRating} />;
	};

	let gaEvent = () => {
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: ratingsGaLabel
		});
	};

	let setLabel = () => {
		if (ratingInfo) {
			let numberOfReviews = ratingInfo.productRating.numberOfReviews.toLocaleString();
			let avgReview = ratingInfo.productRating.avgRating.toFixed(1);
			let reviewCopy = numberOfReviews === 1 ? 'Review' : 'Reviews';

			if (showRating) {
				let url = '/product/' + itemCode + '?tabs=reviews#collapseReviews';
				return (
					<label className='preview-total-reviews'>
						{' '}
						{avgReview}
						<a className='product-reviews-link' href={url} onClick={() => gaEvent()}>
							{numberOfReviews} {reviewCopy}
						</a>
					</label>
				);
			} else {
				return <label className='preview-total-reviews'>({numberOfReviews})</label>;
			}
		}

		return (
			<a className='rating-total-reviews' href={'/jsp/product/common/createReview.jsp?itemCode=' + itemCode}>
				Write the first review
			</a>
		);
	};

	return (
		<SytledProductRatingDetails>
			<ul className='rating-stars'>{setStars()}</ul>
			{setLabel()}
		</SytledProductRatingDetails>
	);
};

ProductRatingStars.propTypes= {
    ratingDetails: PropTypes.object,
	itemCode: PropTypes.string.isRequired,
	showRating: PropTypes.bool,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
	ratingsGaLabel: PropTypes.string
};

ProductRatingStars.defaultProps= {};

export default ProductRatingStars;
