import FoodPairings from '../src/FoodPairings';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('FoodPairings test', () => {
    let itemCode = 'M12145';
    let isMixed = true;
    let setActiveSection = function() { return true };
    let activeSection = 'Food Pairings';
        it('Creates snapshot test for <FoodPairings />', () => {
            const wrapper = shallow(
                <FoodPairings
                    itemCode={itemCode}
                    isMixed={isMixed}
                    setActiveSection={setActiveSection}
                    activeSection={activeSection}            
                >
                </FoodPairings>
            );
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });