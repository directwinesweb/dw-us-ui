import React from 'react';
import PropTypes from 'prop-types';
import { FoodPairingUtils } from '@dw-us-ui/food-pairing-utils';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import styled from "@emotion/styled";

const SytledFoodPairingContainer = styled.div`
	.food-pairings-content {
		.food-pairing-info {
			font-style: italic;
			margin-bottom: 10px;
			font-size: 1.1em;
		}
		.food-pairings-list {
			margin: 0px;
			.food-pairings-row {
				// margin: 10px 0px;
				.food-pairing-color {
					width: 15px;
					height: 35px;
					margin: 0px;
				}
				.food-pairing-label {
					font-weight: bold;
					cursor: default;
					margin: 0px;
					line-height: 20px;
					.vir & {
						font-weight: 500;
					}
				}
				.food-pairing-copy {
					margin: 0px;
				}
			}
		}
	}`

const FoodPairings = ({
	activeSection,
	caseContents,
	foodPairings,
	gaAction,
	gaCategory,
	isMixed,
	itemCode,
	productDetails,
	title,
}) => {
	let foodPairingsConfig = foodPairings;
	let isActive = false;
	var tabName = 'Food Pairings';
	let foodPairingsArray = [];
	let productArr = [];
	let finalPairingArray = [];
	let bottleType = productDetails?.bottleType?.toLowerCase();
	let { setEventTracking } = GoogleAnalytics;
    let { SetFoodPairings, CreateFoodPairingsArray, CreateFinalPairingsArray } = FoodPairingUtils;

	if (activeSection === tabName) {
		isActive = true;
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: 'Food_Pairings_Open'
		});

		if (isMixed) {
			caseContents?.forEach((obj) => {
				let data = obj.referencedProduct;
				data.pairing = false;
				if (data.productType === 'wine') {
					let quantity = obj.quantity;
					let product = {
						product: data
					};
					for (var i = 0; i < quantity; i++) {
						productArr.push(product);
					}
				}
			});
		} else {
			productDetails.pairing = false;
			let product = {
				product: productDetails
			};
			productArr.push(product);
		}

		productArr = SetFoodPairings(productArr, foodPairingsConfig);

		foodPairingsArray = CreateFoodPairingsArray(productArr, gaCategory, 'Errors');
		finalPairingArray = CreateFinalPairingsArray(foodPairingsArray);

		if (!finalPairingArray.length) {
			setEventTracking({
				category: gaCategory,
				action: gaAction,
				label: 'Food_Pairings_No_Copy_' + itemCode
			});
		}
	}
	
    let getContent = () => {
		if (!finalPairingArray.length) {
			return null;
		}

		return (
			<div className='content-container food-pairings-content'>
				<h2>{title}</h2>
				<div className='food-pairings-list row'>
					{finalPairingArray.map((item, index) => {
						let data = item;
						let quantity = data.qty;
						let style = {
							background: data.color
						};
						return (
							<div className='col-xs-12 no-pad food-pairings-row' key={index}>
								<div className='col-xs-12 food-pairing-copy no-pad'>{data.foodPairing}</div>
							</div>
						);
					})}
				</div>
			</div>
		);
	};

	return (
		<SytledFoodPairingContainer>
			<div className='food-pairings-container tab-component'>
				{getContent()}
			</div>
		</SytledFoodPairingContainer>
	);
};

FoodPairings.propTypes= {
    foodPairings: PropTypes.array,
	caseContents: PropTypes.array,
	productDetails: PropTypes.object,
	itemCode: PropTypes.string.isRequired,
	isMixed: PropTypes.bool.isRequired,
	setActiveSection: PropTypes.func.isRequired,
	activeSection: PropTypes.string.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string
};

FoodPairings.defaultProps= {};

export default FoodPairings;
