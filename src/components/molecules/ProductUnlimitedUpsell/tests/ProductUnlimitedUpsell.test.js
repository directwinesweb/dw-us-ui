import ProductUnlimitedUpsell from '../src/ProductUnlimitedUpsell';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('ProductUnlimitedUpsell test', () => {
	let unlimitedCheck = function () {
		return true;
	};
	let unlimitedInCart = true;
	let getCartDetails = function () {
		return true;
	};

	beforeEach(() => {
		global.pageLayer = [{ ou: '0', fsp: false }];
	});
	it('Creates snapshot test for <ProductUnlimitedUpsell />', () => {
		const wrapper = shallow(
			<ProductUnlimitedUpsell
				getCartDetails={getCartDetails}
				unlimitedCheck={unlimitedCheck}
				unlimitedInCart={unlimitedInCart}
			></ProductUnlimitedUpsell>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
