import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { BrandUtil } from '@dw-us-ui/brand-util';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
// import ModalUnlimited from '../modals/form_unlimited_modal';
import { UnlimitedModal } from '@dw-us-ui/modals';
import { GetEnvironment } from '@dw-us-ui/get-environment';
import { unlimitedItemCodes } from '@dw-us-ui/constants';
import { usePageLayer } from '@dw-us-ui/pagelayer-provider';
import axios from 'axios';
import styled from '@emotion/styled';

const StyledFreeCopy = styled.div`
	font-family: ${({ theme }) => theme.fonts.productHeadline};
	font-size: 14px;
	.wsj & {
		font-family: ${({ theme }) => theme.fonts.special};
	}
`;

const StyledUnlimitedCopy = styled.div`
	display: flex;
	.npr & {
		font-size: 12px;
	}
	@media (min-width: 768px) and (max-width: 991px) {
	 	display: inline;
	}
	@media (max-width: 312px) {
		display: inline-block;
	}
`;

const ProductUnlimitedUpsell = ({
	gaAction,
	gaCategory,
	unlimitedCheck,
	unlimitedInCart
}) => {
	const [showModal, setShowModal] = useState(false);
	const [isChecked, setIsChecked] = useState(false);
	const [showError, setShowError] = useState(false);
	// const pageLayer = usePageLayer();

    const getUnlimitedPromotionType = (offerUnlimited) => {
		switch (offerUnlimited) {
			case '1':
				return 'unlimited';
			case '2':
				return 'promotional';
			case '3':
				return 'half';
			default:
				return 'unlimited';
		}
	};

	useEffect(()=>{
		if(showError){
			 let timeoutId = setTimeout(()=>{
				setShowError(false)
			}, 5000);
			return ()=>{
				clearTimeout(timeoutId)
			};
		}
	},[showError]);

    const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
    const { caseImageUrl, email, nameShort, phone, tag, unlimited, unlimitedBom, unlimitedFull, url } = data;
	// let offerUnlimited = pageLayer?.ou;
	// let freeShipProfile = pageLayer?.fsp === 'false' ? false : true;
	let offerUnlimited = pageLayer[0].ou;
	let freeShipProfile = pageLayer[0].fsp === 'false' ? false : true;
	let unlimitedPromotionType = getUnlimitedPromotionType(offerUnlimited);
	let bom = unlimitedItemCodes[GetEnvironment()][tag][unlimitedPromotionType];
    let { setEventTracking } = GoogleAnalytics;
    let unlimitedImageUrl = `${url}${caseImageUrl}${unlimitedBom}`;
	let price = 89;

	if(offerUnlimited == 2) {
		price = 69;
	} else if(offerUnlimited == 3) {
		price = 44;
	}
    
    const unlimitedData = {
		brand: tag,
		brandPhone: phone,
		email: email,
		itemCode: unlimitedBom,
		unlimitedPrice: price,
		unlimitedType: unlimited,
		shortText: nameShort,
		fullText: unlimitedFull,
		images: {
			small: `${unlimitedImageUrl}_S.jpg`,
			large: `${unlimitedImageUrl}_L.jpg`,
			thumb: `${unlimitedImageUrl}_T.jpg`,
		}
	};

	if (offerUnlimited === '0' || freeShipProfile || unlimitedInCart || offerUnlimited === '4') {
		return null;
	}

	let checkboxClick = (e) => {
		let checked = e.target.checked;
		let value = false;
		if(checked) {
			axios.get('/api/cart/list')
			.then((res)=> {
				// checking whether free shipping is already available in cart or not
				const isAlreadyAvailable = res?.data?.response?.lineItems?.some((item)=>
					item.lineItemIdentification?.itemTypeTag === 'SUBSCRIPTION_RECRUITMENT_ITEM'
					)
			   if(!isAlreadyAvailable){
				setIsChecked(true)
				value = bom;
				setEventTracking({
					category: gaCategory,
					action: gaAction,
					label: 'Free_Ship_Add'
				});
			   } else {
				setShowError(true);
			   }
			   unlimitedCheck(value);
			})			
		} else {
			setIsChecked(false)
			setEventTracking({
				category: gaCategory,
				action: gaAction,
				label: 'Free_Ship_Remove'
			});
			unlimitedCheck(value);
		}
	};

	return (
		<div className='product-unlimited-upsell col-xs-12'>
			<div className='unlimited-upsell-copy col-xs-12 no-pad'>
				<div className='col-xs-12 no-pad'>
					<StyledFreeCopy>
						Enjoy free shipping year round
					</StyledFreeCopy>
				</div>
				<div className='col-xs-1 no-pad'>
					<input
						id='unlimited-upsell'
						defaultChecked={false}
						type='checkbox'
						name='unlimited-upsell'
						onChange={(e) => checkboxClick(e, 'checkbox')}
						className='unlimited-upsell-checkbox'
						checked={isChecked}
					/>
				</div>
				<label className='col-xs-11 no-pad unlimited-copy'>
					<StyledUnlimitedCopy>
						Join {unlimitedFull}&mdash;
						<strong>
						<a 
							className="shipping-rates-label" 
							onClick={() => setShowModal(true)} 
							onMouseDown={() => setShowModal(false)}
						>
							Learn More
						</a>
						</strong>
					</StyledUnlimitedCopy>
					<strong>
						{showError &&
						<div className='product-unlimited-error'>
						{unlimitedFull} free shipping membership is already added to cart.
						</div>
						}
						<UnlimitedModal
							triggerModal={showModal}
							unlimitedData={unlimitedData}
						/>
							{/* <UnlimitedModal
								type='link'
								triggerText='Learn More'
								unlimited={offerUnlimited}
								brand={BrandUtil.content(brand)}
								section='mixed_case_page'
								addToCart={true}
								gaCategory={props.gaCategory}
								gaAction={props.gaAction}
								getCartDetails={props.getCartDetails}
							/> */}
					</strong>
				</label>
			</div>
		</div>
	);
};

ProductUnlimitedUpsell.propTypes= {
    unlimitedCheck: PropTypes.func.isRequired,
	unlimitedInCart: PropTypes.bool.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
	getCartDetails: PropTypes.func.isRequired
};

ProductUnlimitedUpsell.defaultProps= {};

export default ProductUnlimitedUpsell;