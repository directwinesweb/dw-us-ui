import DescriptionTab from '../src/DescriptionTab';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('DescriptionTab test', () => {
    let productDetails = {};
    let tabName = 'Case Contents';
    let setActiveSection = function() { return true };
    let activeSection = 'Food Pairings';
        it('Creates snapshot test for <DescriptionTab />', () => {
            const wrapper = shallow(
            <DescriptionTab
                productDetails={productDetails}
                tabName={tabName}
                setActiveSection={setActiveSection}
                activeSection={activeSection}
            >
            </DescriptionTab>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });