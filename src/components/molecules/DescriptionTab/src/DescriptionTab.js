import React from 'react';
import PropTypes from 'prop-types';
import { ProductDescription } from '@dw-us-ui/product-description';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';

const DescriptionTab = ({
	activeSection,
	gaAction,
	gaCategory,
	productDetails,
	tabName,
	descriptionType = 'short'
}) => {
    let isActive = false;
	let tabClass = tabName === 'About This Wine' ? 'wine-description' : 'case-description';
	let gaLabel = tabName === 'About This Wine' ? 'Wine_Description_Open' : 'Case_Description_Open';
    let { setEventTracking } = GoogleAnalytics;

	/*
		Check if Tab is Active
	 */
	if (activeSection === tabName) {
		isActive = true;
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: gaLabel
		});
	}

	let getContent = () => {
			return (
				<div className='content-container'>
					<ProductDescription description={descriptionType === 'short' ? productDetails.description : productDetails.longDescription} />
				</div>
			);
	};

	return (
		<div className={'tab-component ' + tabClass}>
			{getContent()}
		</div>
	);
};

DescriptionTab.propTypes= {
    productDetails: PropTypes.object.isRequired,
	tabName: PropTypes.string.isRequired,
	setActiveSection: PropTypes.func.isRequired,
	activeSection: PropTypes.string.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string
};

DescriptionTab.defaultProps= {};

export default DescriptionTab;
