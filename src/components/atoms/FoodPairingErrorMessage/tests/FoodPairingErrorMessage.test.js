import FoodPairingErrorMessage from '../src/FoodPairingErrorMessage';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('FoodPairingErrorMessage test', () => {
        it('Creates snapshot test for <FoodPairingErrorMessage />', () => {
            const wrapper = shallow(<FoodPairingErrorMessage></FoodPairingErrorMessage>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });