import React from 'react';
import PropTypes from 'prop-types';
import { BrandUtil } from '@dw-us-ui/brand-util';

const FoodPairingErrorMessage = ({message}) => {
    let errorMessage;
    const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const phone = BrandUtil.content(brand).phone;

	if (message) {
		errorMessage = message;
	} else {
		errorMessage =
			'Sorry, it looks like we’re experiencing some technical issues. If you’d like to speak with a customer service representative, please call ' +
			phone +
			'.';
	}

	return (
		<div>
			<p className='alert alert-warning'>{errorMessage}</p>
		</div>
	);
};

FoodPairingErrorMessage.propTypes= {
    message: PropTypes.string
};

FoodPairingErrorMessage.defaultProps= {};

export default FoodPairingErrorMessage;
