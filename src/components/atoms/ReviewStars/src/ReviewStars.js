import { BrandUtil } from '@dw-us-ui/brand-util';
import React from 'react';

const ReviewStars = ({ rating }) => {
	const brandData = BrandUtil.content(BrandUtil.getBrand());
	const brandTag = brandData.tag;

	let ratingDecimal = (rating % 1).toFixed(1);
	let ratingWhole = Math.floor(rating);

	let stars = [];

	let imgDir = brandTag;
	if (brandTag === 'law') {
		imgDir = 'law_rebrand';
	}
	for (var i = 1; i <= 5; i++) {
		if (i <= ratingWhole) {
			stars.push(`/images/us/common/icons/${imgDir}/star_solid.svg`);
		} else if (ratingDecimal >= 0.5) {
			stars.push(`/images/us/common/icons/${imgDir}/star_half.svg`);
			ratingDecimal = 0.0;
		} else {
			stars.push(`/images/us/common/icons/${imgDir}/star_light.svg`);
		}
	}

	return (
		<div className='stars-container'>
			{stars.map(function (val, i) {
				return (
					<img className='star-alignment' key={i} src={val} height='15px' />
				);
			})}
		</div>
	);
};

export default ReviewStars;
