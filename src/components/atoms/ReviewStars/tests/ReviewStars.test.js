import ReviewStars from '../src/ReviewStars';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ReviewStars test', () => {
        it('Creates snapshot test for <ReviewStars />', () => {
            const wrapper = shallow(<ReviewStars></ReviewStars>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });