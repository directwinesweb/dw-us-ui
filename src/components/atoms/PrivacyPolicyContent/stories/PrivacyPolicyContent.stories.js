import PrivacyPolicyContent from '../src/PrivacyPolicyContent';
import React from 'react';

export default {
	title: 'Atoms/PrivacyPolicyContent',
	parameters: {
		component: PrivacyPolicyContent,
	},
};

export const LAW = () => <PrivacyPolicyContent />;
