import PrivacyPolicyContent from '../src/PrivacyPolicyContent';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('PrivacyPolicyContent test', () => {
	it('Creates snapshot test for <PrivacyPolicyContent /> default LAW', () => {
		const wrapper = shallow(<PrivacyPolicyContent></PrivacyPolicyContent>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
