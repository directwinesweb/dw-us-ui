import Button from '../src/Button';
import React from 'react';

export default {
	title: 'Atoms/Button',
	parameters: {
		component: Button,
	},
};

export const Default = () => (
	<div style={{ textAlign: 'center' }}>
		<Button>Hello Button</Button>
	</div>
);
