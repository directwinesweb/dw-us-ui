import Button from '../src/Button';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

const clickFn = jest.fn();

describe('Button test', () => {
	it('renders <Button/> component snapshot', () => {
		const wrapper = shallow(<Button>Test</Button>);

		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('clicks button and fires onClick function', () => {
		shallow(
			<Button className='primary' onClick={clickFn}>
				Test Button
			</Button>
		).simulate('click');
		expect(clickFn).toHaveBeenCalled();
	});
});
