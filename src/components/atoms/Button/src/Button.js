import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';

const StyledButton = styled.button`
	border: ${({ buttonType, theme }) => theme.button[buttonType].border};
	color: ${({ buttonType, theme }) => theme.button[buttonType].textColor};
	background-color: ${({ buttonType, theme }) =>
		theme.button[buttonType].backgroundColor};
	font-family: ${({ theme }) => theme.button.fontFamily} !important;
	font-size: ${({ theme }) => theme.button.fontSize};
	border-radius: ${({ theme }) => theme.button.borderRadius};
	text-transform: ${({ theme }) => theme.button.textTransform} !important;
	font-size: ${({ buttonSize, theme }) => theme.button[buttonSize].fontSize};
	height: ${({ buttonSize, theme }) => theme.button[buttonSize].height};
	width: ${({ buttonSize, theme }) => theme.button[buttonSize].width};
	padding: ${({ buttonSize, theme }) => theme.button[buttonSize].padding};
	letter-spacing: ${({ theme }) => theme.button.letterSpacing};
	font-weight: ${({ theme }) => theme.button.fontWeight};
	transition: 0.3s;
	cursor: pointer;
	text-decoration: none;
	white-space: nowrap;
	border-width: 1px;
	border-style: solid;
	font-style: normal;
	font-stretch: normal;
	line-height: 1.25;
	margin: 0;
	outline: none;
	&:hover {
		background-color: ${({ buttonType, theme }) =>
			theme.button[buttonType].activeBackgroundColor};
		text-decoration: none;
		border-color: ${({ buttonType, theme }) =>
			theme.button[buttonType].activeBackgroundColor};
		color: ${({ buttonType, theme }) =>
			theme.button[buttonType].activeButtonColor || 'inherit'};
	}
	&:focus {
		text-decoration: none;
	}
	&:disabled {
		color: #fff;
		background-color: #9d9d9d;
		border-color: transparent;
		box-shadow: none;
		cursor: not-allowed;
		pointer-events: none;
	}
	@media (max-width: 767px) {
		height: 44px;
	}
`;

/**
 *
 * @param {Object} props - children, className, onClick, disabled, buttonType, buttonSize, type, cypressClass
 *
 * @returns a react button component
 */
const Button = ({
	children,
	className,
	onClick,
	disabled,
	buttonType,
	buttonSize,
	type,
	cypressClass,
}) => {
	let theme = useCustomTheme();

	return (
		<StyledButton
			data-cy={cypressClass}
			className={className}
			disabled={disabled}
			onClick={onClick}
			type={type}
			buttonType={buttonType}
			buttonSize={buttonSize}
			theme={theme}
		>
			{children}
		</StyledButton>
	);
};
Button.propTypes = {
	/**
	 * Children is the content displayed inside the button that you wrap this button component around
	 */
	children: PropTypes.any,
	/**
	 * Adds a string of classnames to your button in order style the button a bit different than it defaults to
	 */
	className: PropTypes.string,
	/*
	Cypress identifier
	**/
	cypressClass: PropTypes.string,
	/**
	 * A callback function that you pass to the buttons onClick handler in order to take care of actions that you'd like to occur on the click of the button
	 */
	onClick: PropTypes.func,
	/**
	 * Disable the click function and update the styling of the button to appear un-clickable
	 */
	disabled: PropTypes.bool,
	/**
	 * Changes the appearance of the button in order to communicate different ideas to the customer
	 */
	buttonType: PropTypes.string,
	/**
	 * Changes the size of the button in order to communicate different ideas to the customer
	 */
	buttonSize: PropTypes.string,
	/**
	 * The type attribute specifies the type of HTML button whether it be a "button", "submit" or "reset" button.
	 */
	type: PropTypes.string,
};

Button.defaultProps = {
	children: null,
	className: '',
	onClick: () => {},
	disabled: false,
	buttonType: 'primary',
	buttonSize: 'flex',
	type: 'button',
	cypressClass: 'btn',
};

export default Button;
