const base = require('../../../../.jest/jest.config.base');
const pack = require('./package');

module.exports = {
	...base,
	name: pack.name,
	displayName: pack.name,
};
