import UnlimitedTerms from '../src/UnlimitedTerms';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('UnlimitedTerms test', () => {
        it('Creates snapshot test for <UnlimitedTerms />', () => {
            const wrapper = shallow(<UnlimitedTerms></UnlimitedTerms>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });