import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import React from 'react';

const StyledUnlimitedTerms = styled.div`
	&&&& {
		text-align: left;
		margin: 0;
		font-size: 0.8rem;
		line-height: 20px;
	}
`;
const UnlimitedTerms = ({ brandTag, fullText, shortText, unlimitedPrice, unlimited }) => {
	return (
		<StyledUnlimitedTerms>
			<strong>Terms and conditions:</strong>{' '}
			{brandTag === 'wsj'
				? "WSJ Wine is operated independently of The Wall Street Journal's news department."
				: null}{' '}
			As a {fullText} member, you will pay an annual membership fee (currently $
			{unlimitedPrice} plus applicable tax) to receive free shipping on all wine orders that are placed during your active
			membership period (certain restrictions apply, see below for more
			details). Fair use of {unlimited} Shipping
				is limited to no more than 6 shipments of 3 bottles or fewer in any
				12—month period. Violation of this may result in refusal of those
				shipments in excess or cancelation of membership. Additional terms of{' '}
				{brandTag === 'wsj' ? 'promotions' : 'promotional offers'} may vary
				and will be specified in the applicable promotion.
			Your membership begins on the day you register and continues for
			12 months. After 12 months, unless you request otherwise, your {unlimited}{' '}
			membership will automatically renew for another year and your credit card
			will be charged the applicable price (currently ${unlimitedPrice} plus
			applicable tax). You will receive at least 30 days advance notice of your auto-renewal, prior to any charges.
			You may cancel your {unlimited} membership at any time
			and receive a full refund, provided you have not yet used the service. 
			All recipients must be 21 years or over. If you purchase wine while an active
			member, but the associated shipping charges are included in a second
			installment that is due after your membership has ended, shipping charges
			will apply. We reserve the right to make changes to these terms, or any
			aspect of the {unlimited} service, by posting revisions on our website.
			Your continued membership after such changes constitutes acceptance of the
			changes. If you do not agree to such changes, you are free to cancel your
			membership. We may terminate your {unlimited} membership at our sole
			discretion without notice (in such cases, you will receive a prorated
			refund). Each state within the United States has certain laws that
			restrict the amount of shipments of wine that can be made each month to
			that state.{' '}
			{brandTag === 'law' || brandTag === 'wsj'
				? `An additional $75 surcharge will apply to each case shipped to Alaska and Hawaii.`
				: null}{' '}For Colorado, a $0.27 retail delivery fee per case applies.
			By purchasing the {unlimited} membership, you accept these terms and
			conditions. Void where prohibited by law.
		</StyledUnlimitedTerms>
	);
};

UnlimitedTerms.propTypes = {
	/**
	 * Brand tag
	 */
	brandTag: PropTypes.string,
	/**
	 * Unlimited name Full Text
	 */
	fullText: PropTypes.string,
	/**
	 * Unlimited name short text
	 */
	shortText: PropTypes.string,
	/**
	 * unlimited price
	 */
	unlimitedPrice: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

UnlimitedTerms.defaultProps = {};

export default UnlimitedTerms;
