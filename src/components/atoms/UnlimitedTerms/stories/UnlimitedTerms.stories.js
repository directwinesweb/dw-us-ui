import React from 'react';
import UnlimitedTerms from '../src/UnlimitedTerms';

export default {
	title: 'Atoms/UnlimitedTerms',
	parameters: {
		component: UnlimitedTerms,
	},
};

export const WSJ = () => (
	<UnlimitedTerms
		brand='wsj'
		fullText='WSJ Wine Advantage'
		shortText='WSJ Wine'
		unlimitedPrice={89}
		unlimited='Advantage'
	/>
);
