import { BrandUtil } from '@dw-us-ui/brand-util';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledDetailsTab = styled.div`
	padding: 20px 30px 20px 60px;
	cursor: pointer;
	line-height: 26px;
	.icon {
		position: absolute;
		top: 50%;
		width: 30px;
		text-align: center;
		left: 10px;
		transform: translateY(-50%);
	}
	.tab-title {
		font-size: 1.7em;
		font-weight: 400;
		display: inline-block;
		vertical-align: middle;
		.wsj & {
			font-family: Arial-condensed, sans-serif;
			font-weight: 500;
			font-size: 1.6em;
		}
		.vir & {
			color: #000;
		}
		.npr & {
			font-size: 1.5em;
		}
		.law & {
			font-family: ${({ theme }) => theme.fonts.productHeadline};
			font-size: 20px;
		}
		${EmotionBreakpoint('tablet')} {
			font-size: 1.4em;
			.npr & {
				font-size: 1.2em;
			}
		}
	}
	.indicator {
		font-size: 1.2em;
		display: inline-block;
		color: #848484;
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		right: 15px;
		${EmotionBreakpoint('mobile')} {
			font-size: 1.4rem;
		}
	}
	.content-container {
		display: inline-block;
	}
	border-top: 1px solid #ccc;
`;

const DetailsTab = ({ imagePath, isActive, setActiveSection, tabName }) => {
	const brandData = BrandUtil.content(BrandUtil.getBrand());
	const brandTag = brandData.tag;

	let openTabClick = () => {
		setActiveSection(tabName);
	};

	return (
		<div className={'col-xs-12'} onClick={() => openTabClick()}>
			<StyledDetailsTab>
				<div className='icon'>
					<img src={imagePath} />
				</div>
				<h3 className='tab-title'>{tabName}</h3>
				<div className='indicator'>
					{isActive ? (
						<span>
							<img
								src={`/images/us/common/icons/${brandTag}/minus_solid_11x14.svg`}
								height='15px'
								width='20px'
							/>
						</span>
					) : (
						<span>
							<img
								src={`/images/us/common/icons/${brandTag}/plus_solid_11x14.svg`}
								height='15px'
								width='20px'
							/>
						</span>
					)}
				</div>
			</StyledDetailsTab>
		</div>
	);
};

DetailsTab.propTypes = {
	imagePath: PropTypes.string.isRequired,
	tabName: PropTypes.string.isRequired,
	setActiveSection: PropTypes.func.isRequired,
	isActive: PropTypes.bool.isRequired,
};

DetailsTab.defaultProps = {};

export default DetailsTab;
