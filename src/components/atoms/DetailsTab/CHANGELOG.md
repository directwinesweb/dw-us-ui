# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/details-tab@1.0.2...@dw-us-ui/details-tab@1.0.3) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/details-tab






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/details-tab@1.0.1...@dw-us-ui/details-tab@1.0.2) (2021-07-02)

**Note:** Version bump only for package @dw-us-ui/details-tab





## 1.0.1 (2021-06-30)


### Bug Fixes

* initial commit on pdp pages ([ffa85f1](https://bitbucket.org/directwinesweb/dw-us-ui/commits/ffa85f10e235e715c80b1e02fd38532d81cefe5a))
* refactored components ([2d1125c](https://bitbucket.org/directwinesweb/dw-us-ui/commits/2d1125c03f06afaebc58b647213b4139a4fe1f42))
* responsive changes ([fc8fda4](https://bitbucket.org/directwinesweb/dw-us-ui/commits/fc8fda422fd64752f78d9711ff54917a9bc928c5))
* test case fixes ([e1d8d94](https://bitbucket.org/directwinesweb/dw-us-ui/commits/e1d8d9428644435578c7603dc2d431627f5f941a))
