import DetailsTab from '../src/DetailsTab';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('DetailsTab test', () => {
    let imagePath = '/images/us/common/icons/wsj/plus_solid_11x14.svg';
    let tabName = 'Case contents';
    let setActiveSection = function() { return true; };
    let isActive = true;

        it('Creates snapshot test for <DetailsTab />', () => {
            const wrapper = shallow(<DetailsTab 
                imagePath={imagePath}
                tabName={tabName}
                setActiveSection={setActiveSection}
                isActive={isActive}
                ></DetailsTab>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });