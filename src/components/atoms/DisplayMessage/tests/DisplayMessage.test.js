import DisplayMessage from '../src/DisplayMessage';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('DisplayMessage test', () => {
	it('Creates snapshot test for <DisplayMessage />', () => {
		const wrapper = shallow(<DisplayMessage>Error Message</DisplayMessage>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
