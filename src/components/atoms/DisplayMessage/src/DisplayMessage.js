import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledDisplayMessage = styled.div`
	&.display-error-msg {
		background-color: #f2dede;
		border-color: #ebccd1;
		color: #a94442;
		border: 1px solid transparent;
		border-radius: 4px;
		padding: 10px 15px;
		margin: 10px 0;
	}
`;

/**
 *
 * @param {String} type - error, warning, or success
 *
 * @returns Styled Display Message
 */
const DisplayMessage = ({ type, children }) => {
	return (
		<StyledDisplayMessage className={`display-${type}-msg`}>
			{children}
		</StyledDisplayMessage>
	);
};

DisplayMessage.propTypes = {
	/**
	 * Type of message
	 */
	type: PropTypes.oneOf(['error', 'warning', 'success']),
	/**
	 * Message to display
	 */
	children: PropTypes.node,
};

DisplayMessage.defaultProps = {
	type: 'error',
	children: null,
};

export default DisplayMessage;
