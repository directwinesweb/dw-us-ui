import DisplayMessage from '../src/DisplayMessage';
import React from 'react';

export default {
	title: 'Atoms/DisplayMessage',
	parameters: {
		component: DisplayMessage,
	},
};

export const Default = () => (
	<DisplayMessage>The default is an error message</DisplayMessage>
);
