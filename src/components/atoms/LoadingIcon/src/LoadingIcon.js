import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';

const StyledLoadingIconContainer = styled.div`
	margin: auto;
	position: relative;
	width: 70px;
	height: 20px;
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: space-between;

	.dot {
		display: inline-block;
		width: 10px;
		height: 10px;
		background: ${({ theme }) => theme.colors.white};
		animation-name: scale;
		animation-duration: 1s;
		animation-iteration-count: infinite;
		animation-timing-function: linear;
	}

	@keyframes scale {
		0% {
			transform: scale(1);
		}
		50% {
			transform: scale(0.6);
		}
		100% {
			transform: scale(1);
		}
	}

	.d1 {
		animation-delay: 0s;
	}
	.d2 {
		animation-delay: 0.33s;
	}
	.d3 {
		animation-delay: 0.66s;
	}
`;
const LoadingIcon = ({ type }) => {
	const theme = useCustomTheme();
	if (type === 'loadingDots') {
		return (
			<StyledLoadingIconContainer>
				<div className='dot d1'></div>
				<div className='dot d2'></div>
				<div className='dot d3'></div>
			</StyledLoadingIconContainer>
		);
	} else {
		if (theme.brand === 'laithwaites') {
			return (
				<img
					src='/images/us/common/loading_icons/dual_ring.svg'
					alt='Loading Icon'
				/>
			);
		}
		return (
			<img
				src='/images/us/common/loading_icons/wine_glass.gif'
				alt='Loading Icon'
			/>
		);
	}
};

LoadingIcon.propTypes = {
	/**
	 * Will display spinner,glass (theme) or loading dots
	 */
	type: PropTypes.oneOf(['default', 'loadingDots', null]),
};
LoadingIcon.defaultProps = {
	type: 'default',
};
export default LoadingIcon;
