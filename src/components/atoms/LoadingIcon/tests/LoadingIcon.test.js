import LoadingIcon from '../src/LoadingIcon';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('LoadingIcon test', () => {
	it('Creates snapshot test for <LoadingIcon /> for law', () => {
		const wrapper = shallow(<LoadingIcon></LoadingIcon>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <LoadingIcon /> for law loading dots', () => {
		const wrapper = shallow(<LoadingIcon type='loadingDots'></LoadingIcon>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
