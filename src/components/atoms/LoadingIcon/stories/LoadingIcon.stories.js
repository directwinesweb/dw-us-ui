import LoadingIcon from '../src/LoadingIcon';
import React from 'react';
import { select } from '@storybook/addon-knobs';

export default {
	title: 'Atoms/LoadingIcon',
	parameters: {
		component: LoadingIcon,
	},
	decorators: [
		(Story) => {
			return (
				<div
					style={{
						backgroundColor: '#ccc',
						padding: '10px',
						textAlign: 'center',
					}}
				>
					<Story />
				</div>
			);
		},
	],
};

export const Default = () => {
	let typeOps = {
		default: 'default',
		loadingDots: 'loadingDots',
	};
	let type = select('Type', typeOps, 'default');

	return <LoadingIcon type={type} />;
};
