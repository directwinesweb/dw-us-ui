# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.6](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.1.5...@dw-us-ui/loading-icon@1.1.6) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/loading-icon






## [1.1.5](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.1.4...@dw-us-ui/loading-icon@1.1.5) (2021-07-02)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## [1.1.4](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.1.3...@dw-us-ui/loading-icon@1.1.4) (2021-06-30)

**Note:** Version bump only for package @dw-us-ui/loading-icon






## [1.1.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.1.2...@dw-us-ui/loading-icon@1.1.3) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## [1.1.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.1.0...@dw-us-ui/loading-icon@1.1.2) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## [1.1.1](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.1.0...@dw-us-ui/loading-icon@1.1.1) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/loading-icon





# [1.1.0](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.0.5...@dw-us-ui/loading-icon@1.1.0) (2021-05-10)


### Features

* updated LoadingIcon to have type prop ([6df8595](https://bitbucket.org/directwinesweb/dw-us-ui/commits/6df85952839d61cc554d9d8e627d9fa4d2a935b6))





## [1.0.5](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.0.4...@dw-us-ui/loading-icon@1.0.5) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## [1.0.4](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.0.3...@dw-us-ui/loading-icon@1.0.4) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.0.2...@dw-us-ui/loading-icon@1.0.3) (2021-03-25)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/loading-icon@1.0.1...@dw-us-ui/loading-icon@1.0.2) (2021-03-09)

**Note:** Version bump only for package @dw-us-ui/loading-icon





## 1.0.1 (2021-03-05)

**Note:** Version bump only for package @dw-us-ui/loading-icon
