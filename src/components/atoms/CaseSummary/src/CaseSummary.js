import { Modal } from '@dw-us-ui/modals';
import PropTypes from 'prop-types';
import React from 'react';

const CaseSummary = ({ caseSummary, hideCaseSummary, showSummary }) => {
	let renderList = (array, type) => {
		if (!array.length && type !== 'Country') {
			return <li className='case-summary-list-row'> No {type} Wines </li>;
		}

		return array.map((item, index) => {
			return (
				<li className='case-summary-list-row' key={index}>
					{item.name} (x{item.qty})
				</li>
			);
		});
	};

	let renderRow = (label, array, type, rowType) => {
		return (
			<div className={'row cs-row clearfix cs-' + rowType}>
				<div className='cs-type-name col-xs-5 no-pad'>{label}</div>
				<div className='col-xs-7'>
					<ul className='case-summary-list'>{renderList(array, type)}</ul>
				</div>
			</div>
		);
	};

	return (
		<div>
			<Modal
				showModal={showSummary}
				disableAnimate={false}
				disableClose={false}
				modalHeader='Case Summary'
				toggleModal={hideCaseSummary}
			>
				<div className='row cs-row cs-odd clearfix'>
					<div className='cs-type-name col-xs-5 no-pad'>Color</div>
					<div className='col-xs-7 cs-type-name'>{caseSummary.caseColor}</div>
				</div>
				{renderRow('Reds', caseSummary.redGrapeArr, 'Red', 'even')}
				{renderRow('Whites', caseSummary.whiteGrapeArr, 'White', 'odd')}
				{renderRow('Rosés', caseSummary.roseGrapeArr, 'Rosé', 'even')}
				{renderRow('Country', caseSummary.countryArr, 'Country', 'odd')}
			</Modal>
		</div>
	);
};

CaseSummary.propTypes = {
	caseSummary: PropTypes.object.isRequired,
	showSummary: PropTypes.bool.isRequired,
	hideCaseSummary: PropTypes.func.isRequired,
};

CaseSummary.defaultProps = {
	caseSummary: {},
	showSummary: false,
	hideCaseSummary: () => {},
};

export default CaseSummary;
