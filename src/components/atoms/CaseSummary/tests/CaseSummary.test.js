import CaseSummary from '../src/CaseSummary';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('CaseSummary test', () => {
	it('Creates snapshot test for <CaseSummary />', () => {
		const wrapper = shallow(
			<CaseSummary
				caseSummary={{
					redGrapeArr: [],
					whiteGrapeArr: [],
					roseGrapeArr: [],
					countryArr: [],
				}}
			></CaseSummary>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
