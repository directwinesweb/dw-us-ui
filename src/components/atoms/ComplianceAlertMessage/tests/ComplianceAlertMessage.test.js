import ComplianceAlertMessage from '../src/ComplianceAlertMessage';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ComplianceAlertMessage test', () => {
        it('Creates snapshot test for <ComplianceAlertMessage />', () => {
            const wrapper = shallow(<ComplianceAlertMessage></ComplianceAlertMessage>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });