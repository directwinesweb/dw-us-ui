import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledComplianceAlert = styled.div`
	&.show {
		color: #000000;
		padding: 10px;
		margin: 0;
		font-size: 0.9rem;
		text-shadow: none;
		background-color: #ffffff;
		border: 1px solid rgba(153, 153, 153, 0.2);
		border-radius: 0;
		transition: opacity 0.5s;
		opacity: 1;
		line-height: 20px;
	}

	&.hide {
		opacity: 0;
		pointer-events: none;
	}
`;

/**
 * @param {string} - retailerMessage - Retailer Message
 */
const ComplianceAlertMessage = ({ retailerMessage }) => {
	const createMarkup = (input) => {
		return { __html: input };
	};
	return (
		<StyledComplianceAlert className={retailerMessage ? 'show' : 'hide'}>
			{retailerMessage ? (
				<span dangerouslySetInnerHTML={createMarkup(retailerMessage)} />
			) : null}
		</StyledComplianceAlert>
	);
};

ComplianceAlertMessage.propTypes = {
	/**
	 * The compliance retailer message to render
	 */
	retailerMessage: PropTypes.node,
};

ComplianceAlertMessage.defaultProps = {};

export default ComplianceAlertMessage;
