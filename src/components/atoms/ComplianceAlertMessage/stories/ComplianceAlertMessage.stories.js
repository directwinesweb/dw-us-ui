import ComplianceAlertMessage from '../src/ComplianceAlertMessage';
import React from 'react';

export default {
	title: 'Atoms/ComplianceAlertMessage',
	parameters: {
		component: ComplianceAlertMessage,
		componentSubtitle: 'Display compliance alert message',
	},
};

export const Default = () => {
	return (
		<ComplianceAlertMessage retailerMessage='If your order is accepted by one of the wineries below, they will then process and fulfill your wine order.<br />Homestead Winery @ Grapevine <br />Homestead Vineyards & Winery <br />Sunset Winery <br />Salado Wine Seller <br />Brennan Vineyards <br />Peach Creek Vineyards <br />Landon Winery <br />Paris Vineyards' />
	);
};
