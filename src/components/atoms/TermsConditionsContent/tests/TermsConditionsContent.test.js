import TermsConditionsContent from '../src/TermsConditionsContent';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('TermsConditionsContent test', () => {
        it('Creates snapshot test for <TermsConditionsContent />', () => {
            const wrapper = shallow(<TermsConditionsContent></TermsConditionsContent>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });