import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import { LoadingIcon } from '@dw-us-ui/loading-icon';
import { useLegalContentful } from '@dw-us-ui/use-contentful';

const StyledContent = styled.div`
	.terms-row {
		padding: 0 1rem;

		p {
			display: flex;
			align-items: baseline;
			justify-content: flex-start;
			span {
				padding-right: 1rem;
			}
		}
	}

	.contact-info {
		p {
			span {
				width: 70px;
			}
		}
		.contact-info-divider {
			padding: 0 1rem;
		}
	}
`;

const StyledLoadingContainer = styled.div`
	height: 100%;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
`;

/**
 * @todo - Add Wineplan Id logic
 * @todo - Add Mystery Logic
 */
const TermsConditionsContent = ({ tag }) => {
	let slug = `${tag}/terms-and-conditions`;

	const { contentfulData, isContentLoading } = useLegalContentful(slug);

	if (isContentLoading) {
		return (
			<>
				<StyledLoadingContainer>
					<LoadingIcon />
				</StyledLoadingContainer>
			</>
		);
	} 
	
	if (contentfulData && contentfulData?.fields) {
		let { legalData } = contentfulData;
		
		return (
			<>
				<style>{legalData.customStyle}</style>
				<StyledContent
					className={`text-md-left text-left ${legalData.cssLayout}`}
					dangerouslySetInnerHTML={{ __html: legalData.htmlContent }}
				/>
			</>
		);
	} else {
		return null;
	}
};

TermsConditionsContent.propTypes = {
	/**
	 * Brand Tag
	 */
	tag: PropTypes.string,
};

TermsConditionsContent.defaultProps = {
	tag: 'law',
};

export default TermsConditionsContent;
