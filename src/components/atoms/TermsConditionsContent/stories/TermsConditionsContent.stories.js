import React from 'react';
import TermsConditionsContent from '../src/TermsConditionsContent';

export default {
	title: 'Atoms/TermsConditionsContent',
	parameters: {
		component: TermsConditionsContent,
	},
};

export const LAW = () => <TermsConditionsContent />;
