import ProductAttributes from '../src/ProductAttributes';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ProductAttributes test', () => {
        it('Creates snapshot test for <ProductAttributes />', () => {
            const wrapper = shallow(<ProductAttributes></ProductAttributes>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });