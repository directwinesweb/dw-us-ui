import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledListItem = styled.li`
	h2,
	h3,
	h4 {
		font-weight: normal;
		font-size: inherit;
	}
`;

const ProductAttributes = ({
	attributeRowClass,
	attributeLabelClass,
	attributeValueClass,
	product,
}) => {
	let attributeRow = (label, attribute, symbol, tag = '2') => {
		const CustomTag = `h${tag}`;

		return (
			<StyledListItem className='col-xs-12 attribute no-pad'>
				<CustomTag>
					<span className={'no-pad ' + attributeLabelClass}>
						<strong>{label}</strong>
					</span>
					<span className={'no-pad ' + attributeValueClass}>
						{attribute}
						{symbol ? symbol : false}
					</span>
				</CustomTag>
			</StyledListItem>
		);
	};

	let vintageText = (vintage) => {
		if (vintage === 'NV') {
			return 'NV (Non-Vintage)';
		} else {
			return vintage;
		}
	};

	let bottleTypeLabel = (product.bottleType?.toLowerCase() === 'can' && !product.itemCode.match('/^[a-z]+$/i')) ? 'Pack' : 'Bottle';
	/*
		Return Attributes
	 */
	return (
		<div className='item-attributes col-xs-12'>
			<ul className={'attributes-row no-pad ' + attributeRowClass}>
				{attributeRow('Wine Style:', product.styleName)}
				{attributeRow('Country:', product.countryName, null, '3')}
				{attributeRow('Region:', product.regionName)}
				{attributeRow('Appellation:', product.appellationName, null, '3')}
			</ul>
			<ul className={'attributes-row no-pad ' + attributeRowClass}>
				{attributeRow('Vintage:', vintageText(product.vintage), null, '4')}
				{attributeRow('Grape:', product.grapeName)}
				{attributeRow('Alcohol:', product.alcoholPercent, '%', '3')}
				{attributeRow(`${bottleTypeLabel} Size:`, parseInt(product.bottleSize), 'ml', '4')}
			</ul>
		</div>
	);
};

ProductAttributes.propTypes = {
	product: PropTypes.object.isRequired,
	attributeRowClass: PropTypes.string,
	attributeLabelClass: PropTypes.string,
	attributeValueClass: PropTypes.string,
};

ProductAttributes.defaultProps = {
	product: {},
	attributeRowClass: 'col-md-12 col-sm-12 col-xs-12',
	attributeLabelClass: 'col-xs-5',
	attributeValueClass: 'col-xs-7',
};

export default ProductAttributes;
