# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/product-attributes@1.0.2...@dw-us-ui/product-attributes@1.0.3) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/product-attributes






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/product-attributes@1.0.1...@dw-us-ui/product-attributes@1.0.2) (2021-07-02)

**Note:** Version bump only for package @dw-us-ui/product-attributes





## 1.0.1 (2021-06-30)


### Bug Fixes

* comments added ([048344f](https://bitbucket.org/directwinesweb/dw-us-ui/commits/048344f9fd299520fdac3ca552c64d79537de70e))
* initial commit on pdp pages ([ffa85f1](https://bitbucket.org/directwinesweb/dw-us-ui/commits/ffa85f10e235e715c80b1e02fd38532d81cefe5a))
