import ProductDescription from '../src/ProductDescription';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ProductDescription test', () => {
        it('Creates snapshot test for <ProductDescription />', () => {
            const wrapper = shallow(<ProductDescription></ProductDescription>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });