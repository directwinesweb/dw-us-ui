import React from 'react';
import PropTypes from 'prop-types';

import { BrandUtil } from '@dw-us-ui/brand-util';

const ProductDescription = ({
	description
}) => {
    const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const tag = BrandUtil.content(brand).tag;

	let getDefaultContent = (tag) => {
		switch (tag) {
			case 'wsj':
				return '<p>This new release is ready to ship, and details are coming soon. Add to cart now and be among the first to taste it.</p>';
			case 'law':
				return '<p>This fresh arrival is ready to ship. Add to cart and be among the first to taste it (and check back later for more details).</p>';
			case 'vir':
				return '<p>Just landed and ready to ship! (Check back soon for full details on this exciting selection.)</p>';
			default:
				return '<p>Please check back soon for more details on this wine.</p>';
		}
	};

	description = description !== undefined ? description : getDefaultContent(brand);

	return <span dangerouslySetInnerHTML={{__html: description}} />;
};

ProductDescription.propTypes= {
    description: PropTypes.string
};

ProductDescription.defaultProps= {};

export default ProductDescription;
