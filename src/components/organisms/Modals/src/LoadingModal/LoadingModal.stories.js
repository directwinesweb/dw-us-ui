import LoadingModal from './LoadingModal';
import React from 'react';
import { boolean } from '@storybook/addon-knobs';

export default {
	title: 'Modals/LoadingModal',
	parameters: {
		component: LoadingModal,
	},
};

export const Default = () => (
	<LoadingModal showModal={boolean('ToggleModal', true)} />
);
