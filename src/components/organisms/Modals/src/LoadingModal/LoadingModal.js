import { LoadingIcon } from '@dw-us-ui/loading-icon';
import { Modal } from '../Modal';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledModalContent = styled.div`
	text-align: center;
`;
/**
 * @param {bool} - triggerModal - Trigger the WD Modal
 */
const LoadingModal = ({ showModal, toggleModal }) => (
	<Modal
		showModal={showModal}
		toggleModal={toggleModal}
		modalHeader={false}
		modalSize={'600px'}
		disableOnClickOutside={true}
		disableClose={true}
		disableAnimate={true}
	>
		<StyledModalContent>
			<LoadingIcon />
		</StyledModalContent>
	</Modal>
);

LoadingModal.defaultProps = {
	showModal: false,
	toggleModal: () => {},
};
LoadingModal.propTypes = {
	/**
	 * render modal
	 */
	showModal: PropTypes.bool,
	/**
	 * Function that toggles modal
	 */
	toggleModal: PropTypes.func,
};
export default LoadingModal;
