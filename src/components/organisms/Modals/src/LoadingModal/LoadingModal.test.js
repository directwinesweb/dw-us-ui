import LoadingModal from './LoadingModal';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('LoadingModal test', () => {
	it('Creates snapshot test for <LoadingModal />', () => {
		const wrapper = shallow(<LoadingModal showModal />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
