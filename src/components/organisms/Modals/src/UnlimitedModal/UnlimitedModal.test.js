import React from 'react';
import UnlimitedModal from './UnlimitedModal';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('UnlimitedModal test', () => {
	it('Creates snapshot test for <UnlimitedModal />', () => {
		const unlimitedData = {
			brand: 'law',
			brandPhone: '1-800-649-4637',
			email: 'customerservice@laithwaites.com',
			itemCode: '15145UL',
			unlimitedPrice: 89,
			unlimitedType: 'unlimited',
			shortText: 'Unlimited',
			fullText: 'Laithwaites Unlimited',
			images: {
				small: 'https://www.laithwaites.com/images/us/en/product/15145UL_S.jpg',
				large: 'https://www.laithwaites.com/images/us/en/product/15145UL_L.jpg',
				thumb: 'https://www.laithwaites.com/images/us/en/product/15145UL_T.jpg',
			},
		};
		const wrapper = shallow(
			<UnlimitedModal triggerModal unlimitedData={unlimitedData} />
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
