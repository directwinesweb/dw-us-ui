import React, { useEffect, useState } from 'react';

import { Button } from '@dw-us-ui/button';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { Modal } from '../Modal';
import PropTypes from 'prop-types';
import { UnlimitedTerms } from '@dw-us-ui/unlimited-terms';
import styled from '@emotion/styled';
import { useModal } from '../hooks/useModal';

const StyledModalContent = styled.div`
	text-align: center;
	padding: 1rem;
	${EmotionBreakpoint('mobile')} {
		padding: 1rem 0;
	}
	color: ${({ theme }) => theme.unlimitedModal.color};
	font-family: ${({ theme }) => theme.unlimitedModal.fontFamily};
	font-size: ${({ theme }) => theme.unlimitedModal.fontSize};
	line-height: ${({ theme }) => theme.unlimitedModal.lineHeight};
	font-weight: ${({ theme }) => theme.unlimitedModal.fontWeight};
	.unlimited-header {
		margin: 1em 0 1em 0;
		font-size: ${({ theme }) => theme.unlimitedModal.header.fontSize};
		font-family: ${({ theme }) => theme.fonts.special};
		color: ${({ theme }) => theme.unlimitedModal.header.color};
		font-weight: ${({ theme }) => theme.unlimitedModal.header.fontWeight};
	}
	.unlimited-sub-header {
		margin: 0;
		font-weight: ${({ theme }) => theme.unlimitedModal.fontWeight};
		font-size: ${({ theme }) => theme.unlimitedModal.subHeader.fontSize};
	}
	.unlimited-intro-content {
		text-align: left;
		margin: 1rem 0 1rem 0;
	}
	.unlimited-bullets {
		text-align: left;
		margin: 1rem 0;
		li {
			margin: 0.3rem 0 0 1.5rem;
			list-style: disc;
		}
	}
	.unlimited-end-content {
		text-align: left;
		margin: 0.5rem 0;
		a {
			display: block;
			text-decoration: none;
			text-align: left;
			font-size: 0.8rem;
			margin: 1rem 0 0;
		}
	}
	img {
		max-width: 100%;
		margin: 1rem 0;
	}
	.unlimited-terms {
		max-height: 12rem;
		margin-bottom: 1rem;
		&.show {
			transition: max-height 0.5s linear;
			overflow: auto;
			border-top: 1px solid #b7b7b7;
			padding-top: 5px;
		}
		&.hide {
			pointer-events: none;
			overflow: hidden;
			max-height: 0;
			transition: max-height 0.5s linear;
		}
	}
`;
const UnlimitedModal = ({ triggerModal, unlimitedData }) => {
	const [showModal, toggleModal] = useModal();
	const [showTerms, setShowTerms] = useState(false);
	useEffect(() => {
		if (triggerModal) {
			toggleModal();
		}
	}, [triggerModal]);
	if (!unlimitedData) {
		return null;
	}
	const { images, brand, fullText, shortText, unlimitedType, unlimitedPrice } = unlimitedData;
	const { thumb } = images;
	return (
		<Modal
			showModal={showModal}
			toggleModal={toggleModal}
			modalSize={'600px'}
			disableTextClose={true}
		>
			<StyledModalContent>
				<img src={thumb} alt={fullText} />
				<h3 className='unlimited-header'>
					{brand === 'wsj'
						? 'One Annual Fee, Then Every Case Ships FREE'
						: 'Pay one fee, then get FREE shipping for a year'}
				</h3>
				<h4 className='unlimited-sub-header'>
					Try <strong>{unlimitedType}</strong> today for JUST ${unlimitedPrice}
				</h4>
				<p className='unlimited-intro-content'>
					{brand === 'vir' ? (
						<>
							{fullText} is our popular (and super-convenient) shipping service,
							designed to reward our best customers. An annual subscription is $
							{unlimitedPrice}. Then, no matter how many times you order, you
							pay no delivery fees for a year.
						</>
					) : (
						<>
							Our {unlimitedType} shipping service rewards you every time you order.
							An annual subscription is ${unlimitedPrice}. Then, no matter how
							many times you order, you pay no delivery fees for a year.
						</>
					)}
				</p>
				<ul className='unlimited-bullets'>
					<li>
						Shop as often as you like &ndash; you{' '}
						<strong>save at least $19.99 on every order</strong>
					</li>
					<li>
						Available on every order,{' '}
						<strong>including wine club cases</strong>
					</li>
					<li>
						Add <strong>multiple shipping addresses</strong> &ndash; ideal
						for sending gifts
					</li>
					<li>Exclusive wine offers throughout the year</li>
					<li>ONLY ${unlimitedPrice} for 12 months</li>
					<li>
						Convenient, automatic renewal (with advance notice & no obligations)
					</li>
				</ul>
				<p className='unlimited-end-content'>
					Thousands of customers are saving on shipping every time they order -
					join them today!
					<a
						href='#'
						onClick={(e) => {
							e.preventDefault();
							setShowTerms(!showTerms);
						}}
					>
						{showTerms ? 'Hide' : 'View'} Terms and Conditions
					</a>
				</p>
				<div className={(showTerms ? 'show' : 'hide') + ' unlimited-terms'}>
					<UnlimitedTerms
						brandTag={brand}
						fullText={fullText}
						shortText={shortText}
						unlimitedPrice={unlimitedPrice}
						unlimited={unlimitedType}
					/>
				</div>
				<Button buttonType='secondary' onClick={() => toggleModal()}>
					Close
				</Button>
			</StyledModalContent>
		</Modal>
	);
};

UnlimitedModal.defaultProps = {
	/**
	 * render modal
	 */
	triggerModal: false,
	/**
	 * UnlimitedData to display
	 */
	unlimitedData: null,
};
UnlimitedModal.propTypes = {
	triggerModal: PropTypes.bool,
	unlimitedData: PropTypes.oneOfType([PropTypes.node, PropTypes.object]),
};

export default UnlimitedModal;
