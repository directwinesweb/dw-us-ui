import React from 'react';
import UnlimitedModal from './UnlimitedModal';
import { boolean } from '@storybook/addon-knobs';
export default {
	title: 'Modals/Unlimited Modal',
	parameters: {
		component: UnlimitedModal,
		componentSubtitle: 'Displays Unlimited Modal',
	},
};

export const Default = () => {
	const unlimitedData = {
		brand: 'law',
		brandPhone: '1-800-649-4637',
		email: 'customerservice@laithwaites.com',
		itemCode: '15145UL',
		unlimitedPrice: 89,
		unlimitedType: 'unlimited',
		shortText: 'Unlimited',
		fullText: 'Laithwaites Unlimited',
		images: {
			small: 'https://www.laithwaites.com/images/us/en/product/15145UL_S.jpg',
			large: 'https://www.laithwaites.com/images/us/en/product/15145UL_L.jpg',
			thumb: 'https://www.laithwaites.com/images/us/en/product/15145UL_T.jpg',
		},
	};
	return (
		<UnlimitedModal
			triggerModal={boolean('toggle', true)}
			unlimitedData={unlimitedData}
		/>
	);
};
