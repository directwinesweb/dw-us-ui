import React from 'react';
import PropTypes from 'prop-types';

import { Seo } from '@dw-us-ui/seo';
import { StateSelector } from './components/organisms/StateSelector';
import { StateSelectorContainer } from './components/templates/StateSelectorContainer';

const StateSelectorModal = ({
	isOfferRedirector = false,
	profile = {},
	trigger = false,
}) => (
	<>
		{isOfferRedirector ? (
			<>
				<Seo title='LW Special Offer' hideRobotsMeta />
				<StateSelector
					renderSelector={true}
					type={'static'}
					profile={profile}
				/>
			</>
		) : (
			<StateSelectorContainer profile={profile} trigger={trigger} />
		)}
	</>
);

StateSelectorModal.defaultProps = {
	isOfferRedirector: false,
};

StateSelectorModal.propTypes = {
	/**
	 * Determines if modal is offer redirector
	 */
	isOfferRedirector: PropTypes.bool,
	profile: PropTypes.object,
};

export default StateSelectorModal;
