import React from 'react';
import StateSelectorModal from './StateSelectorModal';

export default {
	title: 'Organisms/StateSelectorModal',
	parameters: {
		component: StateSelectorModal,
	},
};

export const Default = () => <StateSelectorModal />;
