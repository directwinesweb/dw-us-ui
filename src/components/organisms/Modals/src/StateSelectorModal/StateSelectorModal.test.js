import StateSelectorModal from './StateSelectorModal';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('StateSelectorModal test', () => {
	it('Creates snapshot test for <StateSelectorModal />', () => {
		const wrapper = shallow(<StateSelectorModal></StateSelectorModal>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
