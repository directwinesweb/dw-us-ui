const navigateToRedirect = ({ redirect, params }) => {
	let finalRedirect;
	finalRedirect =
		redirect +
		(params
			? redirect.includes('?')
				? `&${params}`
				: params.includes('?')
				? params
				: `?${params}`
			: '');

	return (window.location.href = `${finalRedirect}`);
};

export default navigateToRedirect;
