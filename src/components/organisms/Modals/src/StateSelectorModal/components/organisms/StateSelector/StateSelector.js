import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { GetParam } from '@dw-us-ui/get-param';
import { GetStateData } from '@dw-us-ui/get-state-data';
import { LoadingIcon } from '@dw-us-ui/loading-icon';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';

import { Header } from '../../atoms/Header';
import { PromoCodeError } from '../../atoms/PromoCodeError';
import { SelectState } from '../../molecules/SelectState';
import { ComplianceRedirect } from '../../molecules/ComplianceRedirect';

import { verifyOffer, navigateToRedirect, getCampaignParam } from './utils';

import StyledSelector from './style';
import STATE_SELECTOR_COPY from '../../../constants/STATE_SELECTOR_COPY';
import STATES_FALLBACK from '../../../constants/STATES_FALLBACK';
import DEFAULT_STATE from '../../../constants/DEFAULT_STATE';
import { useCampaignData } from './hooks';

/**--------------------------------------------------------- 
State Selector 2021
-------------------------
 @description Collects available states from /api/location/states then applies selected State against /api/user/profile/locationContext
 @param {Boolean} renderSelector trigger api collection if selector is rendered (onTrigger for modals, always rendered for page level state selector)
----------------------------------------------------------*/

const StateSelector = ({ renderSelector, type = 'modal' }) => {
	const [stateSelectorView, setSelectorView] = useState('select-state');
	const [isLoading, setIsLoading] = useState(true);
	const [selectedState, setSelectedState] = useState({});
	const [availableStates, setAvailableStates] = useState([DEFAULT_STATE]);
	const theme = useCustomTheme();
	const [promoCodeState, setPromoCodeState] = useState({
		errors: {},
		promoCode: '',
		campaignConfig: false,
		showPromoField: false,
	});
	const [isProcessing, setIsProcessing] = useState(false);
	const [isRedirecting, setIsRedirecting] = useState(false);

	const campaignOffer = type !== 'modal' ? GetParam('campaign') : false;
	useCampaignData({ campaignOffer, promoCodeState, setPromoCodeState });

	// On render, collect available states
	useEffect(() => {
		if (renderSelector && availableStates.length === 1) {
			const populateStates = ({ response = false, apiSuccess = false }) => {
				setIsLoading(false);
				let statesArr = apiSuccess && response ? response : STATES_FALLBACK;
				statesArr.unshift(DEFAULT_STATE);
				setAvailableStates(statesArr);
			};
			axios
				.get('/api/location/states')
				.then((res) => {
					let statesResponse = res.data?.response?.response.states;
					populateStates({
						response: statesResponse.length ? statesResponse : STATES_FALLBACK,
						apiSuccess: true,
					});
				})
				.catch((err) => {
					console.error(err, 'States api failed, setting fallback');
					populateStates({ apiSuccess: false });
				});
		}
	}, [renderSelector, availableStates]);

	// If stateCode param, fire verify method
	useEffect(() => {
		let stateParam = GetParam('stateCode=');
		if (stateParam) {
			let value = stateParam.split('=');
			verifyState(value[1]);
		}
	}, []);

	// Method to fill select values and options (value and display)
	const populateStateSelector = () => {
		let stateOptions = [];
		availableStates.forEach((state) =>
			stateOptions.push({ text: state.stateName, value: JSON.stringify(state) })
		);
		return stateOptions;
	};

	// Reset state selector view and get state from selection
	const retrieveState = (e) => {
		setSelectorView('select-state');
		let selection = JSON.parse(e.target.value);
		setSelectedState(selection);
	};

	// Return if brand is shippable
	const isShippableBrand = (stateInfo) => {
		const { shippableBrands } = stateInfo;

		if (shippableBrands) {
			return (
				shippableBrands.includes('all') ||
				shippableBrands.includes(theme.data.tag)
			);
		}
	};

	//============ State Redirector Helpers ===========
	// relies on current state to be accurate, will need a refactor to utils!
	/* 

	For state redirector offers - filter the offer objects by selected stateCode.
		if(state is a match){
			return [{id, state: 'someStateCode', stateOffers:{ offerObject }];
	 	} else {
			return []
		 }
	*/
	const filterOfferByState = ({ customStates, stateCode }) =>
		customStates.filter(
			(stateObject) => stateObject.state.toUpperCase() === stateCode
		);

	/* 
	returns offer object
		{ defaultOfferCode, promoCodes: [], retailer, showEdd, urlRedirect }
	*/
	const getCampaignOffer = ({ selectedState, stateInfo }) => {
		const { campaignConfig } = promoCodeState;

		const { wineryDirect } = stateInfo;
		const { stateCode } = selectedState;
		const { campaignSettings } = campaignConfig;
		const { customStates, wineryDirectStates, standardStates } =
			campaignSettings;

		let filteredState = filterOfferByState({ customStates, stateCode });

		let offer = filteredState.length
			? filteredState[0].stateOffers
			: wineryDirect
			? wineryDirectStates
			: standardStates;

		return offer;
	};

	// On state selected, collect state offer information if a DM offer (promoCodeState.showPromoField)

	useEffect(() => {
		if (promoCodeState.campaignConfig && promoCodeState.showPromoField) {
			const stateInfo = GetStateData(selectedState.stateCode, 'code');

			const { defaultPromoCode, showEdd, retailer, urlRedirect } =
				getCampaignOffer({ selectedState, stateInfo });

			setPromoCodeState({
				...promoCodeState,
				defaultOffer: defaultPromoCode,
				showEdd,
				retailer,
				urlRedirect,
			});
		}
	}, [selectedState.stateCode]);

	//================================================

	// Location context completion if the state is valid
	const verifyState = (param = null) => {
		let stateValue = param ? param : selectedState.stateCode,
			stateInfo = GetStateData(stateValue, 'code'),
			stateCode = selectedState.stateCode;
		setSelectedState(stateInfo);
		if (isShippableBrand(stateInfo)) {
			setIsProcessing(true);
			axios
				.post('/api/user/profile/locationContext', {
					stateCode: selectedState.stateCode,
				})
				.then(() => {
					setIsProcessing(false);
					if (type === 'modal') {
						renderStateView({ stateInfo });
					} else {
						if (promoCodeState.campaignConfig) {
							setPromoCodeState({
								...promoCodeState,
								campaignLoading: true,
							});
							const { wineryDirect } = stateInfo;
							const { campaignConfig, promoCode, showPromoField } =
								promoCodeState;

							const { campaignSettings } = campaignConfig;
							const { customStates } = campaignSettings;

							let filteredState = filterOfferByState({
								customStates,
								stateCode,
							});

							const {
								defaultPromoCode,
								showEdd,
								retailer,
								urlRedirect,
								promoCodes,
							} = getCampaignOffer({ selectedState, stateInfo });

							setPromoCodeState({
								...promoCodeState,
								urlRedirect: urlRedirect || false,
							});
							if (urlRedirect) {
								renderStateView({ stateInfo, urlRedirect });
							} else {
								verifyOffer({
									filteredState,
									defaultPromoCode,
									promoCode,
									showPromoField,
									showEdd,
									retailer,
									stateCode,
									wineryDirect,
									setPromoCodeState,
									promoCodeState,
									theme,
									promoCodes,
									campaignName: getCampaignParam(campaignOffer),
								});
							}
						} else {
							renderStateView({ stateInfo });
						}
					}
				})
				.catch((err) => {
					setIsProcessing(false);
					console.warn(
						'State selected has returned an issue',
						selectedState.stateCode,
						err.response
					);
				});
		} else {
			setSelectorView('select-state non-ship');
		}
	};

	// Render the appropriate state selector view, or toggle state selector if no compliance messaging
	const renderStateView = ({ stateInfo, urlRedirect }) => {
		const setWindowLocation = () => {
			setIsRedirecting(true);
			urlRedirect
				? navigateToRedirect({
						redirect: urlRedirect,
						params: getCampaignParam(campaignOffer),
				  })
				: location.reload();
		};
		if (stateInfo.wineryDirect) {
			setSelectorView('winery-direct');
		} else if (stateInfo.threeTier || stateInfo.legacyWD) {
			if (stateInfo.retailer) {
				stateInfo.retailer.complianceRedirect
					? setSelectorView('three-tier')
					: setWindowLocation();
			} else {
				console.warn('no retailer for 3T or LWD found');
				setWindowLocation();
			}
		} else {
			setWindowLocation();
		}
	};

	const validatePromoCode = (e) => {
		let promoCode = e.target.value,
			errors = { message: '' };
		if (!promoCode.length) {
			errors.message = 'Please enter your offer code';
		} else if (promoCode.length < 7) {
			errors.message = 'Please enter at least 7 characters';
		} else {
			errors = {};
		}
		setPromoCodeState({
			...promoCodeState,
			promoCode,
			errors,
		});
	};

	// state selector view method
	const renderSelectorView = (stateSelectorView) => {
		if (stateSelectorView.includes('select-state')) {
			return (
				<SelectState
					brandTag={theme.data.tag}
					stateSelectorView={stateSelectorView}
					populateStateSelector={populateStateSelector}
					retrieveState={retrieveState}
					verifyState={verifyState}
					selectedState={selectedState}
					copy={STATE_SELECTOR_COPY}
					promoCodeState={promoCodeState}
					onPromoChange={validatePromoCode}
					isModal={type === 'modal'}
					isProcessing={isProcessing}
					isRedirecting={isRedirecting}
				/>
			);
		} else {
			return (
				<ComplianceRedirect
					selectedState={selectedState}
					copy={STATE_SELECTOR_COPY}
					brandTag={theme.data.tag}
					promoCodeState={promoCodeState}
					campaignOffer={campaignOffer}
				/>
			);
		}
	};

	return (
		<StyledSelector className={type !== 'modal' ? 'promo' : 'std'}>
			<Header theme={theme} />
			{isLoading ? (
				<LoadingIcon />
			) : (
				<>
					{promoCodeState.noCampaign ? (
						<p>{STATE_SELECTOR_COPY.offerRedirector.noCampaign}</p>
					) : (
						renderSelectorView(stateSelectorView)
					)}
				</>
			)}
			{promoCodeState.promoCodeError ? (
				<PromoCodeError
					copy={STATE_SELECTOR_COPY.offerRedirector.promoCodeError}
					link={`${theme.data.url}/${promoCodeState.defaultOffer}?showEdd=${
						promoCodeState.showEdd
					}&retailer=${promoCodeState.retailer}&${getCampaignParam(
						campaignOffer
					)}`}
				/>
			) : null}
			{promoCodeState.showPromoField && selectedState.stateCode ? (
				<>
					<p>{STATE_SELECTOR_COPY.offerRedirector.promoCodePrompt.subheader}</p>
					<a
						href={`${theme.data.url}/${promoCodeState.defaultOffer}?showEdd=${
							promoCodeState.showEdd
						}&retailer=${promoCodeState.retailer}&${getCampaignParam(
							campaignOffer
						)}`}
					>
						{STATE_SELECTOR_COPY.offerRedirector.promoCodePrompt.button}
					</a>
				</>
			) : null}{' '}
		</StyledSelector>
	);
};

export default StateSelector;
