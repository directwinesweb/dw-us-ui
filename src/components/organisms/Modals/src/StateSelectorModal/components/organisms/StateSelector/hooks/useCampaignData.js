import axios from 'axios';
import { useEffect } from 'react';

const useCampaignData = ({
	campaignOffer,
	promoCodeState,
	setPromoCodeState,
}) => {
	useEffect(() => {
		if (
			campaignOffer &&
			!promoCodeState.campaignConfig &&
			!promoCodeState.campaignError
		) {
			let normalizedCampaignOffer = campaignOffer;
			let campaignRegex = /^([A-Z,0-9]{4}-)(.*)/; // XXXX-xxxx
			const matchedPattern = campaignOffer.match(campaignRegex);
			if (matchedPattern?.length === 3) {
				normalizedCampaignOffer =
					matchedPattern[1] + matchedPattern[2].toLowerCase();
			}

			axios
				.get(
					`https://dw-us-offer-redirector.herokuapp.com/campaigns?campaignCode=${normalizedCampaignOffer}`
				)
				.then((res) => {
					let campaignConfig = res.data[0];
					if (campaignConfig) {
						setPromoCodeState({
							...promoCodeState,
							campaignConfig,
							showPromoField:
								campaignConfig.campaignSettings.campaignType === 'dm',
						});
					} else {
						console.error(`No campaign set up for ${campaignOffer}`);
						setPromoCodeState({
							...promoCodeState,
							campaignError: true,
							noCampaign: true,
						});
					}
				})
				.catch((err) => {
					console.error(err, 'The campaign could not be found');
					setPromoCodeState({
						...promoCodeState,
						campaignError: true,
					});
				});
		}
	}, [campaignOffer, promoCodeState]);
};

export default useCampaignData;
