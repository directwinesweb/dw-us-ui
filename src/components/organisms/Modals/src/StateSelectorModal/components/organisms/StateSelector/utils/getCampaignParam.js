const getCampaignParam = (campaign) => {
	let campaignParam = campaign.split('-');
	return campaignParam.length && campaignParam.length !== 2
		? false
		: `campaignName=${campaignParam[1]}`;
};
export default getCampaignParam;
