import React from 'react';
import styled from '@emotion/styled';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';

const StyledStateSelector = styled.div`
	@keyframes fadeInAnimation {
			0% {
				opacity: 0;
			}
			100% {
				opacity: 1;
			}
		}
	@keyframes slideFade {
			0% {
				opacity: 0;
				top: 10vh
			}
			100% {
				opacity: 1;
				top:15vh
			}
		}
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 0 14px;
	&.promo {
		background: rgba(255, 255, 255, 0.89);
		max-width: 710px;
		margin: auto;
		position: absolute;
		margin-left: auto;
		margin-right: auto;
		left: 0;
		right: 0;
		text-align: center;
		top: 15vh;
		animation: slideFade ease 0.7s;
		.logo-container{
			animation: fadeInAnimation ease 0.7s;
		}
	}

	}
	&:empty {
		min-height: 560px;
	}
	span {
		padding-bottom: 30px;
		${EmotionBreakpoint('mobile')} {
			padding: 20px 10px;
		}
	}
	.logo-container {
		padding: 26px 0;
		min-width: 160px;
		max-width: 180px;
		img {
			max-width: 100%;
		}
	}
	p {
		margin: 0;
		padding-bottom: 10px;
	}
	.retailer-compliance-content {
		font-size: 13px;
		line-height: 18px;
		text-align: left;
	}
	span:first-of-type {
		text-align: center;
		font-size: 18px;
		line-height: 25px;
	}
	span:last-of-type {
		font-size: 13px;
		line-height: 18px;
		padding-bottom: 3px;
		padding-top: 15px;
		text-align: center;
	}
	button {
		width: 100%;
		max-width: 120px;
		padding: 0;
	}
	button.continue-promo {
		max-width: 165px;
		height: 44px;
	}
	.continue-button {
		margin: 10px auto;
		font-size: 18px;
		padding: 0;
		max-width: 100%;
		height: 44px;
	}
	.no-pad {
		padding: 0;
	}
	.no-pad-span {
		padding-top: 0%;
	}
	.error {
		color: ${({ theme }) => theme.errors.color};
	}
	.error,
	.error + p {
		font-size: 14px;
		line-height: 22px;
	}
	.law-main-header {
		font-weight: bold;
		font-size: 20px;
		line-height: 24px;
		font-family: "Noto Serif";
		&.redirect{
			padding-bottom: 30px;
		}
	}

`;

const StyledStateSelectorWrapper = ({ children, className }) => (
	<StyledStateSelector className={className}>{children}</StyledStateSelector>
);

export default StyledStateSelectorWrapper;
