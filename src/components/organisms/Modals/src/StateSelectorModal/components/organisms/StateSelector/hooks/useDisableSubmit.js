import { useEffect } from 'react';

const useDisableSubmit = ({
	errors,
	selectedState,
	showPromoField,
	promoCode,
	setDisabled,
}) => {
	useEffect(() => {
		let isStateSelected =
			Object.keys(selectedState).length && selectedState.stateCode;
		let hasPromoErrors = Object.keys(errors).length || !promoCode.length;
		setDisabled(
			showPromoField ? !isStateSelected || hasPromoErrors : !isStateSelected
		);
	}, [errors, selectedState, showPromoField, promoCode]);
};

export default useDisableSubmit;
