const navigateToOffer = ({ offer, base, showEdd, retailer, campaignName }) =>
	(window.location.href = `${base}/${offer}?showEdd=${showEdd}&retailer=${retailer}&${campaignName}`);
export default navigateToOffer;
