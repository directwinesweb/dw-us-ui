import React, { useEffect } from 'react';
import { Modal } from '../../../../Modal';
import { useModal } from '../../../../hooks/useModal';
// import StyledModalWrapper from '../StateSelector/style';
import { StateSelector } from '../StateSelector';

/**--------------------------------------------------------- 
State Selector Modal 2020
-------------------------
 @description triggers the state selector modal based on lack of shoppingLocation or direct trigger
 @param {Function} triggerModal controls modal open or closed
----------------------------------------------------------*/

const StateSelectorModal = ({ triggerModal, profile = {} }) => {
	const [showModal, toggleModal] = useModal();

	useEffect(() => {
		const shoppingLocationContextCookiePresent = () => {
			let shoppingLocationContextCookie = `; ${document.cookie}`.match(
				`;\\s*${'shoppinglocation'}=([^;]+)`
			);
			return shoppingLocationContextCookie ? true : false;
		};

		let profileStateCode =
			profile?.userInformation?.shoppingLocationContext?.stateCode || false;

		if (
			(triggerModal ||
				!profileStateCode ||
				!shoppingLocationContextCookiePresent()) &&
			!showModal
		) {
			toggleModal();
		}
	}, [showModal, triggerModal, profile, showModal]);

	return (
		<Modal
			showModal={showModal}
			toggleModal={toggleModal}
			modalSize={'600px'}
			disableTextClose={true}
			disableClose={true}
			disableOnClickOutside={true}
			minHeight={'210px'}
		>
			<StateSelector renderSelector={showModal} type={'modal'} />
		</Modal>
	);
};

export default StateSelectorModal;
