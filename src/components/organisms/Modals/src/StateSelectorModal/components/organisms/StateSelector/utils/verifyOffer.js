import navigateToOffer from './navigateToOffer';

const verifyOffer = ({
	filteredState,
	defaultPromoCode,
	showEdd,
	retailer,
	showPromoField,
	setPromoCodeState = () => {},
	promoCodeState,
	theme,
	promoCode,
	promoCodes,
	campaignName,
}) => {
	setPromoCodeState({ ...promoCodeState, promoCodeError: false });
	if (showPromoField) {
		let finalCodes = filteredState.length
			? filteredState[0].stateOffers.promoCodes
			: promoCodes;
		let filteredPromos = finalCodes.filter((offer) => {
			return offer.promoCode === promoCode;
		});
		if (filteredPromos.length > 0 || promoCode === defaultPromoCode) {
			navigateToOffer({
				offer: promoCode,
				base: theme.data.url,
				showEdd,
				retailer,
				campaignLoading: false,
				campaignName,
			});
		} else {
			setPromoCodeState({
				...promoCodeState,
				promoCodeError: true,
				defaultOffer: defaultPromoCode,
				showEdd,
				retailer,
				campaignLoading: false,
			});
		}
	} else {
		navigateToOffer({
			offer: defaultPromoCode,
			base: theme.data.url,
			showEdd,
			retailer,
			campaignLoading: false,
			campaignName,
		});
	}
};
export default verifyOffer;
