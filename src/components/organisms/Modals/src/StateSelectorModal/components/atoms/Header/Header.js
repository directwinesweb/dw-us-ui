import React from 'react';
import StyledHeaderWrapper from './style';

const Header = ({ theme }) => {
	const logoUrl =
		theme.data.tag === 'law'
			? 'https://www.laithwaites.com/html/content/offers/us/offer-redirector/assets/LAW.svg'
			: theme.data.logo;
	return (
		<StyledHeaderWrapper>
			<div
				className={`${
					theme.data.tag === 'wsj' ? 'no-pad ' : null
				} logo-container`}
			>
				<img src={logoUrl} alt='Logo' className='header-image' />
			</div>
		</StyledHeaderWrapper>
	);
};
export default Header;
