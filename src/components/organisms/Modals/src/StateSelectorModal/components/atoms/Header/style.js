import React from 'react';
import styled from '@emotion/styled';

const StyledHeaderWrapper = styled.div`
	.logo-container {
		padding: 26px 0;
		min-width: 160px;
		max-width: 180px;
		img {
			max-width: 100%;
		}
	}
`;

const StyledHeaderWrapperWrapper = ({ children }) => (
	<StyledHeaderWrapper>{children}</StyledHeaderWrapper>
);

export default StyledHeaderWrapperWrapper;
