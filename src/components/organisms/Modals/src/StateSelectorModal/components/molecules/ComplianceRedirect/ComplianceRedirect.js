import { BrandUtil } from '@dw-us-ui/brand-util';
import { Button } from '@dw-us-ui/button';
import React from 'react';

import { getCampaignParam, navigateToRedirect } from '../../organisms/StateSelector/utils';

const ComplianceRedirect = ({
	selectedState,
	copy,
	brandTag,
	promoCodeState,
	campaignOffer,
}) => {
	const brand = BrandUtil.content(BrandUtil.getBrand());
	const { phone } = brand;
	const { urlRedirect } = promoCodeState;
	const createMarkup = (input) => {
		return { __html: input };
	};
	const isLaw = brandTag === 'law';

	/* request for LAW to render different language - currently will work for all states besides:
	states that use Lionstone Interational (to continue use of bottle limit copy work - "Good to know" section)
	*/
	const lawRedirectLang = ({ retailer, state, retailerAddress }) => {
		if (retailerAddress) {
			// If you can set the short address, do so, and split, this is ignored for CT and WA
			if (retailerAddress.includes(',')) {
				let shortRetailerAddress = retailerAddress.split(',');
				let join = `${shortRetailerAddress[1]}, ${shortRetailerAddress[2]
					.trim()
					.substring(0, 2)}`;
				retailerAddress = join;
			}
		}
		// Retailer is harcoded to California for Lionstone West customers
		return `Thank you for shopping with Laithwaites. <br /><br />Your wine adventure will continue with ${retailer}, a ${
			retailer === 'Lionstone West' ? 'California' : state
		} licensed retailer who can legally fulfill orders in your state. <br /><br />Any orders you place will be forwarded to ${retailer} ${
			retailerAddress && state !== 'Connecticut' && state !== 'Washington'
				? `in ${retailerAddress}`
				: ''
		} for processing and shipping.`;
	};

	return (
		<>
			{isLaw ? (
				<p className='law-main-header redirect'>
					{copy.stateSelector.lawRedirectHeader}
				</p>
			) : null}{' '}
			<p
				className='retailer-compliance-content'
				dangerouslySetInnerHTML={
					isLaw &&
					selectedState.retailer.retailerName !== 'Lionstone International'
						? createMarkup(
								lawRedirectLang({
									retailer: selectedState.retailer.retailerName,
									state: selectedState.stateName,
									retailerAddress: selectedState.retailer.address,
								})
						  )
						: createMarkup(selectedState.retailer.complianceRedirect)
				}
			/>
			{selectedState.retailer.belowZipCityState &&
			selectedState.stateCode !== 'VA' &&
			selectedState.stateCode !== 'AL' &&
			selectedState.stateCode !== 'NY' ? (
				<p
					className='retailer-compliance-content'
					dangerouslySetInnerHTML={createMarkup(
						selectedState.retailer.belowZipCityState
					)}
				/>
			) : null}
			<Button
				className='continue-button'
				buttonType='primary'
				onClick={() => {
					urlRedirect
						? navigateToRedirect({
								redirect: urlRedirect,
								params: getCampaignParam(campaignOffer),
						  })
						: location.reload();
				}}
			>
				{copy.redirect.button}
			</Button>
			{isLaw ? (
				<p className='retailer-compliance-content'>
					Any questions? Give us a call at {phone}
				</p>
			) : null}
		</>
	);
};
export default ComplianceRedirect;
