import React from 'react';
import styled from '@emotion/styled';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';

const StyledSelectState = styled.div`
	transition: all 0.2s ease 0s;
	margin-bottom: 10px;
	display: flex;
	flex-direction: column;
	.select-wrapper {
		position: relative;
		&.promo {
			display: flex;
			width: 100%;
			margin-bottom: 10px;
			justify-content: space-between;
			transition: all 0.2s ease 0s;

			${EmotionBreakpoint('mobile')} {
				flex-direction: column;
			}
		}
	}
	.select-wrapper:after {
		border-style: solid;
		border-width: 2px 2px 0 0;
		width: 8px;
		content: '';
		display: inline-block;
		height: 8px;
		position: absolute;
		vertical-align: top;
		transform: rotate(135deg);
		pointer-events: none;
		top: 15px;
		right: 10px;
	}
	.select-wrapper:not(.std):after {
		left: 181px;
		${EmotionBreakpoint('mobile')} {
			left: unset;
			right: 10px;
		}
	}
	select {
		appearance: none;
	}
	button,
	select {
		height: 44px;
	}
	.select-error {
		color: ${({ theme }) => theme.errors.color};
	}
	.select-error,
	.select-error + p {
		font-size: 14px;
		line-height: 22px;
	}

	.state-container {
		display: flex;
		margin: auto;
		width: 350px;
		&.promo {
			flex-wrap: wrap;
		}
		&.continue-promo {
			width: 380px;
		}
		${EmotionBreakpoint('mobile')} {
			width: 245px;
		}
		${EmotionBreakpoint('mobile')} {
			flex-direction: column;
		}
		justify-content: space-between;
		select {
			font-size: 15px;
			border-radius: 0px;
			margin-bottom: 10px;
			border: 0;
			border-bottom: 2px solid;
			background-color: ${({ theme }) => theme.colors.textInputBackgroundColor};
			padding-left: 7px;
			${EmotionBreakpoint('mobile')} {
				max-width: 100%;
				margin-bottom: 20px;
			}
			label {
				display: none;
			}
		}
		select,
		.input-container {
			@media (min-width: 768px) {
				width: 200px;
			}
		}
		div.text-input {
			label {
				padding-bottom: 5px;
			}
			.state-selector-promo-code {
				height: 44px;
			}
		}
		div.text-input > label.input-label-container.hide-placeholder {
			transform: translate(0, -1px) scale(0.75) !important;
		}
		&.promo {
			width: 430px;
			${EmotionBreakpoint('mobile')} {
				width: 100%;
			}
			button {
				max-width: unset;
			}
		}
	}
	${EmotionBreakpoint('mobile')} {
		select,
		button,
		button.continue-promo {
			max-width: unset;
			width: 100%;
		}
	}
	button > img {
		width: 40px;
	}
	.redirecting {
		font-size: 10px;
	}
`;

const StyledSelectStateWrapper = ({ children }) => (
	<StyledSelectState>{children}</StyledSelectState>
);

export default StyledSelectStateWrapper;
