import React, { useState, useEffect } from 'react';
import { SelectInput, TextInput } from '@dw-us-ui/form-utils';
import { Button } from '@dw-us-ui/button';

import StyledSelectState from './style';
import { useDisableSubmit } from '../../organisms/StateSelector/hooks';
import { LoadingIcon } from '@dw-us-ui/loading-icon';

const SelectState = ({
	brandTag,
	stateSelectorView,
	selectedState,
	copy,
	promoCodeState,
	isModal,
	populateStateSelector = () => {},
	retrieveState = () => {},
	verifyState = () => {},
	onPromoChange = () => {},
	isProcessing = false,
	isRedirecting = false,
}) => {
	const staticCopy = isModal
		? copy.stateSelector.button
		: copy.offerRedirector.button;
	const [disabled, setDisabled] = useState(!Object.keys(selectedState).length);
	const [buttonCopy, setButtonCopy] = useState(staticCopy);
	const { showPromoField, errors, promoCode, campaignLoading } = promoCodeState;
	useDisableSubmit({
		errors,
		selectedState,
		showPromoField,
		promoCode,
		setDisabled,
	});
	const formErrors = Object.keys(promoCodeState.errors).length
		? { promoCode: promoCodeState.errors }
		: { errors: {} };

	const renderHeaderCopy = () => {
		const { stateSelector } = copy;
		const { header } = stateSelector;

		const isLaw = brandTag === 'law';

		const headerObject = {
			lawCopy: isLaw ? stateSelector.lawMainHeader : false,
			header:
				header[
					isLaw ? (showPromoField ? 'lawHeaderDM' : 'lawHeader') : 'other'
				],
		};

		return (
			<>
				{isLaw ? (
					<p className='law-main-header'>{headerObject.lawCopy}</p>
				) : null}{' '}
				{headerObject.header}
			</>
		);
	};
	useEffect(() => {
		if (campaignLoading) {
			!disabled ? setDisabled(true) : null;
			setButtonCopy('Loading');
		} else {
			setButtonCopy(staticCopy);
		}
	}, [campaignLoading, disabled]);

	return (
		<StyledSelectState>
			<span
				className={`${
					brandTag === 'wsj' ? 'no-pad-span ' : null
				}select-state-copy`}
			>
				{stateSelectorView.includes('non-ship') ? (
					<>
						<div className='select-error'>
							{copy.stateSelector.error.header}
							{selectedState.stateName}
						</div>
						<p> {copy.stateSelector.error.subheader}</p>
					</>
				) : (
					<>{renderHeaderCopy()}</>
				)}
			</span>
			<div
				className={`state-container ${showPromoField ? 'promo' : 'std'} ${
					isModal ? '' : 'continue-promo'
				}`}
			>
				<div className={`select-wrapper ${showPromoField ? 'promo' : 'std'}`}>
					<SelectInput
						optionsList={populateStateSelector('options')}
						onChange={(e) => retrieveState(e)}
						name='state-selector'
					/>
					{showPromoField ? (
						<TextInput
							className='state-selector-promo-code'
							name='promoCode'
							placeholder='Offer Code'
							maxLength={7}
							onChange={onPromoChange}
							errors={formErrors}
						/>
					) : null}
				</div>
				<Button
					buttonType='primary'
					onClick={() => verifyState()}
					disabled={disabled || isProcessing || isRedirecting}
					className={
						isModal ? (isRedirecting ? 'redirecting' : 'std') : 'continue-promo'
					}
				>
					{isProcessing ? (
						<LoadingIcon />
					) : isRedirecting ? (
						'Loading'
					) : (
						buttonCopy
					)}
				</Button>
			</div>
			<span>
				{isModal
					? copy.stateSelector.disclaimer
					: copy.offerRedirector.disclaimer}
			</span>
		</StyledSelectState>
	);
};

export default SelectState;
