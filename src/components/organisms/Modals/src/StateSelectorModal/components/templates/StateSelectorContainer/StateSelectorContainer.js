import React, { useState, useEffect } from 'react';
import { StateSelectorModal } from '../../organisms/StateSelectorModal';

/**--------------------------------------------------------- 
State Selector Modal 2020
-------------------------
 @description Collects available states from /api/location/states then applies selected State against /api/user/profile/locationContext
 @param {Object} profile passes in profile information from user/details
----------------------------------------------------------*/

const StateSelectorContainer = ({ profile = {}, trigger }) => {
	const [triggered, setTriggered] = useState(false);

	useEffect(() => {
		trigger && !triggered ? setTriggered(true) : null;
	}, [trigger, triggered]);

	return (
		<>
			<span aria-hidden='true' style={{ display: 'none' }}>
				selector mounted
			</span>
			<StateSelectorModal triggerModal={triggered} profile={profile} />
		</>
	);
};

export default StateSelectorContainer;
