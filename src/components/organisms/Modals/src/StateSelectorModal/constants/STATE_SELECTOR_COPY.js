export default {
	stateSelector: {
		header: {
			other: 'Where are you having your wines shipped?',
			lawHeader: 'Tell us where to send the wine.',
			lawHeaderDM:
				'Tell us where to send your wine and input your offer code (find your 7-digit code located on the front of your voucher card or above your address on your mailed invitation).',
		},
		lawMainHeader: 'Before your wine adventure begins…',
		lawRedirectHeader: 'This Way to the Wine!',
		error: {
			header: "We're sorry. Due to regulations, we cannot ship wine to ",
			subheader: 'Please change your selection to proceed.',
		},
		prompt: 'Please change your selection to proceed.',
		button: 'Verify',
		disclaimer:
			'By clicking "Verify," you acknowledge that you are at least 21 years old.',
	},
	redirect: {
		button: 'Continue',
	},
	offerRedirector: {
		noCampaign:
			"Apologies, it looks like we can't find your offer, please try again later",
		promoCodeError: {
			header: "We're sorry, we can't find that offer",
		},
		promoCodePrompt: {
			subheader: "Don't have an offer code? ",
			button: 'Click Here',
		},
		validation: {
			length: 'Please enter at least 7 characters',
			noCode: 'Please enter your offer code',
		},
		button: 'Continue',
		disclaimer:
			'By clicking "Continue," you acknowledge that you are at least 21 years old.',
	},
};
