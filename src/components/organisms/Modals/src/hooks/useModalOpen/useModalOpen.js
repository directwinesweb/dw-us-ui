import { useLayoutEffect } from 'react';

function useModalOpen(modalOpen) {
	useLayoutEffect(() => {
		if (modalOpen) {
			document.body.classList.add('modal-open');
		} else {
			document.querySelector('body').classList.remove('modal-open');
		}

		return () => document.querySelector('body').classList.remove('modal-open');
	}, [modalOpen]);
}

export default useModalOpen;
