import { useState } from 'react';
/**
 *
 * @param {Boolean} initialState
 * @returns showModal Boolean and toggleModal Function
 */
const useModal = (initialState = false) => {
	const [showModal, setShowModal] = useState(initialState);

	function toggleModal() {
		setShowModal(!showModal);
	}

	return [showModal, toggleModal];
};

export default useModal;
