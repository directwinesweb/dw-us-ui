import Modal from './Modal';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Modal test', () => {
	it('Creates snapshot test for <Modal />', () => {
		const wrapper = shallow(
			<Modal showModal={true} modalHeader='Modal Header'>
				modal content
			</Modal>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
