import { boolean, text } from '@storybook/addon-knobs';

import Modal from './Modal';
import React from 'react';

export default {
	title: 'Modals/Modal',
	parameters: {
		component: Modal,
	},
};

export const Default = () => (
	<Modal
		showModal={boolean('ToggleModal', true)}
		disableAnimate={boolean('disableAnimate', false)}
		disableClose={boolean('disableClose', false)}
		modalHeader={text('modalHeader', 'Storybook Modal Test')}
		minHeight={text('minHeight', '100px')}
		modalPadding={text('modalPadding', '10px')}
	>
		Modal Content
	</Modal>
);
