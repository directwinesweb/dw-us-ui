import React, { useEffect, useRef, useState } from 'react';

import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';
import styled from '@emotion/styled';
import { useFocus } from '@dw-us-ui/use-focus';
import { useLockBodyScroll } from '@dw-us-ui/use-lockbody-scroll';
import { useModalOpen } from '../hooks/useModalOpen';
import { useOnClickOutside } from '@dw-us-ui/use-onclick-outside';

const StyledModalWrapper = styled.div`
	font-family: ${({ theme }) => theme.fonts.primary};
	color: ${({ theme }) => theme.colors.body};
	letter-spacing: ${({ theme }) => theme.fonts.spacing.bodyLetterSpacing};
	line-height: ${({ theme }) => theme.fonts.lineHeight};
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	z-index: 1050;
	margin: 0;
	overflow-x: hidden;
	overflow-y: auto;
	outline: 0;
	width: 100%;

	.rc-modal.in {
		top: 0;
		opacity: 1;
	}
	.center {
		text-align: center;
	}
`;

const StyledModalBackdrop = styled.div`
	position: fixed;
	width: 100%;
	height: 100%;
	background-color: ${({ theme }) => theme.modal.backdrop.backgroundColor};
	position: fixed;
	top: 0;
	left: 0;
	z-index: 0;
`;

const StyledModalContainer = styled.div`
	width: 100%;
	max-width: ${(props) =>
		props.modalSize ? props.modalSize : ({ theme }) => theme.modal.width};
	${EmotionBreakpoint('mobile')} {
		max-width: 100%;
	}

	background-color: ${({ theme }) => theme.modal.backgroundColor};
	margin: 16px auto;
	z-index: 1;
	position: relative;
	transition: all 0.15s ease-in-out;
	opacity: ${({ disableAnimate }) => (disableAnimate ? '1' : '0')};

	.rc-modal-header {
		background-color: ${({ theme }) => theme.modal.header.backgroundColor};
		color: ${({ theme }) => theme.modal.header.color};
		font-family: ${({ theme }) => theme.modal.header.fontFamily};
		font-size: ${({ theme }) => theme.modal.header.fontSize};
		line-height: ${({ theme }) => theme.modal.header.lineHeight};
		margin-bottom: 20px;
		display: flex;
		align-items: center;
		justify-content: space-around;
		text-align: left;
		&.alternative-color {
			background-color: ${({ theme }) => theme.modal.backgroundColor};
		}

		.rc-modal-title {
			margin: 31px auto;
			text-align: center;
			font-family: ${({ theme }) => theme.modal.header.fontFamily};
			font-size: ${({ theme }) => theme.modal.header.fontSize};
			font-weight: 400;
			text-transform: ${({ theme }) => theme.modal.header.textTransform};
			${EmotionBreakpoint('mobile')} {
				margin-left: 16px;
				font-size: 20px;
				text-align: left;
				margin-left: 16px;
			}
		}
		.full-modal-title {
			margin-left: auto;
		}
		.rc-modal-close {
			cursor: pointer;
			position: absolute;
			z-index: 2;
			top: 0;
			right: 0;
			padding: 10px 20px 0px 0px;
			font-size: 26px;
			${EmotionBreakpoint('mobile')} {
				padding: 13px;
			}
			&.alternative-header {
				flex-basis: 8%;
			}
		}
	}
	.rc-modal-close {
		z-index: 2;
		cursor: pointer;
		color: ${({ theme }) => theme.modal.header.close.color};
		font-size: ${({ theme }) => theme.modal.header.close.fontSize};
		background: none;
		border: none;
	}
	.rc-modal-body {
		min-height: ${({ minHeight }) => (minHeight ? minHeight : 'inherit')};
		padding: ${({ modalPadding }) =>
			modalPadding ? modalPadding : '0px 20px 30px 20px'};
		${EmotionBreakpoint('mobile')} {
			padding: ${({ modalPadding }) =>
				modalPadding ? modalPadding : '0px 16px 30px 16px'};
		}
		.rc-modal-close {
			position: absolute;
			top: 0;
			right: 0;
			padding: 10px;
		}
	}
	.close-text {
		cursor: pointer;
		font-size: 13px;
		line-height: 20px;
		position: absolute;
		right: 0;
		bottom: 9px;
		padding: 0 25px;
		color: ${({ theme }) => theme.colors.link};
		font-family: ${({ theme }) => theme.button.fontFamily};
		letter-spacing: ${({ theme }) => theme.button.letterSpacing};
		text-transform: ${({ theme }) => theme.button.textTransform};
	}

	.rc-modal-body-zoom {
		width: 100%;
		height: 542px;
		.wsj & {
			height: 540px;
		}
		.npr & {
			height: 542px;
		}
		.mcy & {
			height: 542px;
		}
		overflow: hidden;
		padding: 0;
		position: relative;
		.zoom-image-container {
			width: 100%;
			height: 100%;
			cursor: move;
			.zoom-image {
				pointer-events: none;
				cursor: all-scroll;
			}
		}
	}
`;

const Modal = ({
	showModal,
	toggleModal,
	modalHeader,
	alternativeHeaderColor,
	children,
	modalSize,
	disableOnClickOutside,
	disableClose,
	disableTextClose,
	modalPadding,
	disableAnimate,
	minHeight,
	zoomImageClassName,
	brandTag,
	id,
	className,
}) => {
	// Create a ref that we add to the element for which we want to detect outside clicks
	const ref = useRef();

	const [modalRef, setModalFocus] = useFocus();
	const [animate, setAnimate] = useState(false);
	const [modalAnimate, setModalAnimate] = useState(false);
	useEffect(() => {
		if (showModal && !animate) {
			setModalFocus();
			setModalAnimate(true);
			setTimeout(() => {
				setAnimate(true);
			}, 100);
		} else if (!showModal) {
			setAnimate(false);
			if (modalAnimate) {
				setTimeout(() => {
					setModalAnimate(false);
				}, 300);
			}
		}
	}, [showModal, animate]);

	// Call hook passing in the ref and a function to call on outside click
	useOnClickOutside(
		ref,
		disableOnClickOutside ? () => {} : () => toggleModal()
	);

	// Call hook to lock body scroll
	useLockBodyScroll(modalAnimate);

	// Call hook to toggle 'modal-open' class
	useModalOpen(modalAnimate);

	const renderModal = () => {
		return createPortal(
			<StyledModalWrapper ref={modalRef} tabIndex='-1'>
				<StyledModalContainer
					className={`rc-modal ${animate ? 'in' : ''} ${className}`}
					modalSize={modalSize}
					aria-modal
					aria-hidden={`${showModal}`}
					tabIndex={-1}
					modalPadding={modalPadding}
					disableAnimate={disableAnimate}
					minHeight={minHeight}
					id={id}
				>
					<div ref={ref} className={`rc-modal-dialog`} role='dialog'>
						<div className={`rc-modal-content`}>
							{modalHeader ? (
								<div
									className={`rc-modal-header ${
										alternativeHeaderColor ? 'alternative-color' : ''
									}`}
								>
									<h4
										className={`rc-modal-title ${
											disableClose ? 'full-rc-modal-title' : ''
										}
										`}
									>
										{modalHeader}
									</h4>
									{!disableClose ? (
										<div
											data-dismiss='rc-modal'
											aria-hidden='true'
											className={`rc-modal-close ${
												alternativeHeaderColor ? 'alternative-header' : ''
											}`}
											onClick={toggleModal}
										>
											<img
												src={`/images/us/common/icons/${brandTag}/times_light_16x16.svg`}
												alt='times'
												height='16px'
												width='16px'
											/>
										</div>
									) : null}
								</div>
							) : null}
							<div className={`rc-modal-body ${zoomImageClassName}`}>
								{modalHeader === false && !disableClose ? (
									<div
										data-dismiss='rc-modal'
										aria-hidden='true'
										className='rc-modal-close'
										onClick={toggleModal}
									>
										<img
											src={`/images/us/common/icons/${brandTag}/times_light_16x16.svg`}
											alt='times'
											height='16px'
											width='16px'
										/>
									</div>
								) : null}
								{children}
							</div>
						</div>
						{!disableClose && !disableTextClose ? (
							<div
								data-dismiss='rc-modal'
								aria-hidden='true'
								className='rc-modal-close close-text'
								onClick={toggleModal}
							>
								Close
							</div>
						) : null}
					</div>
				</StyledModalContainer>
				<StyledModalBackdrop
					className={`rc-modal-backdrop rc-modal-stack fade ${
						showModal ? 'in' : ''
					}`}
				/>
			</StyledModalWrapper>,
			document.body
		);
	};

	return <>{modalAnimate ? renderModal() : false}</>;
};

Modal.propTypes = {
	/**
	 * Optional additional classes and IDs
	 */
	className: PropTypes.string,
	id: PropTypes.string,
	/**
	 * Determines whether to show or hide modal
	 */
	showModal: PropTypes.bool,
	/**
	 * Function that toggles modal
	 */
	toggleModal: PropTypes.func,
	/**
	 * Content to display as the modal header
	 */
	modalHeader: PropTypes.any,
	alternativeHeaderColor: PropTypes.bool,
	/**
	 * Modal content
	 */
	children: PropTypes.node,
	/**
	 * Modal size
	 */
	modalSize: PropTypes.node,
	/**
	 * Outside modal click to trigger Modal
	 */
	disableOnClickOutside: PropTypes.bool,
	/**
	 * Determines whether to disable the ability to close the modal
	 */
	disableClose: PropTypes.bool,
	disableTextClose: PropTypes.bool,
	/**
	 * Modal padding
	 */
	modalPadding: PropTypes.node,
	/**
	 * Disables modal animation effect
	 */
	disableAnimate: PropTypes.bool,
	/**
	 * Modal min height
	 */
	minHeight: PropTypes.node,
	zoomImageClassName: PropTypes.string,
	/**
	 * provide brandtag for icon
	 */
	brandTag: PropTypes.string,
};
Modal.defaultProps = {
	showModal: false,
	toggleModal: () => {},
	modalHeader: false,
	alternativeHeaderColor: false,
	disableOnClickOutside: false,
	disableClose: false,
	disableTextClose: false,
	modalPadding: null,
	disableAnimate: false,
	minHeight: false,
	zoomImageClassName: '',
	brandTag: 'wsj',
};
export default Modal;
