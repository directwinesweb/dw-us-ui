import React, { useEffect, useState } from 'react';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { Modal } from '../Modal';
import PropTypes from 'prop-types';
import { TermsConditionsContent } from '@dw-us-ui/terms-conditions-content';
import styled from '@emotion/styled';

const StyledModalContent = styled.div`
	padding: 0.5rem 1rem;
	font-size: 0.9rem;
	line-height: 20px;
	.rc-modal-content {
		height: 50%;
		overflow-y: scroll;
	}
`;

const TermsConditionsModal = ({ triggerModal, toggleModal }) => {
	const [isOpen, setIsOpen] = useState(triggerModal);
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	const { email, name, phone, POBox, url, tag } = data;
	useEffect(() => {
		setIsOpen(triggerModal);
	}, [triggerModal]);

	return (
		<Modal
			showModal={isOpen}
			toggleModal={() => toggleModal()}
			modalSize={'600px'}
			modalHeader='Terms & Conditions'
		>
			<StyledModalContent>
				<div className='rc-modal-content'>
					<TermsConditionsContent
						url={url}
						phone={phone}
						email={email}
						name={name}
						POBox={POBox}
						tag={tag}
					/>
				</div>
			</StyledModalContent>
		</Modal>
	);
};

TermsConditionsModal.propTypes = {
	/**
	 * render modal
	 */
	triggerModal: PropTypes.bool,
	/**
	 * Function that toggles modal
	 */
	toggleModal: PropTypes.func,
};
TermsConditionsModal.defaultProps = {
	triggerModal: false,
	toggleModal: () => {},
};
export default TermsConditionsModal;
