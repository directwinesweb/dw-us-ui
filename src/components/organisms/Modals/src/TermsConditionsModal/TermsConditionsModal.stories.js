import React from 'react';
import TermsConditionsModal from './TermsConditionsModal';
import { boolean } from '@storybook/addon-knobs';

export default {
	title: 'Modals/Terms & Conditions Modal',
	parameters: {
		component: TermsConditionsModal,
		componentSubtitle: 'Displays Terms & Conditions Modal',
	},
};

export const Default = () => {
	return <TermsConditionsModal triggerModal={boolean('ToggleModal', true)} />;
};
