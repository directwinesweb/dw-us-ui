import React from 'react';
import TermsConditionsModal from './TermsConditionsModal';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('TermsConditionsModal test', () => {
	it('Creates snapshot test for <TermsConditionsModal />', () => {
		const wrapper = shallow(<TermsConditionsModal triggerModal />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
