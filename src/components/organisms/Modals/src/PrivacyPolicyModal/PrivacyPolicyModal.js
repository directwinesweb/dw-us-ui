import React, { useEffect, useState } from 'react';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { Modal } from '../Modal';
import { PrivacyPolicyContent } from '@dw-us-ui/privacy-policy-content';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const StyledModalContent = styled.div`
	padding: 0.5rem 1rem;
	font-size: 0.9rem;
	line-height: 20px;
	.rc-modal-content {
		height: 50%;
		overflow-y: scroll;
	}
`;

const PrivacyPolicyModal = ({ triggerModal, toggleModal }) => {
	const [isOpen, setIsOpen] = useState(triggerModal);
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	const { email, name, phone, tag, POBox, url } = data;
	useEffect(() => {
		setIsOpen(triggerModal);
	}, [triggerModal]);

	return (
		<Modal
			showModal={isOpen}
			toggleModal={() => toggleModal()}
			modalSize={'600px'}
			modalHeader={`Privacy ${tag === 'mcy' ? 'Statement' : 'Policy'}`}
		>
			<StyledModalContent>
				<div className='rc-modal-content'>
					<PrivacyPolicyContent
						tag={tag}
						name={name}
						email={email}
						phone={phone}
						POBox={POBox}
						url={url}
					/>
				</div>
			</StyledModalContent>
		</Modal>
	);
};

PrivacyPolicyModal.propTypes = {
	/**
	 * render modal
	 */
	triggerModal: PropTypes.bool,
	/**
	 * Function that toggles modal
	 */
	toggleModal: PropTypes.func,
};
PrivacyPolicyModal.defaultProps = {
	triggerModal: false,
	toggleModal: () => {},
};
export default PrivacyPolicyModal;
