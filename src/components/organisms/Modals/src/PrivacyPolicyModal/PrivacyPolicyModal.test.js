import PrivacyPolicyModal from './PrivacyPolicyModal';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('PrivacyPolicyModal test', () => {
	it('Creates snapshot test for <PrivacyPolicyModal />', () => {
		const wrapper = shallow(<PrivacyPolicyModal triggerModal />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
