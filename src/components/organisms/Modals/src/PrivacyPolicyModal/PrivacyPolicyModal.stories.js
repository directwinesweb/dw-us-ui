import PrivacyPolicyModal from './PrivacyPolicyModal';
import React from 'react';
import { boolean } from '@storybook/addon-knobs';

export default {
	title: 'Modals/PrivacyPolicyModal',
	parameters: {
		component: PrivacyPolicyModal,
	},
};

export const Default = () => (
	<PrivacyPolicyModal triggerModal={boolean('ToggleModal', true)} />
);
