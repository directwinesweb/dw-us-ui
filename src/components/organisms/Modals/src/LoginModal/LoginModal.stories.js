import { boolean, text } from '@storybook/addon-knobs';

import LoginModal from './LoginModal';
import React from 'react';

export default {
	title: 'Modals/Login Modal',
	parameters: {
		component: LoginModal,
	},
};

export const Default = () => (
	<LoginModal
		showModal={boolean('Show Modal', true)}
		modalTitle={text('Modal Title', 'Modal Title')}
	/>
);
