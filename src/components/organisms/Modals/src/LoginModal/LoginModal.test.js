import LoginModal from './LoginModal';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Login Modal test', () => {
	it('Creates snapshot test for <LoginModal />', () => {
		const wrapper = shallow(
			<LoginModal showModal={true} modalTitle='LoginModal Header'></LoginModal>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
