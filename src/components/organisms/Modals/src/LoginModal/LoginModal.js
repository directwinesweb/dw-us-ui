import { LoginForm } from '@dw-us-ui/form-utils';
import { Modal } from '../Modal';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
const ModalContainer = styled.div`
	p {
		font-size: 0.9rem;
		margin: 0 20px 30px 20px;
	}
`;

const LoginModal = ({ showModal, toggleModal, modalTitle }) => {
	return (
		<Modal
			showModal={showModal}
			toggleModal={toggleModal}
			modalHeader={modalTitle}
		>
			<ModalContainer className='login-modal'>
				<LoginForm />
			</ModalContainer>
		</Modal>
	);
};

LoginModal.defaultProps = {
	showModal: false,
	toggleModal: () => {},
	modalTitle: '',
};
LoginModal.propTypes = {
	/**
	 * Determines whether to show the modal
	 */
	showModal: PropTypes.bool,
	/**
	 * Function that toggles modal
	 */
	toggleModal: PropTypes.func,
	/**
	 * A title to be given to the modal
	 */
	modalTitle: PropTypes.any,
};
export default LoginModal;
