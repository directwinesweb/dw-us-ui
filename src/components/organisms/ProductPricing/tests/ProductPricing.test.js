import ProductPricing from '../src/ProductPricing';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ProductPricing test', () => {
        it('Creates snapshot test for <ProductPricing />', () => {
            const wrapper = shallow(<ProductPricing></ProductPricing>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });