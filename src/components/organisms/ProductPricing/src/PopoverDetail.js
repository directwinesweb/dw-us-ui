import PropTypes from 'prop-types';
import React from 'react';
import Popover  from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import { GoogleAnalytics } from 'dw-us-ui/src/bundles/product-bundle';

const PopoverDetail = ({
	anchorClass,
	content,
	container,
	gaAction,
	gaCategory,
	id,
	label,
	placement,
	popoverClass,
	title,
	trigger
}) => {
	let { setEventTracking } = GoogleAnalytics;
	
	const gaEvent = () => {
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: 'Shipping_Rates_Click'
		});
	}

	const popoverContent = (
		<Popover id={id} title={title} className={popoverClass}>
			<button className='btn close-btn pull-right' onClick={() => refs.overlay.hide()}>
				<i className='fa fa-times' />
			</button>
			{content}
		</Popover>
	);

	return (
		<OverlayTrigger
			trigger={trigger}
			placement={placement}
			overlay={popoverContent}
			onEnter={gaEvent}
			ref='overlay'
			container={container}
		>
			<a className={anchorClass}>{label}</a>
		</OverlayTrigger>
	);
}

/*
     PropTypes
 */
PopoverDetail.propTypes = {
	id: PropTypes.string,
	title: PropTypes.string,
	content: PropTypes.object,
	label: PropTypes.string,
	popoverClass: PropTypes.string,
	anchorClass: PropTypes.string,
	labelClass: PropTypes.string,
	placement: PropTypes.string,
	trigger: PropTypes.string,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
	gaLabel: PropTypes.string,
	container: PropTypes.object.isRequired
};

export default PopoverDetail;
