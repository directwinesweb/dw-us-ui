import React, { useEffect, useState } from 'react';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { FormatPrice } from '@dw-us-ui/format-price';
import { FormatSkuData } from '@dw-us-ui/format-sku-data';
import { GetParam } from '@dw-us-ui/get-param';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
// import { LiveChat } from '@dw-us-ui/live-chat';
import { MiniCart } from '@dw-us-ui/mini-cart';
import ProductPricingDesktop from './ProductPricingDesktop';
import ProductPricingMobile from './ProductPricingMobile';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const StyledBuyMoreCopy = styled.div`
	font-family: ${({ theme }) => theme.fonts.productHeadline};
	font-size: 16px;
	padding: 10px;
	.wsj & {
		font-family: ${({ theme }) => theme.fonts.special};
	}
`;

const ProductPricing = ({
	enableVPP,
	gaAction,
	gaCategory,
	isPrePackedItem,
	productConfig,
	productDetails,
	triggerTab,
	userPhoneNumber = '',
	isPreSellItem = false,
}) => {
	const [productPricing, setProductPricing] = useState({
		productDetails: productDetails,
		gaCategory: gaCategory,
		gaAction: gaAction,
		skus: {},
		isMixed: false,
		itemCode: '',
		unlimitedCheckbox: false,
		unlimitedInCart: false,
		inventoryInfo: {},
		numberOfBottles: 0,
		selectedIndex: 0,
		qty: 1,
		stockLevel: '',
		stockQty: '',
		productDetailsVisiblility: 'hidden',
		viewPricingVisiblility: '',
		fewerOptionVisiblility: 'hidden',
	});
	const [triggerLiveChat, setTriggerLiveChat] = useState(false);
	const currencySymbol = '$';
	let { setEventTracking } = GoogleAnalytics;
	let { GetCartDetails, AddItemToCart, BatchAddToCart } = MiniCart;
	const brandData = BrandUtil.content(BrandUtil.getBrand());
	const { tag: brandTag, url } = brandData;

	/*
		Component Did Mount()
	*/
	useEffect(() => {
		setPricingData();
	}, []);

	/*
		Get Cart Details
	 */
	const getCartDetails = () => {
		GetCartDetails(setProductPricing);
	};

	useEffect(() => {
		getCartDetails();
	}, [
		productPricing.productDetails,
		productPricing.isMixed,
		productPricing.skus,
		productPricing.numberOfBottles,
		productPricing.stockLevel,
		productPricing.stockQty,
	]);

	const setAttentiveProductView = () => {
		const { name, id, itemCode, productType, largeImage, skus, mixed } = productPricing.productDetails;
		let price = mixed ? skus?.salePrice : skus[0]?.salePrice;

		if (window && window.attentive) {
			window.attentive.analytics.productView(
				{
					items:[
						{
							productId: itemCode,
							productVariantId: id,
							name: name,
							productImage: url + largeImage,
							category: productType,
							price: {
							value: price,
							currency: 'USD',
							},
							quantity: productPricing.qty,
						}
					],
					user: {
						phone: userPhoneNumber
					}
				}
			);
		}
	}

	/*
		Set Pricing Data
	 */
	const setPricingData = () => {
		let productDetails = {
			product: productPricing.productDetails,
		};
		let productArray = [productDetails];
		/*
	    Format Sku Data
	   */
		productArray = FormatSkuData(productArray, enableVPP);
		productDetails = productArray?.product ? productArray : productArray[0];
		let isMixed = productDetails.product.mixed || isPrePackedItem;
		let numberOfBottles = productDetails.product.skus[0].numberOfBottles;
		let inventoryInfo = productDetails.product.inventoryInfo;

		//Check the param for an override
		let stockLevel = inventoryInfo.summaryAvailabilityStatus;
		let stockOverride = GetParam('dw_stock');
		let stockQty = inventoryInfo.stockQty
			? inventoryInfo.stockQty.toLocaleString()
			: false;
		//For Testing the Out Of Stock Scenario
		if (stockOverride === 'out') {
			stockLevel = 'out_of_stock';
		} else if (stockOverride === 'substitute') {
			stockLevel = 'substitute_stock';
		} else if (stockOverride === 'low') {
			stockLevel = 'low_stock';
		}

		setProductPricing((prevState) => {
			return {
				...prevState,
				productDetails,
				isMixed,
				skus: isMixed
					? productDetails.product.skus[0]
					: productDetails.product.skus,
				itemCode: productDetails.product.itemCode,
				numberOfBottles,
				stockLevel,
				stockQty,
			};
		});

		if(brandTag === 'law') {
			setAttentiveProductView();
		}
	};

	const skuRadioClick = (e, index, numberOfBottles) => {
		if (productPricing.stockLevel !== 'out_of_stock') {
			let selectedIndex;

			if (e === 'label') {
				selectedIndex = index;
			} else {
				selectedIndex = e.target.value;
			}

			let parsedIndex = parseInt(selectedIndex);

			setEventTracking({
				category: gaCategory,
				action: gaAction,
				label: numberOfBottles + '_Bottle_Select',
			});

			setProductPricing((prevState) => {
				return {
					...prevState,
					selectedIndex: parsedIndex,
				};
			});
		}
	};

	const qtyChange = (e) => {
		e.persist();
		setProductPricing((prevState) => {
			return {
				...prevState,
				qty: parseInt(e.target.value),
			};
		});
	};

	const addViewPricingOverlay = () => {
        const checkSingleBottleID = document.querySelector("#single-bottle-page-app");
            if (checkSingleBottleID) {
                checkSingleBottleID.classList.add("pdp-price-overlay");
            }
        const checkMixedBottleID = document.querySelector("#mixed-case-page-app");
            if (checkMixedBottleID) {
                checkMixedBottleID.classList.add("pdp-price-overlay");
            }
    }

	const viewPricingClick = () => {
		setProductPricing((prevState) => {
			return {
				...prevState,
				productDetailsVisiblility: '',
				viewPricingVisiblility: 'hidden',
			};
		});
		addViewPricingOverlay();
	};

	const itemNumberRender = () => {
		return (
			<div>
				<span className='item-no col-xs-6 col-xs-12'>
					Item No. {productPricing.itemCode}
				</span>
				{/*This should go to the right of the drop down menu*/}
				{/* <span className='close-product-details col-xs-6'>
					<img
						src="https://www.wsjwine.com/images/us/common/icons/wsj/minus_solid_11x14.svg"
						width="18px"
						alt="minus"
						onClick={closeProductDetails}
					/>
				</span> */}
			</div>
		);
	};

	const closeViewPricingOverlay = () => {
        const findOverlayElement = document.querySelectorAll(".pdp-price-overlay");
        findOverlayElement[0]?.classList?.remove("pdp-price-overlay");
    }

	const closeProductDetails = () => {
		setProductPricing((prevState) => {
			return {
				...prevState,
				productDetailsVisiblility: 'hidden',
				viewPricingVisiblility: '',
			};
		});
		closeViewPricingOverlay();
	};

	const setSingleBottlePricing = () => {
		let skus = productPricing.skus;
		let disableElement = productPricing.stockLevel === 'out_of_stock';

		if (!skus.length) {
			return null;
		}

		let singleSku = skus[0];
		let singleSalePrice = FormatPrice(singleSku.salePrice);
		let productData = productPricing.productDetails.product;
		let hasBCode = productData.isBCode ? true : false;
		let hasCCode = productData.isCCode ? true : false;
		let isFourPackCan = productData?.isFourPackCan;
		let isAdvent = productData?.itemCode?.substr(-2) == 50 && !productData?.itemCode?.match("/^[a-z]+$/i")
		let isVpp = enableVPP === 'true' && productData.isVpp ? true : false;
		let singleVppPrice = isVpp ? FormatPrice(singleSku.vppPrice) : false;
		let qtyArray = [];

		for (let x = 1; x <= 11; x++) {
			qtyArray.push(x);
		}

		let pricePanel = (price, priceInfo, priceClass) => {
			return (
				<div className='price-panel price-panel-header col-xs-7 col-sm-8'>
					<div className='price-row'>
						<label className={priceClass + ' price'}>
							{price}
							<span
								className='price-info'
								dangerouslySetInnerHTML={{ __html: priceInfo }}
							/>
						</label>
					</div>
				</div>
			);
		};

		const renderSingleSkuDesktop = (pricePerBottle, index, hideRadio) => {
			return (
				<div className='col-xs-12 no-pad'>
					{hideRadio ? null : createSkuRadio(index, productPricing.qty)}
					<div className='col-xs-3 no-pad'>
						<select
							className='form-control qty-select'
							onChange={qtyChange}
							disabled={disableElement}
							value={productPricing.qty}
						>
							{qtyArray.map((qty, key) => {
								return (
									<option key={key} value={qty}>
										{qty}
									</option>
								);
							})}
						</select>
					</div>
					{/* comment out according to figma design*/}
					{/* <div className='col-xs-8 no-pad-right sale-price'>
						{hideRadio ? <span>&nbsp;</span> : <strong>{pricePerBottle} each  {isFourPackCan ? 'pack': ''}</strong> }
					</div>
					*/}
					{pricePanel(singleSalePrice + ' each', `(${parseInt(productData.bottleSize)}ml)`, '')}
				</div>
			);
		};

		const renderSingleSkuMobile = (pricePerBottle, index) => {
			let bottleType = isFourPackCan ? 'pack': 'btl';

			let mobileBottleCount =
				productPricing.qty === 1
					? ` - 1 ${bottleType}.`
					: ` - ${productPricing.qty} ${bottleType}s.`;
			let mobilePricePerBottle = pricePerBottle + mobileBottleCount;

			// for previous Mobile design in Single Bottle Case
			// let qtyChangeDown = () => {
			// 	if (productPricing.qty > 1) {
			// 		setProductPricing((prevState) => {
			// 			return {
			// 				...prevState,
			// 				qty: prevState.qty - 1,
			// 			};
			// 		});
			// 	}
			// };

			// let qtyChangeUp = () => {
			// 	setProductPricing((prevState) => {
			// 		return {
			// 			...prevState,
			// 			qty: prevState.qty + 1,
			// 			fewerOptionVisiblility: '',
			// 		};
			// 	});
			// };

			return (
				<div className='col-xs-12 no-pad'>
					{/* {createSkuRadio(index, productPricing.qty)} */}

					<div className='col-xs-3 no-pad'>
						<select
							className='form-control qty-select'
							onChange={qtyChange}
							disabled={disableElement}
							value={productPricing.qty}
						>
							{qtyArray.map((qty, key) => {
								return (
									<option key={key} value={qty}>
										{qty}
									</option>
								);
							})}
						</select>
					</div>

					{/*old design for single bottle - mobile viewport - increase/decrease quanitity bottle*/}
					{/* <div className='col-xs-5 sale-price single-sku-price'>
						<strong>{mobilePricePerBottle}</strong>
					</div> */}
					{/* <div className='col-xs-3 no-pad'>
						<div
							className={'qty-chevron ' + productPricing.fewerOptionVisiblility}
							onClick={() => qtyChangeDown()}
						>
							Fewer <i className='fa fa-chevron-down' />
						</div>
					</div>
					<div className='col-xs-3 no-pad'>
						<div className='qty-chevron' onClick={() => qtyChangeUp()}>
							More <i className='fa fa-chevron-up' />
						</div>
					</div> */}
					{pricePanel(singleSalePrice + ' each', `(${parseInt(productData.bottleSize)}ml)`, '')}
					{/*part of old design */}
					{/* <span className='close-product-details col-xs-2'>
						<img
							src="https://www.wsjwine.com/images/us/common/icons/wsj/minus_solid_11x14.svg"
							width="18px"
							alt="minus"
							onClick={closeProductDetails}
						/>
					</span> */}
					<div className = 'col-xs-2 no-pad rectangle'>
						<span className='close-rectangle'
							onClick={closeProductDetails}
							alt="Close View Pricing"
						>
						</span>
					</div>
				</div>
			);
		};

		useEffect(() => {
			if (productPricing.qty === 1) {
				setProductPricing((prevState) => {
					return {
						...prevState,
						fewerOptionVisiblility: 'hidden',
					};
				});
			}
		}, [productPricing.qty]);

		const createSkuRadio = (index, numberOfBottles) => {
			return (
				<div className='col-xs-1 no-pad'>
					<input
						type='radio'
						id={index}
						checked={productPricing.selectedIndex === index}
						onChange={() => skuRadioClick('label', index, numberOfBottles)}
						value={index}
						className='sku-radio'
						disabled={disableElement}
					/>
				</div>
			);
		};

		const renderSpecialsHeader = () => {
			if (hasBCode || hasCCode) {
				return (
					<div className='specials-header-container col-xs-12'>
						<div className='divider-horizontal desktop' />
						<h3 className='specials-header'>
							Get the BEST PRICE per bottle
							<img
								className='on-sale-tag'
								src='/images/us/common/icon_sale_tag.svg'
								title='On sale'
							/>
						</h3>
					</div>
				);
			}

			return null;
		};

		const renderSkuList = (sku, index, hideRadio = false) => {
			let numberOfBottles = sku.numberOfBottles;
			let shortBtlText = sku.shortBtlText;
			let pricePerBottle = FormatPrice(sku.salePricePerBottle);
			let salePrice = FormatPrice(sku.salePrice);
			let listPrice = FormatPrice(sku?.listPrice);
			let itemCode = sku.itemCode;
			let savingsPercent = sku.savingsPercent;

			let skuContent = () => {
				return (
					<label
						className='price-row'
						htmlFor={itemCode + '_' + numberOfBottles}
						onClick={() => skuRadioClick('label', index, numberOfBottles)}
					>
						<div className='col-xs-12 no-pad'>
							{createSkuRadio(index, numberOfBottles)}
							<div className='col-xs-11 no-pad'>
								<div className='sku-radio-price col-xs-4 col-sm-4'>
									{numberOfBottles} {shortBtlText}{' '}
									{/* <strong>{pricePerBottle} each</strong> */}
									{/* <strong>{pricePerBottle}</strong> */}
								</div>

								{/* <div className='sku-sale-price col-xs-3'>
									<label className='sale-price highlight'>{salePrice}</label>
								</div> */}
								{savingsPercent ? (
									// <div className='sale-price highlight col-xs-12 savings'>
									// 	SAVE {savingsPercent}
									// </div>
									<>
										<div className='sku-sale-price col-xs-3 col-sm-4'>
											<label className='reg-strike-price'>{listPrice}</label>
										</div>
										<div className='sku-sale-price col-xs-4 col-sm-3'>
											<label className='sale-price highlight'>{salePrice}</label>
										</div>
										{/* <div className={`sku-sale-price col-xs-${brandTag === 'law' ? 2: 3}`}>
											&nbsp;
										</div> */}
									</>
								) : (
									/*A product may be VPP however - sometimes their 6 bottles does not have a volume discount - below will render according to that condition */
										listPrice == salePrice ? (
											<>
												<div className='sku-sale-price col-xs-3 col-sm-4'>
													<label className='reg-price'>{listPrice}</label>
												</div>

												<div className={`sku-sale-price col-sm-3 col-xs-${brandTag === 'law' ? 3: 4}`}>
													&nbsp;
												</div>
											</>
										)
										:	(
											<>
											<div className='sku-sale-price col-xs-3 col-sm-4'>
												<label className='reg-strike-price'>{listPrice}</label>
											</div>
											<div className='sku-sale-price col-xs-4 col-sm-4'>
												<label className='sale-price highlight'>{salePrice}</label>
											</div>
											{/* <div className={`sku-sale-price col-sm-3 col-xs-${brandTag === 'law' ? 2: 3}`}>
												&nbsp;
											</div> */}
											</>
											)
									)
								}
							</div>
						</div>
					</label>
				);
			};

			if (numberOfBottles === 1) {
				return (
					<label
						className={itemCode === 'Q3995019' ? 'hidden' :'price-row'}
						htmlFor={itemCode + '_' + numberOfBottles}
						onClick={() => skuRadioClick('label', index, numberOfBottles)}
					>
						<div className='col-xs-12 no-pad'>
							<div className='desktop'>
								{renderSingleSkuDesktop(pricePerBottle, index, hideRadio)}
							</div>
							<div className='mobile'>
								{renderSingleSkuMobile(pricePerBottle, index)}
							</div>
						</div>
					</label>
					/*
				Other Skus / 3 / 6 / 12 Btl Counts
				 */
				);
			} else {
				/*
					VPP ENABLED
					NO B CODE
					YES C CODE
				 */
				if (isVpp && !hasBCode && hasCCode) {
					if (numberOfBottles === 12) {
						return skuContent();
						/*
						All Other Sku Scenarios
				 	 */
					}
				} else {
					return skuContent();
				}
			}
		};

		return (
			<div className='single-bottle-pricing col-xs-12'>
				{/* DEFAULT REGULAR PRICE COPY */}
				<div className='price-panel-group'>
					{skus.map((sku, index) => {
						return index === 0 ? (
							<div
								key={index}
								className='price-panel price-panel-section col-xs-12'
							>
								{renderSkuList(sku, index, true)}
							</div>
						) : (
							false
						);
					})}

					{/* IF WINE HAS VPP SHOW MESSAGING, IF NOT RETURN FALSE */}
					{/* Per Lauren D. request - commented out */}
					{/* {isVpp
						? pricePanel(
								singleVppPrice,
								'As part of any 12+ bottle order', //  <br/> (price will apply in cart)
								'highlight')
						 : false
					} */}

					{/* Loop Through Skus */}

				</div>
				{
					!(isPrePackedItem || isFourPackCan || isAdvent) ?
						<div className="col-xs-12 hidden-xs" style={{padding: 0, marginBottom: 0, marginTop: 0, background: 'none', lineHeight: 0}}>&nbsp;</div>
						: null
				}
				{/* {renderSpecialsHeader()} */}
				<div className='price-panel-group'>
					{/* {
						skus?.filter(sku => sku?.savingsPercent)?.length || (hasBCode || hasCCode) ?
						<div className='col-xs-12 no-pad'><StyledBuyMoreCopy>The more you buy, the more you save</StyledBuyMoreCopy></div>
						: null
					} */}
					{skus.map((sku, index) => {
						return index !== 0 ? (
							<div
								key={index}
								className={
									productData.itemCode === '3778418'
										? 'hidden'
										: 'price-panel price-panel-section col-xs-12'
								}
							>
								{renderSkuList(sku, index)}
							</div>
						) : (
							false
						);
					})}
				</div>
			</div>
		);
	};

	const setMixedPricing = () => {
		let skus = productPricing.skus;

		//this is for the quantity drop down menu.
		let disableElement = productPricing.stockLevel === 'out_of_stock';
		let qtyArray = [];
		for (let x = 1; x <= 11; x++) {
			qtyArray.push(x);
		}


		if (Object.keys(skus).length) {
			let savings = skus.savings;
			let listPrice = skus.listPrice.toFixed(2);
			let salePrice = skus.salePrice.toFixed(2);
			let numberOfBottles = skus.numberOfBottles;
			let bottleText = skus.bottleText;
			let salePricePerBottle = skus.salePricePerBottle
				? skus.salePricePerBottle.toFixed(2)
				: false;
			let productData = productPricing.productDetails.product;

			let pricePerBottle = () => {
				/*
					Don't show bottle price for PrePack Items
				 */
				if (isPrePackedItem) {
					return <div className='price-header' />;
				}

				/*
					Price Per Bottle Text
				 */
				if (numberOfBottles === 1) {
					return (
						<div className='price-header'>
							{/* {numberOfBottles} {bottleText}  */}
							{/* for JUST {currencySymbol} {salePricePerBottle} */}
						</div>
					);
				} else if (productData.itemCode === 'A07897') {
					return (
						<div className='price-header'>14 Bottles + 4 Ros&eacute; Cans</div>
					);
				} else if (
					productData.itemCode === 'A07874' ||
					productData.itemCode === 'A07934' ||
					productData.itemCode === 'A07939'
				) {
					return (
						<div className='price-header'>12 Bottles + 12 Ros&eacute; Cans</div>
					);
				} else if (productData.itemCode === 'A07948') {
					return (
						<div className='price-header'>14 Bottles + 4 Ros&eacute; Cans</div>
					);
				} else if (productData.itemCode === 'A07885') {
					return (
						<div className='price-header'>
							12 Bottles + Magnum + 8 Ros&eacute; Cans
						</div>
					);
				} else if (productData.itemCode === 'J05425') {
					return (
						<div className='price-header'>12 cans for JUST $4.17 each</div>
					);
				} else if (
					productData.itemCode === 'A07944' ||
					productData.itemCode === 'A07933' ||
					productData.itemCode === 'A07946' ||
					productData.itemCode === 'A07938'
				) {
					return (
						<div className='price-header'>
							12 btls + Magnum + 8 Ros&eacute; Cans
						</div>
					);
				} else {
					return (
						<div className='price-header'>
							{/* {numberOfBottles} {bottleText} */}
							{/* for JUST {currencySymbol} {salePricePerBottle} each */}
						</div>
					);
				}
			};

			return (
				<div className='mixed-case-pricing col-sm-12 col-xs-12'>
					{/* <div className='arrow-left desktop' /> */}
					{/* <div className='col-xs-12 no-pad'> */}
					{/* {hideRadio ? null : createSkuRadio(index, productPricing.qty)} */}
					<div className='col-xs-3 no-pad'>
						<select
							className='form-control qty-select'
							onChange={qtyChange}
							disabled={disableElement}
							value={productPricing.qty}
						>
							{qtyArray.map((qty, key) => {
								return (
									<option key={key} value={qty}>
										{qty}
									</option>
								);
							})}
						</select>
					</div>
					{/* comment out according to figma design*/}
					{/* <div className='col-xs-8 no-pad-right sale-price'>
						{hideRadio ? <span>&nbsp;</span> : <strong>{pricePerBottle} each  {isFourPackCan ? 'pack': ''}</strong> }
					</div>
					*/}
					{/* {pricePanel(singleSalePrice + ' each', `(${parseInt(productData.bottleSize)}ml)`, '')} */}
				{/* </div> */}

					{pricePerBottle()}
					{savings ? (
						//<div className='row'>
							<div className='price-panel price-panel-header col-xs-6 col-sm-7'>
								<div className='price-row'>
								<span className='reg-strike-price'>
									{currencySymbol}
									{listPrice}
								</span>&nbsp;&nbsp;&nbsp;
									<label className='sale-price highlight'>
										{currencySymbol}
										{salePrice}
									</label>
									{/* <span className='reg-strike-price'>
										Reg.&nbsp;{currencySymbol}
										{listPrice}
									</span> */}
									{/* <label className='savings'>
										SAVE&nbsp;{currencySymbol}
										{savings}
									</label> */}
								</div>
							</div>
						//</div>
					) : (
						//<div className='row'>
							<div className='price-panel price-panel-header col-xs-6 col-sm-7'>
								<div className='price-row'>
									<label className='sale-price highlight'>
										{currencySymbol}
										{salePrice}
									</label>
								</div>
							</div>
						//</div>
					)}
					<div className = 'col-xs-3 no-pad rectangle'>
						<span className='close-rectangle'
							onClick={closeProductDetails}
							alt="Close View Pricing"
						>
						</span>
					</div>
				</div>
			);
		} else {
			return null;
		}
	};

	const unlimitedCheck = (value) => {
		setProductPricing((prevState) => {
			return {
				...prevState,
				unlimitedCheckbox: value,
			};
		});
	};

	const addToCart = (e) => {
		//Prevent Default
		e.preventDefault();
		/*
			GA Event Add To Cart Button
		 */
		setEventTracking({
			category: gaCategory,
			action: gaAction,
			label: 'Add_to_Cart_Click',
		});

		let qty = 1;
		let itemCode = productPricing.itemCode;
		if (!productPricing.isMixed) {
			let skuData = productPricing.skus[productPricing.selectedIndex];
			itemCode = skuData.itemCode;

			let isNotBCode = skuData.isNotBCode;
			let isNotCCode = skuData.isNotCCode;
			let numberOfBottles = skuData.numberOfBottles;

			/*
				Check if Item Code is
				B / C Code
		   */
			if (isNotBCode || isNotCCode) {
				qty = numberOfBottles;
			} else if (productPricing.selectedIndex === 0) {
				/*
					Check if Dropdown Radio Selected
				 */
				qty = productPricing.qty;
			}
		}

		if (productPricing.isMixed) {
			qty = productPricing.qty;
		}

		/*
			Non-Batch Add to Cart
		 */
		if (productPricing.unlimitedCheckbox === false) {
			AddItemToCart(itemCode, qty, brandTag, userPhoneNumber, url, isPreSellItem);
		} else {
			/*
				Batch Add To Cart
			 */
			let batchItems = [];
			let cartObject;

			//Main Item
			batchItems.push({ itemCode: productPricing.itemCode, quantity: qty });
			//Unlimited Item
			batchItems.push({
				itemCode: productPricing.unlimitedCheckbox,
				quantity: 1,
			});

			//Batch Items
			cartObject = {
				cartItems: batchItems,
			};

			BatchAddToCart(cartObject, setProductPricing, brandTag, userPhoneNumber, url, isPreSellItem);
		}
	};

	/*
		Return Product Pricing
	 */
	return (
		<div>
			{/* <button
				id='live-chat-trigger'
				style={{ visibility: 'hidden', display: 'none' }}
				onClick={() => setTriggerLiveChat(true)}
				aria-hidden='true'
			/>
			<LiveChat brand={brandTag} trigger={triggerLiveChat} /> */}
			<div className='desktop'>
				<ProductPricingDesktop
					getCartDetails={getCartDetails}
					addToCart={addToCart}
					currencySymbol={currencySymbol}
					triggerTab={triggerTab}
					setMixedPricing={setMixedPricing}
					setSingleBottlePricing={setSingleBottlePricing}
					unlimitedCheck={unlimitedCheck}
					productConfig={productConfig}
					data={productPricing}
					isPrePackedItem={isPrePackedItem}
					isPreSellItem={isPreSellItem}
				/>
			</div>
			<div className='mobile'>
				<ProductPricingMobile
					getCartDetails={getCartDetails}
					addToCart={addToCart}
					currencySymbol={currencySymbol}
					triggerTab={triggerTab}
					setMixedPricing={setMixedPricing}
					setSingleBottlePricing={setSingleBottlePricing}
					viewPricingClick={viewPricingClick}
					itemNumberRender={itemNumberRender}
					closeProductDetails={closeProductDetails}
					productConfig={productConfig}
					data={productPricing}
					isPrePackedItem={isPrePackedItem}
					unlimitedCheck={unlimitedCheck}
					isPreSellItem={isPreSellItem}
				/>
			</div>
		</div>
	);
};

ProductPricing.propTypes = {
	enableVPP: PropTypes.string,
	productDetails: PropTypes.object.isRequired,
	productConfig: PropTypes.object,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
	triggerTab: PropTypes.func,
	isPrePackedItem: PropTypes.bool,
};

ProductPricing.defaultProps = {
	productDetails: {},
	enableVPP: 'false',
};

export default ProductPricing;