import PropTypes from 'prop-types';
import React from 'react';

import { Button } from '@dw-us-ui/button';
import { ProductUnlimitedUpsell } from '@dw-us-ui/product-unlimited-upsell';

import ShippingRates from './ShippingRates';
import UnlimitedReminder from './UnlimitedReminder';
import ProductAddon from './ProductAddon';
import { StyledProductPricingContainer } from './ProductPricingStyled';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';

const ProductPricingDesktop = ({
	addToCart,
	container,
	currencySymbol,
	data,
	getCartDetails,
	isPrePackedItem,
	productConfig,
	productDetails,
	setMixedPricing,
	setSingleBottlePricing,
	triggerTab,
	unlimitedCheck,
	isPreSellItem,
}) => {
	let {
		gaAction,
		gaCategory,
		isMixed,
		itemCode,
		numberOfBottles,
		skus,
		stockLevel,
		stockQty,
		unlimitedInCart,
	} = data;
	let { setEventTracking } = GoogleAnalytics;

	let gaEventAddtoCart = () => {
        setEventTracking({
          category: 'Product_Detail_Page',
          action: 'Add_to_Cart',
          label: itemCode + "|" + data?.productDetails?.product?.name
        });
    };
	const btnLabel = isPreSellItem ? 'Reserve Today' : 'Add to Cart';
	isMixed = isMixed || isPrePackedItem;

	if (stockLevel === 'out_of_stock') {
		let stockMessage = () => {
			if (productConfig.stockMessage) {
				return (
					<p
						className='col-xs-12 text-center out-of-stock-message no-pad'
						dangerouslySetInnerHTML={{ __html: productConfig.stockMessage }}
					/>
				);
			}

			if (isMixed) {
				return (
					<p className='col-xs-12 text-center out-of-stock-message no-pad'>
						Sorry, this case is unavailable.
						<br />
						But you may also <a href='/wines/_/N-1h'>enjoy these cases.</a>
					</p>
				);
			} else {
				return (
					<p className='col-xs-12 text-center out-of-stock-message no-pad'>
						Sorry, this wine is no longer available.
						<br />
						But you may also{' '}
						<a href='#' onClick={(e) => triggerTab(e, 'Similar Wines')}>
							enjoy these similar wines.
						</a>
					</p>
				);
			}
		};

		return (
			<StyledProductPricingContainer>
				<div className='col-xs-12 no-pad'>
					<div
						className={
							'product-pricing col-xs-12 ' +
							(isMixed ? 'pricing-version-mixed no-pad' : 'pricing-version-single')
						}
					>
						{stockMessage()}
						<span className='item-no col-xs-12'>Item No. {itemCode}</span>
					</div>
				</div>
			</StyledProductPricingContainer>
		);
	}

	return (
		<StyledProductPricingContainer>
			<div className='col-xs-12 no-pad'>
				<div
					className={
						'product-pricing col-xs-12 ' +
						(isMixed ? 'pricing-version-mixed no-pad' : 'pricing-version-single')
					}
				>
					{!isMixed ? (
						<label className='col-xs-12 text-center no-pad savings-header highlight'>
							{/* Low Stock Messaging for Single Bottles or bottles for special one box  */}
							{stockLevel === 'low_stock'
								? 'ONLY ' + stockQty + ' Bottles Left'
								: false}
						</label>
					) : (
						false
					)}
					<div className='product-pricing-panel-container col-xs-12'>
						{isMixed ? setMixedPricing() : setSingleBottlePricing()}
						<ProductUnlimitedUpsell
							unlimitedCheck={unlimitedCheck}
							unlimitedInCart={unlimitedInCart}
							gaCategory={gaCategory}
							gaAction={gaAction}
							getCartDetails={getCartDetails}
						/>
						<div className='col-xs-12 text-center'>
							<Button
								className='btn-product-cart'
								buttonType='primary'
								onClick={(e)=> {addToCart(e);
									gaEventAddtoCart()}}
							>
								 {btnLabel}
							</Button>
						</div>
						<span className='item-no col-xs-12 text-center'>
							Item No. {itemCode}
						</span>
						{/* upon request from Lauren D. that this be comment out */}
						{/* <div className='col-xs-12 text-center' style={{padding: '10px', borderTop: '1px solid #e5e5e5'}}>
							<ShippingRates
								unlimitedInCart={unlimitedInCart}
								gaCategory={gaCategory}
								gaAction={gaAction}
								getCartDetails={getCartDetails}
							/>
						</div> */}
					</div>

					{stockLevel === 'substitute_stock' ? (
						<p className='substitute-stock col-xs-12 text-center'>
							The wines in this case may vary slightly from the originally
							advertised selection.
						</p>
					) : (
						false
					)}
					{/* Upon request from Lauren D. this is to be commented out */}
					{/* <UnlimitedReminder
						unlimitedInCart={unlimitedInCart}
						bottleCount={numberOfBottles}
					/> */}
					{null}
				</div>
				{isMixed ? (
					<ProductAddon
						unlimitedInCart={unlimitedInCart}
						productConfig={productConfig}
						currencySymbol={currencySymbol}
						skus={skus}
					/>
				) : (
					false
				)}
			</div>
		</StyledProductPricingContainer>
	);
};

/*
	Prop Types
 */
ProductPricingDesktop.propTypes = {
	unlimitedCheck: PropTypes.func.isRequired,
	getCartDetails: PropTypes.func.isRequired,
	addToCart: PropTypes.func.isRequired,
	currencySymbol: PropTypes.string,
	triggerTab: PropTypes.func,
	setMixedPricing: PropTypes.func.isRequired,
	setSingleBottlePricing: PropTypes.func.isRequired,
	data: PropTypes.object.isRequired,
	isPrePackedItem: PropTypes.bool,
	productConfig: PropTypes.object,
	container: PropTypes.object,
};

export default ProductPricingDesktop;
