import PropTypes from 'prop-types';
import React from 'react';
import { BrandUtil } from '@dw-us-ui/brand-util';
import { StyledProductAddonContainer } from './ProductPricingStyled';

const ProductAddOn = ({
	currencySymbol,
	productConfig,
	skus,
	unlimitedInCart
}) => {
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const tag = BrandUtil.content(brand).tag;
	let freeShipProfile = pageLayer[0].fsp === 'false' ? false : true;
	let offerUnlimited = pageLayer[0].ou;
	let addOnData = productConfig.addOnPricing ? productConfig.addOnPricing : false;
	let unlimitedText = tag === 'wsj' ? 'Advantage' : 'Unlimited';
	let hasUnlimited = offerUnlimited === '0' || freeShipProfile;
	
	if (!addOnData) {
		return null;
	}

	let addOnPrice = addOnData.addOnPrice;
	let bottleCount = addOnData.onBottleCount;
	let pricePerBottle = (addOnPrice / skus.numberOfBottles).toFixed(2);
	let listPrice = skus.listPrice.toFixed(2);
	let savings = (listPrice - addOnPrice).toFixed(2);

	let qualifyCopy = () => {
		//Advantage / Unlimited Copy
		if (bottleCount === 'Adv/Unlim') {
			if (unlimitedInCart) {
				//Unlim / Adv in your Cart
				return (
					<span>
						(with FREE shipping) because you've already added {unlimitedText} to your order.
						<strong> Offer ends midnight.</strong>
					</span>
				);
			} else if (!unlimitedInCart) {
				//No Unlim / Adv in your Cart
				return (
					<span>
						when you add {unlimitedText} (at 50% OFF) to today’s order.
						<strong> Offer ends midnight.</strong>
					</span>
				);
			}
		}

		return 'when added to any other ' + bottleCount + ' btls';
	};

	let addOnHeader = () => {
		if (bottleCount === 'Adv/Unlim') {
			if (hasUnlimited) {
				return 'IT’S YOUR LUCKY DAY!';
			} else if (unlimitedInCart) {
				//Unlim / Adv in your Cart
				return 'You’re getting a better deal ...';
			} else if (!unlimitedInCart) {
				//No Unlim / Adv in your Cart
				return 'For an even better deal ...';
			}
		}

		return 'FOR AN EVEN BETTER DEAL...';
	};
	
	let setAddOnContent = () => {
		return (
			<div className='text-center'>
				<div className='arrow-down' />
				{/* <h3 className='product-addon-header text-center'>{addOnHeader()}</h3> */}
				<label className='product-addon-copy'>
					{!unlimitedInCart && !hasUnlimited && bottleCount === 'Adv/Unlim' ? 'Pay ' : false}
					<strong>
						JUST {currencySymbol}
						{pricePerBottle} per btl{' '}
					</strong>
					{qualifyCopy()}
				</label>
				<div className='addon-pricing col-xs-12 no-pad'>
					Reg.
					<span className='reg-strike-price'>
						{currencySymbol}
						{listPrice}{' '}
					</span>
					<span className='highlight addon-price'>
						{currencySymbol}
						{addOnPrice}
					</span>
					<span className='savings'>
						{' '}
						SAVE {currencySymbol}
						{savings}
					</span>
				</div>
			</div>
		);
	};

	if (bottleCount === 'Adv/Unlim' && addOnPrice === 0 && hasUnlimited) {
		return (
			<StyledProductAddonContainer>
				<div className='col-sm-12 product-addon clearfix'>
					<div className='text-center'>
						<div className='arrow-down' />
						<h3 className='product-addon-header text-center'>{addOnHeader()}</h3>
						<label className='product-addon-copy'>
							This offer is available EXCLUSIVELY to {unlimitedText} members like you, and it ships FREE.
						</label>
					</div>
					<label className='addon-info text-center col-sm-12 no-pad'>Offer ends midnight.</label>
				</div>
			</StyledProductAddonContainer>
		);
	} else if (addOnPrice !== 0) {
		if (bottleCount === 'Adv/Unlim' && hasUnlimited) {
			return null;
		}
		//Non - Adv / Unlim
		return (
			<div className='product-addon-container col-sm-12 product-addon clearfix'>
				{setAddOnContent()}
				<label className='addon-info text-center col-sm-12 no-pad'>Price will update in cart</label>
			</div>
		);
	}
};

ProductAddOn.propTypes = {
	productConfig: PropTypes.object.isRequired,
	skus: PropTypes.object.isRequired,
	currencySymbol: PropTypes.string.isRequired,
	unlimitedInCart: PropTypes.bool.isRequired
};

export default ProductAddOn;
