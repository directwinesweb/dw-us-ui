import PropTypes from 'prop-types';
import React from 'react';
import { BrandUtil } from '@dw-us-ui/brand-util';
import { StyledUnlimitedReminderContainer } from './ProductPricingStyled';

const UnlimitedReminder = (props) => {
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const unlimitedCopy = BrandUtil.content(brand).unlimited;
	let bottleCount = props.bottleCount;
	let offerUnlimited = pageLayer[0].ou;
	let freeShipProfile = pageLayer[0].fsp === 'false' ? false : true;
	let unlimitedInCart = props.unlimitedInCart;

	let setIcon = () => {
		return <img
			className='hightlight col-xs-2 no-pad'
			src='/images/us/common/icons/law/truck_solid_23x22.svg'
			alt='truck'
			height='25px'
		/>;
	};

	let getContent = () => {
		if (bottleCount >= 12) {
			return (
				<StyledUnlimitedReminderContainer className='col-xs-12'>
					{setIcon()}
					<p className='col-xs-10 no-pad unlimited-reminder-copy'>
						Don't forget, this case <span className='highlight'>ships FREE</span> with your {unlimitedCopy} membership.
					</p>
				</StyledUnlimitedReminderContainer>
			);
		}

		return (
			<StyledUnlimitedReminderContainer className='col-xs-12'>
				{setIcon()}
				<p className='col-xs-10 no-pad unlimited-reminder-copy'>
					<strong>Don't forget:</strong> All orders <span className='highlight'>SHIP FREE</span> with your{' '}
					{unlimitedCopy} membership.
				</p>
			</StyledUnlimitedReminderContainer>
		);
	};

	if (offerUnlimited === '0' || freeShipProfile || unlimitedInCart) {
		return getContent();
	} else {
		return null;
	}
};

UnlimitedReminder.propTypes = {
	unlimitedInCart: PropTypes.bool.isRequired,
	bottleCount: PropTypes.number
};

export default UnlimitedReminder;
