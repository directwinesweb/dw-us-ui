import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import styled from '@emotion/styled';

export const StyledProductPricingContainer = styled.div`
	.close-rectangle {
		display: none;
		height: 2px;
		width: 18px;
		background-color: #000000;
		margin-top: .5em;
		margin-left: 1.5em;
		border-radius: 20px;
		@media (max-width: 360px) {
			margin-left: .5em;
		}
		${EmotionBreakpoint('mobile')} {
			display: flex;
		}
	}
	.rectangle {
		display: flex;
	}
	button.btn-product-cart:disabled {
		color: #fff;
		background-color: #9d9d9d;
		border-color: transparent;
		box-shadow: none;
		cursor: not-allowed;
	}
	.btn-view-pricing,
	.btn-product-cart {
		${EmotionBreakpoint('mobile')} {
			.law & {
				background-color: ${({ theme }) => theme.colors.highlight};
				border-width: 0;
				border-color: unset;
			}
			width: 100%;
		}
		font-size: 1.6em;
		.law & {
			font-size: 0.8rem;
		}
		.wsj & {
			font-size: 1.5em;
		}
	}
	${EmotionBreakpoint('mobile')} {
		.out-of-stock-message {
			padding: 20px;
			background: ${({ theme }) => theme.colors.outOfStockMessageBackground};
		}
		position: fixed;
		left: 0;
		bottom: 0;
		right: 0;
		z-index: 10;
	}
	.reg-strike-price {
		text-decoration: line-through;
	}

	.product-pricing {
		// background: #f2f2f2;
		.law & {
			padding: 0.6rem 1.6rem;
		}
		label {
			margin: 0;
		}
		.savings-header {
			font-size: 1.4em;
			font-weight: bold;
			cursor: text;
			.wsj & {
				font-size: 1.3em;
			}
			.vir & {
				font-weight: 500;
				font-size: 1.4em;
			}
			.savings-amount {
				.gil & {
					font-family: georgia, sans-serif;
					font-style: italic;
				}
			}
		}
		.substitute-stock {
			font-size: 1em;
			margin: 10px 0;
		}
		.out-of-stock-message {
			font-weight: bold;
			margin: 0;
			font-size: 1.2em;
			color: #000;
			.vir & {
				font-weight: 500;
			}
			.wsj & {
				font-size: 1.1em;
			}
			.law & {
				color: ${({ theme }) => theme.colors.body};
			}
		}
		.product-out-of-stock.product-out-of-stock {
			color: #999999;
			.highlight {
				color: #999999;
			}
			&.product-pricing-panel-container {
				.single-bottle-pricing {
					margin-bottom: 15px;
					.price-panel {
						.price-row {
							cursor: default;
							.price-info.price-info {
								color: #999999;
							}
							.sale-price.sale-price {
								cursor: default;
							}
						}
					}
				}
			}
		}
		.product-pricing-panel-container {
			padding: 0;
			color: #000;
			.law & {
				color: ${({ theme }) => theme.colors.body};
			}
			.popover-container {
				a {
					.law & {
						font-size: 14px;
					}
				}
			}
			.shipping-rates-label {
				text-align: center;
				font-weight: bold;
				text-decoration: none;
				font-size: 14px;
				display: inline-block;
				.vir & {
					font-weight: 500;
				}
				.wsj & {
					color: ${({ theme }) => theme.colors.link};
				}
				.law & {
					color: ${({ theme }) => theme.colors.link};
				}
				.npr & {
					font-size: 13px;
				}
				${EmotionBreakpoint('mobile')} {
					display: block;
					text-align: center;
					clear: both;
				}
			}
			.product-unlimited-upsell {
				.unlimited-upsell-copy {
					font-size: 1.1em;
					text-align: left;
					padding: 0 10px;
					.wsj & {
						font-size: 1em;
					}
					.unlimited-upsell-checkbox {
						margin: 6px 0 0;
					}
				}
			}
			.price-panel {
				.gil & {
					font-family: georgia, sans-serif;
					font-style: italic;
				}
				strong {
					.gil & {
						font-family: georgia, sans-serif;
						font-style: italic;
					}
				}
				label {
					display: block;
				}
			}
			.specials-header-container {
				padding: 10px;
				cursor: default;
				.specials-header {
					font-size: 1.1rem;
					${EmotionBreakpoint('mobile')} {
						font-size: 1rem;
					}
				}
				${EmotionBreakpoint('mobile')} {
					text-align: center;
					padding: 15px 0 5px;
				}
			}
			${EmotionBreakpoint('mobile')} {
				.price-panel-group {
					background: #fff;
					padding-top: 10px;
					padding-bottom: 0px;
					//border: 1px solid #d7d7d7;
					border: none;
					width: 100%;
					float: left;
				}
				.price-panel-header {
					text-align: center;
					//padding-bottom: 5px;

				}
				.price {
				}
			}
		}
		.item-no {
			color: #5a5a5a;
			padding: 5px 10px;
			font-size: 0.9em;
			text-align: center;
			justify-content: center;
			${EmotionBreakpoint('mobile')} {
				text-align: center;
				padding-bottom: 5px;
				padding-top: 0px;
			}
		}
		.close-product-details {
			font-size: 1.4rem;
			text-align: right;
			color: #848484;
			margin-top: 5px;
			.fa-minus {
				cursor: pointer;
			}
		} // Version One SASS
		&.pricing-version-single {
			padding: 30px;
			background: #fff !important;
			.popover-container {
				a {
					font-size: 1.1em;
					padding: 10px;
				}
			}
			.savings-header {
				margin: 15px 0;
				cursor: text;
			}
			.product-pricing-panel-container {
				background: #fff;
				padding-top: 20px;
				border: 1px solid #d7d7d7;
				.price-panel {
					padding: 0 10px;
					.price-row {
						font-size: 0.9rem;
						padding-bottom: 5px;
						.law & {
							font-size: 14px;
						}
						.wsj & {
							font-size: 0.8rem;
						}
						.reg-strike-price {
							padding-right: 10px;
							font-size: 14px;
							.npr & {
								font-weight: 400;
							}
							.wsj & {
								font-weight: 700;
							}
						}
						.reg-price {
							padding-right: 10px;
							font-size: 14px;
							.law & {
								font-weight: 600;
							}
							.npr & {
								font-weight: 400;
							}
							.wsj & {
								font-weight: 700;
							}
						}

						.sale-price {
							font-size: 1.4em;
							display: inline-block;
						}
						.sku-radio-price {
							font-size: 14px;
							padding: 0;
							.law & {
								font-weight: 600;
							}
							.npr & {
								font-weight: 400;
							}
							.wsj & {
								font-weight: 700;
							}
						}
						.sku-sale-price {
							padding: 0;
							${EmotionBreakpoint('mobile')} {
								text-align: right;
								padding-right: 15px;
							}
						}
						.qty-chevron {
							display: inline-block;
						}
						${EmotionBreakpoint('mobile')} {
							padding-bottom: 0;
						}
					}
				}
				// .popover-container {
				// 	a {
				// 		.law & {
				// 			@media (min-width: 992px) {
				// 				margin-left: 30px;
				// 			}
				// 		}
				// 	}
				// }
				.single-bottle-pricing {
					padding: 0;
					.specials-header-container {
						.on-sale-tag {
							width: 25px;
							margin-left: 5px;
						}
						.divider-horizontal {
							margin: 0px -10px 20px;
							border-top: 1px solid #e1e1e1;
						}
						.gil & {
							font-style: normal;
							font-family: 'ProximaNova-Regular', georgia, sans-serif;
						}
					}
					.price-panel-header.price-panel-header {
						padding-top: 0px;
						padding-bottom: 5px;
						.price {
							cursor: default;
						}
					}
					.price-panel {
						padding-left: 10px;
						.price-row {
							.sku-radio {
								margin: 6px 0 0;
								${EmotionBreakpoint('mobile')} {
									-webkit-appearance: button;
									-moz-appearance: button;
									appearance: button;
									border: 1px solid #ccc;
									border-top-color: #bbb;
									border-left-color: #bbb;
									background: #fff;
									width: 12px;
									height: 12px;
									border-radius: 50% !important;
									&:focus {
										outline: none;
									}
									&:checked {
										border: 7px solid #4099ff;
									}
								}
							}
							.qty-select {
								height: 22px;
								margin: 0;
								padding: 0;
							}
							.price {
								font-size: 20px;
								font-weight: 700;
								.vir & {
									font-weight: 500;
								}
								.npr & {
									font-weight: bold;
								}
								${EmotionBreakpoint('mobile')} {
									font-size: 16px;
								}
								${EmotionBreakpoint('tablet')} {
									margin-left: 1rem;
									font-size: 18px;
								}
							}
							.price-info {
								font-size: 12px;
								font-weight: normal;
								color: #000;
								margin-left: 5px;
								.law & {
									color: ${({ theme }) => theme.colors.body};
								}
								.npr & {
									font-weight: 325;
								}
								.wsj & {
									font-weight: 300;
								}
							}
							.sale-price {
								font-size: 14px;
								display: inline-block;
								.wsj & {
									font-size: 14px;
								}
								.law & {
									font-weight: 600;
									color: #CF004F;
								}
								&.single-sku-price {
									${EmotionBreakpoint('mobile')} {
										padding-left: 5px;
										padding-right: 10px;
										font-size: 0.9rem;
										.npr & {
											font-size: 0.8rem;
										}
										.wsj & {
											font-size: 0.75rem;
										}
									}
								}
							}
							.savings {
								margin-top: 5px;
								padding-left: 0;
								${EmotionBreakpoint('mobile')} {
									padding-left: 15px;
								}
							}
						}
					}
					${EmotionBreakpoint('mobile')} {
						padding: 5px 15px;
					}
				}
				.product-unlimited-upsell {
					padding: 15px 10px;
					margin-top: 10px;
					margin-bottom: 10px;
					border-bottom: .5px solid #969696;
					border-top: .5px solid #969696;
					background: #F6F6F6;
					${EmotionBreakpoint('mobile')} {
						padding: 15px 15px 5px;

					}
				}
				.product-unlimited-error {
					color: ${({ theme }) => theme.button.primary.backgroundColor};
					line-height: 20px;
				}
				${EmotionBreakpoint('mobile')} {
					background: initial;
					border: none;
					padding-top: 0;
				}
			}
			${EmotionBreakpoint('mobile')} {
				padding: 0;
			}
			${EmotionBreakpoint('notMobile')} {
				.btn-product-cart {
					height: 45px;
					margin-top: 10px;
					padding: 0 30px;
					width: auto !important;
					.wsj & {
						padding: 0 20px;
					}
				}

			}
		}
		&.pricing-version-mixed {
			border: 1px solid #d7d7d7;
			${EmotionBreakpoint('mobile')} {
				padding: 0px;
				background: #fff;
			}
			.shipping-rates-label {
				//padding: 10px 0;
				${EmotionBreakpoint('mobile')} {
					padding: 0 0 10px;
				}
			}
			.product-pricing-panel-container {
				.mixed-case-pricing {
					padding-left: 10px;
					margin-bottom: 10px;
					.form-control {
						//margin-top: 1em;
					}
					.qty-select {
						height: 22px;
						margin-top: 1.5em;
						padding: 0;
						${EmotionBreakpoint('mobile')} {
							margin-top: 1.7em;
						}
					}
					.law & {
						color: ${({ theme }) => theme.colors.body};
					}
					.arrow-left {
						border-top: 25px solid transparent;
						border-bottom: 25px solid transparent;
						border-right: 25px solid #f2f2f2;
						position: absolute;
						top: 60px;
						left: -40px;
						${EmotionBreakpoint('mobile')} {
							padding: 0;
						}
					}
					.price-header {
						font-size: 1.2em;
						padding: 10px;
						margin: 0;
						font-weight: bold;
						// border-bottom: 1px solid #cfcfcf;
						.vir & {
							font-weight: 500;
						}
						.wsj & {
							font-family: ${({ theme }) => theme.fonts.primary};
							font-weight: 700;
							font-size: 24px;
						}
						.law & {
							color: ${({ theme }) => theme.colors.body};
						}
						${EmotionBreakpoint('mobile')} {
							border-bottom: none;
							padding: 10px 0 15px;
						}

					}
					.price-row {
						// border-bottom: 1px solid #cfcfcf;
						display: flex;
						.npr & {
							padding: 0 0 0 10px;
						}
						.wsj & {
							padding: 0 0 0 10px;
						}
						.sale-price {
							font-weight: 600;
							font-size: 1.3em;
							display: inline-block;
							cursor: default;
						}
						.reg-strike-price {
							display: inline-block;
							font-weight: 400;
							color: #202020;
							font-size: 1.3em;
							.wsj & {
								font-size: 1.3em;
							}
							${EmotionBreakpoint('mobile')} {
								display: inline-block;
								padding: 0;
							}
						}
						.savings {
							font-size: 1.3em;
							font-weight: bold;
							cursor: default;
							.vir & {
								font-weight: 500;
							}
							.law & {
								color: ${({ theme }) => theme.colors.body};
							}
							${EmotionBreakpoint('mobile')} {
								display: inline-block;
								margin-left: 10px;
								font-size: 1.5em;
							}
						}
						${EmotionBreakpoint('mobile')} {
							border-bottom: none;
							padding: 0 0 10px;
							display: inline-block;
						}
					}
					${EmotionBreakpoint('mobile')} {
						padding: 0 15px;
						text-align: center;
						// border: 1px solid #d7d7d7;
						background: #fff;
						margin: 5px 15px 10px;
					}
				}
				.product-unlimited-upsell {
					padding: 15px 10px;
					margin-top: 10px;
    				margin-bottom: 10px;
    				border-bottom: 0.5px solid #969696;
    				border-top: 0.5px solid #969696;
    				background: #F6F6F6;
					${EmotionBreakpoint('mobile')} {
						padding: 15px 15px 5px;
					}
				}
				.product-unlimited-error {
					color: ${({ theme }) => theme.button.primary.backgroundColor};
					line-height: 20px;
				}
				.unlimited-copy {
					// line-height: 5px;
				}
			}
			.out-of-stock-message {
				margin-bottom: 15px;
			}
			${EmotionBreakpoint('tablet')} {
				.item-no {
					padding-top: 10px;
					border-bottom: 1px solid #e5e5e5;
				}
				.btn-product-cart {
					padding: 4px 12px;
					margin-top: 15px;
					width: 68% !important;
					.wsj & {
						width: 84% !important;
					}
					.npr & {
						width: 53% !important;
					}
				}
			}
			${EmotionBreakpoint('desktop')} {
				.item-no {
					padding-top: 10px;
					border-bottom: 1px solid #e5e5e5;
				}
				.btn-product-cart {
					padding: 4px 12px;
					margin-top: 15px;
					width: 40% !important;
					.wsj & {
						width: 57% !important;
					}
					.npr & {
						width: 50% !important;
					}
				}
			}
		}
	}

	.shipping-rates-popover.popover {
		margin-top: 0;
		z-index: 10;
		.law & {
			max-width: 35rem;
		}
		.popover-content {
			.close-btn {
				background: none;
				padding: 0;
				color: #000;
				position: absolute;
				top: 0;
				right: 5px;
				font-size: 1.5em;
				.wsj & {
					font-size: 1.4em;
				}
				.law & {
					color: #000037;
				}
			}
			p {
				font-size: 1em;
				margin: 14px 0;
			}
		}
	}
`;

export const StyledUnlimitedReminderContainer = styled.div`
	float: left;
	padding: 15px 0;
	.fa-truck.fa-truck {
		font-size: 1.6em;
		padding: 0 5px;
		font-weight: normal;
		.wsj & {
			font-size: 1.4em;
		}
	}
	.unlimited-reminder-copy {
		margin: 0;
		font-size: 1.1em;
		.wsj &,
		.npr & {
			font-size: 1em;
		}
	}
	${EmotionBreakpoint('mobile')} {
		padding: 15px;
	}
`;

export const StyledProductAddonContainer = styled.div`
	margin: 30px 0 0;
	padding: 15px 10px 10px;
	border: 1px solid #d7d7d7;
	background: #fff;
	color: #000;
	.law & {
		color: ${({ theme }) => theme.colors.body};
	}

	label {
		cursor: default;
	}

	.product-addon-header {
		padding: 5px 0;
		${EmotionBreakpoint('mobile')} {
			margin-bottom: 5px;
		}
	}
	.product-addon-copy {
		font-weight: normal;
		font-style: italic;
		font-size: 0.9rem;
		line-height: 16px;
		${EmotionBreakpoint('mobile')} {
			margin-bottom: 5px;
			font-size: 0.75rem;
		}
	}
	.addon-info {
		margin-top: 5px;
		font-size: 0.9rem;
		font-style: italic;
		${EmotionBreakpoint('mobile')} {
			margin-bottom: 5px;
			font-size: 0.75rem;
		}
	}
	.addon-pricing {
		font-size: 1.1em;
		.savings {
			font-weight: bold;
			.vir & {
				font-weight: 500;
			}
		}
		${EmotionBreakpoint('mobile')} {
			margin-bottom: 5px;
			font-size: 1rem;
			.wsj & {
				font-size: 0.9rem;
			}
		}
	}
	.arrow-down {
		border-left: 25px solid transparent;
		border-right: 25px solid transparent;
		border-top: 25px solid ${({ theme }) => theme.colors.highlight};
		border-top-width: 15px !important;
		position: absolute;
		top: 0;
		left: 40%;
		${EmotionBreakpoint('mobile')} {
			top: 0;
			left: 0;
			right: 0;
			width: 0;
			margin: auto;
		}
	}
	.product-addon {
		padding-bottom: 5px;
		.product-addon-header {
			margin: 5px 0 5px;
		}
		.addon-pricing {
			text-align: left;
			.addon-price {
				padding: 0 10px;
			}
		}
		.addon-info {
			color: #333333;
			font-style: normal;
		}
	}
	${EmotionBreakpoint('mobile')} {
		font-size: 0.8rem;
		margin: 20px 15px 10px;
		padding: 15px 10px 0px;
		position: relative;
		.wsj & {
			font-size: 0.75rem;
		}
	}
`;