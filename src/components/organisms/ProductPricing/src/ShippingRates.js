import { BrandUtil } from '@dw-us-ui/brand-util';
import { UnlimitedModal } from '@dw-us-ui/modals';
import { usePageLayer } from '@dw-us-ui/pagelayer-provider';
import { Tooltip } from '@dw-us-ui/tooltip';
import PropTypes from 'prop-types';
import React, { useState } from 'react';

const ShippingRates = (props) => {
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	// const pageLayer = usePageLayer();
	const {
		caseImageUrl,
		email,
		nameShort,
		phone,
		tag,
		unlimited,
		unlimitedBom,
		unlimitedFull,
		url,
	} = data;
	// let offerUnlimited = pageLayer?.ou;
	// let freeShipProfile = pageLayer?.fsp === 'false' ? false : true;
	let offerUnlimited = pageLayer[0].ou;
	let freeShipProfile = pageLayer[0].fsp === 'false' ? false : true;
	let unlimitedInCart = props.unlimitedInCart;
	let unlimitedImageUrl = `${url}${caseImageUrl}${unlimitedBom}`;
	let [showModal, setShowModal] = useState(false);
	let price = 89;

	if (offerUnlimited == 2) {
		price = 69;
	} else if (offerUnlimited == 3) {
		price = 44;
	}

	const unlimitedData = {
		brand: tag,
		brandPhone: phone,
		email: email,
		itemCode: unlimitedBom,
		unlimitedPrice: price,
		unlimitedType: unlimited,
		shortText: nameShort,
		fullText: unlimitedFull,
		images: {
			small: `${unlimitedImageUrl}_S.jpg`,
			large: `${unlimitedImageUrl}_L.jpg`,
			thumb: `${unlimitedImageUrl}_T.jpg`,
		},
	};

	if (offerUnlimited === '0' || freeShipProfile || unlimitedInCart) {
		return null;
	}

	let setContent = () => {
		return (
			<div>
				<p>
				Unless otherwise stated, shipping is $19.99 for each case (up to 15 bottles).
					{tag === 'law' || tag === 'wsj'
						? ` Please note: for Alaska and Hawaii residents, a $75 shipping surcharge per case applies.`
						: null}
					{' '}For Colorado, a $0.27 retail delivery fee per case applies.
				</p>

				{offerUnlimited === '4' ? (
					false
				) : (
					<p>
						For {unlimitedFull} members, all orders ship
						free.{' '}
						<strong>
							<a
								onClick={() => setShowModal(true)}
								onMouseDown={() => setShowModal(false)}
							>
								Click here
							</a>
							<UnlimitedModal
								triggerModal={showModal}
								unlimitedData={unlimitedData}
							/>
						</strong>{' '}
						to find out more.
					</p>
				)}
			</div>
		);
	};

	return (
		<Tooltip tooltipContent={setContent()} tooltipPosition='top'>
			<strong>Shipping Rates</strong>
		</Tooltip>
	);
};

ShippingRates.propTypes = {
	unlimitedInCart: PropTypes.bool.isRequired,
	gaCategory: PropTypes.string,
	gaAction: PropTypes.string,
	getCartDetails: PropTypes.func.isRequired,
};

export default ShippingRates;
