import PropTypes from 'prop-types';
import React from 'react';
import { ProductUnlimitedUpsell } from '@dw-us-ui/product-unlimited-upsell';

import { Button } from '@dw-us-ui/button';
import ShippingRates from './ShippingRates';
import ProductAddon from './ProductAddon';
import { StyledProductPricingContainer } from './ProductPricingStyled';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';

const ProductPricingMobile = ({
	addToCart,
	closeProductDetails,
	container,
	currencySymbol,
	data,
	getCartDetails,
	isPrePackedItem,
	itemNumberRender,
	productDetails,
	productConfig,
	setMixedPricing,
	setSingleBottlePricing,
	triggerTab,
	viewPricingClick,
	unlimitedCheck,
	isPreSellItem,
}) => {
	let {
		gaAction,
		gaCategory,
		isMixed,
		itemCode,
		productDetailsVisiblility,
		skus,
		stockLevel,
		stockQty,
		unlimitedInCart,
		viewPricingVisiblility,
	} = data;
	const btnLabel = isPreSellItem ? 'Reserve Today' : 'Add to Cart';
	isMixed = isMixed || isPrePackedItem;

	let { setEventTracking } = GoogleAnalytics;

	let gaEventAddtoCart = () => {
        setEventTracking({
          category: 'Product_Detail_Page',
          action: 'Add_to_Cart',
          label: itemCode + "|" + data?.productDetails?.product?.name
        });
    };

	if (stockLevel === 'out_of_stock') {
		let stockMessage = () => {
			if (productConfig.stockMessage) {
				return (
					<p
						className='col-xs-12 text-center out-of-stock-message'
						dangerouslySetInnerHTML={{ __html: productConfig.stockMessage }}
					/>
				);
			}

			if (isMixed) {
				return (
					<p className='col-xs-12 text-center out-of-stock-message'>
						Sorry, this case is unavailable.
						<br />
						But you may also <a href='/wines/_/N-1h'>enjoy these cases.</a>
					</p>
				);
			} else {
				return (
					<div className='col-xs-12 out-of-stock-message'>
						<p className='col-xs-12 text-center'>
							Sorry, this wine is no longer available.
							<br />
							But you may also{' '}
							<a
								href='#'
								onClick={(e) => {
									closeProductDetails();
									triggerTab(e, 'Similar Wines');
								}}
							>
								enjoy these similar wines.
							</a>
						</p>
					</div>
				);
			}
		};

		return (
			<StyledProductPricingContainer>
				<div className='col-xs-12 no-pad product-pricing out-of-stock'>
					<div
						className={
							'product-pricing col-xs-12 ' +
							(isMixed ? 'pricing-version-mixed ' : 'pricing-version-single ') +
							productDetailsVisiblility
						}
					>
						{itemNumberRender()}
						{stockMessage()}
						<div className='product-pricing-panel-container col-xs-12 product-out-of-stock'>
							{isMixed ? setMixedPricing() : setSingleBottlePricing()}
						</div>
					</div>
					{stockMessage()}
					<Button
						className={'btn-view-pricing ' + viewPricingVisiblility}
						buttonType='primary'
					>
						Out of Stock
					</Button>
				</div>
			</StyledProductPricingContainer>
		);
	}

	return (
		<StyledProductPricingContainer>
			<div className='col-xs-12 no-pad'>
				<div
					className={
						'product-pricing col-xs-12 ' +
						(isMixed ? 'pricing-version-mixed ' : 'pricing-version-single ') +
						productDetailsVisiblility
					}
				>

					{/* Low Stock Messaging for Single Bottles or bottles for special one box */}
					{!isMixed && stockLevel === 'low_stock' ? (
						<label className='col-xs-12 text-center no-pad savings-header highlight'>
							{'ONLY ' + stockQty + ' Bottles Left'}
						</label>
					) : (
						false
					)}
					<div className='product-pricing-panel-container col-xs-12'>
						{isMixed ? setMixedPricing() : setSingleBottlePricing()}
						{isMixed ? (
							<ProductAddon
								productConfig={productConfig}
								currencySymbol={currencySymbol}
								skus={skus}
								unlimitedInCart={unlimitedInCart}
							/>
						) : (
							false
						)}
						{/*as per Lauren D.'s request, shipping rates section removed*/}
						{/* <div className='col-xs-12 text-center'>
							<ShippingRates
								unlimitedInCart={unlimitedInCart}
								gaCategory={gaCategory}
								gaAction={gaAction}
								getCartDetails={getCartDetails}
							/>
						</div> */}
						<div className='col-xs-12 text-center no-pad'>
							<ProductUnlimitedUpsell
									unlimitedCheck={unlimitedCheck}
									unlimitedInCart={unlimitedInCart}
									gaCategory={gaCategory}
									gaAction={gaAction}
									getCartDetails={getCartDetails}
								/>
						</div>
						{itemNumberRender()}
						<Button
							className='btn-product-cart'
							buttonType='primary'
							onClick={(e)=> {addToCart(e);
								gaEventAddtoCart()}}
						>
							 {btnLabel}
						</Button>
					</div>

					{stockLevel === 'substitute_stock' ? (
						<p className='substitute-stock col-xs-12 text-center'>
							The wines in this case may vary slightly from the originally
							advertised selection.
						</p>
					) : (
						false
					)}
				</div>

				<Button
					className={'btn-view-pricing ' + viewPricingVisiblility}
					buttonType='primary'
					onClick={viewPricingClick}
				>
					View Pricing
				</Button>
			</div>
		</StyledProductPricingContainer>
	);
};

/*
	Prop Types
 */
ProductPricingMobile.propTypes = {
	unlimitedCheck: PropTypes.func.isRequired,
	getCartDetails: PropTypes.func.isRequired,
	addToCart: PropTypes.func.isRequired,
	currencySymbol: PropTypes.string,
	triggerTab: PropTypes.func,
	setMixedPricing: PropTypes.func.isRequired,
	setSingleBottlePricing: PropTypes.func.isRequired,
	data: PropTypes.object.isRequired,
};

export default ProductPricingMobile;
