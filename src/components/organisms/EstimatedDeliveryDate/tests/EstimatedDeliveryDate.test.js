import EstimatedDeliveryDate from '../src/EstimatedDeliveryDate';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('EstimatedDeliveryDate test', () => {
	it('Creates snapshot test for <EstimatedDeliveryDate />', () => {
		const wrapper = shallow(<EstimatedDeliveryDate></EstimatedDeliveryDate>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
