import { useEffect, useState } from 'react';

import axios from 'axios';
import { usePrevious } from '@dw-us-ui/use-previous';
import { FormatDate } from '@dw-us-ui/format-date';

/**
 * @description This hook posts to the estimated delivery date api. Documentation can be found at https://www.laithwaiteswine.com/api/reference/swagger-ui/index.html?urls.primaryName=Checkout%20-%20Shipping%20API#/Shipping%20API%20Suite/getEstimatedDeliveryDate
 * @param {Object} param0 params expected by api endpoint and a trigger post request bool param
 */
const useEstimatedDeliveryDate = ({
	triggerApi = false,
	countryCode = 'US',
	deliveryStateCode = null,
	propertyType = 'residential',
	zipCode,
	isOnReviewPage = false,
	orderId
}) => {
	const previousZip = usePrevious(zipCode);
	const previousState = usePrevious(deliveryStateCode);
	const previousPropertyType = usePrevious(propertyType);

	let apiObject = {
		baseUrl: '/api/checkout/shipping/edd',
		params: `?country=${countryCode}&state=${deliveryStateCode}&zipCode=${zipCode}&deliveryType=${propertyType}`,
	};
	if(orderId) {
		apiObject = {
			baseUrl: '/api/user/order/deliverydates',
			params: `?orderId=${orderId}`,
		};
	}

	const [apiResponse, setApiResponse] = useState(null);
	const [isLoading, setIsLoading] = useState(false);
	const [shippingTo, setShippingTo] = useState(null);
	const [shippingFrom, setShippingFrom] = useState(null);

	useEffect(() => {
		const fetchDate = () => {
			if ((zipCode && deliveryStateCode) || orderId) {
				axios
					.get(`${apiObject.baseUrl}${apiObject.params}`)
					.then((res) => {
						const value = res.data.response;
						setApiResponse(value);
						setIsLoading(false);
						let shippingFromApi =
							value.receiptDateFrom || value.shippingDateFrom;
						let shippingToApi = value.receiptDateTo || value.shippingDateTo;
						if(orderId) {
							let deliveryDateLength = value?.deliveryDateDetails?.length - 1;
							let eddObject = value?.deliveryDateDetails
								? value.deliveryDateDetails[deliveryDateLength]
								: false;
							if (eddObject) {
								let { minDate, maxDate } = value.deliveryDateDetails[deliveryDateLength];
								minDate = minDate?.split('T')[0];
								maxDate = maxDate?.split('T')[0];

								shippingFromApi = FormatDate(minDate, 'yyyy-mm-dd');
								shippingToApi = FormatDate(maxDate, 'yyyy-mm-dd');
							}
						}
						setShippingFrom(shippingFromApi);
						setShippingTo(shippingToApi);
						sessionStorage.setItem(
							'orderEdd',
							JSON.stringify({
								shippingTo: shippingToApi,
								shippingFrom: shippingFromApi,
							})
						);
					})
					.catch((err) => {
						sessionStorage.setItem('orderEdd', false);
						setApiResponse(err.response);
						setIsLoading(false);
					});
			}
		};
		if (isOnReviewPage && !apiResponse?.status) {
			// check storage
			let eddSessionStorage = sessionStorage.getItem('orderEdd');
			if (eddSessionStorage !== 'false') {
				let eddObj;
				eddObj = JSON.parse(eddSessionStorage);
				setShippingFrom(eddObj?.shippingFrom);
				setShippingTo(eddObj?.shippingTo);
				setApiResponse({ status: 200, fetched: true });
			} else {
				setApiResponse({ status: 422 });
			}
		} else {
			if (
				triggerApi === true &&
				(previousPropertyType !== propertyType ||
					previousZip !== zipCode ||
					previousState !== deliveryStateCode || orderId)
			) {
				setIsLoading(true);
				fetchDate();
			}
		}
	}, [deliveryStateCode, propertyType, triggerApi, zipCode, apiObject, orderId]);

	return { isLoading, apiResponse, shippingFrom, shippingTo };
};

export default useEstimatedDeliveryDate;
