import React, { useEffect, useState } from 'react';

import { FormatDate } from '@dw-us-ui/format-date';
import { LoadingIcon } from '@dw-us-ui/loading-icon';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import useEstimatedDeliveryDate from './useEstimatedDeliveryDate';

const StyledContainer = styled.div`
	font-family: ${({ theme }) => theme.fonts.primary};
	padding: 16px 0 0 0;

	.edd-header {
		font-family: ${({ theme }) => theme.fonts.estimatedDelivery};
		text-transform: uppercase;
		margin: 0 0 5px;
		color: ${(props) =>
			props.isHighlighted ? ({ theme }) => theme.colors.highlight : 'inherit'};
		font-size: 16px;
		line-height: 24px;
		font-weight: 600;
		letter-spacing: ${({ theme }) => theme.fonts.spacing.headerSpacing};
	}
	.edd-range {
		p {
			margin: 0;
			font-weight: ${(props) => (props.isHighlighted ? 'bold' : 'normal')};
			font-size: 14px;
			line-height: 24px;
		}
	}
	.edd-content {
		font-size: 14px;
		line-height: 24px;
		margin-top: 15px;
		margin-block-end: 40px;
	}
	.loading {
		display: flex;
		justify-content: center;
	}
`;

/**
 *
 * @param {String} param0 values expected by api endpoint - see https://www.laithwaiteswine.com/api/reference/swagger-ui/index.html?urls.primaryName=Checkout%20-%20Shipping%20API#/Shipping%20API%20Suite/getEstimatedDeliveryDate
 * @description This component gets data from useEstimatedDeliveryDate and renders it
 */
const EstimatedDeliveryDate = ({
	deliveryStateCode,
	propertyType,
	isHighlighted,
	zipCode,
	isOnReviewPage,
	gaEvent,
	orderId = false
}) => {
	const [apiObject, setApiObject] = useState({
		triggerApi: deliveryStateCode !== null && zipCode !== '' && !isOnReviewPage,
		deliveryStateCode,
		propertyType,
		zipCode,
		isOnReviewPage,
		orderId
	});

	const [error, setError] = useState({
		exists: false,
		status: '',
		statusText: '',
	});
	const [estimatedRange, setEstimatedRange] = useState(null);
	const { isLoading, shippingFrom, shippingTo, apiResponse } =
		useEstimatedDeliveryDate(apiObject);

	// useEffect(() => {
	// 	if (error.exists) {
	// 		setEstimatedRange(
	// 			<DisplayMessage type='error'>{error.statusText}</DisplayMessage>
	// 		);
	// 	}
	// }, [error]);

	useEffect(() => {
		const pattern = 'dddd, mmmm dS';
		if (shippingFrom && shippingTo) {
			const shippingFromVal = FormatDate(shippingFrom, pattern);
			const shippingToVal = FormatDate(shippingTo, pattern);

			setEstimatedRange(
				<p className='edd-range-value'>{`${shippingFromVal}
				- ${shippingToVal}`}</p>
			);
			gaEvent(
				deliveryStateCode,
				`${shippingFromVal}
				- ${shippingToVal}`
			);
		}
	}, [shippingFrom, shippingTo]);

	useEffect(() => {
		setApiObject((prevProps) => ({
			...prevProps,
			deliveryStateCode,
			zipCode,
			propertyType,
			triggerApi: true,
			orderId
		}));
	}, [deliveryStateCode, propertyType, zipCode, orderId]);

	useEffect(() => {
		if (apiResponse) {
			setApiObject((prevProps) => ({
				...prevProps,
				triggerApi: false,
			}));
			if (apiResponse.status > 299) {
				setError({
					exists: true,
					status: apiResponse.status,
				});
			}
		}
	}, [apiResponse]);

	if (error.exists === false) {
		return (
			<StyledContainer isHighlighted={isHighlighted}>
				<h3 className='edd-header'>Estimated Delivery:</h3>
				<div className={`edd-range ${isLoading ? 'loading' : ''}`}>
					{isLoading ? <LoadingIcon /> : estimatedRange}
				</div>
				<h4 className='edd-content'>
					All wine deliveries require an adult signature (21+).
				</h4>
			</StyledContainer>
		);
	}
	return null;
};
EstimatedDeliveryDate.propTypes = {
	/**
	 * delivery state code
	 */
	deliveryStateCode: PropTypes.any,
	/**
	 * Property type
	 */
	propertyType: PropTypes.string,
	/**
	 * Determines if text is highlighted
	 */
	isHighlighted: PropTypes.bool,
	/**
	 * ZipCode to use
	 */
	zipCode: PropTypes.any,
	/**
	 * used for Google GoogleAnalytics
	 */
	gaEvent: PropTypes.func,
};

EstimatedDeliveryDate.defaultProps = {
	deliveryStateCode: null,
	propertyType: 'residential',
	isHighlighted: true,
	zipCode: '',
	gaEvent: ()=>{},
};

export default EstimatedDeliveryDate;
