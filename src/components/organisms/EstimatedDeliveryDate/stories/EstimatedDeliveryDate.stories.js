import EstimatedDeliveryDate from '../src/EstimatedDeliveryDate';
import React from 'react';

export default {
	title: 'Organisms/EstimatedDeliveryDate',
	parameters: {
		component: EstimatedDeliveryDate,
		componentSubtitle: 'Displays the estimated delivery date component',
	},
};

export const Default = () => (
	<EstimatedDeliveryDate deliveryStateCode='NY' zipCode='12345' />
);
