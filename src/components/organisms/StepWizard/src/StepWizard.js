import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';

const StyledWizard = styled.div`
	.nav-row {
		display: flex;
		align-items: flex-start;
		justify-content: space-between;
		height: 30px;
		letter-spacing: ${({ theme }) => theme.stepWizard.letterSpacing};
		font-weight: ${({ theme }) => theme.stepWizard.fontWeight};
		${EmotionBreakpoint('mobile')} {
			.nav-step {
				font-size: 13px;
				height: 100%;
				width: auto;
				padding: 15px 0 0 0;
				justify-content: center;
				&.active {
					border-bottom: 2px solid ${({ theme }) => theme.colors.highlight};
				}
			}
		}
		${EmotionBreakpoint('mobile')} {
			display: flex;
			justify-content: space-between;
		}
	}
`;

const Step = styled.div`
	font-family: ${({ theme }) => theme.stepWizard.fontFamily};
	color: ${({ theme }) => theme.stepWizard.step.color};
	display: flex;
	align-items: center;
	cursor: pointer;
	&.disabled {
		cursor: default;
	}
	&.plain-number-container {
		&.active {
			border-bottom: 2px solid ${({ theme }) => theme.colors.highlight};
		}
		&.disabled {
			color: #999999;
		}
	}
	&.roundel-number-container {
		.roundel-number {
			display: flex;
			align-items: center;
			justify-content: center;
			text-transform: unset;
			width: 25px;
			height: 25px;
			border-radius: 50%;
			background-color: #fff;
			border: ${({ theme }) => theme.stepWizard.step.navNumberContainer.border};
			&.active {
				background-color: ${({ theme }) =>
					theme.stepWizard.step.active.backgroundColor};
				color: ${({ theme }) => theme.stepWizard.step.active.color};
			}
			&.disabled {
				border: 1px solid #999999;
			}
		}
		&.disabled {
			color: #999999;
		}
	}
	.step-name {
		margin-left: 0.5rem;
		text-transform: ${({ theme }) => theme.stepWizard.textTransform};
	}
`;

const StepWizard = ({ nav, numberType, goToPage, isFullNavEnabled }) => {
	const theme = useCustomTheme();
	const currentPage = window.location.hash.replace('#', '');
	let navInner = [];
	let trackDisabled = false;

	const navSteps = nav.map((navItem) => {
		const path = navItem.path;
		const stepName =
			navItem.stepName !== undefined
				? navItem.stepName
				: navItem.path.replace('/', '').replace('-', ' ');
		const isActive = currentPage === navItem.path;
		if (isFullNavEnabled) {
			trackDisabled = false;
		} else if (isActive) {
			trackDisabled = true;
		}
		return {
			path,
			stepName,
			isActive,
			isDisabled: trackDisabled,
		};
	});

	Object.values(navSteps).forEach((navItem, idx) => {
		const path = navItem.path;
		const stepName = navItem.stepName;
		const isActive = navItem.isActive;
		const isDisabled = navItem.isDisabled;

		const activeClass = isActive ? 'active' : '';
		const disabledClass = isDisabled && !isActive ? 'disabled' : '';

		let count;
		if (numberType === 'roundel') {
			count = idx + 1;
		} else if (numberType === 'plain') {
			count = '0' + (idx + 1);
		}

		navInner.push(
			<Step
				key={idx + 1}
				onClick={() => {
					if (!isDisabled) {
						goToPage(path);
					}
				}}
				className={`nav-step ${numberType}-number-container ${activeClass} ${disabledClass}`}
			>
				<div className={`${numberType}-number ${activeClass} ${disabledClass}`}>
					<span>{count}</span>
				</div>
				<div className='step-name'>{stepName}</div>
			</Step>
		);

		return navInner;
	});

	return (
		<StyledWizard theme={theme}>
			<div className='nav-row'>{navInner}</div>
		</StyledWizard>
	);
};

StepWizard.propTypes = {
	/**
	 * An array of nav URLs used to render text for each step
	 */
	nav: PropTypes.array,
	/**
	 * Either 'roundel', 'plain', or null to render numbers alongside each step
	 */
	numberType: PropTypes.string,
	/**
	 * Method to navigate between pages
	 */
	goToPage: PropTypes.func,
	isFullNavEnabled: PropTypes.bool,
};

StepWizard.defaultProps = {
	nav: [
		{
			path: '/start',
		},
		{
			path: '/shipping',
		},
		{
			path: '/payment',
		},
		{
			path: '/review',
		},
	],
	numberType: 'roundel',
	goToPage: () => {},
};

export default StepWizard;
