import StepWizard from '../src/StepWizard';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('StepWizard test', () => {
        it('Creates snapshot test for <StepWizard />', () => {
            const wrapper = shallow(<StepWizard></StepWizard>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });