import React from 'react';
import StepWizard from '../src/StepWizard';

export default {
	title: 'Organisms/StepWizard',
	parameters: {
		component: StepWizard,
		componentSubtitle: 'Displays the step wizard component',
	},
};

let navContent = [
	{
		path: '/start',
		stepName: 'Begin',
	},
	{
		path: '/shipping',
	},
	{
		path: '/payment',
	},
	{
		path: '/review',
	},
];

let history = window;
let goToPage = () => {};

export const Default = () => (
	<StepWizard
		nav={navContent}
		history={history}
		goToPage={goToPage}
	></StepWizard>
);

export const Checkout = () => (
	<StepWizard
		nav={navContent}
		history={history}
		numberType='plain'
		goToPage={goToPage}
	></StepWizard>
);

export const Signup = () => (
	<StepWizard
		nav={navContent}
		history={history}
		numberType='roundel'
		goToPage={goToPage}
	></StepWizard>
);
