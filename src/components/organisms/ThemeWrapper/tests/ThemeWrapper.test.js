import React from 'react';
import ThemeWrapper from '../src/ThemeWrapper';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('ThemeWrapper test', () => {
	it('Creates snapshot test for <ThemeWrapper /> defaults to law theme', () => {
		const wrapper = shallow(<ThemeWrapper>TestChild</ThemeWrapper>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
