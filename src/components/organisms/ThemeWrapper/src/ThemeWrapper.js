import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';
const StyledWrapper = styled.div`
	font-family: ${({ theme }) => theme.fonts.primary};
	color: ${({ theme }) => theme.colors.body};
	letter-spacing: ${({ theme }) => theme.fonts.spacing.bodyLetterSpacing};
	line-height: ${({ theme }) => theme.fonts.lineHeight};
	p,
	input,
	label {
		letter-spacing: ${({ theme }) => theme.fonts.spacing.bodyLetterSpacing};
		line-height: ${({ theme }) => theme.fonts.lineHeight};
	}
	input {
		border-radius: 0;
	}
	a {
		color: ${({ theme }) => theme.colors.link};
	}
	hr {
		border-top: ${({ theme }) => theme.colors.borderLight};
	}
`;
const ThemeWrapper = ({ children }) => {
	const theme = useCustomTheme();
	return (
		<StyledWrapper className='rc-library' theme={theme}>
			{children}
		</StyledWrapper>
	);
};

ThemeWrapper.propTypes = {
	children: PropTypes.node,
};

ThemeWrapper.defaultProps = { children: null };

export default ThemeWrapper;
