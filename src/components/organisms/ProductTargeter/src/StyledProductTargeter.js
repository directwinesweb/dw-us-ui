import styled from '@emotion/styled';

export const StyledProductDefaultTargeter = styled.div` 
	max-width: 750px;
	clear: both;
	padding-top: 50px;
	margin: 0 auto;

	.product-targeter-container {
		border: 1px solid #dcdcdc;
		display: flex;
		align-items: center;

		.product-details-container {
			border-right: 1px solid #dcdcdc;

			.targeter-header {
				background-color: #dcdcdc;
				padding: 10px;
				text-align: center;
				font-size: 1.1rem;
			}

			.product-details {
				display: flex;
				padding: 5px;
				align-items: center;

				img {
					flex-shrink: 0;
					margin-left: 10px;
				}

				.single {
					max-height: 135px;
					margin-right: 10px;
				}

				.product-info {
					margin: 0 20px;
					padding-top: 10px;

					.product-name {
						font-size: 0.9rem;
					}

					.description {
						font-size: 0.9rem;
						line-height: 23px;
					}

					.product-reviews-link,
					.write-review-link {
						display: none;
					}

					.case-rating {
						label {
							font-size: 0.8rem;
						}
                    }
                    
                    .stars-container img{
                        margin-left: 0px;
                    }

					.single-bottle-rating {
						ul, span {
							display: inline-block;
						}

						label {
							margin-left: 10px;
						}
					}
				}
			}
		}

		.product-targeter-pricing {
			flex-basis: 70%;
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
			text-align: center;

			.reg-price {
				font-size: 0.8rem;
				margin: 0 0 10px;
				line-height: 20px;
			}

			h4 {
				font-size: 1.3rem;
			}

			.savings {
				margin: 0 0 20px;
				line-height: 20px;
			}

			small {
				display: block;
				margin: 10px 0;
			}

			.reg-strike-price {
				text-decoration: line-through;
            }
            
            button {
                padding: 0 10px;
            }
		}
	}`;

export const StyledProductPanelTargeter = styled.div`
	padding: 15px 10px 10px;
	background: #fff;
	color: #000;

	.targeter-header {
		font-size: 1rem;
		margin-bottom: 20px;
		padding: 10px;
		background-color: #dcdcdc;
		text-align: center;

		span {
			display: block;
		}
	}

	.product-name {
		font-size: 0.9rem;
		margin-bottom: 10px;
	}

	img {
		margin-bottom: 10px;
	}

	.view-product-info {
		display: block;
		margin-bottom: 10px;
		font-size: 0.8rem;
	}

	.description {
		font-size: 0.9rem;
		line-height: 23px;
		text-align: left;
	}

	.case-rating {
		margin-top: 15px;
	}

	.product-reviews-link,
	.write-review-link {
		display: none;
	}

	.single-bottle-rating {
		ul, span {
			display: inline-block;
		}

		label {
			margin-left: 10px;
		}
	}

	.product-targeter-pricing {
		padding: 10px 0;
		text-align: center;

		p {
			margin: 0;
		}

		.reg-price {
			font-size: 0.8rem;
		}

		.sale-price {
			margin: 5px 0 25px;
			font-size: 1.4em;
		}

		small {
			display: block;
			margin: 10px 0;
			font-size: 0.8rem;
		}

		button {
			width: 100%;
			font-size: 2.3em;
		}
	}`;
