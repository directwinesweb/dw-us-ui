import React, { useEffect, useState } from 'react';
import {
	StyledProductDefaultTargeter,
	StyledProductPanelTargeter,
} from './StyledProductTargeter';

import { Button } from '@dw-us-ui/button';
import { CaseRating } from '@dw-us-ui/case-rating';
import { FormatPrice } from '@dw-us-ui/format-price';
import { FormatSkuData } from '@dw-us-ui/format-sku-data';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import { ImageZoom } from '@dw-us-ui/image-zoom';
import { MiniCart } from '@dw-us-ui/mini-cart';
import PropTypes from 'prop-types';
import { SingleBottleRating } from '@dw-us-ui/single-bottle-rating';
import { TargetedProductValidity } from '@dw-us-ui/targeted-product-validity';
import { TargetedUserType } from '@dw-us-ui/targeted-user-type';
import axios from 'axios';
import { useProductContentful } from '@dw-us-ui/use-product-contentful';

const ProductTargeter = ({ gaCategory, targeterData, type }) => {
	const [state, setState] = useState({
		apiResponse: false,
		caseContents: false,
		productDetails: false,
		viewProductInfo: false,
		productConfig: {},
	});
	let { setEventTracking } = GoogleAnalytics;
	let { AddItemToCart } = MiniCart;

	useEffect(() => {
		let itemCode = targeterData.itemCode;
		let url = '/api/product/item/' + itemCode;

		axios.get(url).then((item) => {
			let productDetails = {
				product: item.data.response,
			};
			let productArray = [productDetails];
			productArray = FormatSkuData(productArray);
			productDetails = productArray?.product
				? productArray.product
				: productArray[0].product;
			let apiResponse = productDetails.mixed ? false : true;
			setState((prevState) => {
				return {
					...prevState,
					productDetails,
					apiResponse,
				};
			});
		});
	}, []);

	useEffect(() => {
		if (state.productDetails.mixed) {
			let itemCode = targeterData.itemCode;
			let url = '/api/product/case/' + itemCode;

			axios
				.get(url)
				.then((item) => {
					let caseContents = item.data.response.contentProducts;
					setState((prevState) => {
						return {
							...prevState,
							caseContents,
							apiResponse: true,
						};
					});
				})
				.catch(() => {
					setState((prevState) => {
						return {
							...prevState,
							caseContents: [],
							apiResponse: true,
						};
					});
				});
		}
	}, [state.productDetails.mixed]);

	const addToCart = (e, name, itemCode) => {
		e.preventDefault();
		setEventTracking({
			category: gaCategory,
			action: 'Targeter_Add_to_Cart',
			label: name,
		});
		AddItemToCart(itemCode, 1);
	};

	const toggleProductInfo = (e) => {
		e.preventDefault();
		setState((prevState) => {
			return {
				...prevState,
				viewProductInfo: !state.viewProductInfo,
			};
		});
	};

	const { productConfig } = useProductContentful(targeterData.itemCode);

	useEffect(() => {
		if (productConfig) {
			setState((prevState) => {
				return {
					...prevState,
					productConfig,
				};
			});
		}
	}, [productConfig]);

	if (!state.apiResponse || !state.productDetails) {
		return null;
	}

	let product = state.productDetails;
	let name = product.name;
	let productType = product.mixed ? 'mixed' : 'single';
	product.salesMessage = ``;
	//Pricing
	let skuLength = product.skus.length;
	let skus = product.skus[skuLength - 1];
	let visitorTypeDetailed = pageLayer[0].visitorTypeDetailed;
	let { targeterHeadline, unidentifiedHeadline, userType, startDate, endDate } =
		targeterData;
	let header =
		visitorTypeDetailed === 'Unidentified'
			? unidentifiedHeadline
			: targeterHeadline;
	header = header.replace('{price}', FormatPrice(skus.salePricePerBottle));
	header = header.replace('{name}', `Hello ${pageLayer[0].name}`);
	header = header.replace('{{', '<span class="highlight">');
	header = header.replace('}}', '</span>');
	let salePrice = FormatPrice(skus.salePrice);
	let listPrice = FormatPrice(skus.listPrice);
	let savings = skus.savings ? FormatPrice(skus.savings) : false;
	if (state.productConfig?.addOnPricing) {
		let addOnData = state.productConfig.addOnPricing;
		let bottleCount = addOnData.onBottleCount;
		salePrice = addOnData.addOnPrice;
		listPrice = FormatPrice(skus.listPrice);
		savings = FormatPrice(listPrice - salePrice);
		product.salesMessage = `with any other 12+ btls`;
	}
	let isTargetedUser = TargetedUserType(userType, visitorTypeDetailed);
	let hasProductValidity = TargetedProductValidity(startDate, endDate);

	if (type === 'default' && isTargetedUser && hasProductValidity) {
		return (
			<StyledProductDefaultTargeter>
				<div className='product-targeter-container'>
					<div className='product-details-container'>
						<h3
							className='targeter-header'
							dangerouslySetInnerHTML={{ __html: header }}
						/>
						<div className='product-details'>
							<ImageZoom
								productDetails={product}
								type={productType}
								gaCategory={gaCategory}
								gaAction='Product_Page_Targeter'
								disableZoom={!product.mixed}
								className='targeter-zoom'
							/>
							<div className='product-info'>
								<h4 className='product-name'>{name}</h4>
								<p
									className='description'
									dangerouslySetInnerHTML={{ __html: product.description }}
								/>
								{product.mixed ? (
									<CaseRating
										caseContents={state.caseContents}
										productDetails={product}
										gaCategory={gaCategory}
										gaAction='Product_Page_Targeter'
									/>
								) : (
									<SingleBottleRating
										ratingDetails={product.ratingDetails}
										itemCode={product.itemCode}
										gaCategory={gaCategory}
										gaAction='Product_Page_Targeter'
									/>
								)}
							</div>
						</div>
					</div>
					<div className='product-targeter-pricing'>
						<p className='reg-price'>
							<strong>{skus.numberOfBottles} bottles</strong> <br />
							Reg. <span className='reg-strike-price'>{listPrice}</span>
						</p>
						<h4 className='highlight'>Now: {salePrice}</h4>
						<p className='savings'>
							<strong>{savings && `(SAVE ${savings})`}</strong>
						</p>
						{product?.salesMessage ? (
							<small>{product?.salesMessage}</small>
						) : (
							false
						)}
						<Button
							className='btn-product-cart'
							buttonType='primary'
							onClick={(e) => addToCart(e, name, product.itemCode)}
						>
							Add to Cart
						</Button>
					</div>
				</div>
			</StyledProductDefaultTargeter>
		);
	}

	if (isTargetedUser && hasProductValidity) {
		return (
			<StyledProductPanelTargeter>
				<div className='col-xs-12'>
					<div className='arrow-down' />
					<div className='product-targeter-container'>
						<h3
							className='targeter-header'
							dangerouslySetInnerHTML={{ __html: header }}
						/>
						<h4 className='product-name'>{name}</h4>
						<ImageZoom
							productDetails={product}
							type={productType}
							gaCategory={gaCategory}
							gaAction='Product_Page_Targeter'
							disableZoom={!product.mixed}
							className='targeter-zoom'
						/>
						<a
							href='#'
							className='view-product-info'
							onClick={(e) => toggleProductInfo(e)}
						>
							<strong>
								{state.viewProductInfo ? 'Hide Details' : 'View Details'}
							</strong>
						</a>
						{state.viewProductInfo ? (
							<p
								className='description'
								dangerouslySetInnerHTML={{ __html: product.description }}
							/>
						) : (
							false
						)}
						{product.mixed ? (
							<CaseRating
								caseContents={state.caseContents}
								productDetails={product}
								gaCategory={gaCategory}
								gaAction='Product_Page_Targeter'
							/>
						) : (
							<SingleBottleRating
								ratingDetails={product.ratingDetails}
								itemCode={product.itemCode}
								gaCategory={gaCategory}
								gaAction='Product_Page_Targeter'
							/>
						)}
						<div className='product-targeter-pricing'>
							<p className='reg-price'>
								<strong>{skus.numberOfBottles} bottles </strong>
								Reg. <span className='reg-strike-price'>{listPrice}</span>
							</p>
							<p className='sale-price'>
								<span className='highlight'>Now: {salePrice}</span>{' '}
								<strong>{savings && `(SAVE ${savings})`}</strong>
							</p>
							{product?.salesMessage ? (
								<small>{product?.salesMessage}</small>
							) : (
								false
							)}
							<Button
								className='btn-product-cart'
								buttonType='primary'
								onClick={(e) => addToCart(e, name, product.itemCode)}
							>
								Add to Cart
							</Button>
						</div>
					</div>
				</div>
			</StyledProductPanelTargeter>
		);
	} else {
		return null;
	}
};

ProductTargeter.propTypes = {
	targeterData: PropTypes.object.isRequired,
	type: PropTypes.string,
	gaCategory: PropTypes.string,
};

ProductTargeter.defaultProps = {
	targeterData: {},
};

export default ProductTargeter;
