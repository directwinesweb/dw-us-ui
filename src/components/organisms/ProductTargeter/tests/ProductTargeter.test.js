import ProductTargeter from '../src/ProductTargeter';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import React from 'react';

describe('ProductTargeter test', () => {
        it('Creates snapshot test for <ProductTargeter />', () => {
            const wrapper = shallow(<ProductTargeter></ProductTargeter>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });