import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { Modal } from '@dw-us-ui/modals';
import PromoteSubscription from '../PromoteSubscription';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledModalContent = styled.div`
	text-align: center;
	padding: 0 2.5rem 1rem;
	${EmotionBreakpoint('mobile')} {
		padding: 0;
	}
	color: ${({ theme }) => theme.unlimitedModal.color};
	font-family: ${({ theme }) => theme.unlimitedModal.fontFamily};
	font-size: ${({ theme }) => theme.unlimitedModal.fontSize};
	line-height: ${({ theme }) => theme.unlimitedModal.lineHeight};
	font-weight: ${({ theme }) => theme.unlimitedModal.fontWeight};
	.unlimited-header {
		margin: 0 0 0.5em;
		font-size: ${({ theme }) => theme.unlimitedModal.header.fontSize};
		font-family: ${({ theme }) => theme.fonts.special};
		color: ${({ theme }) => theme.unlimitedModal.header.color};
		font-weight: ${({ theme }) => theme.unlimitedModal.header.fontWeight};
	}
	.unlimited-sub-header {
		margin: 0;
		font-weight: ${({ theme }) => theme.unlimitedModal.fontWeight};
		font-size: ${({ theme }) => theme.unlimitedModal.subHeader.fontSize};
	}
	.unlimited-intro-content {
		text-align: left;
		margin: 0.5rem 0;
	}
	.unlimited-bullets {
		text-align: left;
		margin: 1rem 0;

		li {
			margin: 0.3rem 0;
		}
	}
	.unlimited-end-content {
		text-align: left;
		margin: 0.5rem 0;
		a {
			display: block;
			text-decoration: none;
			text-align: left;
			font-size: 0.8rem;
			margin: 1rem 0 0;
		}
	}
	img {
		max-width: 100%;
		margin-bottom: 1rem;
	}
	.unlimited-terms {
		max-height: 12rem;
		&.show {
			transition: max-height 0.5s linear;
			overflow: auto;
			border-top: 1px solid #b7b7b7;
			padding-top: 5px;
		}

		&.hide {
			pointer-events: none;
			overflow: hidden;
			max-height: 0;
			transition: max-height 0.5s linear;
		}
	}
`;

const PromoteSubscriptionModal = ({ showModal, toggleModal }) => {
	return (
		<Modal
			showModal={showModal}
			toggleModal={toggleModal}
			modalHeader={true}
			alternativeHeaderColor={true}
			modalSize={'990px'}
		>
			<StyledModalContent>
				<PromoteSubscription modal={true} />
			</StyledModalContent>
		</Modal>
	);
};

PromoteSubscriptionModal.propTypes = {
	/**
	 * render modal
	 */
	showModal: PropTypes.bool,
	/**
	 * Function that toggles modal
	 */
	toggleModal: PropTypes.func,
};
export default PromoteSubscriptionModal;
