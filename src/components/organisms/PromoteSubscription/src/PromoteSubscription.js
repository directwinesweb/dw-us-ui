import React, { Fragment, useEffect, useState } from 'react';
import { unlimitedItemCodes, wineClubLinks } from '@dw-us-ui/constants';

import { Button } from '@dw-us-ui/button';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { GetEnvironment } from '@dw-us-ui/get-environment';
import { PromoteHeroImage } from './components/PromoteHeroImage';
import { PromoteSubscriptionTable } from './components/PromoteSubscriptionTable';
import { PromoteSubscriptionTerms } from './components/PromoteSubscriptionTerms';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { useCart } from '@dw-us-ui/cart-provider';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';
import { usePageLayer } from '@dw-us-ui/pagelayer-provider';

const StyledContainer = styled.div`
	color: ${({ theme }) => theme.unlimitedPage.color};
	font-family: ${({ theme }) => theme.unlimitedPage.fontFamily};
	font-size: ${({ theme }) => theme.unlimitedPage.fontSize};
	line-height: ${({ theme }) => theme.unlimitedPage.lineHeight};
	font-weight: ${({ theme }) => theme.unlimitedPage.fontWeight};

	#unlimited-details-header-section {
		#unlimited-details-header {
			font-size: ${({ theme }) => theme.unlimitedPage.header.fontSize};
			line-height: ${({ theme }) => theme.unlimitedPage.header.lineHeight};
			color: ${({ theme }) => theme.unlimitedPage.header.color};
			font-weight: ${({ theme }) => theme.unlimitedPage.header.fontWeight};
			text-align: center;
			margin-bottom: 0px;
			padding-bottom: 5px;
			@media (max-width: 767px) {
				font-size: 2.2rem;
				line-height: 2.8rem;
				margin: 10px 0px;
			}
		}
	}
	#unlimited-details-sub-header {
		font-size: ${({ theme }) => theme.unlimitedPage.subHeader.fontSize};
		line-height: ${({ theme }) => theme.unlimitedPage.subHeader.lineHeight};
		color: ${({ theme }) => theme.unlimitedPage.subHeader.color};
		text-align: center;
		margin-bottom: 15px;
		@media (max-width: 767px) {
			font-size: 1.4rem;
			line-height: 1.8rem;
		}
	}
`;

const Content = styled.div`
	width: 100%;
	max-width: 800px;
	margin: 0 auto;
	text-align: left;
	margin-bottom: 30px;
	@media (max-width: 767px) {
		text-align: left;
	}
	&.rc-modal {
		width: 50%;
		margin: 0rem 2.5rem 2.5rem;
		${EmotionBreakpoint('mobile')} {
			width: 100%;
			margin: 0 auto 2.5rem;
		}
	}
	.headline {
		display: flex;
		flex-flow: row;
		font-size: 18px;
		font-family: ${({ theme }) => theme.fonts.subHeader};
		height: 3rem;
	}
	.content-terms {
		margin-top: 1rem;
	}
	strong {
		font-family: ${({ theme }) => theme.fonts.subHeader};
	}
`;

const StyledTopButton = styled.div`
	@media (max-width: 767px) {
		width: 100%;
		max-width: initial;
		height: auto;
		white-space: inherit;
	}

	display: block;
	margin: 0 auto 40px;
	text-align: center;
	&.rc-modal {
		display: flex;
		flex-flow: column;
		align-self: center;
		margin: 2rem auto 0;
	}
`;

const LatestOffersCopy = styled.div`
	width: 100%;
	max-width: 600px;
	color: #848484;
	font-size: 16px;
	line-height: 24px;
	font-weight: normal;
	text-align: center;
	margin: 0px auto 25px;
`;

const StyledImageAndContent = styled.div`
	&.rc-modal {
		border-bottom: 2px solid #efefef;
		margin-bottom: 2rem;
		display: flex;
		justify-content: justify;
		flex-flow: row wrap;
	}
`;

const ComplianceCheckboxCopy = styled.label`
	display: flex;
	font-weight: normal;
	margin-left: 25px;
	margin-bottom: 25px;
	span {
		margin-left: 10px;
	}
	@media (max-width: 767px) {
		margin-left: 0;
	}
`;

const BottomButtonContainer = styled.div`
	border-bottom: 1px solid #ccc;
	display: block;
	padding-bottom: 70px;
	margin: 0 auto 30px;
	text-align: center;
`;
const PromoteSubscription = ({ modal }) => {
	const theme = useCustomTheme();

	const brandData = theme.data;

	const pageLayer = usePageLayer();
	const { cartActions } = useCart();
	const environment = GetEnvironment();
	const [itemCode, setItemCode] = useState('');
	const [itemPrice, setItemPrice] = useState('');
	const [wineClubLink, setWineClubLink] = useState('');
	const [unlimitedInCart, setUnlimitedInCart] = useState(false);
	const isUnlimitedComplianceState =
		pageLayer.stateCode === 'CA' ||
		pageLayer.stateCode === 'ND' ||
		pageLayer.stateCode === 'VT';

	useEffect(() => {
		// Define unlimited code logic
		let unlimitedCode;
		let unlimitedPrice;
		if (pageLayer) {
			if (pageLayer.ou === '1' || pageLayer.ou === '') {
				unlimitedCode =
					unlimitedItemCodes[environment][brandData.tag].unlimited;
				unlimitedPrice = '$89';
			} else if (pageLayer.ou === '2') {
				unlimitedCode =
					unlimitedItemCodes[environment][brandData.tag].promotional;
				unlimitedPrice = '$69';
			} else if (pageLayer.ou === '3') {
				unlimitedCode = unlimitedItemCodes[environment][brandData.tag].half;
				unlimitedPrice = '$44';
			} else {
				unlimitedCode =
					unlimitedItemCodes[environment][brandData.tag].unlimited;
				unlimitedPrice = '$89';
			}

			setItemCode(unlimitedCode);
			setItemPrice(unlimitedPrice);

			// Define wine club link logic
			let wineClubLink;
			let country = pageLayer.country;
			if (pageLayer.brandDomain === 'npr') {
				brandData.tag = 'npr';
			}

			let promoteType;
			if (pageLayer.transient === 'true') {
				promoteType = 'unidentified';
			} else if (pageLayer.wcp === 'Low') {
				promoteType = 'lowPromo';
			} else if (pageLayer.wcp === 'Core') {
				promoteType = 'corePromo';
			} else if (pageLayer.wcp === 'High') {
				promoteType = 'highPromo';
			} else if (
				pageLayer.wcp === 'No_Promote' ||
				pageLayer.wcp === 'Not_Eligible'
			) {
				promoteType = 'noPromo';
			} else {
				promoteType = 'otherIdentified';
			}

			wineClubLink = wineClubLinks[country][brandData.tag][promoteType];

			setWineClubLink(wineClubLink);
		}
	}, [brandData, pageLayer]);

	const [displayUnlimitedCompliance, setDisplayUnlimitedCompliance] =
		useState(false);
	const [unlimitedComplianceChecked, setUnlimitedComplianceChecked] =
		useState(false);

	useEffect(() => {
		if (isUnlimitedComplianceState) {
			setDisplayUnlimitedCompliance(true);
			const sessionStorageComplianceChecked =
				sessionStorage.getItem('unlimitedComplianceChecked') === 'true' ||
				false;

			if (sessionStorageComplianceChecked) {
				setUnlimitedComplianceChecked(true);
			} else {
				setUnlimitedComplianceChecked(false);
			}
		}
	}, [isUnlimitedComplianceState]);

	useEffect(() => {
		if (isUnlimitedComplianceState) {
			if (unlimitedComplianceChecked) {
				sessionStorage.setItem('unlimitedComplianceChecked', true);
			} else {
				sessionStorage.setItem('unlimitedComplianceChecked', false);
			}
		}
	}, [isUnlimitedComplianceState, unlimitedComplianceChecked]);

	const promotionalMessage = () => {
		// eslint-disable-next-line no-undef
		if (pageLayer.ou === '1' || pageLayer.ou === '') {
			return (
				<span>
					Join {brandData.unlimited} for <strong>just {itemPrice}</strong>, then
					every order of 12+ bottles will ship free for an entire year &ndash;
					including
				</span>
			);
		} else if (pageLayer.ou === '2') {
			return (
				<span>
					Join {brandData.unlimited} &ndash;{' '}
					<strong>normally $89 a year</strong> &ndash; for just {itemPrice} as
					part of this special, limited-time offer. Then every order of 12+
					bottles will ship free for an entire year &ndash; including
				</span>
			);
		} else if (pageLayer.ou === '3') {
			return (
				<span>
					Join {brandData.unlimited} today at <strong>HALF PRICE</strong>.
					Normally $89 a year, it&apos;s yours for{' '}
					<strong>just {itemPrice}</strong> with this limited-time offer. Then
					every order of 12+ bottles will ship free for an entire year &ndash;
					including
				</span>
			);
		} else {
			return (
				<span>
					Join {brandData.unlimited} for <strong>just {itemPrice}</strong>, then
					every order of 12+ bottles will ship free for an entire year &ndash;
					including
				</span>
			);
		}
	};

	const buttonDisabled = isUnlimitedComplianceState
		? !unlimitedInCart && unlimitedComplianceChecked
		: !unlimitedInCart;

	if (!pageLayer.country) {
		return null;
	}

	return (
		<Fragment>
			<StyledContainer>
				{!modal ? (
					<div id='unlimited-details-header-section'>
						<h1 className='bold primary-font' id='unlimited-details-header'>
							Get the Best Deal on Shipping
						</h1>

						<h2 className='highlight' id='unlimited-details-sub-header'>
							Unlimited FREE delivery &ndash; plus access to exclusive deals
						</h2>
					</div>
				) : null}
				<StyledImageAndContent className={modal ? 'rc-modal' : null}>
					<PromoteHeroImage className={modal ? 'rc-modal' : null} />
					<Content className={modal ? 'rc-modal' : null}>
						{modal ? (
							<span className='headline'>
								<strong>
									{'Laithwaites 1-Year Unlimited Delivery Membership'}
								</strong>
							</span>
						) : null}
						{promotionalMessage()}{' '}
						{modal ? (
							<strong>wine club cases</strong>
						) : (
							<a className='bold' href={wineClubLink}>
								wine club cases
							</a>
						)}
						, gifts of{' '}
						{modal ? (
							<strong>fine wine</strong>
						) : (
							<a className='bold' href='/wines/_/N-1e'>
								fine wine
							</a>
						)}{' '}
						you send to friends or relatives, and more. As a member, you&apos;ll
						also gain exclusive access to special, member&ndash;only deals. See
						all the benefits below, and give {brandData.unlimited} a try today.
						{modal ? (
							<PromoteSubscriptionTerms
								modal={modal}
								className='content-terms'
							/>
						) : null}
						<StyledTopButton className={modal ? 'rc-modal' : null}>
							<Button
								className={
									buttonDisabled
										? 'btn btn-primary'
										: 'btn btn-primary disabled'
								}
								onClick={() =>
									cartActions.addItem({ itemCode: itemCode, quantity: 1 })
								}
							>
								Add {brandData.unlimited} to Cart {!modal ? 'Now' : ''}
							</Button>
							{modal ? (
								<span className='subtext'>
									12 Months &ndash; <strong>{itemPrice}</strong>
								</span>
							) : null}
						</StyledTopButton>
					</Content>
				</StyledImageAndContent>

				<PromoteSubscriptionTable brandData={brandData} itemPrice={itemPrice} />

				{!modal ? (
					<LatestOffersCopy>
						<a className='bold' href='/current-offers'>
							View our latest offers
						</a>{' '}
						and put {brandData.unlimited} to use today. You&apos;ll be joining
						thousands of other customers who save big on shipping with every
						order.
					</LatestOffersCopy>
				) : null}
				{displayUnlimitedCompliance && (
					<ComplianceCheckboxCopy>
						<input
							type='checkbox'
							name='checkbox'
							checked={unlimitedComplianceChecked}
							onClick={() =>
								setUnlimitedComplianceChecked(!unlimitedComplianceChecked)
							}
						/>
						<span>
							Your purchase today includes an {brandData.unlimited} membership
							which enables you to receive unlimited shipping on all wine orders
							of 12 bottles or more. Your membership period begins on the day
							you register and unless you request otherwise, your{' '}
							{brandData.unlimited} membership will automatically renew for one
							year and your credit card will be charged the applicable price
							(currently $89 plus applicable tax). You may cancel your{' '}
							{brandData.unlimited} membership at any time via {brandData.email}{' '}
							or by calling {brandData.phone}.
						</span>
					</ComplianceCheckboxCopy>
				)}
				{!modal ? (
					<BottomButtonContainer>
						<Button
							className={
								buttonDisabled ? 'btn btn-primary' : 'btn btn-primary disabled'
							}
							onClick={() =>
								cartActions.addItem({ itemCode: itemCode, quantity: 1 })
							}
						>
							Add {brandData.unlimited} to Cart Now
						</Button>
					</BottomButtonContainer>
				) : (
					''
				)}
				<PromoteSubscriptionTerms modal={modal} />
			</StyledContainer>
		</Fragment>
	);
};

PromoteSubscription.propTypes = {
	/**
	 * Indicates if promote subscriptions is used as a modal
	 */
	modal: PropTypes.bool,
};

PromoteSubscription.defaultProps = {
	modal: false,
};

export default PromoteSubscription;
