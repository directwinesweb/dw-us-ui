import React, { Fragment } from 'react';

import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import styled from '@emotion/styled';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';

const Image = styled.div`
	${EmotionBreakpoint('mobile')} {
		background-image: ${({ brandData }) =>
			`url(
				/images/us/${brandData.tag}/${brandData.unlimited.toLowerCase()}/${
				brandData.tag
			}_${brandData.unlimited.toLowerCase()}_hero_desktop.jpg
			)`};
	}
	${EmotionBreakpoint('desktop')} {
		background-image: ${({ brandData }) =>
			`url(
				/images/us/${brandData.tag}/${brandData.unlimited.toLowerCase()}/${
				brandData.tag
			}_${brandData.unlimited.toLowerCase()}_hero_desktop.jpg
			)`};
	}

	margin-bottom: 30px;
	width: 100%;
	&.rc-modal {
		border: 1px solid #000037;
		margin: 0 0 2rem 0;
		${EmotionBreakpoint('mobile')} {
			margin: 0 auto 2.2rem;
		}
		&.law {
			width: 360px;
			min-width: 360px;
			height: 425px;
			background-image: ${({ brandData }) =>
				`url('/images/us/${
					brandData.tag
				}/${brandData.unlimited.toLowerCase()}/Unlimited_ProductID_Special.jpg')`};
		}
	}
`;

const PromoteHeroImage = ({ className = null }) => {
	const theme = useCustomTheme();
	const brandData = theme.data;

	return (
		<Fragment>
			<Image
				brandData={brandData}
				className={`img-responsive ${className}  ${brandData.tag}`}
			/>
		</Fragment>
	);
};

export default PromoteHeroImage;
