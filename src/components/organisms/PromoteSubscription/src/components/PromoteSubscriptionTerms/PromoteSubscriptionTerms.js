import { usePageLayer } from '@dw-us-ui/pagelayer-provider';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import React, { Fragment, useCallback, useEffect, useState } from 'react';

const TermsContent = styled.div`
	width: 100%;
	max-width: 780px;
	margin: 0 auto 35px;
	color: #6d6e71;
	&.rc-modal {
		color: ${({ theme }) => theme.unlimitedModal.color};
		text-align: left;
		margin: 0 0 0.5rem;
		font-size: 12px;
	}
	.modal-terms {
		text-decoration: underline;
		color: ${({ theme }) => theme.unlimitedModal.color};
		font-size: 12px;
	}
`;

const PromoteSubscriptionTerms = ({ modal }) => {
	const theme = useCustomTheme();
	const brandData = theme.data;
	const pageLayer = usePageLayer();
	const [modalTerms, setModalTerms] = useState(false);
	const [termsDisplay, setTermsDisplay] = useState(null);

	const [shippingState] = useState(pageLayer.stateCode);
	const [isVT, setIsVT] = useState();

	useEffect(() => {
		if (shippingState.toLowerCase() === 'vt') {
			setIsVT(true);
		}
	}, [shippingState]);

	const renderTermsContent = useCallback(() => {
		const renderWsjContent = () => {
			if (brandData.tag === 'wsj') {
				return (
					<span>
						WSJ Wine is operated independently of The Wall Street Journal&apos;s
						news department.
					</span>
				);
			}
		};

		const renderAlaskaContent = () => {
			if (brandData.tag === 'law' || brandData.tag === 'wsj') {
				return (
					<span>
						An additional $75 surcharge will apply to each case shipped to
						Alaska and Hawaii.
					</span>
				);
			}
		};
		return (
			<span>
				{renderWsjContent()} As a {brandData.name} {brandData.unlimited} member,
				you will pay an annual membership fee (currently $89 plus applicable
				tax) to receive unlimited shipping on all wine orders of 12 bottles or
				more that are placed during your active membership period (certain
				restrictions apply, see below for more details).
				Your membership begins on the day you register and continues for 12
				months. After 12 months, unless you request otherwise, your{' '}
				{brandData.unlimited} membership will automatically renew for another
				year and your credit card will be charged the applicable price
				(currently $89 plus applicable tax). You may cancel your{' '}
				{brandData.unlimited} membership at anytime and receive a full refund,
				provided you have not yet used the service. You can register up to five
				delivery addresses for each membership (all recipients must be 21 years
				or over). If you purchase wine while an active member, but the
				associated shipping charges are included in a second installment that is
				due after your membership has ended, shipping charges will apply. We
				reserve the right to make changes to these terms, or any aspect of the{' '}
				{brandData.unlimited} service, by posting revisions on our website. Your
				continued membership after such changes constitutes acceptance of the
				changes. If you do not agree to such changes, you are free to cancel
				your membership. We may terminate your {brandData.unlimited} membership
				at our sole discretion without notice (in such cases, you will receive a
				prorated refund). Each state within the United States has certain laws
				that restrict the amount of shipments of wine that can be made each
				month to that state. {renderAlaskaContent()}{' '}For Colorado, a $0.27 retail delivery fee per case applies. By purchasing the {brandData.unlimited} membership,
				you accept these terms and conditions. Void where prohibited by law.
			</span>
		);
	}, [brandData]);

	useEffect(() => {
		if (modal) {
			setTermsDisplay(
				<Fragment>
					<span
						className='modal-terms'
						onClick={() => {
							setModalTerms(!modalTerms);
						}}
					>
						Terms and Conditions
					</span>
					{modalTerms ? <div>{renderTermsContent()}</div> : null}
				</Fragment>
			);
		} else if (isVT) {
			setTermsDisplay(
				<Fragment>
					<strong>Terms and conditions: </strong>
					<strong>{renderTermsContent()}</strong>
				</Fragment>
			);
		} else {
			setTermsDisplay(
				<Fragment>
					<strong>Terms and conditions: </strong>
					<div>{renderTermsContent()}</div>
				</Fragment>
			);
		}
	}, [modal, isVT, modalTerms, renderTermsContent]);

	return (
		<TermsContent className={modal ? 'rc-modal' : ''}>
			{termsDisplay}
		</TermsContent>
	);
};

PromoteSubscriptionTerms.propTypes = {
	/**
	 * Indicates if promote subscriptions is used as a modal
	 */
	modal: PropTypes.bool,
};

PromoteSubscriptionTerms.defaultProps = {
	modal: false,
};

export default PromoteSubscriptionTerms;
