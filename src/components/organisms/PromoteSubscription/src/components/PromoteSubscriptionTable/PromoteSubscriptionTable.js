import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';
const UnlimitedChart = styled.table`
	max-width: 940px;
	width: 100%;
	margin: 0 auto 30px;
	text-align: left;
	border-spacing: 0;
	border-collapse: collapse;
	thead {
		background-color: #efefef;
	}
	tr {
		border-bottom: 1px solid #ccc;
		height: 50px;
		th:first-of-type {
			padding: 5px 25px 5px 15px;
			font-size: 1.3rem;
			@media (max-width: 767px) {
				padding: 5px 10px 5px 15px;
				font-size: 1rem;
			}
		}
		td:first-of-type {
			padding: 20px 25px 20px 15px;
			font-size: 1rem;
		}
		th:not(:first-of-type) {
			border-left: 1px solid #ccc;
			font-size: 1rem;
			font-weight: bold;
			text-align: center;
			width: 150px;
			@media (max-width: 767px) {
				width: 85px;
				padding: 0 5px;
			}
		}
		td:not(:first-of-type) {
			font-size: 1.5rem;
			border-left: 1px solid #ccc;
			text-align: center;
			width: 150px;
			@media (max-width: 767px) {
				width: 85px;
				padding: 0 10px;
			}
		}
	}
	.fa-check-circle.not-member {
		color: #848484;
	}
	.fa-check-circle.member {
		color: #007236;
	}
`;

const PromoteSubscriptionTable = ({ itemPrice, brandData }) => {
	const tableContents = [
		{
			benefitCopy: 'Access to over 600 exceptional wines',
			members: true,
			nonmembers: true,
		},
		{
			benefitCopy: 'First-class customer service',
			members: true,
			nonmembers: true,
		},
		{
			benefitCopy: `100% money&#8208;back guarantee`,
			members: true,
			nonmembers: true,
		},
		{
			benefitCopy: `ONLY ${itemPrice} for a full year`,
			members: true,
			nonmembers: false,
		},
		{
			benefitCopy: 'Free shipping on 12+ bottles (including wine club cases)',
			members: true,
			nonmembers: false,
		},
		{
			benefitCopy:
				'FREE shipping to 5 different addresses &ndash; ideal for sending gifts',
			members: true,
			nonmembers: false,
		},
		{
			benefitCopy: `Exclusive member&#8208;only deals throughout the year`,
			members: true,
			nonmembers: false,
		},
	];

	const constructTable = () => {
		let table = [];
		for (let i = 0; i < tableContents.length; i++) {
			let tableRow = (
				<tr key={i.toString()}>
					<td
						dangerouslySetInnerHTML={{ __html: tableContents[i].benefitCopy }}
					></td>
					{tableContents[i].nonmembers ? (
						<td>
							<img
								className='fa fa-check-circle not-member'
								aria-hidden='true'
								src='/images/us/common/icons/law/check-circle-regular.svg'
								alt='check-circle'
								height='15px'
								width='15px'
							/>
						</td>
					) : (
						<td> </td>
					)}

					<td>
						<img
							className='fa fa-check-circle not-member'
							aria-hidden='true'
							src='/images/us/common/icons/law/check-circle-regular.svg'
							alt='check-circle'
							height='15px'
							width='15px'
						/>
					</td>
				</tr>
			);
			table.push(tableRow);
		}
		return table;
	};
	return (
		<UnlimitedChart>
			<thead>
				<tr>
					<th>Why become an {brandData.unlimited} member?</th>
					<th>
						<strong>Standard</strong>
					</th>
					<th>
						<strong>{brandData.unlimited}</strong>
					</th>
				</tr>
			</thead>
			<tbody>{constructTable({ itemPrice })}</tbody>
		</UnlimitedChart>
	);
};

PromoteSubscriptionTable.propTypes = {
	itemPrice: PropTypes.any,
	brandData: PropTypes.any,
};

PromoteSubscriptionTable.defaultProps = {};
export default PromoteSubscriptionTable;
