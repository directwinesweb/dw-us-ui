import { CartProvider } from '@dw-us-ui/cart-provider';
import { PageLayerProvider } from '@dw-us-ui/pagelayer-provider';
import PromoteSubscription from '../src/PromoteSubscription';
import { PromoteSubscriptionModal } from '../src/PromoteSubscriptionModal';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
describe('PromoteSubscription test', () => {
	it('Creates snapshot test for <PromoteSubscription />', () => {
		const wrapper = shallow(
			<PageLayerProvider>
				<CartProvider>
					<PromoteSubscription></PromoteSubscription>
				</CartProvider>
			</PageLayerProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <PromoteSubscriptionModal />', () => {
		const wrapper = shallow(
			<PageLayerProvider>
				<CartProvider>
					<PromoteSubscriptionModal />
				</CartProvider>
			</PageLayerProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
