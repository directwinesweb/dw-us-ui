import { CartProvider } from '@dw-us-ui/cart-provider';
import { PageLayerProvider } from '@dw-us-ui/pagelayer-provider';
import { PromoteSubscriptionModal } from '../src';
import React from 'react';
import { boolean } from '@storybook/addon-knobs';
export default {
	title: 'Modals/Promote Subscription Modal',
	parameters: {
		component: PromoteSubscriptionModal,
	},
};

export const Default = () => {
	return (
		<PageLayerProvider>
			<CartProvider>
				<PromoteSubscriptionModal showModal={boolean('Show Modal', false)} />
			</CartProvider>
		</PageLayerProvider>
	);
};
