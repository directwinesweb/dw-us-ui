import { CartProvider } from '@dw-us-ui/cart-provider';
import { PageLayerProvider } from '@dw-us-ui/pagelayer-provider';
import PromoteSubscription from '../src/PromoteSubscription';
import React from 'react';
export default {
	title: 'Organisms/Promote Subscription',
	parameters: {
		component: PromoteSubscription,
	},
};

export const Default = () => {
	return (
		<PageLayerProvider>
			<CartProvider>
				<PromoteSubscription />
			</CartProvider>
		</PageLayerProvider>
	);
};
