import { BrandUtil } from '@dw-us-ui/brand-util';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import React from 'react';

/**
 * @param {string} title - tag for the page
 * @param {string} robots - for SEO to follow/index the pages
 * @param {string} description - SEO description for the page
 * @todo - Add Descriptions to each page - talk to Tom
 * @todo - Add Font Scripts here as well
 * @todo - Add GA scripts here / page view and setup
 */
const Seo = ({ title, robots, description, useGtm, hideRobotsMeta, hideGoogleBot }) => {
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	const { name, favIcon, googleAnalytics } = data;
	const { merchantId, gtmId } = googleAnalytics;

	return (
		<Helmet>
			<meta charSet='utf-8' />
			<meta httpEquiv='Content-Language' content='en-us'></meta>
			<meta httpEquiv='X-UA-Compatible' content='IE=edge' />
			<meta
				name='viewport'
				content='width=device-width, initial-scale=1.0, maximum-scale=1.0, shrink-to-fit=no'
			/>
			{!hideRobotsMeta ? (
				<meta name='robots' content={robots ? robots : 'index, follow'}></meta>
			) : null}
			{hideGoogleBot ? (
				<meta name="googlebot" content="noindex"></meta>
			) : null}
			<meta name='google-site-verification' content={merchantId} />
			{description ? (
				<meta name='description' content={description}></meta>
			) : null}
			<title>{title ? `${title} | ${name}` : `${name}`}</title>
			<link href={favIcon} rel='shortcut icon'></link>
			<noscript>
				{`
          <iframe src={//www.googletagmanager.com/ns.html?id=${gtmId}} height="0" width="0" style="display:none;visibility:hidden"></iframe>
          `}
			</noscript>
			<script>
				{`
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','${gtmId}');
      `}
			</script>
		</Helmet>
	);
};

Seo.defaultProps = {
	description: null,
	title: null,
	robots: 'index, follow',
	useGtm: false,
	hideRobotsMeta: false,
};
Seo.propTypes = {
	/**
	 * will hide robots meta tag
	 */
	hideRobotsMeta: PropTypes.bool,
	/**
	 * seo description for the page
	 */
	description: PropTypes.string,
	/**
	 *
	 */
	robots: PropTypes.string,
	/**
	 * Title tag for the page
	 */
	title: PropTypes.string,
	/**
	 * use GTM, currently disabled
	 */
	useGtm: PropTypes.bool,
};

export default Seo;
