import React from 'react';
import Seo from '../src/Seo';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Seo test', () => {
	it('Creates snapshot test for <Seo />', () => {
		const wrapper = shallow(<Seo></Seo>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
