import OrderSummary from '../src/OrderSummary';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('OrderSummary test', () => {
	it('Creates snapshot test for <OrderSummary />', () => {
		const orderPriceInfo = {
			pricingBreakdown: [],
			total: 129,
			rawSubtotal: 119,
			shipping: 19.99,
			tax: 5,
			savings: 24,
			discounted: false,
		};
		const wrapper = shallow(
			<OrderSummary orderPriceInfo={orderPriceInfo}></OrderSummary>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
