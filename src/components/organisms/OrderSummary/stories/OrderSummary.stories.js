import { boolean, number } from '@storybook/addon-knobs';

import OrderSummary from '../src/OrderSummary';
import React from 'react';

export default {
	title: 'Organisms/OrderSummary',
	parameters: {
		component: OrderSummary,
	},
};

export const Default = () => (
	<OrderSummary
		orderPriceInfo={{
			pricingBreakdown: [],
			discounted: false,
			total: number('total', 50),
			rawSubtotal: number('rawSubtotal', 50),
			shipping: number('shipping', 0),
			tax: number('tax', 0),
			savings: number('savings', 0),
		}}
		showViewCartLink={boolean('Show View Cart Link', true)}
		vouchers={[{ remainingAmount: 10 }]}
	/>
);
