import React, { useCallback } from 'react';

import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { FormatPrice } from '@dw-us-ui/format-price';
import OrderSummaryRow from './OrderSummaryRow';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const StyledOrderSummary = styled.div`
	font-family: ${({ theme }) => theme.fonts.default};

	${EmotionBreakpoint('mobile')} {
		margin: 0px;
		font-size: 16px;
		padding-top: 40px;
	}
	.order-summary-header {
		display: flex;
		align-items: center;
		justify-content: space-between;
		font-size: 14px;
		${EmotionBreakpoint('mobile')} {
			font-size: 16px;
		}
		line-height: 17px;
		border-bottom: 1px solid ${({ theme }) => theme.colors.borderLight};
		padding-bottom: 0.5rem;
		font-weight: 400;
		letter-spacing: ${({ theme }) =>
			theme.fonts.spacing.subHeaderLetterSpacing};
		a {
			font-size: 12px;
			line-height: 24px;
			color: ${({ theme }) => theme.colors.link};
			text-decoration: none;
			&:hover,
			&:active {
				color: ${({ theme }) => theme.colors.linkHover || theme.colors.link};
			}
		}
	}

	.order-summary-breakdown {
		display: flex;
		flex-direction: column;
		justify-content: space-around;
		padding: 20px 0;
		.subtotal-savings {
			font-size: 14px;
			font-weight: 600;
			font-family: ${({ theme }) => theme.fonts.total};
		}
	}

	.order-summary-subtotal-savings {
		padding-bottom: 20px;
		border-bottom: 1px solid rgba(153, 153, 153, 0.2);
		margin-bottom: 20px;
		.secondary-color {
			font-size: 14px;
			font-weight: 600;
			color: ${({ theme }) => theme.colors.highlight};
			font-family: ${({ theme }) => theme.fonts.total};
		}
	}

	.order-summary-savings {
		padding-top: 10px;
		padding-bottom: 20px;
		border-top: 1px solid ${({ theme }) => theme.colors.borderLight};
		${EmotionBreakpoint('mobile')} {
			padding: 5px 0px 30px;
		}
	}

	.order-summary-row {
		display: flex;
		align-items: center;
		justify-content: space-between;
		line-height: 20px;
		font-size: 14px;
		margin-top: 10px;
		letter-spacing: ${({ theme }) => theme.fonts.spacing.orderSummarySpacing};
		&.cart-total {
			border-top: 1px solid ${({ theme }) => theme.colors.borderBottom};
			font-weight: 600;
			font-size: 15px;
			margin-bottom: 10px;
			padding-bottom: 0;
			padding-top: 10px;
			font-family: ${({ theme }) => theme.fonts.total};
			letter-spacing: ${({ theme }) => theme.fonts.spacing.totalLetterSpacing};
		}
		&.savings {
			font-weight: 600;
			font-size: ${({ theme }) => theme.fonts.size || '14px'};
			color: ${({ theme }) => theme.colors.highlight};
			font-family: ${({ theme }) => theme.fonts.total};
			letter-spacing: ${({ theme }) => theme.fonts.spacing.totalLetterSpacing};
		}
		${EmotionBreakpoint('mobile')} {
			font-size: 16px;
		}
	}
	.voucher-last {
		padding-bottom: 25px;
		${EmotionBreakpoint('mobile')} {
			padding-bottom: 15px;
		}
	}

	.wineclub-order-summary-breakdown {
		.full-order-link {
			text-decoration: none;
			font-size: 14px;
		}
		.wineclub-items {
			margin-left: 15px;
			.order-summary-row {
				.order-summary-label {
					display: list-item;
					list-style-type: disc;
					list-style-position: inside;
				}
			}
		}
		.order-summary-breakdown {
			border-top: 1px solid rgba(153, 153, 153, 0.5);
			img {
				width: 357px;
				height: 224px;
				${EmotionBreakpoint('notMobile')} {
					margin: 0 auto;
					width: 100%;
					height: 100%;
				}

				${EmotionBreakpoint('mobile')} {
					width: 100%;
					height: 224px;
				}
			}
			.wineclub-header {
				.order-summary-label {
					font-size: 14px;
					font-weight: 600px;
				}
			}
		}
	}
`;

const OrderSummary = ({
	orderPriceInfo,
	showViewCartLink,
	className,
	vouchers,
	gaEvent,
	isWineClubOrder,
	offerData,
	isWineClubCases,
	finalCartItems,
	triggerCartCallback,
	paymentDetails
}) => {
	const { rawSubtotal, shipping, tax, total, savings } = orderPriceInfo;
	const wineClubCase = isWineClubCases?.length ? isWineClubCases[0] : false;

	const renderVouchers = () => {
		let voucherTotal = 0;
		let onAccountAmount = 0;
		vouchers.forEach((voucher) => {
			let voucherRemainingAmount = voucher.remainingAmount || voucher.amount;
			voucherTotal = voucherTotal + voucherRemainingAmount;
		});
		if (voucherTotal >= total) {
			voucherTotal = total;
		}
		if(paymentDetails) {
			onAccountAmount = paymentDetails.find((detail) => detail.type === 'onAccount')?.amount;
		}
		let cardTotal = total - voucherTotal;
		return (
			<>
				<OrderSummaryRow
					text='Amount Paid by Gift Card'
					value={FormatPrice(voucherTotal)}
					className='gift-card-total'
				/>
				{
					onAccountAmount !== undefined && onAccountAmount >= cardTotal ?
						null :
						<OrderSummaryRow
							text='Amount Paid by Credit Card'
							value={FormatPrice(cardTotal < 0 ? 0 : cardTotal)}
							className='voucher-last credit-card-total'
						/>
				}
			</>
		);
	};

	let getImage = '';
	if (isWineClubOrder && wineClubCase && offerData) {
		getImage = offerData.items.filter(
			(item) => item.itemCode === wineClubCase.itemCode
		)[0];
	}

	/**
	 * Will update the CartProvider context
	 * so the modal can be popped somewhere else
	 */
	const triggerCart = useCallback(
		(e) => {
			e.preventDefault();
			triggerCartCallback();
		},
		[triggerCartCallback]
	);

	const processSavings = () => {
		return (
			<div className='order-summary-subtotal-savings'>
				<OrderSummaryRow
					text='Original Subtotal'
					value={FormatPrice(total - tax - shipping + savings)}
					className='original-subtotal'
				/>
				<OrderSummaryRow
					text='Savings'
					value={FormatPrice(0 - savings)}
					className='secondary-color savings-total'
				/>
			</div>
		);
	};

	return (
		<StyledOrderSummary className={className}>
			<div className='order-summary-header'>
				<span>ORDER SUMMARY</span>
				{showViewCartLink ? (
					<a
						data-cy='view-cart-link'
						href='#'
						className='gi-view-cart'
						onClick={(e) => {
							if (gaEvent) {
								gaEvent();
							}
							triggerCart(e);
						}}
					>
						VIEW CART
					</a>
				) : null}
			</div>
			{isWineClubOrder && offerData && wineClubCase ? (
				<div className='wineclub-order-summary-breakdown'>
					<div className='order-summary-breakdown'>
						{getImage && (
							<img
								src={getImage.image}
								onError={(e) => {
									e.target.src =
										'/images/us/law/offers/checkout/lw_6btl_generic_2x.png';
								}}
							/>
						)}

						<OrderSummaryRow
							text='Wine Club Intro Case'
							value={wineClubCase.subTotalDetails.salePrice}
							className='wineclub-header'
						/>
						<div className='wineclub-items'>
							{offerData.offerDetails.freeGiftItem.map(
								(freeGiftItem, index) => (
									<OrderSummaryRow
										text={freeGiftItem}
										value={FormatPrice(0.0)}
										className=''
										key={index}
									/>
								)
							)}

							<OrderSummaryRow
								text='Tasting Notes'
								value='Included'
								className=''
							/>
							<OrderSummaryRow
								text='Wine Club Rewards'
								value='Included'
								className=''
							/>
						</div>
						{finalCartItems?.length !== isWineClubCases?.length ? (
							<div className='order-summary-row'>
								<span className='order-summary-label'>
									<a
										href='#'
										className='full-order-link'
										onClick={(e) => {
											if (gaEvent) {
												gaEvent();
											}
											triggerCart(e);
										}}
									>
										Full Order Summary
									</a>
								</span>
								<span className='order-summary-value'>
									{FormatPrice(
										rawSubtotal - wineClubCase.subTotalDetails.rawSalePrice
									)}
								</span>
							</div>
						) : null}
					</div>
					<div className='order-summary-breakdown'>
						<OrderSummaryRow
							text='Shipping'
							value={FormatPrice(shipping)}
							className='shipping-total'
						/>
						<OrderSummaryRow
							text='Tax'
							value={FormatPrice(tax)}
							className='tax-total'
						/>
						<OrderSummaryRow
							text='Order Total'
							value={FormatPrice(total)}
							className='cart-total'
						/>
					</div>
					{savings > 0 ? (
						<div className='order-summary-savings'>
							<OrderSummaryRow
								text='TOTAL SAVINGS'
								value={FormatPrice(savings)}
								className='savings'
							/>
						</div>
					) : null}
				</div>
			) : (
				<>
					<div className='order-summary-breakdown'>
						{savings > 0 ? processSavings() : ''}
						<OrderSummaryRow
							text={`${savings > 0 ? "Today's " : ''}Subtotal`}
							value={FormatPrice(total - tax - shipping)}
							className={`${savings > 0 ? 'subtotal-savings' : ''}`}
						/>
						<OrderSummaryRow
							text='Shipping'
							value={FormatPrice(shipping)}
							className='shipping-total'
						/>
						<OrderSummaryRow
							text='Tax'
							value={FormatPrice(tax)}
							className='tax-total'
						/>
						<OrderSummaryRow
							text='TOTAL'
							value={FormatPrice(total)}
							className='cart-total'
						/>
					</div>
					{vouchers.length ? renderVouchers() : null}
					{savings > 0 ? (
						<div className='order-summary-savings'>
							<OrderSummaryRow
								text='TOTAL SAVINGS'
								value={FormatPrice(savings)}
								className='savings'
							/>
						</div>
					) : null}
				</>
			)}
		</StyledOrderSummary>
	);
};

OrderSummary.propTypes = {
	showViewCartLink: PropTypes.bool,
	orderPriceInfo: PropTypes.shape({
		total: PropTypes.number.isRequired,
		rawSubtotal: PropTypes.number.isRequired,
		shipping: PropTypes.number.isRequired,
		tax: PropTypes.number.isRequired,
		savings: PropTypes.number.isRequired,
	}).isRequired,
	className: PropTypes.string,
	vouchers: PropTypes.array,
	isWineClubOrder: PropTypes.bool,
	finalCartItems: PropTypes.node,
	isWineClubCases: PropTypes.node,
	gaEvent: PropTypes.func,
	triggerCartCallback: PropTypes.func,
	offerData: PropTypes.node,
};

OrderSummary.defaultProps = {
	showViewCartLink: true,
	className: '',
	vouchers: [],
	gaEvent: () => {},
	isWineClubOrder: false,
	offerData: false,
	triggerCartCallback: () => {},
	isWineClubCases: false,
	finalCartItems: false,
};

export default OrderSummary;
