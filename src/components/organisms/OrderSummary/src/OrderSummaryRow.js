import PropTypes from 'prop-types';
import React from 'react';
/**
 *
 * @param {String} text
 * @param String val
 * @param String className
 * @returns Order summary row
 */
const OrderSummaryRow = ({ text, value, className }) => {
	return (
		<div className={`${className} order-summary-row`}>
			<span className='order-summary-label'>{text}</span>
			<span className='order-summary-value'>{value}</span>
		</div>
	);
};

OrderSummaryRow.propTypes = {
	text: PropTypes.node,
	value: PropTypes.node,
	className: PropTypes.node,
};
export default OrderSummaryRow;
