import { ObscureCredit } from '@dw-us-ui/obscure-credit';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledPaymentInfoCard = styled.div`
	padding: 20px 0 25px;
	display: flex;
	flex-direction: column;
	.card-info-row {
		display: flex;
		flex-direction: row;
		padding-top: 8px;
		font-size: 14px;
		line-height: 20px;
	}
	.card-number-row {
		padding-left: 1em;
	}
	span {
		align-self: center;
	}
	p {
		line-height: 20px;
	}
	.card-type-icon {
		height: 25px;
		width: 40px;
		&.discover-icon {
			background: url('/images/us/common/recr/cc-icon.png') -101px -25px
				no-repeat;
		}
		&.americanexpress-icon {
			background: url('/images/us/common/recr/cc-icon.png') -153px -25px
				no-repeat;
		}
		&.mastercard-icon {
			background: url('/images/us/common/recr/cc-icon.png') -51px -25px
				no-repeat;
		}
		&.visa-icon {
			background: url('/images/us/common/recr/cc-icon.png') -2px -26px no-repeat;
		}
		&.paypal-icon {
			background: url('/images/us/en/common/checkout/paypal_64.png') -7px -13px
				no-repeat;
			background-size: 53px;
		}
	}
`;

const PaymentInfoCard = ({
	paymentMethod,
	creditCardNumber,
	expirationMonth,
	expirationYear,
	expirationDate,
	cardType,
}) => {
	//Deal with Expiration date
	let expiration;
	expirationMonth && expirationYear
		? (expiration = `${expirationMonth}/${expirationYear}`)
		: (expiration = expirationDate);
	let displayType;

	//Edge case for American Express Card Type (comes back as a single string)
	cardType === 'AmericanExpress'
		? (displayType = 'American Express')
		: (displayType = cardType);

	//Deal with Paypal
	paymentMethod === 'paypal'
		? ((cardType = 'paypal'), (displayType = 'Paypal'))
		: null;

	return (
		<StyledPaymentInfoCard>
			<div className='card-info-row'>
				<span
					className={`card-type-icon ${cardType.toLowerCase()}-icon`}
				></span>
				{paymentMethod !== 'paypal' ? (
					<span className='card-number-row'>
						{`${displayType} ${ObscureCredit(creditCardNumber)}`}
					</span>
				) : null}
			</div>
			{expiration && paymentMethod !== 'paypal' ? (
				<div className='card-info-row'>Expires {expiration}</div>
			) : null}
		</StyledPaymentInfoCard>
	);
};

PaymentInfoCard.propTypes = {
	/**
	 * Available payment methods creditCard, paypal
	 */
	paymentMethod: PropTypes.oneOf(['creditCard', 'paypal']),
	/**
	 * as name implies
	 */
	creditCardNumber: PropTypes.string,
	/**
	 * expiring month of the card
	 */
	expirationMonth: PropTypes.string,
	/**
	 * expiring year of the card
	 */
	expirationYear: PropTypes.string,
	/**
	 * in instances where the expiration date is given, (from a card added via form)
	 */
	expirationDate: PropTypes.string,
	/**
	 * Type of card
	 */
	cardType: PropTypes.oneOf([
		'AmericanExpress',
		'Visa',
		'Discover',
		'MasterCard',
	]),
};

export default PaymentInfoCard;
