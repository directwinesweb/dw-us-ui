import { ComplianceAlertMessage } from '@dw-us-ui/compliance-alert-message';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledShippingAddressCard = styled.address`
	font-style: normal;
	font-size: 14px;

	&.hal-address {
		.shipping-name {
			font-weight: 600;
			padding-bottom: 10px;
		}
	}
`;

/**
 * Component that consumes shipping address information and returns a formatted card
 *
 * @param {string} firstName - As name implies
 *
 * @param {string} lastName - As name implies
 *
 * @param {string} address1 - General concatenated address (house number, street, can be anything)
 *
 * @param {string} address2 - Any additional address information provided that isn't mandatory is provided here
 *
 * @param {string} city - As name implies
 *
 * @param {string} state - As name implies
 *
 * @param {string} zipCode - As name implies
 *
 * @param {node} retailer - As name implies
 */
//@TODO - front end compliance

const AddressCard = ({
	firstName,
	lastName,
	address1,
	address2,
	city,
	state,
	zipCode,
	retailer,
	companyName,
	phoneNumber,
	className,
}) => {
	const shippingFullName = `${firstName} ${lastName}`;
	const shippingFullAddress = `${address1} ${address2 || ''}`;
	const shippingFullLocation = `${city}, ${state} ${zipCode}`;

	return (
		<>
			<StyledShippingAddressCard className={className}>
				<div className='shipping-name'>{shippingFullName}</div>
				{companyName.length ? (
					<div className='shipping-company'>{companyName}</div>
				) : null}
				<div className='shipping-full-address'>{shippingFullAddress}</div>
				<div className='shipping-full-location'>{shippingFullLocation}</div>
				{phoneNumber.length ? (
					<div className='shipping-phone'>{phoneNumber}</div>
				) : null}
			</StyledShippingAddressCard>
			{retailer ? (
				<div className='compliance-alert'>
					<ComplianceAlertMessage retailerMessage={retailer} />
				</div>
			) : null}
		</>
	);
};

AddressCard.propTypes = {
	firstName: PropTypes.string,
	lastName: PropTypes.string,
	/**
	 * General concatenated address (house number, street, can be anything)
	 */
	address1: PropTypes.string,
	/**
	 * Any additional address information provided that isn't mandatory is provided here
	 */
	address2: PropTypes.string,
	city: PropTypes.string,
	state: PropTypes.string,
	zipCode: PropTypes.string,
	/**
	 * Node that could be either Element, string, bool etc
	 */
	retailer: PropTypes.node,
	companyName: PropTypes.string,
	phoneNumber: PropTypes.string,
	className: PropTypes.node,
};
AddressCard.defaultProps = {
	retailer: false,
	companyName: '',
	phoneNumber: '',
	className: '',
};
export default AddressCard;
