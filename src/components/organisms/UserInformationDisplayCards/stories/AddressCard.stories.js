import AddressCard from '../src/AddressCard';
import React from 'react';
import { text } from '@storybook/addon-knobs';

export default {
	title: 'Organisms/AddressCard',
	parameters: {
		component: AddressCard,
	},
};
const testRetailer =
	'If your order is accepted by one of the wineries below, they will then process and fulfill your wine order.<br />Homestead Winery @ Grapevine <br />Homestead Vineyards & Winery <br />Sunset Winery <br />Salado Wine Seller <br />Brennan Vineyards <br />Peach Creek Vineyards <br />Landon Winery <br />Paris Vineyards';

export const Default = () => (
	<AddressCard
		firstName={text('firstName', 'TestName')}
		lastName={text('lastName', 'LastName')}
		address1={text('address1', '180 West St')}
		address2={text('address2', '')}
		city={text('city', 'Danbury')}
		state={text('state', 'CT')}
		zipCode={text('zipCode', '06810')}
		companyName={text('companyName', '')}
		phoneNumber={text('phoneNumber', '203-111-4568')}
		retailer={testRetailer}
	/>
);
