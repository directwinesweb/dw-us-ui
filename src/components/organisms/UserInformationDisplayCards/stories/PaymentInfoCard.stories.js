import { select, text } from '@storybook/addon-knobs';

import PaymentInfoCard from '../src/PaymentInfoCard';
import React from 'react';

export default {
	title: 'Organisms/PaymentInfoCard',
	parameters: {
		component: PaymentInfoCard,
	},
};

export const Default = () => (
	<PaymentInfoCard
		paymentMethod={select(
			'paymentMethod',
			{ paypal: 'paypal', creditCard: 'creditCard' },
			'creditCard'
		)}
		creditCardNumber={text('creditCardNumber', '370000000000002')}
		expirationMonth={text('expirationMonth', '05')}
		expirationYear={text('expirationYear', '2020')}
		expirationDate={text('expirationDate', '05/2020')}
		cardType={select(
			'cardType',
			{
				MasterCard: 'MasterCard',
				Visa: 'Visa',
				Discover: 'Discover',
				AmericanExpress: 'AmericanExpress',
			},
			'AmericanExpress'
		)}
	/>
);
