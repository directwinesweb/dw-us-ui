import AddressCard from '../src/AddressCard';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('AddressCard test', () => {
	it('Creates snapshot test for <AddressCard />', () => {
		const wrapper = shallow(
			<AddressCard
				firstName='TestName'
				lastName='LastName'
				address1='180 West'
				address2=''
				city='Test City'
				state='CT'
				zipCode='06810'
				phoneNumber='203-111-4568'
				companyName=''
			></AddressCard>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <AddressCard /> retailer', () => {
		const testRetailer =
			'If your order is accepted by one of the wineries below, they will then process and fulfill your wine order.<br />Homestead Winery @ Grapevine <br />Homestead Vineyards & Winery <br />Sunset Winery <br />Salado Wine Seller <br />Brennan Vineyards <br />Peach Creek Vineyards <br />Landon Winery <br />Paris Vineyards';
		const wrapper = shallow(
			<AddressCard
				firstName='TestName'
				lastName='LastName'
				address1='180 West'
				address2=''
				city='Test City'
				state='CT'
				zipCode='06810'
				phoneNumber='203-111-4568'
				companyName=''
				retailer={testRetailer}
			></AddressCard>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
