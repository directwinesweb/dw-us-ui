import PaymentInfoCard from '../src/PaymentInfoCard';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('PaymentInfoCard test', () => {
	it('Creates snapshot test for <PaymentInfoCard />', () => {
		const wrapper = shallow(
			<PaymentInfoCard
				paymentMethod='creditCard'
				creditCardNumber='370000000000002'
				expirationMonth='03'
				expirationYear='2020'
				cardType='AmericanExpress'
			></PaymentInfoCard>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <PaymentInfoCard /> using paypal payment method', () => {
		const wrapper = shallow(
			<PaymentInfoCard
				paymentMethod='paypal'
				creditCardNumber='370000000000002'
				expirationMonth='03'
				expirationYear='2020'
				cardType='AmericanExpress'
			></PaymentInfoCard>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
