const colors = {
	white: '#ffffff',
	body: '#000000',
	highlight: '#cf004f',
	borderLight: 'rgba(153, 153, 153, 0.2)',
	placeholderText: 'rgba(0, 0, 0, 0.7)',
	textInputBackgroundColor: '#f5f5f5',
	borderDark: '#000000',
	backgroundModal: 'rgba(0, 0, 0, 0.65)',
	backgroundLight: '#faf3e6',
	backgroundDark: '#000000',
	borderDarkGrey: 'rgb(0,0,0,.8)',
	backgroundGreyBlue: '#82B4C8',
	link: '#055faf',
	containerBackground: '#faf3e6',
	outOfStockMessageBackground: "#faf3e6",
	grey: '#ccc',
	toolTipInput: 'rgb(60,60,60)',
	headerRed: '#cf004f',
	borderBottom:'#ABC7AB',
};

const fonts = {
	primary: '"OpenSans Regular", georgia, sans-serif',
	default: '"OpenSans SemiBold", georgia, sans-serif',
	secondary: '"OpenSans Regular", georgia, sans-serif',
	special: '"Oswald Regular", georgia, sans-serif',
	subHeader: '"OpenSans Bold", georgia, sans-serif',
	total: `"OpenSans Bold", georgia, sans-serif`,
	productHeadline: '"Noto Serif", sans-serif',
	estimatedDelivery: `"OpenSans Bold", georgia, sans-serif`,
	size: '16px',
	headerSize: '40px',
	lineHeight: '24px',
	spacing: {
		pageHeaderSpacing: '0px',
		letterSpacing: '0px',
		totalLetterSpacing: '0px',
		subHeaderLetterSpacing: '0px',
		bodyLetterSpacing: '0px',
		orderSummarySpacing: '0px',
		headerSpacing: '0px',
	},
	paymentHeaderLineHeight: '38px',
	inputLineHeight: '30px',
	regularWeight: '400',
};

const lawTheme = {
	name: 'Laithwaites',
	brand: 'laithwaites',
	colors: {
		...colors,
	},
	fonts: {
		...fonts,
	},
	body: {
		width: '80%',
	},
	button: {
		fontFamily: '"OpenSans Bold", georgia, sans-serif',
		fontSize: '16px',
		borderRadius: '0px',
		letterSpacing: '.9px',
		textTransform: 'uppercase',
		fontWeight: 'normal',
		primary: {
			border: '1px solid #CF004F',
			textColor: '#ffffff',
			backgroundColor: '#CF004F',
			activeBackgroundColor: '#D83E55',
			padding: '0px 0px',
			activeButtonColor: '#fff',
		},
		secondary: {
			border: '1px solid #000000',
			textColor: '#000000',
			backgroundColor: '#fff',
			activeBackgroundColor: '#D83E55',
			padding: '0px 0px',
			activeButtonColor: '#fff',
		},
		flex: {
			fontSize: '16px',
			height: '34px',
			padding: '0px 50px',
		},
		large: {
			fontSize: '16px',
			height: '50px',
			padding: '0px 40px',
		},
		small: {
			fontSize: '16px',
			height: '44px',
			padding: '0px 20px',
		},
	},
	input: {
		fontFamily: fonts.primary,
		fontSize: '14px',
		color: colors.body,
		borderWidth: '0 0 2px 0' /* top right bottom left */,
		borderStyle: 'none, none, solid, none',
		borderColor: ` ${colors.borderDarkGrey}`,
		placeholder: `${colors.placeholderText}`,
		dobWidth: '50%',
		labelTranslate : `translate(0, -10px) scale(0.75)`
	},
	checkbox: {
		checkBorder: `1px solid #ccc`,
		borderRadius: 'none',

		checkedBackgroundHeight: '21px',
		checkedBackgroundWidth: '21px',
		checkedBackgroundColor: `${colors.backgroundDark}`,

		checkMarkWidth: '5px',
		checkMarkHeight: '15px',
		checkMarkBorder: `solid ${colors.white}`,
		checkMarkBorderWidth: '0 1.5px 1.5px 0',
	},
	radioButton: {
		checkMarkBorder: `1px solid ${colors.borderLight}`,
		checkMarkBorderWidth: '21px',
		checkMarkBorderHeight: '21px',
		borderRadius: '50%',

		checkedBackgroundHeight: '21px',
		checkedBackgroundWidth: '21px',
		checkedBackgroundColor: `${colors.backgroundDark}`,

		checkMarkWidth: '6px',
		checkMarkHeight: '13px',
		checkMarkColor: `solid ${colors.white}`,
		checkMark: '0 1.5px 1.5px 0',
	},
	stepWizard: {
		fontFamily: fonts.secondary,
		color: colors.body,
		textTransform: 'uppercase',
		letterSpacing: '1.2px',
		fontWeight: '600',
		step: {
			backgroundColor: colors.white,
			color: colors.body,
			navNumberContainer: {
				display: 'inline-block',
				border: `1px solid ${colors.backgroundDark}`,
			},
			navNumber: {
				display: 'inline-block',
			},
			active: {
				backgroundColor: colors.backgroundDark,
				color: colors.white,
			},
		},
	},
	modal: {
		header: {
			backgroundColor: `${colors.backgroundLight}`,
			textTransform:'uppercase',
			fontSize: '24px',
			lineHeight: '30px',
			fontFamily: '"Oswald Bold", georgia, sans-serif',
			color: `${colors.backgroundDark}`,
			close: {
				color: `${colors.backgroundDark}`,
				fontSize: '1.2rem',
			},
		},
		backdrop: {
			backgroundColor: `${colors.backgroundModal}`,
		},
		width: '80%',

		backgroundColor: `${colors.white}`,
	},
	header: {
		logo: {
			width: '180px',
		},
		guarantee: {
			title:{
				font: fonts.subHeader,
			},
			font: '"Noto Serif", georgia, sans-serif',
		},
	},
	footer: {
		borderColor:'#ABC7AB',
		font: {
			color: '#000000',
		},
		background: {
			light: `${colors.backgroundLight}`,
			dark: `${colors.backgroundDark}`,
		},

	},
	errors: {
		fontSize: `12px`,
		color: `${colors.highlight}`,
		formErrors: {
			fontSize: `16px`,
			lineHeight: `20px`,
		},
	},
	unlimitedModal: {
		color: colors.body,
		fontFamily: fonts.secondary,
		fontSize: '16px',
		lineHeight: '26px',
		fontWeight: '400',
		header: {
			fontSize: '22px',
			color: colors.body,
			fontWeight: '400',
		},
		subHeader: {
			fontSize: '16px',
		},
	},
	headerFontWeights: {
		h2: 'inherit',
	},
	unlimitedPage: {
		color: colors.body,
		fontFamily: fonts.secondary,
		fontSize: '16px',
		lineHeight: '26px',
		fontWeight: '400',
		header: {
			fontSize: '2.6rem',
			lineHeight: '3.2rem',
			color: colors.body,
			fontWeight: '400',
		},
		subHeader: {
			fontSize: '1.8rem',
			lineHeight: '2.6rem',
			color: colors.highlight,
		},
	},
};

export default lawTheme;
