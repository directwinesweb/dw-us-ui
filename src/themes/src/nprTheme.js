const colors = {
	white: '#ffffff',
	body: '#000005',
	highlight: '#c84646',
	borderLight: 'rgba(153, 153, 153, 0.2)',
	placeholderText: 'rgba(0, 0, 0, 0.7)',
	textInputBackgroundColor: '#f5f5f5',
	borderDark: '#000000',
	modalHeader: '#F9F9F9',
	backgroundLight: '#f9f8f0',
	backgroundDark: '#000000',
	backgroundModal: 'rgba(0, 0, 0, 0.65)',
	borderDarkGrey: '#9595A7',
	backgroundGreyBlue: '#82B4C8',
	link: '#237bbd',
	containerBackground: 'rgba(153, 153, 153, 0.05);',
	toolTipInput: 'rgb(60,60,60)',
	headerRed: '#c84646',
	borderBottom:'#ddd',
	transparent: 'transparent',
};

const fonts = {
	primary: '"Gotham SSm", georgia, sans-serif',
	default: '"Gotham SSm", georgia, sans-serif',
	secondary: '"Gotham SSm", georgia, sans-serif',
	special: '"Gotham SSm", georgia, sans-serif',
	subHeader: '"Gotham SSm", georgia, sans-serif',
	estimatedDelivery: '"Gotham SSm", georgia, sans-serif',
	total: '"Gotham SSm", georgia, sans-serif',
	size: '16px',
	headerSize: '34px',
	lineHeight: '24px',
	paymentHeaderLineHeight: '44px',
	spacing: {
		pageHeaderSpacing: '0px',
		letterSpacing: '0px',
		totalLetterSpacing: '0px',
		subHeaderLetterSpacing: '0px',
		bodyLetterSpacing: '0px',
		orderSummarySpacing: '0px',
		headerSpacing: '0px',
	},

	inputLineHeight: '30px',
	regularWeight: '400',
};

const nprTheme = {
	name: 'NPR',
	brand: 'npr',
	colors: {
		...colors,
	},
	fonts: {
		...fonts,
	},
	body: {
		width: '90%',
	},
	button: {
		fontFamily: fonts.primary,
		fontSize: '16px',
		borderRadius: '0px',
		textTransform: 'capitalize',
		letterSpacing: '0px',
		fontWeight: 'normal',
		primary: {
			border: '1px solid #D62020',
			textColor: '#ffffff',
			backgroundColor: '#D62020',
			activeBackgroundColor: '#c0491b',
			padding: '0px 0px',
			activeButtonColor: '#fff',
		},
		primaryalt: {
			border: '1px solid #0274A9',
			textColor: '#ffffff',
			backgroundColor: '#0274A9',
			activeBackgroundColor: '#0274A9',
			padding: '0px 0px',
			activeButtonColor: '#fff',
			activeBorderColor: '#444444',
		},
		secondary: {
			border: '1px solid #444444',
			textColor: '#444444',
			backgroundColor: '#ffffff',
			activeBackgroundColor: '#F5F5F5',
			padding: '0px 0px',
			activeButtonColor: '#444444',
			activeBorderColor: '#444444',
		},
		flex: {
			fontSize: '20px',
			height: '34px',
			padding: '0px 60px',
		},
		flexshort: {
			fontSize: '14px',
			height: '27px',
			padding: '0px 28px',
		},
		large: {
			fontSize: '24px',
			height: '50px',
			padding: '0px 60px',
		},
		xlarge: {
			fontSize: '16px',
			height: '50px',
			padding: '0px 60px',
		},
		small: {
			fontSize: '14px',
			height: '30px',
			padding: '0px 13px',
		},
		search: {
			fontSize: '14px',
			height: '28px',
			padding: '0px 13px',
		},
	},
	checkbox: {
		checkBorder: `1px solid #ccc`,
		borderRadius: 'none',

		checkedBackgroundWidth: '18px',
		checkedBackgroundHeight: '18px',
		checkedBackgroundColor: `${colors.transparent}`,

		checkMarkWidth: '4px',
		checkMarkHeight: '12px',
		checkMarkBorderRadius: 'none',
		checkMarkBorderWidth: '0 1.5px 1.5px 0',
		checkMarkBorder: `solid ${colors.backgroundDark}`,
	},
	radioButton: {
		checkMarkBorder: `1px solid ${colors.borderDark}`,
		checkMarkBorderWidth: '21px',
		checkMarkBorderHeight: '21px',
		borderRadius: '50%',

		checkedBackgroundWidth: '15px',
		checkedBackgroundHeight: '15px',
		checkedBackgroundColor: `${colors.backgroundDark}`,

		checkMarkWidth: '0',
		checkMarkHeight: '0',
		checkMarkColor: `none`,
		checkMark: '0',
	},
	input: {
		fontFamily: fonts.primary,
		fontSize: '14px',
		color: colors.body,
		placeholder: `${colors.placeholderText}`,
		borderWidth: '0 0 1px 0' /* top right bottom left */,
		borderStyle: 'none, none, solid, none',
		borderColor: ` ${colors.borderDark}` /* top right bottom left */,
		dobWidth: '52%',
		labelTranslate : `translate(0, -14px) scale(0.75)`
	},
	modal: {
		header: {
			backgroundColor: `${colors.modalHeader}`,
			textTransform:'none',
			fontSize: '24px',
			lineHeight: '0px',
			fontFamily: `${fonts.special}`,
			color: `${colors.backgroundDark}`,
			close: {
				color: `${colors.backgroundDark}`,
				fontSize: '1.2rem',
			},
		},
		backdrop: {
			backgroundColor: `${colors.backgroundModal}`,
		},
		width: '80%',

		backgroundColor: `${colors.white}`,
	},
	stepWizard: {
		fontFamily: fonts.primary,
		color: colors.body,
		textTransform: 'uppercase',
		letterSpacing: '0px',
		fontWeight: '600',
		step: {
			backgroundColor: colors.white,
			color: colors.body,
			navNumberContainer: {
				display: 'inline-block',
				border: `1px solid ${colors.backgroundDark}`,
			},
			navNumber: {
				display: 'inline-block',
			},
			active: {
				backgroundColor: colors.backgroundDark,
				color: colors.white,
			},
		},
	},
	header: {
		logo: {
			width: '120px',
		},
		guarantee: {
			font: fonts.primary,
		},
	},
	footer: {
		borderColor:'rgba(153, 153, 153, 0.2)',
		font: {
			color: `${colors.body}`,
		},
		background: {
			light: `${colors.backgroundLight}`,
			dark: `${colors.backgroundDark}`,
		},
	},

	errors: {
		fontSize: `12px`,
		color: `${colors.highlight}`,
		formErrors: {
			fontSize: `16px`,
			lineHeight: `20px`,
		},
	},
	unlimitedModal: {
		color: colors.body,
		fontFamily: fonts.primary,
		fontSize: '18px',
		lineHeight: '26px',
		fontWeight: '400',
		header: {
			fontSize: '24px',
			color: '#000',
			fontWeight: '600',
		},
		subHeader: {
			fontSize: '18px',
		},
	},
	headerFontWeights: {
		h2: '400',
	},
};

export default nprTheme;
