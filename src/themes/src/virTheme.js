const colors = {
	white: '#ffffff',
	body: '#000000',
	highlight: '#cc0000',
	borderLight: 'rgba(153, 153, 153, 0.2)',
	placeholderText: 'rgba(0, 0, 0, 0.7)',
	backgroundDark: '#000000',
	borderDark: '#000000',
	backgroundLight: '#dcdcdc;',
	backgroundModal: 'rgba(0, 0, 0, 0.8)',
	textInputBackgroundColor: '#f5f5f5',
	modalHeader: '#F9F9F9',
	link: '#027bc2',
	linkHover: '#325f88',
	containerBackground: 'rgba(153, 153, 153, 0.05);',
	toolTipInput: 'rgb(60,60,60)',
	headerRed: '#cc0000',
	borderBottom:'#ddd',
};

const fonts = {
	primary: '"fira-sans", sans-serif',
	default: '"fira-sans", sans-serif',
	secondary: '"fira-sans", sans-serif',
	special: '"fira-sans", sans-serif',
	subHeader: '"fira-sans", sans-serif',
	estimatedDelivery: '"fira-sans", sans-serif',
	lineHeight: '24px',
	paymentHeaderLineHeight: '44px',
	size: '16px',
	headerSize: '40px',
	spacing: {
		pageHeaderSpacing: '0px',
		letterSpacing: '0px',
		totalLetterSpacing: '0px',
		subHeaderLetterSpacing: '0px',
		bodyLetterSpacing: '0px',
		orderSummarySpacing: '0px',
		headerSpacing: '0px',
	},
	inputLineHeight: '30px',
	regularWeight: '400',
};

const virTheme = {
	name: 'Virgin Wines',
	brand: 'virgin',
	colors: {
		...colors,
	},
	fonts: {
		...fonts,
	},
	body: {
		width: '100%',
	},
	button: {
		fontFamily: fonts.secondary,
		fontSize: '16px',
		borderRadius: '3px',
		textTransform: 'capitalize',
		fontWeight: 'normal',
		primary: {
			border: '1px solid #cc0000',
			textColor: '#ffffff',
			backgroundColor: '#cc0000',
			activeBackgroundColor: '#990000',
			padding: '0px 0px',
			activeButtonColor: '#fff',
		},
		secondary: {
			border: '1px solid #000',
			textColor: '#000000',
			backgroundColor: '#ffffff',
			activeBackgroundColor: '#F5F5F5',
			padding: '0px 0px',
			activeButtonColor: '#444444',
		},
		flex: {
			fontSize: '16px',
			height: '34px',
			padding: '0px 50px',
		},
		large: {
			fontSize: '16px',
			height: '50px',
			padding: '0px 40px',
		},
		small: {
			fontSize: '16px',
			height: '44px',
			padding: '0px 20px',
		},
	},

	checkbox: {
		checkBorder: `1px solid #ccc`,
		borderRadius: 'none',

		checkedBackgroundWidth: '18px',
		checkedBackgroundHeight: '18px',
		checkedBackgroundColor: `${colors.white}`,

		checkMarkWidth: '4px',
		checkMarkHeight: '12px',
		checkMarkBorderRadius: 'none',
		checkMarkBorderWidth: '0 1.5px 1.5px 0',
		checkMarkBorder: `solid ${colors.backgroundDark}`,
	},
	radioButton: {
		checkMarkBorder: `1px solid ${colors.borderDark}`,
		checkMarkBorderWidth: '21px',
		checkMarkBorderHeight: '21px',
		borderRadius: '50%',

		checkedBackgroundWidth: '15px',
		checkedBackgroundHeight: '15px',
		checkedBackgroundColor: `${colors.backgroundDark}`,

		checkMarkWidth: '0',
		checkMarkHeight: '0',
		checkMarkColor: `none`,
		checkMark: '0',
	},
	input: {
		fontFamily: fonts.primary,
		fontSize: '14px',
		color: colors.body,
		placeholder: `${colors.placeholderText}`,
		borderWidth: '0 0 1px 0' /* top right bottom left */,
		borderStyle: 'none, none, solid, none',
		borderColor: ` ${colors.borderDark}` /* top right bottom left */,
		dobWidth: '50%',
		labelTranslate : `translate(0, -14px) scale(0.75)`
	},
	modal: {
		header: {
			backgroundColor: `${colors.modalHeader}`,
			textTransform:'none',
			fontSize: '24px',
			lineHeight: '0px',
			fontFamily: `${fonts.special}`,
			color: `${colors.backgroundDark}`,
			close: {
				color: `${colors.backgroundDark}`,
				fontSize: '1.2rem',
			},
		},
		backdrop: {
			backgroundColor: `${colors.backgroundModal}`,
		},
		width: '80%',

		backgroundColor: `${colors.white}`,
	},
	stepWizard: {
		fontFamily: fonts.secondary,
		color: colors.body,
		textTransform: 'uppercase',
		step: {
			backgroundColor: colors.white,
			color: colors.body,
			navNumberContainer: {
				display: 'inline-block',
				border: `1px solid ${colors.backgroundDark}`,
			},
			navNumber: {
				display: 'inline-block',
			},
			active: {
				backgroundColor: colors.backgroundDark,
				color: colors.white,
			},
		},
	},
	header: {
		logo: {
			width: '120px',
		},
		guarantee: {
			font: fonts.secondary,
		},
	},
	footer: {
		borderColor:'rgba(153, 153, 153, 0.2)',
		font: {
			color: `${colors.body}`,
		},
		background: {
			light: `${colors.backgroundLight}`,
			dark: `${colors.backgroundDark}`,
		},
	},
	errors: {
		fontSize: `16px`,
		color: `${colors.highlight}`,
	},
	unlimitedModal: {
		color: '#333',
		fontFamily: fonts.secondary,
		fontSize: '15.6px',
		lineHeight: '20px',
		fontWeight: '400',
		header: {
			fontSize: '24px',
			color: colors.highlight,
			fontWeight: '400',
		},
		subHeader: {
			fontSize: '18px',
		},
	},
	headerFontWeights: {
		h2: 'inherit',
	},
};

export default virTheme;
