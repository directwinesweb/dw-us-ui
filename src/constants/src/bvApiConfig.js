/* Config Module */

const bvApiConfig = {
    apiVersion: "5.4",
    sortBy: "SubmissionTime:desc",
    numberOfPages: 0,
    currentPage: 1,
    offSet: 0,
    staging: {
      url: "//stg.api.bazaarvoice.com",
      us: {
        law: {
          key: "skktf57raycmmd993wj9npgy",
          profileUrl:
            "//laithwaiteswine.ugc.bazaarvoice.com/bvstaging/profiles/3129-en_us",
        },
        vir: {
          key: "hzackg6b2vfcxw49ya58zju5",
          profileUrl: "//virginwines.ugc.bazaarvoice.com/bvstaging/profiles/2511",
        },
        wsj: {
          key: "mhm2fyta7p9kf566vj8zpmk3",
          profileUrl: "//wsjwine.ugc.bazaarvoice.com/bvstaging/3131",
        },
        mcy: {
          key: "caBuMVWgdcOkLSZtRfXig9jJ8ynxaF6s8skNjJGcYLT5I",
          profileUrl: "",
        },
        npr: {
          key: "skktf57raycmmd993wj9npgy",
          profileUrl:
            "//laithwaiteswine.ugc.bazaarvoice.com/bvstaging/profiles/3129-en_us",
        },
      },
      au: {
        wpe: {
          key: "caf7bTV36rr0TZA4kPYRvcIBispDRtBicu6qayTgl2B5U",
          profileUrl: "//winepeople.ugc.bazaarvoice.com/profiles/3128-en_au",
        },
        adc: {
          key: "caowN1d8bC3Ss4WUxq25fjdZWFlwlCfqt1yGjSE12bDYU",
          profileUrl: "//australianwine.ugc.bazaarvoice.com/profiles/3127-en_au",
        },
        vir: {
          key: "ca0JnXb7xvthbhhegOe0UwDebroETzUoY0NOErikfhJaM",
          profileUrl: "//virginwines.ugc.bazaarvoice.com/profiles/2511-en_au",
        },
        vws: {
          key: "cagRjImz4VnJ2NOpqaDnS9nUB22u1pxEg3qexDmzO9NLA",
          profileUrl:
            "//velocitywine-au.ugc.bazaarvoice.com/bvstaging/profiles/3129-en_au",
        },
      },
      uk: {
        law: {
          key: "esbj3hf7z6kxfeq5vubzt83b",
        },
        stw: {
          key: "mafdrezee3sjyzetv2ez7d52",
        },
        avy: {
          key: "cagVpfljJpMIKwxUplPprWyCv3Hntz2R3IM0AaxMGTyak",
        },
      },
      nz: {
        law: {
          key: "cao4zWhmDEyhzDEBUefcxDsG5vkia33ebE4NRIZMjw0EQ",
          profileUrl:
            "//laithwaiteswine-en_nz.ugc.bazaarvoice.com/profiles/1956-en_nz",
        },
      },
      tw: {
        law: {
          key: "",
          profileUrl: "",
        },
      },
      hk: {
        hlw: {
          key: "",
          profileUrl: "",
        },
      },
    },
    production: {
      url: "//api.bazaarvoice.com",
      us: {
        law: {
          key: "lmzo8bsuy3ia1ubomqcrzrktq",
          profileUrl: "//laithwaiteswine.ugc.bazaarvoice.com/profiles/3129-en_us",
        },
        vir: {
          key: "3f2z1is5bk9n9iosddcx6pagf",
          profileUrl: "//virginwines.ugc.bazaarvoice.com/profiles/2511",
        },
        wsj: {
          key: "9m8wfbdu0ss2y8vtdauab5yhs",
          profileUrl: "//wsjwine.ugc.bazaarvoice.com/3131",
        },
        mcy: {
          key: "caWTd3SjVMMOVTkbh58qo51gTgQMilHu4oE1oePFjQE6s",
          profileUrl: "",
        },
        npr: {
          key: "lmzo8bsuy3ia1ubomqcrzrktq",
          profileUrl: "//laithwaiteswine.ugc.bazaarvoice.com/profiles/3129-en_us",
        },
      },
      au: {
        wpe: {
          key: "ca665eav4OYUWEymzLF6lQBlllFvnMZZBw3OfuE0fuS8g",
          profileUrl: "//winepeople.ugc.bazaarvoice.com/profiles/3128-en_au",
        },
        adc: {
          key: "caHNkDYKiPJvnvodwq2JDlqGEGUAUxB9mNVHwTidv6HdI",
          profileUrl: "//australianwine.ugc.bazaarvoice.com/profiles/3127-en_au",
        },
        vir: {
          key: "car99fS36vLHFWFInxuBG5H1D743MtP74kzCC6r6dTYpU",
          profileUrl: "//virginwines.ugc.bazaarvoice.com/profiles/2511-en_au",
        },
        vws: {
          key: "caQ84bsnfYjqWOKjDA07DoApDJXZHnTBxnaS1F3pCwZUs",
          profileUrl: "//velocitywine-au.ugc.bazaarvoice.com/profiles/3129-en_au",
        },
      },
      uk: {
        law: {
          key: "9qponzptd2jbpidju10bd6uer",
        },
        stw: {
          key: "ey3rg9tkv2ktivxt9mwkfbj2k",
        },
        avy: {
          key: "ca45SB9eim04AAy3xrE1hEOJZdxlHjfD0d9uzJA6tfA4E",
        },
      },
      nz: {
        law: {
          key: "caLNyH7RHZvXiQJh2NgbULDJ7f5cjHUaRvr0ppiuNm6Q0",
          profileUrl:
            "//laithwaiteswine-en_nz.ugc.bazaarvoice.com/profiles/1956-en_nz",
        },
      },
      tw: {
        law: {
          key: "",
          profileUrl: "",
        },
      },
      hk: {
        hlw: {
          key: "",
          profileUrl: "",
        },
      },
    },
  };
  
  export default bvApiConfig;
  