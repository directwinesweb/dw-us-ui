/**
 * Add wineid to hide the ability to change shipping dates
 */
const hideNextDelivery = [
	'RedHeadLW',
	'RedHeadWSJ',
	'BRUNLW',
	'BRUNWSJ',
	'CNDP16WSJ',
	'CNDP16LW',
	'CLARWSJ',
	'CLARLW',
	'SELVLW',
	'SELVWSJ',
	'CABALLW',
	'PIGWSJ',
	'PIGLW',
	'CABALWSJ',
	'PBCLW',
	'WSJPBC',
	'WSJPBC2'
];

export default hideNextDelivery;
