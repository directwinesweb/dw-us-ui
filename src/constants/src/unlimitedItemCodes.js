/*
	Unlimited Item Codes
 */
const UnlimitedItemCodes = {
	staging: {
		wsj: {
			unlimited: '00034SV',
			promotional: '17110UL',
			half: '17111UL',
			sunUnlimited: '00034SV',
		},
		law: {
			unlimited: '00033SV',
			promotional: '17096UL',
			half: '17100UL',
			sunUnlimited: '00033SV',
		},
		npr: {
			unlimited: '00033SV',
			promotional: '17096UL',
			half: '17100UL',
			sunUnlimited: '00033SV',
			//IS THIS ACCURATE FOR NPR
		},
		vir: {
			unlimited: '00035SV',
			promotional: '17112UL',
			half: '17113UL',
			sunUnlimited: '00035SV',
		},
		mcy: {
			unlimited: '00036SV',
			promotional: '18015UL',
			half: '18016UL',
			sunUnlimited: '00036SV',
		},
	},
	production: {
		wsj: {
			unlimited: '00034SV',
			promotional: '17110UL',
			half: '17111UL',
			sunUnlimited: '00034SV',
		},
		law: {
			unlimited: '00033SV',
			promotional: '17096UL',
			half: '17100UL',
			sunUnlimited: '00033SV',
		},
		npr: {
			unlimited: '00033SV',
			promotional: '17096UL',
			half: '17100UL',
			sunUnlimited: '00033SV',
			//IS THIS ACCURATE FOR NPR
		},
		vir: {
			unlimited: '00035SV',
			promotional: '17112UL',
			half: '17113UL',
			sunUnlimited: '00035SV',
		},
		mcy: {
			unlimited: '00036SV',
			promotional: '18015UL',
			half: '18016UL',
			sunUnlimited: '00036SV',
		},
	},
};

export default UnlimitedItemCodes;
