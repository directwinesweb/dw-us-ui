import { BrandUtil } from '@dw-us-ui/brand-util';

const brandTag =
		// eslint-disable-next-line no-undef
    typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
const brand = BrandUtil.getBrand(brandTag);
const tag = BrandUtil.content(brand).tag;

export default {
    DEFAULT_SINGLE_S_IMG: 
        (tag === "vir" || tag === "wsj")
            ? `/images/us/${tag}/common/image_unavailable/bottle_unavailable_small.png`
            : "/images/us/common/default_bottle_small.png",
    DEFAULT_SINGLE_T_IMG:
        (tag === "vir" || tag === "wsj")
            ? `/images/us/${tag}/common/image_unavailable/bottle_unavailable_${
                tag === "vir" ? 66 : 100
            }x240.png`
            : "/images/us/common/default_bottle_thumb.png",
    DEFAULT_SINGLE_L_IMG:
        (tag === "vir" || tag === "wsj")
            ? `/images/us/${tag}/common/image_unavailable/bottle_unavailable.png`
            : "/images/us/common/default_bottle_large.png",
    DEFAULT_MIXED_IMG:
        (tag === "vir" || tag === "wsj")
            ? `/images/us/${tag}/common/image_unavailable/case_unavailable.png`
            : "/images/us/en/common/default_mixed_case.jpg",
    DEFAULT_MIXED_IMG_ALT:
        "/images/us/common/default_mixed_bottle.png",
    DEFAULT_PREMIUM_IMG:
        " /images/us/common/default_premium.png"
}
