/*
	Wine Club Links
 */
const wineClubLinks = {
	us: {
		wsj: {
			unidentified: '/current-offers',
			lowPromo: '/vineyard-values',
			corePromo: '/upgradeoffer',
			highPromo: '/jsp/offer/wineclub/us/wsj/premier_club.jsp',
			noPromo: '/jsp/account/common/wp_summary.jsp',
			otherIdentified: '/upgradeoffer',
		},
		law: {
			unidentified: '/current-offers',
			lowPromo: '/essentials-club',
			corePromo: '/upgradeoffer',
			highPromo: '/jsp/offer/wineclub/us/law/reserve_club.jsp',
			noPromo: '/jsp/account/common/wp_summary.jsp',
			otherIdentified: '/upgradeoffer',
		},
		vir: {
			unidentified: '/current-offers',
			lowPromo: '/upgradeoffer',
			corePromo: '/upgradeoffer',
			highPromo: '/vir-vip-club',
			noPromo: '/jsp/account/common/wp_summary.jsp',
			otherIdentified: '/upgradeoffer',
		},
		mcy: {
			unidentified: '/current-offers',
			lowPromo: '/mcy-upgrade',
			corePromo: '/mcy-upgrade',
			highPromo: '/mcy-reserve',
			noPromo: '/jsp/account/common/wp_summary.jsp',
			otherIdentified: '/mcy-upgrade',
		},
		npr: {
			unidentified: '/current-offers',
			lowPromo: '/npr-upgrade',
			corePromo: '/npr-upgrade',
			highPromo: '/reserve',
			noPromo: '/jsp/account/common/wp_summary.jsp',
			otherIdentified: '/npr-upgrade',
		},
	},
};

export default wineClubLinks;
