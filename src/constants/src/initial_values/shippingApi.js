export default {
	shippingAddress: {
		address: {
			type: 'US',
			deliveryName: '',
			postalCode: '',
			houseNumber: '',
			flatNumber: '',
			address1: '',
			address2: '',
			city: '',
			town: '',
			county: '',
			country: 'US',
			deliveryInstructions: {
				deliveryInstruction: '',
				deliveryInstructionTxtLine1: '',
				deliveryInstructionTxtLine2: '',
				deliveryInstructionTxtLine3: '',
				deliveryInstructionTxtLine4: '',
			},
		},
		contactInfo: {
			email: '',
			companyName: '',
			deliveryPhoneNumber: '',
		},
		personalInfo: {
			salutation: '',
			firstName: '',
			lastName: '',
		},
		type: 'hardGoodShippingType',
	},
};
