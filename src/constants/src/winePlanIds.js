/**
 * Wine Plans ID / Business Partners
 * To control the cart Language
 */
export default [
	{
		ids: ['Alaska Airlines'],
		data: {
			price: 149.99,
			plusCopy:
				'plus applicable tax and $19.99 shipping, additional $75 shipping surcharge for deliveries to AK; (please allow 5-10 days for delivery depending on shipping state)',
		},
	},
	{
		ids: ['Southwest', 'British Airways'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['BHG', 'BHG_WD'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['BHG9BTL', 'BHG9WD'],
		data: {
			bottleAmount: '9-bottle selection',
			deliveryPeriod: '10 weeks',
			seasonalCases: '1 extra-special seasonal case',
			price: 114.99,
			plusCopy: 'plus $14.99 shipping & tax',
		},
	},
	{
		ids: ['4S', '4S_WD', '4SREWARD', '4SREWARDWD'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['MACYS', 'MACYS_WD'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['MACYSFS', 'MACYSFSWD'],
		data: {
			price: 159.99,
			bottleAmount: '9-bottle selection (plus 3 bonus bottles)',
			plusCopy: 'plus applicable tax. Shipping is free on all club cases',
		},
	},
	{
		ids: ['MCYRESERVE'],
		data: {
			price: 199.99,
		},
	},
	{
		ids: ['NatGeo', 'NATGEO_WD', 'NGREWARD', 'NGREWARDWD'],
		data: {
			price: 149.99,
			phoneNumber: '1-844-485-1027',
		},
	},
	{
		ids: ['NPR'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['TCM', 'TCM_WD'],
		data: {
			price: 149.99,
			phoneNumber: '1-844-946-3826',
			seasonalCases: '1 extra-special seasonal case',
		},
	},
	{
		ids: ['Explorer', 'THECLUB_WD'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['Discovery', 'DISC_WD', 'DISCREWARD', 'DISCREWRWD', 'DISCDIGTN'],
		data: {
			price: 159.99,
		},
	},
	{
		ids: ['Discover', 'Discover_WD'],
		data: {
			price: 159.99,
		},
	},
	{
		ids: ['LW_Reserve', 'ZgtReserve'],
		data: {
			price: 219.99,
		},
	},
	{
		ids: ['LWRESERVWD'],
		data: {
			price: 179.99,
		},
	},
	{
		ids: ['LW_ESSENT', 'ZGT_ESSENT'],
		data: {
			price: 119.99,
		},
	},
	{
		ids: ['4S_Six'],
		data: {
			price: 79.99,
			bottleAmount: '6-bottle selection',
		},
	},
	{
		ids: ['4S_Nine', '4S_NineWD'],
		data: {
			price: 114.99,
			plusCopy: 'plus $14.99 shipping & tax',
			seasonalCases: '1 extra-special seasonal case',
			deliveryPeriod: '10 weeks',
			bottleAmount: '9-bottle selection',
		},
	},
	{
		ids: ['Favorites'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['ZAGATWINE'],
		data: {
			price: 149.99,
		},
	},
	{
		ids: ['Premier'],
		data: {
			price: 259.99,
		},
	},
	{
		ids: ['WSJ_ESSENT'],
		data: {
			price: 119.99,
		},
	},
	{
		ids: ['VWVIP'],
		data: {
			price: 219.99,
		},
	},
	{
		ids: ['ExplorerFS'],
		data: {
			price: 149.99,
			plusCopy: 'plus applicable tax. Shipping is FREE',
		},
	},
	{
		ids: ['RedHeadLW', 'RedHeadWSJ'],
		data: {
			price: 149.99,
			caseEligibleCopy: '',
			plusCopy: 'plus $14.99 shipping & tax',
			seasonalCases: '',
			deliveryPeriod: '12 months',
			bottleAmount: '6-bottle selection',
		},
	},
	{
		ids: ['SELVLW'],
		data: {
			price: 155.99,
			plusCopy: 'plus $19.99 shipping & tax',
			seasonalCases: '',
			deliveryPeriod: '12 months',
			bottleAmount: '6-bottle selection',
		},
	},
	{
		ids: ['CNDP16WSJ', 'CNDP16LW'],
		data: {
			price: 269.94,
			plusCopy: 'plus $14.99 shipping & tax',
			deliveryPeriod: '12 months',
			bottleAmount: '6-bottle selection',
		},
	},
];
