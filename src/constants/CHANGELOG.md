# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/constants@1.0.2...@dw-us-ui/constants@1.0.3) (2021-07-08)


### Bug Fixes

* added missing styles and design tweaks ([57dcc84](https://bitbucket.org/directwinesweb/dw-us-ui/commits/57dcc840669eed1bc17e45d8265da85969b8b41b))






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/constants@1.0.1...@dw-us-ui/constants@1.0.2) (2021-07-02)


### Bug Fixes

* exported files ([de7e522](https://bitbucket.org/directwinesweb/dw-us-ui/commits/de7e5220b4723fd3d95788c9cc7f0013213ed9f4))





## 1.0.1 (2021-06-30)


### Bug Fixes

* down merge with develop ([4ebda58](https://bitbucket.org/directwinesweb/dw-us-ui/commits/4ebda5886377131d7c78d8b27db1af5960282401))
* initial commit on pdp pages ([ffa85f1](https://bitbucket.org/directwinesweb/dw-us-ui/commits/ffa85f10e235e715c80b1e02fd38532d81cefe5a))
* optimized code and ui fixes ([069b043](https://bitbucket.org/directwinesweb/dw-us-ui/commits/069b043299c9a184246ceff62ea4121a4170c922))
