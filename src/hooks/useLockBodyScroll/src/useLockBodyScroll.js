import { useLayoutEffect } from 'react';

// Hook
/**
 *
 * @param {Boolean} isLocked - if true prevent scrolling on mount
 */
const useLockBodyScroll = (isLocked) => {
	useLayoutEffect(() => {
		// Get original body overflow
		// const originalStyle = window.getComputedStyle(document.body).overflow;
		// edit - inconsistent with some modals, overriding for now
		if (isLocked) {
			// Prevent scrolling on mount
			document.body.style.overflow = 'hidden';
		} else {
			document.body.style.overflow = 'visible';
		}

		// Re-enable scrolling when component unmounts
		return () => (document.body.style.overflow = 'visible');
	}, [isLocked]); // Empty array ensures effect is only run on mount and unmount
};

export default useLockBodyScroll;
