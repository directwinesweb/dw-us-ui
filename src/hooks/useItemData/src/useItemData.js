import { useEffect, useState } from 'react';

import { FormatSkuData } from '@dw-us-ui/format-sku-data';
import PropTypes from 'prop-types';
import axios from 'axios';

const useItemData = (itemCode, formatSkus = true) => {
	const [itemData, setItemData] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			const data = await axios
				.get(`/api/product/item/${itemCode}`)
				.then((res) => {
					const response = res.data.response;
					return {
						product: response,
					};
				})
				.catch((err) => {
					const error = err.response;
					if (error) {
						return error;
					}
					return false;
				});
			setItemData(data);
		};

		if (itemCode) {
			fetchData();
		}
	}, [itemCode]);

	return formatSkus ? FormatSkuData([itemData]) : itemData;
};

useItemData.propTypes = {
	itemCode: PropTypes.string.isRequired,
	formatSkus: PropTypes.bool,
};

export default useItemData;
