import { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import axios from 'axios';

/**
 *
 * @param {Array} requests - can handle more than one api requests in order.
 * Not recommended for responses that depend on previous response in the sequence
 * @param {Boolean} trigger
 * @param {Function} setTrigger
 *
 * Hook will make an api call when triggered.
 * @returns
 * FinalResponse - if api call is successful response will be an array for when multiple requests, object for single request, otherwise it will be null
 * error - if api call is not successful error will be an object otherwise it will be null
 * isLoading - loading state that will be useful for UI
 * setTrigger is a function that will update the trigger value.
 */
const useAxios = (requests, trigger = false, setTrigger) => {
	const [response, setResponse] = useState(null);
	const [error, setError] = useState(null);
	const [isLoading, setIsLoading] = useState(null);

	useEffect(() => {
		let unmounted = false;
		let source = axios.CancelToken.source();
		const apiCall = () => {
			setIsLoading(true);

			axios
				.all(
					requests.map((request) => {
						return axios({ ...request, cancelToken: source.token });
					})
				)
				.then((res) => {
					if (!unmounted) {
						setResponse(res);
						setIsLoading(false);
						setTrigger(false);
					}
				})
				.catch((error) => {
					if (!unmounted) {
						setError(error);
						setResponse(null);
						setIsLoading(false);
						setTrigger(false);
					}
				});
		};
		if (trigger) {
			apiCall();
		}

		return () => {
			unmounted = true;
			source.cancel('Cancelling in cleanup');
		};
	}, [trigger]);
	let finalResponse = response;
	if (response && response.length === 1) {
		finalResponse = response[0];
	}
	return [finalResponse, error, isLoading];
};
export default useAxios;

useAxios.propTypes = {
	/**
	 * requests array that will be used to make the api call
	 */
	requests: PropTypes.arrayOf(
		PropTypes.exact({
			url: PropTypes.string.isRequired,
			method: PropTypes.oneOf(['post', 'get', 'put', 'delete']).isRequired,
			options: PropTypes.object,
		})
	).isRequired,
	/**
	 * used to make api call
	 */
	trigger: PropTypes.bool.isRequired,
	/**
	 * function that will handle the logic for the trigger value
	 */
	setTrigger: PropTypes.func.isRequired,
};
