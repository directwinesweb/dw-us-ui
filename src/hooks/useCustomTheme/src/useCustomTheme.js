import { lawTheme } from '@dw-us-ui/themes';
import { useTheme } from 'emotion-theming';

const useCustomTheme = () => {
	let theme = useTheme();

	if (typeof theme.name === 'undefined') {
		theme = lawTheme;
	}
	return theme;
};

export default useCustomTheme;
