import axios from 'axios';
import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

const useCaseData = (itemCode) => {
	const [caseData, setCaseData] = useState(false);

	/*
        ToDo : Need to add Pricing to case items
    */
	useEffect(() => {
		const fetchData = async () => {
			const data = await axios
				.get(`/api/product/case/${itemCode}`)
				.then((res) => {
					const response = res.data.response;
					return response;
				})
				.catch((err) => {
					const error = err.response;
					if (error) {
						return error;
					}
					return false;
				});
			setCaseData(data);
		};

		if (itemCode) {
			fetchData();
		}
	}, [itemCode]);

	return caseData;
};

useCaseData.propTypes = {
	itemCode: PropTypes.string.isRequired,
	formatSkus: PropTypes.bool,
};

export default useCaseData;
