import { useEffect, useState } from 'react';

import axios from 'axios';

/**
 *
 * @param {*} stateCode
 * @param {*} redirect - redirect flag
 */
const useStateProfile = (
	stateCode = false,
	isSundance = false,
	zipCode = null
) => {
	const [stateProfile, setStateProfile] = useState(false);
	const [invoked, setInvoked] = useState(false);
	const [fetching, setFetching] = useState(false);
	const [fetched, setFetched] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			setFetched(false);
			setFetching(true);
			const data = await axios
				.post(
					isSundance
						? '/api/user/profile/locationContext'
						: `/api/user/profile/state/${stateCode}`,
					isSundance ? { zipCode } : null
				)
				.then((res) => {
					setFetching(false);
					setFetched(true);
					const response = res.data;
					return response;
				})
				.catch((err) => {
					setFetching(false);
					const error = err.response;
					if (error) {
						return error;
					}
					return false;
				});

			if (data.statusCode === 0) {
				setStateProfile(true);
				setInvoked(false);
			} else {
				setStateProfile(false);
			}
		};

		if ((stateCode && !isSundance) || (zipCode && isSundance)) {
			setStateProfile(false);
			setInvoked(true);
			fetchData();
		} else {
			setStateProfile(false);
		}
		return () => {
			setInvoked(false);
		};
	}, [stateCode, zipCode, isSundance]);

	return { stateProfile, invoked, fetching, fetched };
};

export default useStateProfile;
