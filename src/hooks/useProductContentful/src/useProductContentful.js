import axios from 'axios';
import { BrandUtil } from '@dw-us-ui/brand-util';
import { useContentful } from '@dw-us-ui/use-contentful';
import { useEffect, useState } from 'react';

const useProductContentful = (itemCode) => {
    const [productConfig, setProductConfig] = useState({
        productConfig: {}
    });

    const brandTag =
            // eslint-disable-next-line no-undef
            typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
    const brand = BrandUtil.getBrand(brandTag);
    const tag = BrandUtil.content(brand).tag;

    const contentfulOptions = {
        spaceId: 'k1ni6e3k3b2n',
        productionToken:
            '0090c721437fa809001de135b0bc66ac81c9598f2092b9d513f693dd70d5f781',
        contentfulMethod: 'getEntries',
        entryId: false,
        includeValue: 3,
        queryObject: {
            content_type: 'product',
            'fields.itemCode': itemCode
        }
    };
    const { response, isLoading, error } = useContentful(contentfulOptions);

    useEffect(() => {
        if(response) {
            getProductConfig(response);
        }
        if(error) {
            setProductConfig({
                productConfig: {}
            });
        }
    }, [response, error]);

    const filterBrand = (obj, type) => {
        var arr = obj.filter((item) => {
            return item.brand.toLowerCase() === type.toLowerCase();
        });

        return arr;
    }

    const getProductData = ({
        stockMessage, 
        addOnPricing, 
        alternateWines, 
        flavorProfile, 
        customCaseContents, 
        productTargeter
    }) => {
        let axiosPromises = [];
        let caseContents = [];
        customCaseContents.forEach((item) => {
            axiosPromises.push(axiosCalls(item));
        });

        axios.all(axiosPromises).then((results) => {
            results.forEach((response) => {
                if (response !== false) {
                    let data = response.data;
                    caseContents.push({
                        skuItemCode: false,
                        quantity: false,
                        referencedProduct: data.response
                    });
                }
            });
            customCaseContents.forEach((obj) => {
                caseContents.forEach((item) => {
                    if (obj.itemCode === item.referencedProduct.itemCode) {
                        item.skuItemCode = obj.itemCode;
                        item.quantity = obj.quantity;
                    }
                });
            });
            
            let productConfig = {
                addOnPricing,
                alternateWines,
                caseContents,
                flavorProfile,
                productTargeter,
                stockMessage
            }

            setProductConfig({productConfig});
        });
    }

    const axiosCalls = (item) => {
        let itemCode = item.itemCode;

        let url = '/api/product/item/' + itemCode;
        //Push Product API Calls
        return axios
            .get(url)
            .then((response) => {
                return response;
            })
            .catch(() => {
                return false;
            });
    }
    
    const getProductConfig = (response) => {
        if(response?.items?.length) {
            const fields = response.items[0].fields;
            const stockConfig = fields?.stockOverrideMessage?.stockOverrideMsg;
            const addOnConfig = fields?.addOnPricing?.addOnPrices;
            const alternateConfig = fields?.alternateWines?.alternateWineDetails;
            const productTargeterConfig = fields?.addOnTargeter?.sys?.id;
            const flavorProfileConfig = fields?.flavorProfileContent?.sys?.id;
            let customCaseContentsConfig = [];
            if (typeof fields.customCaseContents !== 'undefined') {
                customCaseContentsConfig = fields?.customCaseContents?.customCaseContentsDetails;
            }

            let stockMessage = false;
            let addOnPricing = false;
            let alternateWines = false;
            let flavorProfile = false;
            let customCaseContents = false;
            let productTargeter = false;

            if (response.hasOwnProperty('includes')) {
                for(let i = 0; i < response.includes.Entry.length; i++) {
                    if(response.includes.Entry[i].sys.id === flavorProfileConfig) {
                        flavorProfile = response.includes.Entry[i].fields.content;
                    }

                    if(response.includes.Entry[i].sys.id === productTargeterConfig) {
                        productTargeter = response.includes.Entry[i].fields;
                    }
                }
            }

            if (stockConfig?.length) {
                stockMessage = filterBrand(stockConfig, tag);
                if (!stockMessage.length) {
                    stockMessage = filterBrand(stockConfig, 'All');
                }

                stockMessage = stockMessage.length ? stockMessage[0].stockMsg : false;
            }

            if (addOnConfig?.length) {
                addOnPricing = filterBrand(addOnConfig, tag);
                if (!addOnPricing.length) {
                    addOnPricing = filterBrand(addOnConfig, 'All');
                }

                addOnPricing = addOnPricing.length ? addOnPricing[0] : false;
            }

            if (alternateConfig?.length) {
                alternateWines = filterBrand(alternateConfig, tag);
                if (!alternateWines.length) {
                    alternateWines = filterBrand(alternateConfig, 'All');
                }
                alternateWines = alternateWines[0]?.hasOwnProperty('alternates') ? alternateWines[0].alternates : [];
            }

            if (customCaseContentsConfig?.length) {
                customCaseContents = filterBrand(customCaseContentsConfig, tag);
                if (!customCaseContents.length) {
                    customCaseContents = filterBrand(customCaseContentsConfig, 'All');
                }
                customCaseContents = customCaseContents[0]?.hasOwnProperty('customCaseContents')
                    ? customCaseContents[0].customCaseContents
                    : [];
            }

            if (customCaseContents?.length) {
                let config = {
                    stockMessage, 
                    addOnPricing, 
                    alternateWines, 
                    flavorProfile, 
                    customCaseContents, 
                    productTargeter
                }
                getProductData(config);
            } else {
                let productConfig = {
                    stockMessage,
                    addOnPricing,
                    alternateWines,
                    flavorProfile,
                    productTargeter
                }

                setProductConfig({productConfig});
            }
        } else {
            setProductConfig({
                productConfig: {}
            });
        }
    }

    return productConfig;
};

export default useProductContentful;
