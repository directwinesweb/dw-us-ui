import { GetEnvironment } from '@dw-us-ui/get-environment';
import { unlimitedItemCodes } from '@dw-us-ui/constants';
import { useCustomTheme } from '@dw-us-ui/use-custom-theme';
import { useItemData } from '@dw-us-ui/use-item-data';

/**
 * @param {string} - env(staging, production) - To choose env for unlimited Itemcodes
 * @param {number} - offerUnlimited - To chooose the Unlimited Type
 */
const useUnlimitedInfo = (offerUnlimited = '', envOverride = false) => {
	const theme = useCustomTheme();
	const { data } = theme;
	const { url, unlimited, unlimitedFull, tag, phone, email } = data;

	let unlimitedType;
	const env = envOverride || GetEnvironment();
	switch (offerUnlimited) {
		case 1:
		case '':
			unlimitedType = 'unlimited';
			break;
		case 2:
			unlimitedType = 'promotional';
			break;
		case 3:
			unlimitedType = 'half';
			break;
		default:
			unlimitedType = false;
			break;
	}
	//Will need other types eventually
	//TODO more unlimited products

	const itemCode = unlimitedItemCodes[env][tag][unlimitedType] || false;
	const itemData = useItemData(itemCode, false);
	if (itemData) {
		const { product } = itemData;
		if (product) {
			const { skus, smallImage, largeImage, thumbnailImage } = product;

			if (skus) {
				return skus[0]
					? {
							brand: tag,
							brandPhone: phone,
							email,
							itemCode,
							unlimitedPrice: skus[0].listPrice,
							unlimitedType,
							shortText: unlimited,
							fullText: unlimitedFull,
							images: {
								small: url + smallImage,
								large: url + largeImage,
								thumb: url + thumbnailImage,
							},
					  }
					: false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
};

export default useUnlimitedInfo;
