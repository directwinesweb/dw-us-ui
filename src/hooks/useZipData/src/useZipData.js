import { useEffect, useState } from 'react';

import axios from 'axios';

/**
 * @param {string} - zipCode
 * Zipcode Hook to grab zipcode info
 */
const useZipData = (zipCode = '') => {
	const initialState = {
		city: false,
		finalZip: false,
		stateCode: false,
		country: false,
		error: false,
	};
	const [zipData, setZipData] = useState(initialState);

	useEffect(() => {
		const fetchData = async () => {
			setZipData({
				city: false,
				state: false,
				finalZip: false,
				stateCode: false,
				country: false,
			});
			const data = await axios
				.get(`/api/address/zipcode/${zipCode}`)
				.then((res) => {
					const response = res.data.response;
					const { zipCode, city, stateCode, country, stateName } = response;
					if (zipCode && city && stateCode && country && stateName) {
						return {
							city,
							state: stateName,
							stateCode,
							country,
							finalZip: zipCode,
						};
					} else {
						return {
							error:
								'An error has occurred with your zip code, please try another.',
						};
					}
				})
				.catch((err) => {
					const error = err.response;
					if (error) {
						return {
							error: 'The zip code is invalid, please check and try again.',
						};
					}
				});
			setZipData(data);
		};

		if (zipCode.length === 5) {
			fetchData();
		} else if (zipCode.length === 0) {
			setZipData({
				city: false,
				state: false,
				finalZip: false,
				stateCode: false,
				country: false,
			});
		}
	}, [zipCode]);

	return zipData;
};

export default useZipData;
