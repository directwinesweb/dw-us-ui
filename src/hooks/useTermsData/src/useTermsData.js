import axios from 'axios';
import { useState, useEffect } from 'react';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { useContentful } from '@dw-us-ui/use-contentful'

/**
 * Terms API Logic
 * logic copied from us_compliance.js from dwint_store.war
 * @todo add CART logic?
 * @todo add logic for other pages besides checkout
 */
const useTermsData = () => {
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
    const brand = BrandUtil.getBrand(brandTag);
	const tag = BrandUtil.content(brand).tag;
	const { response, isLoading, error } = useContentful({
		spaceId: 'k1ni6e3k3b2n',
		contentfulMethod: 'getEntries',
		entryId: false,
		includeValue: 10,
		productionToken: '0090c721437fa809001de135b0bc66ac81c9598f2092b9d513f693dd70d5f781',
		queryObject: {
			content_type: 'contentPage',
			'fields.slug[in]': `${tag}/terms-and-conditions,${tag}/privacy-policy`,
			include: '10'
		}
	});

    useEffect(() => {
		if(response) {
			contenfulPrivacyPolicy(response);
		}
		if(error) {
			setTermsUpdated('');
		}
    }, [response, error]);
	
	const [showTerms, setShowTerms] = useState('');
	const [termsUpdated, setTermsUpdated] = useState('');
	const [termsData, setTermsData] = useState([]);
	
	//Versions
	const currentVersion = {
		law: '6',
		npr: '6',
		vir: '6',
		wsj: '7'
	};
	const currentVersionCart = '1';
	//Key
	const key = '9b7cb312-7973-42c8-86ab-c2250c9f063b:';
	const encodedKey = 'Basic ' + btoa(key);
	//URL
	const termsApi = 'https://dwi-api-terms.herokuapp.com/api/legal/';

	//Create Section
	const createSection = (page) => {
		if (page === 'account') {
			return 'My_Wine_Cellar';
		} else if (page === 'checkout') {
			return 'Main_Checkout';
		} else if (page === 'landing_page') {
			return 'Landing_Page';
		}
	};

	/**
	 *
	 * @param {arr} arr - Data from the Terms API for the list of terms versions
	 */
	useEffect(() => {
		if (termsData.length >= 1) {
			const latestLegalEntry = termsData[0];
			sessionStorage.setItem('termsUser', latestLegalEntry.id);
			const existingVersions = latestLegalEntry.versions;
			sessionStorage.setItem(
				'existingVersions',
				JSON.stringify(existingVersions)
			);
			if (Array.isArray(existingVersions)) {
				// If array exists
				let copyExistingVersions = [...existingVersions];
				let lastAgreedVersion = copyExistingVersions.pop()?.version;
				currentVersion[tag] = termsUpdated;

				if (termsUpdated == lastAgreedVersion) {
					sessionStorage.setItem('existingTerms', true);
					setShowTerms(false);
				}
				else {
					sessionStorage.setItem('existingTerms', true);
					setShowTerms(true);
				}
			} else {
				setShowTerms(true);
				sessionStorage.setItem('existingTerms', true);
			}
		} else {
			sessionStorage.setItem('existingTerms', false);
			setShowTerms(true);
		}
	}, [termsData, termsUpdated]);

	const contenfulPrivacyPolicy = (response) => {
		if(response) {
			let filteredArray = response?.includes?.Entry?.filter(
				content => content?.sys?.contentType?.sys?.id === 'legalBlock'
			).map(
				val => val?.fields?.version
			);
			let recentVersion = filteredArray[0] > filteredArray[1]
				? filteredArray[0]
				: filteredArray[1];

			setTermsUpdated(recentVersion?.toString());
		}
	}

	/**
	 *
	 * @param {number} profileID - User Profile Id
	 * Check if the Users exists in the Terms DB
	 */
	const getTermsUser = (profileID) => {
		const fetchTerms = async () => {
			const data = await axios
				.get(`${termsApi}query?where={"atg_profile":"${profileID}"}`, {
					headers: {
						Authorization: encodedKey,
					},
				})
				.then((res) => {
					setTermsData(res.data.legals);
					return;
				})
				.catch((err) => {
					console.log(err);
				});
			return data;
		};

		if (profileID) {
			fetchTerms();
		}
	};

	/**
	 *
	 * @param {string} page - the current page the user is on
	 * @param {string} brand - the current brand the user is on
	 * @param {string} profileID - User Profile Id
	 * @param {string} newVersion - Legal contents latest version
	 * Create new Terms user
	 */
	const createTeamsUser = async (page, brand, profileID, newVersion) => {
		//Create new versions array with date for terms
		var currentDate = new Date();
		var versionArray = [];
		var versionObj = {
			version: newVersion || currentVersion[tag] || '1',
			date: new Date(currentDate).toLocaleString(),
		};
		versionArray.push(versionObj);

		//Create Data JSON
		const data = JSON.stringify({
			atg_profile: profileID.toString(),
			customer_opt_in: 'yes',
			created_at: currentDate,
			updated_at: currentDate,
			brand: brand.toUpperCase(),
			section: createSection(page),
			version: newVersion || currentVersion[tag] || '1',
			versions: versionArray,
		});

		await axios
			.post(termsApi, data, {
				headers: {
					Authorization: encodedKey,
					'Content-Type': 'application/json; charset=utf-8',
				},
			})
			.then(() => {
				sessionStorage.removeItem('termsChecked');
				sessionStorage.removeItem('existingTerms');
				return;
			});
	};

	/**
	 *
	 * @param {string} page - the current page the user is on
	 * @param {string} newVersion - Legal contents latest version
	 * Update a current user terms info
	 */
	const updateTermsUser = (page, newVersion) => {
		// Retrieve the object from storage
		const currentId = sessionStorage.getItem('termsUser');
		//If there is an array of versions (from filterResponse) in sessionStorage, use that.
		//Otherwise, use a blank array
		let versionsArray = [];
		if (sessionStorage.getItem('existingVersions') !== 'undefined') {
			versionsArray = JSON.parse(sessionStorage.getItem('existingVersions'));
		}
		//Create Updated Versions Object
		var currentDate = new Date();
		var versionObj = {
			version: newVersion || currentVersion[tag] || '1',
			date: new Date(currentDate).toLocaleString(),
		};
		versionsArray.push(versionObj);
		const data = JSON.stringify({
			section: createSection(page),
			version: newVersion || currentVersion[tag] || '1',
			versions: versionsArray,
			updated_at: currentDate
		});

		axios
			.put(termsApi + currentId, data, {
				headers: {
					Authorization: encodedKey,
					'Content-Type': 'application/json; charset=utf-8',
				},
			})
			.then(() => {
				if (page === 'checkout') {
					sessionStorage.removeItem('termsChecked');
				}
			});
	};

	/**
	 *
	 * @param {string} page - the current page the user is on
	 * @param {string} brand - the current brand the user is on
	 * @param {string} profileID - User Profile Id
	 * @param {string} version - Legal contents latest version
	 * Decide which path the user will go into update or create terms flows
	 */
	const setTermsUser = (page, brand, profileID, version) => {
		let isSundance = window.location.host.split('.')[0] === 'sun-uat',
			isProduction = window.location.host.split('.')[0] === 'www';

		if (
			sessionStorage.getItem('termsChecked') === 'true' &&
			(isProduction || isSundance)
		) {
			if (sessionStorage.getItem('existingTerms') === 'true' || JSON.parse(sessionStorage.getItem('existingVersions'))?.length > 0) {
				updateTermsUser(page, version);
			} else if (profileID) {
				createTeamsUser(page, brand, profileID, version);
			} else {
				sessionStorage.removeItem('termsChecked');
			}
		}

		if (!isProduction && !isSundance) {
			sessionStorage.removeItem('termsChecked');
		}
	};

	return {
		showTerms,
		getTermsUser,
		setTermsUser,
		setShowTerms,
	};
};

export default useTermsData;
