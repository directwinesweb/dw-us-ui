import useContentfulPreview from '../src/useContentfulPreview';

describe('useContentfulPreview test', () => {
	const originalLocation = window.location;

	beforeEach(() => {
		delete window.location;
		window.location = {
			href: '',
		};

		const url =
			'http://test.com/?abc=4&contentful_dw_preview=asd3Daf32vb&no_value=';

		Object.defineProperty(window, 'location', {
			value: new URL(url),
		});
		window.location.href = url;
	});

	afterAll(() => {
		window.location.href = originalLocation;
	});
	it('returns true for preview and sysId', () => {
		let { isPreview, sysId } = useContentfulPreview('abc');
		expect(isPreview).toEqual(true);
		expect(sysId).toEqual('4');
	});
	it('returns true for preview and sysId', () => {
		let { isPreview, sysId } = useContentfulPreview('contentful_dw_preview');
		expect(isPreview).toEqual(true);
		expect(sysId).toEqual('asd3Daf32vb');
	});
	it('returns false for preview and empty sysId', () => {
		let { isPreview, sysId } = useContentfulPreview('contentful');
		expect(isPreview).toEqual(false);
		expect(sysId).toEqual('');
	});
	it('returns false for preview and empty sysId for no value', () => {
		let { isPreview, sysId } = useContentfulPreview('no_value');
		expect(isPreview).toEqual(false);
		expect(sysId).toEqual('');
	});
});
