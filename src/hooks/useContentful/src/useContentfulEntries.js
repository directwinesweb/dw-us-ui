import { useEffect, useState } from 'react';

import useContentful from './useContentful';

/**
 *
 * @param {String} contentType
 * @param {String} environment - contentful space environment
 * @param {String} brand
 * @param {Object} additionalFilters - any additional filters
 * @param {Object} contentfulProperties = properties to pass to useContentful hook
 *
 * @returns
 * available entries from query for a brand,
 */
const useContentfulEntries = ({
	contentType,
	environment = 'master',
	additionalFilters = {},
	contentfulProperties = {},
}) => {
	const [items, setItems] = useState(null);

	const contentfulQuery = {
		content_type: contentType,
		include: '4',
		...additionalFilters,
	};

	const { response, error, isLoading } = useContentful({
		spaceId: 'k1ni6e3k3b2n',
		productionToken: 'wvjyW-zr51YWLmFHfHAyq8hYqwIQC2OuaJFuMl8xKrw',
		previewToken: 'C4twFy9_lOdTcwrqh2yloRwl-TpH_VwcZE0sgiqVU1M',
		environment,
		contentfulMethod: 'getEntries',
		entryId: false,
		includeValue: 10, //- doesnt do anything for entries
		queryObject: contentfulQuery,
		...contentfulProperties,
	});

	useEffect(() => {
		if (!isLoading) {
			if (response && !error) {
				// if fetching preview data prioritize that entry
				if (response.items.length > 1) {
					let contentFound = false;
					let contentItems = response.items;

					contentItems.forEach((contentItem) => {
						if (contentItem?.fields?.endDate) {
							// item has valid fields and an end date
							let date = new Date();
							if (
								new Date(contentItem.fields.endDate) > date &&
								new Date(contentItem.fields.startDate) < date
							) {
								contentFound = true;
								setItems(contentItem.fields);
							}
						}
					});
					// If no valid item can be found, check for ignoreEndDate field
					// If no ignoreEndDate can be found, set item to be last edited item

					if (!contentFound) {
						let ignoreEndDateItems = response.items.filter((item) => {
							return item.fields.ignoreEndDate;
						});
						if (ignoreEndDateItems.length) {
							setItems(ignoreEndDateItems[0].fields);
						} else {
							let lastIndex = contentItems.length - 1;
							setItems(contentItems[lastIndex].fields);
						}
					}
				} else if (response.items.length === 1) {
					// If only one item in the return, use that item

					setItems(response.items[0].fields);
				}
			} else if (!response && error) {
				console.error(error);
			}
		}
	}, [response, error, isLoading]);

	return items;
};

export default useContentfulEntries;
