import { useEffect, useState } from 'react';
const contentful = require(`contentful`);

/**
 *
 * @param {String} spaceId -  This is the space ID. A space is like a project folder in Contentful terms
 * @param {String} accessToken - This is setup to hold onto the FAQ config - but we should deprecate this after.
 * @param {String} previewToken - This is the preview token to be able to pull in content that hasnt been published and is in draft mode.
 * @param {String} environment - the environment to query
 * @param {String} productionToken - This is the production token to be pull in content that HAS been published.
 * @param {String} contentfulMethod - Default is getEntry, if getEntries is passed - can pull many entries using a search query
 * @param {String} host - cdn.contentful.com is the default, if pass in preview.contentful.com in conjunction with preview access token, you can get draft content
 * @param {String} entryId - If known to get exact entry
 * @param {Number} includeValue - number between 0 and 10 default to 1
 * @param {Number} queryObject - can pass in different contentTypes, fields, slugs, etc. Refer to documentation below for more info
 *
 * Note: The include parameter resolves links between entries and assets within a space. Links between content types within a space are not included in the response.
 * token in the Contentful web app
 * SDK
 * https://contentful.github.io/contentful.js/contentful/7.14.8/
 *
 * @returns the response from contentful - > Note: this hook is only for a single entry Id. Will need to expand to multiple entries
 */

// the two methods
// client.getEntries() - getEntries
// client.getEntry() - 1 entry

const useContentful = ({
	spaceId,
	productionToken,
	previewToken,
	environment = 'master',
	contentfulMethod = 'getEntry',
	entryId = false,
	includeValue = 1,
	queryObject = { contentType: '', 'fields.slug': '' },
	isProduction = true,
}) => {
	// Always expect an array from response
	const [response, setResponse] = useState(null);
	const [error, setError] = useState(false);
	const [isLoading, setIsLoading] = useState(true);

	let host, accessToken;

	if (!isProduction) {
		accessToken = previewToken;
		host = 'preview.contentful.com';
	} else {
		accessToken = productionToken;
		host = 'cdn.contentful.com';
	}

	const client = contentful.createClient({
		space: spaceId,
		accessToken,
		host: host,
		environment,
	});

	const resolveResponse = (response) => {
		setResponse(response);
		setIsLoading(false);
		setError(null);
	};

	const resolveError = (error) => {
		setError(error);
		setResponse(null);
		setIsLoading(false);
	};
	useEffect(() => {
		if (entryId && contentfulMethod === 'getEntry') {
			setIsLoading(true);
			client
				.getEntry(entryId, { include: includeValue })
				.then((entryResponse) => resolveResponse(entryResponse))
				.catch((entryError) => resolveError(entryError));
		}

		if (!entryId && contentfulMethod === 'getEntries') {
			setIsLoading(true);
			client
				.getEntries(queryObject)
				.then((entriesResponse) => resolveResponse(entriesResponse))
				.catch((entriesError) => resolveError(entriesError));
		}
	}, [entryId, contentfulMethod]);
	return { response, error, isLoading };
};

export default useContentful;
