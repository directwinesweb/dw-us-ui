import { useEffect, useState } from 'react';
import useContentful from './useContentful';

const useLegalContentful = (slug = '') => {
    const [pageConfig, setPageConfig] = useState({
		contentfulData: {},
		isContentLoading: true,
    });
    
    const object = {
		content_type: 'contentPage',
		'fields.slug': slug,
		include: 10,
	};

	const contentfulOptions = {
		spaceId: 'k1ni6e3k3b2n',
		productionToken:
			'0090c721437fa809001de135b0bc66ac81c9598f2092b9d513f693dd70d5f781',
		previewToken:
			'c449e56576102b809549fcb9fb3ca5cc7cc2f6fcb27d15100a7e146bdb816857',
		contentfulMethod: 'getEntries',
		entryId: false,
		includeValue: 10, //- doesnt do anything
		queryObject: object,
		isProduction: true,
	};

    const { response, isLoading, error } = useContentful(contentfulOptions);

	useEffect(() => {
        if(response) {
            getLegalData(response);
        }
        if(isLoading) {
            setPageConfig((prevState) => {
                return {
                    ...prevState,
                    contentfulData: {},
                    isContentLoading: isLoading
                };
            });
        }
        if(error) {
            setPageConfig((prevState) => {
                return {
                    ...prevState,
                    contentfulData: {},
                    isContentLoading: isLoading
                }
            })
        }
    }, [response, isLoading, error]);

    const getLegalData = (response) => {
        if(response?.items?.length) {
            let { fields } = response.items[0];
            let legalData = fields?.contentBlocks[0]?.fields?.blocks[0]?.fields;
            let contentfulData = {
                fields,
                legalData
            }

            setPageConfig((prevState) => {
                return {
                    ...prevState,
                    contentfulData,
                    isContentLoading: isLoading,
                };
            });
        } 

        return;
    }

    return pageConfig;
};

export default useLegalContentful;
