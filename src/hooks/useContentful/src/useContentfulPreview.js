import { GetParam } from '@dw-us-ui/get-param';

/**
 *
 * @param {String} queryParam - parameter key to find in url
 * Checks params and determines to use preview or not
 * @returns {Object} - isPreview and SysId, after checking parameter a sysId value must be present
 */

const useContentfulPreview = (queryParam = '') => {
	let isPreview = false;
	let sysId = '';

	if (queryParam?.length) {
		let foundParam = GetParam(queryParam);

		if (foundParam?.length) {
			isPreview = true;
			sysId = foundParam;
		}
	}

	return { isPreview, sysId };
};

export default useContentfulPreview;
