# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.1](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.1.0...@dw-us-ui/use-contentful@1.1.1) (2021-07-08)


### Bug Fixes

* removed forced brand filter ([b4d8df8](https://bitbucket.org/directwinesweb/dw-us-ui/commits/b4d8df8cdec569a60a862ca03c04fc95c5675118))






# [1.1.0](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.9...@dw-us-ui/use-contentful@1.1.0) (2021-07-02)


### Bug Fixes

* down merge master ([174f206](https://bitbucket.org/directwinesweb/dw-us-ui/commits/174f206b37d77cf305cac57abb6bfc780074f8f1))
* updated usecontentfulPreview to search for given key ([4370959](https://bitbucket.org/directwinesweb/dw-us-ui/commits/43709599c9cc7452609b6273e0e1618345e5db80))


### Features

* added useContentfulPreview and updated contentful logic and bundle exports ([7f793f9](https://bitbucket.org/directwinesweb/dw-us-ui/commits/7f793f9855bae0faecb188c02c3d2b342ec08f8c))





## [1.0.9](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.8...@dw-us-ui/use-contentful@1.0.9) (2021-06-30)


### Bug Fixes

* updated use contentful to receive environment ([3bd6cbb](https://bitbucket.org/directwinesweb/dw-us-ui/commits/3bd6cbb2ff0f97e1248342a9002332f8cf621bf3))






## [1.0.8](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.7...@dw-us-ui/use-contentful@1.0.8) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## [1.0.7](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.5...@dw-us-ui/use-contentful@1.0.7) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## [1.0.6](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.5...@dw-us-ui/use-contentful@1.0.6) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## [1.0.5](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.4...@dw-us-ui/use-contentful@1.0.5) (2021-05-10)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## [1.0.4](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.3...@dw-us-ui/use-contentful@1.0.4) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.2...@dw-us-ui/use-contentful@1.0.3) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/use-contentful@1.0.1...@dw-us-ui/use-contentful@1.0.2) (2021-03-25)

**Note:** Version bump only for package @dw-us-ui/use-contentful





## 1.0.1 (2021-03-09)

**Note:** Version bump only for package @dw-us-ui/use-contentful
