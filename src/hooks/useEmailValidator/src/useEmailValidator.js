import { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import axios from 'axios';
import { emailRegex } from '@dw-us-ui/constants';

const useEmailValidator = (email) => {
	const [emailExists, setEmailExists] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			const data = await axios
				.post(`/api/account/email`, { email: email })
				.then((res) => {
					const response = res.data;
					return response;
				})
				.catch((err) => {
					const error = err.response;
					if (error) {
						return error;
					}
					return false;
				});
			if (data.statusCode === 0) {
				setEmailExists(true);
			} else {
				setEmailExists(false);
			}
		};

		if (email && emailRegex.test(String(email).toLowerCase())) {
			fetchData();
		} else {
			setEmailExists(false);
		}
	}, [email]);

	return emailExists;
};

useEmailValidator.PropTypes = {
	/**
	 * Email address to pass to API
	 */
	email: PropTypes.string,
};

export default useEmailValidator;
