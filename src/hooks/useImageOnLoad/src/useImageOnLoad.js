import { useEffect, useState } from 'react';

/**
 *
 * @param {Object}
 *                  @param windowWidth - the window width
 *                  @param image - image src
 *                  @param mobileImage - mobile image source
 * @returns appropriate image src based on windowWidth
 */
const useImageOnLoad = ({ windowWidth = 769, image, mobileImage = false }) => {
	const [imageState, setImageState] = useState(false);

	useEffect(() => {
		const imageLoader = new Image();
		const imgSrc = windowWidth < 768 && mobileImage ? mobileImage : image;
		imageLoader.src = imgSrc;
		imageLoader.onload = () => {
			setImageState(imgSrc);
		};
	}, [image, mobileImage, windowWidth]);

	return imageState;
};
export default useImageOnLoad;
