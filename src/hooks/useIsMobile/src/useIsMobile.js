import { useEffect, useState } from 'react';

/**
 *
 * @param {Number} - width to check
 * @returns {Boolean} - if window is less than given param
 */
const useIsMobile = (width = 767) => {
	const [isMobile, setIsMobile] = useState();
	useEffect(() => {
		window.addEventListener('resize', () => {
			setIsMobile(window.outerWidth > width);
		});
	}, []);
	return isMobile;
};

export default useIsMobile;
