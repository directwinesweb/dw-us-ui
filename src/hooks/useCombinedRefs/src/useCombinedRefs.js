import { useEffect, useRef } from 'react';

import PropTypes from 'prop-types';

/**
 * This hook facilitates usage of multiple refs in concert with each other
 * @param  {...any} refs Accepts one or more refs to link
 * @returns {object or function} Returns a new ref that will update each linked ref any time one ref changes
 */
const useCombinedRefs = (...refs) => {
	// create an intermediary ref
	const targetRef = useRef();
	// bubble changes up to each linked ref whenever intermediary updates
	useEffect(() => {
		for (let i = 0; i < refs.length; i++) {
			let ref = refs[i];
			if (!ref) return;

			if (typeof ref === 'function') {
				ref(targetRef.current);
			} else {
				ref.current = targetRef.current;
			}
		}
	}, [refs]);
	// use this ref in your target component to link your additional refs
	return targetRef;
};

useCombinedRefs.PropTypes = {
	/**
	 * An array of refs to synchronize
	 */
	refs: PropTypes.array,
};

export default useCombinedRefs;
