import { useEffect } from 'react';

/**
 *
 * @param {String} expirationDate  = input expiration date value
 * @param {Function} setValue - react hook form set value method
 */
const useExpirationDateInput = (expirationDate = '', setValue) => {
	let updatedDateInput = expirationDate;
	if (updatedDateInput.length === 2) {
		updatedDateInput += '/';
	}

	useEffect(() => {
		const dateLength = 5;
		const updatedDate = updatedDateInput.substring(0, dateLength);
		setValue('expirationDate', updatedDate);
	}, [setValue, updatedDateInput]);
};

export default useExpirationDateInput;
