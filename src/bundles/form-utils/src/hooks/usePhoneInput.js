import { FormatPhone } from '@dw-us-ui/format-phone';
import { useEffect } from 'react';
/**
 *
 * @param {String} phoneInput - phone number input value
 * @param {*} setValue - react hook form set value
 * @param {*} name - phone number input name
 */
const usePhoneInput = (phoneInput = '', setValue, name) => {
	let updatedPhoneInput = FormatPhone(phoneInput);

	useEffect(() => {
		const phoneNumberLength = 12;
		const updatedPhone = updatedPhoneInput.substring(0, phoneNumberLength);
		setValue(name, updatedPhone);
	}, [setValue, updatedPhoneInput]);
};

export default usePhoneInput;
