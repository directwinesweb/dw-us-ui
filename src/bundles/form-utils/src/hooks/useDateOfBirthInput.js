import { useEffect } from 'react';

const useDateOfBirthInput = (DateInput = '', setValue) => {
	let updatedDateInput = DateInput;
	if (updatedDateInput.length === 2 || updatedDateInput.length === 5) {
		updatedDateInput += '/';
	}

	useEffect(() => {
		const dateLength = 10;
		const updatedDate = updatedDateInput.substring(0, dateLength);
		setValue('date', updatedDate);
	}, [setValue, updatedDateInput]);
};

export default useDateOfBirthInput;
