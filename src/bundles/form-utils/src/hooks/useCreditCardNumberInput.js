import { useEffect } from 'react';

const useCreditCardNumberInput = (
	creditCardNumber = '',
	cardType,
	setValue
) => {
	let updatedCreditCardNumberInput = creditCardNumber;
	let updatedCardType = cardType;

	let creditCardNumberLength;
	if (updatedCardType === 'AmericanExpress') {
		creditCardNumberLength = 17;
		updatedCreditCardNumberInput.length === 4 ||
		updatedCreditCardNumberInput.length === 11
			? (updatedCreditCardNumberInput += ' ')
			: null;
	} else {
		creditCardNumberLength = 19;
		if (
			updatedCreditCardNumberInput.length === 4 ||
			updatedCreditCardNumberInput.length === 9 ||
			updatedCreditCardNumberInput.length === 14
		) {
			updatedCreditCardNumberInput += ' ';
		}
	}
	useEffect(() => {
		const updatedCreditCardNumber = updatedCreditCardNumberInput.substring(
			0,
			creditCardNumberLength
		);
		setValue('creditCardNumber', updatedCreditCardNumber);
	}, [
		setValue,
		updatedCreditCardNumberInput,
		creditCardNumberLength,
		updatedCardType,
	]);
};

export default useCreditCardNumberInput;
