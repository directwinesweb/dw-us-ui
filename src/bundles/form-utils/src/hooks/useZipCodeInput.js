import { useEffect } from 'react';
import { useZipData } from '@dw-us-ui/use-zip-data';

/**
 * Get City And State From ZIP Code
 * @param {String} zipCode - zip code input value
 * @param {Function} setValue - react hook form method
 * @param {Function} setError - react hook form method
 * @param {Function} clearError  - react hook form method
 * @param {Function} validateAPI  - fetch use zip data response
 */
const useZipCodeInput = (zipCode = '', setValue, setError, clearError) => {
	let data = useZipData(zipCode);

	useEffect(() => {
		const { city, country, stateCode, state, finalZip } = data;
		if (finalZip !== false) {
			clearError('zipCode');
			clearError('city');
			clearError('stateCode');
			setValue('city', city);
			setValue('stateCode', stateCode);
			setValue('country', country);
			setValue('state', state);
		} else if (zipCode.length && finalZip === false) {
			setValue('city', '');
			setValue('stateCode', '');
			setValue('state', '');
		}
	}, [clearError, data, setValue, setError, zipCode]);

	useEffect(() => {
		const { finalZip } = data;
		if (zipCode.length === 0 && finalZip === false) {
			setValue('city', '');
			setValue('stateCode', '');
			setValue('state', '');
		}
	}, [zipCode, setValue, data]);
};

export default useZipCodeInput;
