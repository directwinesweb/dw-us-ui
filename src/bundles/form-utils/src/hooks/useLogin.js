import { useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import axios from 'axios';

/**
 *
 * @param {Object} userData
 * @returns loggedState
 */
const useLogin = (userData) => {
	const [loggedStateData, setLoggedStateData] = useState({
		isLoggedIn: false,
		response: null,
		loading: false,
		message: null,
	});

	useEffect(() => {
		if (userData) {
			setLoggedStateData((prevState) => ({
				...prevState,
				loading: true,
			}));
			axios
				.post('/api/authentication/login', userData)
				.then((res) => {
					setLoggedStateData({
						isLoggedIn: true,
						response: res,
						loading: false,
						message: '',
					});
				})
				.catch((err) => {
					let error = err.response ? err.response : null,
						message =
							'Sorry, it looks like something went wrong. Please try re-entering your information.';
					if (error) {
						// use message from backend
						message = error.data.statusMessage;
						//TODO REMOVE WHEN BACKEND UPDATES - NEED FOR TESTING SIGNOFF
						message.includes('maximum no of login attempts') ||
						message.includes('failed attempts')
							? (message = `You have had too many failed attempts – please try again after 5 minutes.`)
							: null;
					}
					setLoggedStateData({
						isLoggedIn: false,
						response: error ? error : 'API failure',
						loading: false,
						message,
					});
				});
		}
	}, [userData]);
	return loggedStateData;
};
export default useLogin;

useLogin.propTypes = {
	/**
	 * An object of user details following the email: foo, password: bar format
	 */
	userDetails: PropTypes.object,
};
