import { useEffect } from 'react';

const useCvvInput = (cvv = '', setValue) => {
	let updatedDateInput = cvv;

	useEffect(() => {
		const cvvLength = 4;
		const updatedCvv = updatedDateInput.substring(0, cvvLength);
		setValue('cvv', updatedCvv);
	}, [setValue, updatedDateInput]);
};

export default useCvvInput;
