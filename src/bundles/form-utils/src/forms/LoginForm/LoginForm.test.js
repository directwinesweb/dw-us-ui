import LoginForm from './LoginForm';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Login Form test', () => {
	it('Creates snapshot test for Login Form ', () => {
		const wrapper = shallow(<LoginForm />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
