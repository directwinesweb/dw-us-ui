import LoginForm from './LoginForm';
import React from 'react';

export default {
	title: 'Form/Forms/ Login Form',
	parameters: {
		component: LoginForm,
		componentSubtitle: 'Generic Login Form using DWI API',
	},
};

export const Default = () => <LoginForm />;
