import React, { useState } from 'react';

import { Button } from '@dw-us-ui/button';
import { EmailInput } from '../../components/custom-inputs/EmailInput';
import { FormErrors } from '../../components/FormErrors';
import { LoadingIcon } from '@dw-us-ui/loading-icon';
import { PasswordInput } from '../../components/custom-inputs/PasswordInput';
import styled from '@emotion/styled';
import useForm from 'react-hook-form';
import useLogin from '../../hooks/useLogin';

const StyledForm = styled.form`
	width: 100%;
	.form-row {
		padding: 1.5em 0em;
		.input-container {
			max-width: 100%;
			margin: auto;
			input {
				height: 30px;
				border-width: 0 0 2px 0;
				border-color: #9595a7;
				font-size: 16px;
				background-color: transparent;
				width: 100%;
			}
		}
	}
	.form-row:last-of-type {
		margin-bottom: 1em;
	}

	button {
		width: 100%;
		margin-bottom: 1em;
	}
`;

const LoginForm = () => {
	const { register, errors, handleSubmit, formState } = useForm();
	const [data, setData] = useState(false);

	/**
	 * useLogin(input) returns an object:
	 * {
	 * 	isLoggedIn: bool,
	 * 	response: obj/null,
	 * 	loading: bool,
	 * 	message: str/null
	 * }
	 */

	let loginObj = useLogin(data);

	const onSubmit = (data) => {
		setData(data);
	};

	return (
		<StyledForm className='login-form' onSubmit={handleSubmit(onSubmit)}>
			<FormErrors errors={errors} />
			<div className='form-row'>
				<EmailInput register={register} errors={errors} />
			</div>
			<div className='form-row'>
				<PasswordInput register={register} errors={errors} />
			</div>
			{loginObj.loading ? (
				<LoadingIcon />
			) : (
				<Button
					type='submit'
					buttonType='primary'
					disabled={formState.isSubmitting}
				>
					Sign In
				</Button>
			)}
			<br />
			{loginObj.message ? <p>{loginObj.message}</p> : null}
		</StyledForm>
	);
};

export default LoginForm;
