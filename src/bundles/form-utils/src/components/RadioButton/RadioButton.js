import PropTypes from 'prop-types';
import React from 'react';
import { Tooltip } from '@dw-us-ui/tooltip';
import styled from '@emotion/styled';

const StyledRadioButton = styled.label`
	display: flex;
	align-items: center;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	font-family: ${({ theme }) => theme.fonts.secondary};
	color: ${({ theme }) => theme.colors.body};

	/* &:not(:last-of-type) {
		margin-right: 50px;
	} */

	p {
		margin: 0 0 0 5px;
		padding-left: 36px;
	}

	/* Hide the browser's default checkbox */
	input {
		position: absolute;
		opacity: 0;
		height: 0;
		width: 0;
	}

	.checkmark-border {
		align-items: center;
		justify-content: center;
		display: flex;
		position: absolute;
		align-self: flex-start;
		cursor: pointer;
		height: ${({ theme }) => theme.radioButton.checkMarkBorderHeight};
		width: ${({ theme }) => theme.radioButton.checkMarkBorderWidth};
		border: ${({ theme }) => theme.radioButton.checkMarkBorder};
		border-radius: ${({ theme }) => theme.radioButton.borderRadius};
	}

	/* When the checkbox is checked, add a blue background */
	input:checked ~ .checkmark-border .checkmark-background {
		align-items: center;
		justify-content: center;
		display: flex;
		position: relative;
		border-radius: ${({ theme }) => theme.radioButton.borderRadius};

		width: ${({ theme }) => theme.radioButton.checkedBackgroundWidth};
		height: ${({ theme }) => theme.radioButton.checkedBackgroundHeight};
		background-color: ${({ theme }) =>
			theme.radioButton.checkedBackgroundColor};
	}

	/* Show the checkmark when checked */
	input:checked ~ .checkmark-border .checkmark-background .checkmark-check {
		width: ${({ theme }) => theme.radioButton.checkMarkWidth};
		height: ${({ theme }) => theme.radioButton.checkMarkHeight};
		border: ${({ theme }) => theme.radioButton.checkMarkColor};
		border-width: ${({ theme }) => theme.radioButton.checkMark};
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	}

	/* Hide the checkmark by default */
	.checkmark:after {
		content: '';
		display: none;
	}
	label {
		padding-left: 20px;
	}
`;

/**
 * An uncontrolled custom visual radio button that can be utilized throughout the site in order to utilize styled radio buttons as part of our forms.
 * @param {string} name - Determines the name used for each radio input based on the groupName passed into the RadioGroup component.
 * @param {string} value - Determines the value of a radio button that will come through as part of the form submission for this group.
 * @param {string} label - Determines the customer facing text which get displayed next to the radio button.
 * @param {object} tooltip - Determines whether there will be a tool tip linked to a certain radio button.
 * @param {function} register - Determines the register function from the useForm hook from react-hook-form to register an input pass this into the RadioGroup component
 **/
const RadioButton = ({
	name,
	value,
	label,
	tooltip,
	register,
	cypressClass,
	onChange,
	isChecked,
	reactHookForm,
	isControl,
	...props
}) => {
	return (
		<StyledRadioButton data-cy={cypressClass}>
			{register && !isControl ? (
				<input
					type='radio'
					name={name}
					value={value}
					onChange={onChange}
					ref={register}
					{...props}
				/>
			) : (
				<input
					type='radio'
					name={name}
					value={value}
					onChange={onChange}
					checked={isChecked}
					{...props}
				/>
			)}
			<span className='checkmark-border'>
				<span className='checkmark-background'>
					<span className='checkmark-check' />
				</span>
			</span>

			{name === 'addressValue' ? label : <p>{label}</p>}

			{tooltip ? (
				<div className='checkbox-tooltip ml-3'>
					<Tooltip
						tooltipContent={tooltip.content}
						tooltipPosition={tooltip.position}
					>
						<img
							src='/assets/icons/us/law/tooltip.svg'
							alt='tooltip'
							width='18px'
							height='18px'
						/>
					</Tooltip>
				</div>
			) : (
				''
			)}
		</StyledRadioButton>
	);
};

RadioButton.propTypes = {
	/**
	 * The name that the radio button set belongs to, this gets set
	 * in the RadioGroup component as the groupName prop
	 */
	name: PropTypes.string.isRequired,
	/**
	 * The value to be used if this radio button is selected
	 */
	value: PropTypes.string.isRequired,
	/**
	 * The value that gets displayed to the user next to the radio button.
	 */
	label: PropTypes.string,
	/**
	 * A custom tooltip component that will be passed in to display information regarding
	 * specific radio button
	 */
	tooltip: PropTypes.oneOfType([
		PropTypes.bool,
		PropTypes.shape({
			content: PropTypes.any,
			position: PropTypes.oneOf(['bottom', 'top', 'left', 'right']),
		}),
	]),
	/**
	 * A function that gets passed for the useForm hook from react-hook-form to register an input
	 */
	register: PropTypes.func.isRequired,
	/**
	 * Cypress name
	 */
	cypressClass: PropTypes.string,
	/**
	 * on change method
	 */
	onChange: PropTypes.func,
	/**
	 * If true, the button will be checked
	 */
	isChecked: PropTypes.bool,
	/**
	 * Determines if button is a controlled, not using any other library (i.e ReactHookForm)
	 */
	isControl: PropTypes.bool,
};

RadioButton.defaultProps = {
	name: '',
	value: '',
	label: '',
	tooltip: false,
	register: () => {},
	onChange: () => {},
	cypressClass: 'radiobtn',
	isChecked: false,
	isControl: false,
};

export default RadioButton;
