import RadioButton from './RadioButton';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Radio Button test', () => {
	it('Creates snapshot test for Radio Button ', () => {
		const wrapper = shallow(<RadioButton />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
