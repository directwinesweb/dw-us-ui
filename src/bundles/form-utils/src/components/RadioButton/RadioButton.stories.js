import React, { useState } from 'react';
import { select, text } from '@storybook/addon-knobs';

import RadioButton from './RadioButton';
import useForm from 'react-hook-form';

export default {
	title: 'Form/RadioButton',
	parameters: {
		component: RadioButton,
	},
};

export const Default = () => {
	const [checked, setChecked] = useState(false);
	const toggleCheck = () => {
		setChecked(!checked);
	};
	return (
		<RadioButton
			label={text('Label', 'RadioButton label')}
			isChecked={checked}
			onChange={toggleCheck}
			isControl
		/>
	);
};

export const ReactHookForm = () => {
	const [checked, setChecked] = useState(false);

	const { register, errors } = useForm({
		mode: 'onBlur',
	});
	return (
		<RadioButton
			label={text('Label', 'RadioButton label')}
			isChecked={checked}
			register={register}
		/>
	);
};

export const Tooltip = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	const positions = {
		top: 'top',
		left: 'left',
		right: 'right',
		bottom: 'bottom',
	};
	return (
		<form>
			<RadioButton
				label={text('Label', 'RadioButton label')}
				register={register}
				tooltip={{
					content: text('Tooltip content', 'I am tooltip'),
					position: select('Position', positions, 'bottom'),
				}}
			/>
		</form>
	);
};
