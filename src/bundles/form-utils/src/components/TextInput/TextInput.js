import React, { useEffect, useRef, useState } from 'react';

import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import InputErrorMessage from './InputErrorMessage';
import PropTypes from 'prop-types';
import { Tooltip } from '@dw-us-ui/tooltip';
import styled from '@emotion/styled';
import { useCombinedRefs } from '@dw-us-ui/use-combined-refs';

const StyledTextInput = styled.div`
	font-family: ${({ theme }) => theme.fonts.secondary};
	color: ${({ theme }) => theme.input.color};
	.text-input {
		display: flex;
		background-color: ${({ theme }) => theme.colors.textInputBackgroundColor};
	}
	/* Change autocomplete styles in WebKit */
	input:-webkit-autofill,
	input:-webkit-autofill:hover,
	input:-webkit-autofill:focus,
	textarea:-webkit-autofill,
	textarea:-webkit-autofill:hover,
	textarea:-webkit-autofill:focus,
	select:-webkit-autofill,
	select:-webkit-autofill:hover,
	select:-webkit-autofill:focus {
		border: ${({ theme }) => theme.input.border};
		background-color: ${({ theme }) => theme.colors.textInputBackgroundColor};
		-webkit-text-fill-color: ${({ theme }) => theme.input.color};
		transition: background-color 5000s ease-in-out 0s;
	}
	.input-label-container {
		position: relative;
		width: 100%;
		input {
			background-color: ${({ theme }) => theme.colors.textInputBackgroundColor};
			border: ${({ theme }) => theme.input.border};
			border-width: ${({ theme }) => theme.input.borderWidth};
			border-color: ${({ theme }) => theme.input.borderColor};
			font-size: 15px;
			padding: 26px 0px 7px 10px;
			width: 100%;
			&::placeholder {
				color: transparent;
			}
			&::-webkit-input-placeholder {
				color: transparent;
			}
			&:-ms-input-placeholder {
				color: transparent !important;
			}
			&::-moz-placeholder {
				color: transparent;
			}
			:focus {
				outline: none;
			}
		}

		.error-border,
		.clear {
			height: 0;
			border-bottom: 0px solid ${({ theme }) => theme.errors.color};
			transition: all 0.2s ease;
			transform: scale(0);
			position: absolute;
			bottom: 0;
			width: 0%;
		}
		.error-border.animated {
			border-bottom: 2px solid ${({ theme }) => theme.errors.color};
			transform: scale(1);
			width: 100%;
		}
		.label {
			position: absolute;
			font-size: 15px;
			text-align: left;
			color: ${({ theme }) => theme.input.placeholder};
			pointer-events: none;
			padding-left: 10px;
			padding-bottom: 8px;
			margin: 0;
			bottom: 5px;
			transform: translate(0, 0) scale(1);
			transform-origin: top left;
			transition: all 0.1s ease-in-out;
		}

		&.hide-placeholder {
			.label {
				transform: ${({ theme }) => theme.input.labelTranslate};
				padding-left: 14px;
				padding-bottom: 15px;
				width: 100%;
			}
		}
	}
	input[type='hidden'] {
		display: none;
	}
	a {
		color: #999999;
	}
	.checkbox-tooltip {
		display: flex;
		position: relative;
		font-size: 16px;
		top: 24px;
		height: 12px;
		width: 0px;
		left: -10px;
		letter-spacing: ${({ theme }) => theme.fonts.spacing.letterSpacing};
		justify-content: flex-end;
		${EmotionBreakpoint('mobile')} {
			font-size: 24px;
			top: 20px;
		}
		.fa-info-circle path {
			fill: ${({ theme }) => theme.colors.toolTipInput};
		}
	}
`;

const TextInput = ({
	type,
	className,
	placeholder,
	name,
	validation,
	maxLength,
	onChange,
	onPaste,
	onBlur,
	onFocus,
	decorator,
	tooltip,
	register,
	errors,
	cypressClass,
	inputRef,
	brandTag,
	...props
}) => {
	// Lets us intercept the value of an input value on load and deal with hiding placeholder component
	let textInput = useRef();
	let refList = [textInput, register];
	if (inputRef) {
		refList.push(inputRef);
	}
	let combinedRef = useCombinedRefs(...refList);

	const hasErrors = !(
		Object.keys(errors).length === 0 &&
		errors.constructor === Object &&
		errors[name] === undefined
	);
	const [inputClass, setInputClass] = useState('');

	useEffect(() => {
		if (combinedRef.current) {
			combinedRef.current.value ? setInputClass('hide-placeholder') : null;
		}
	}, [combinedRef]);

	const onChangeHandler = (e) => {
		if (e.target.value.length) {
			setInputClass('hide-placeholder');
		}
		//If onChange comes as a prop run inputs on onChange
		if (onChange) {
			onChange(e);
		}
	};

	const onBlurChange = (e) => {
		if (e.target.value === '') {
			setInputClass('');
		}
		//If onBlur comes as a prop run inputs on onBlur
		if (onBlur) {
			onBlur(e);
		}
	};

	const onFocusChange = () => {
		setInputClass('hide-placeholder');
		//If onFocus comes as a prop run inputs on onFocus
		if (onFocus) {
			onFocus();
		}
	};

	return (
		<StyledTextInput
			className='input-container'
			data-cy={`${cypressClass}-container`}
		>
			<div className={`input-label-container ${inputClass}`}>
				<div className='text-input'>
					{placeholder && type !== 'hidden' ? (
						<label
							className='label w-100 d-flex justify-content-between'
							htmlFor='inputField'
						>
							{placeholder}
						</label>
					) : null}

					<input
						type={type}
						className={`input ${className ? className : ''}`}
						placeholder={placeholder}
						name={name}
						validation={validation}
						maxLength={maxLength}
						onChange={(e) => onChangeHandler(e)}
						onPaste={onPaste}
						onBlur={(e) => onBlurChange(e)}
						onFocus={onFocusChange}
						ref={combinedRef}
						data-cy={cypressClass}
						{...props}
					/>
					{tooltip ? (
						<div className='checkbox-tooltip ml-3'>
							<Tooltip
								tooltipContent={tooltip.content}
								tooltipPosition={tooltip.position}
							>
								<img
									src={`/images/us/common/icons/${brandTag}/info_circle_light_16x16.svg`}
									alt='info'
									height='16px'
									width='16px'
								/>
							</Tooltip>
						</div>
					) : null}
				</div>

				<div
					className={`error-border ${
						hasErrors && errors[name] !== undefined ? 'animated' : 'clear'
					}`}
				/>
			</div>

			{hasErrors ? (
				<InputErrorMessage errors={errors} name={name} brandTag={brandTag} />
			) : null}
			{decorator ? <span>{decorator}</span> : null}
		</StyledTextInput>
	);
};

TextInput.propTypes = {
	/**
	 * brand tag
	 */
	brandTag: PropTypes.string,
	/**
	 * Class names to append to the input element
	 */
	className: PropTypes.string,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,

	decorator: PropTypes.any,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * Controls whether or not the element will be displayed
	 */
	hidden: PropTypes.bool,
	/**
	 * input ref variable
	 */
	inputRef: PropTypes.any,
	/**
	 * Max input length
	 */
	maxLength: PropTypes.any,
	/**
	 * Text input name
	 */
	name: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * On focus method
	 */
	onFocus: PropTypes.func,
	/**
	 * The function that will be sent the React event whenever the event is clicked.
	 * Useful when integrating this as a controlled element in a larger form (e.g. Formik)
	 */
	onChange: PropTypes.func,

	/**
	 * on Paste method
	 */
	onPaste: PropTypes.func,
	/**
	 * Placeholder text to display before user enters a value
	 */
	placeholder: PropTypes.string,
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * The name of the input
	 */
	type: PropTypes.string.isRequired,
	/**
	 * tooltip properties
	 */
	tooltip: PropTypes.oneOfType([
		PropTypes.bool,
		PropTypes.shape({
			content: PropTypes.any,
			position: PropTypes.oneOf(['bottom', 'top', 'left', 'right']),
		}),
	]),
	/**
	 * Validation rules represented as either a string or an object
	 */
	validation: PropTypes.oneOf([PropTypes.string, PropTypes.object]),
	/**
	 * The value to be used if this input is selected
	 */
	value: PropTypes.any,
};

TextInput.defaultProps = {
	type: 'text',
	hidden: false,
	cypressClass: 'txt',
	onPaste: () => {},
	register: () => {},
	onBlur: () => {},
	onFocus: () => {},
	decorator: null,
	tooltip: false,
	errors: {},
	brandTag: 'law',
};

export default TextInput;
