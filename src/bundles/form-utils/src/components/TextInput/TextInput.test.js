import React from 'react';
import TextInput from './TextInput';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Text Input test', () => {
	it('Creates snapshot test for <TextInput />', () => {
		const wrapper = shallow(<TextInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
