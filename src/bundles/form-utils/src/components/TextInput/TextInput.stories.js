import { number, select, text } from '@storybook/addon-knobs';

import React from 'react';
import TextInput from './TextInput';
import { action } from '@storybook/addon-actions';

export default {
	title: 'Form/Text Input',
	parameters: {
		component: TextInput,
	},
	argTypes: {
		onBlur: { action: 'onBlur' },
		onFocus: { action: 'onFocus' },
	},
};

export const Default = () => (
	<TextInput
		placeholder={text('Placeholder', 'Placeholder Value')}
		onBlur={action('onBlur')}
		onFocus={action('onFocus')}
		maxLength={number('maxLength', 10)}
	/>
);
const positions = {
	top: 'top',
	left: 'left',
	right: 'right',
	bottom: 'bottom',
};
export const Tooltip = () => (
	<TextInput
		placeholder={text('Placeholder', 'Example')}
		tooltip={{
			content: text('Tooltip content', 'I am tooltip'),
			position: select('Position', positions, 'bottom'),
		}}
		onBlur={action('onBlur')}
		onFocus={action('onFocus')}
	/>
);

export const Error = () => (
	<TextInput
		placeholder={text('Placeholder', 'Has Errors')}
		tooltip={{
			content: text('Tooltip content', 'I am tooltip'),
			position: select('Position', positions, 'bottom'),
		}}
		onBlur={action('onBlur')}
		onFocus={action('onFocus')}
		errors={{
			text: {
				message: 'This is error message',
			},
		}}
		name='text'
	/>
);
