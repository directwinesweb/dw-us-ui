import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledInputErrorMessage = styled.div`
	color: ${({ theme }) => theme.errors.color};
	font-size: ${({ theme }) => theme.errors.fontSize};
	line-height: 18px;
	align-items: flex-start;
	display: flex;
	margin: 0px;
	padding: 1rem 0;
	.exclamation-error {
		margin-right: 10px;
	}
	${EmotionBreakpoint('mobile')} {
		width: 330px;
		max-width: 80vw;
	}
`;
const InputErrorMessage = ({ errors, name, brandTag }) => {
	const currentError = errors[name];

	// Note: if you are using FormContext, then you can use Errors without props eg:
	// const { errors } = useFormContext();
	if (typeof currentError === 'undefined') return null;

	return (
		<StyledInputErrorMessage>
			<div>
				<img
					className='exclamation-error'
					src={`/images/us/common/icons/${brandTag}/exclamation_triangle_solid_16x14.svg`}
					alt='exclamation-triangle'
					height='14px'
					width='16px'
				/>
			</div>
			{currentError.message}
		</StyledInputErrorMessage>
	);
};

InputErrorMessage.propTypes = {
	brandTag: PropTypes.string,
	errors: PropTypes.object,
	name: PropTypes.string,
};

InputErrorMessage.defaultProps = {
	brandTag: 'law',
	errors: {},
	name: '',
};
export default InputErrorMessage;
