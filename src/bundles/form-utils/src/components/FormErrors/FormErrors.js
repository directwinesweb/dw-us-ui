import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledErrorContainer = styled.div`
	color: ${({ theme }) => theme.errors.color};
	font-size: ${({ theme }) => theme.errors.formErrors.fontSize};
	line-height: ${({ theme }) => theme.errors.formErrors.lineHeight};
	display: flex;
	ul {
		font-size: ${({ theme }) => theme.errors.fontSize};
		max-width: 200px;
		width: 100%;
		display: block;
		margin: 0 auto 50px;
	}
`;

const FormErrors = ({ errors }) => {
	return (
		<>
			{Object.keys(errors).length !== 0 ? (
				<StyledErrorContainer>
					<div className='errors'>
						{Object.values(errors).map((error, index) => {
							const errorMessage = error.message;
							return <li key={index}>{errorMessage}</li>;
						})}
					</div>
				</StyledErrorContainer>
			) : null}
		</>
	);
};

FormErrors.propTypes = {
	/**
	 * An object of errors from a react-hook-forms based form, can be used by others so long as they follow desired format
	 * Structure:
	 * {
	 * 	key : {
	 * 		message: ''
	 * 	}
	 * }
	 */
	errors: PropTypes.object,
};

export default FormErrors;
