import React, { useState } from 'react';

import FormErrors from './FormErrors';
import { text } from '@storybook/addon-knobs';

export default {
	title: 'Form/FormErrors',
	parameters: {
		component: FormErrors,
	},
};

export const Default = () => {
	return (
		<FormErrors
			errors={{
				error1: { message: text('Error 1: ', 'Name error one') },
				error2: { message: text('Error 2', 'Random list error') },
			}}
		/>
	);
};
