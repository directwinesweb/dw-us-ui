import FormErrors from './FormErrors';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Form Errors test', () => {
	it('Creates snapshot test for <FormErrors />', () => {
		const wrapper = shallow(
			<FormErrors
				errors={{
					name: {
						message: 'This is error message',
					},
				}}
			/>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
