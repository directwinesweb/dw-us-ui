import React, { useState } from 'react';

import { RadioButton } from '../RadioButton';
import RadioGroup from './RadioGroup';
import { text } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Radio Group',
	parameters: {
		component: RadioGroup,
		componentSubtitle: 'Displays a branded radio group component',
	},
};

export const Default = () => {
	const [selectedAddressType, setSelectedAddressType] = useState('home');
	const handleAddressTypeChange = (e) => {
		setSelectedAddressType(e.target.value);
	};
	return (
		<RadioGroup
			groupName='groupStory'
			isControl
			onChange={handleAddressTypeChange}
			selectedValue={selectedAddressType}
		>
			<RadioButton
				label={text('Radio 1 label', 'Home')}
				value='home'
			></RadioButton>
			<RadioButton
				label={text('Radio 2 label', 'Business')}
				value='business'
			></RadioButton>
		</RadioGroup>
	);
};

export const ReactHookForm = () => {
	const { register } = useForm({
		mode: 'onBlur',
	});
	return (
		<RadioGroup register={register} groupName='groupStory'>
			<RadioButton
				label={text('Button 1 Label', 'Button 1')}
				value='button1'
			></RadioButton>
			<RadioButton
				label={text('Button 2 Label', 'Button 2')}
				value='button2'
			></RadioButton>
			<RadioButton
				label={text('Button 3 Label', 'Button 3')}
				value='button3'
			></RadioButton>
		</RadioGroup>
	);
};
