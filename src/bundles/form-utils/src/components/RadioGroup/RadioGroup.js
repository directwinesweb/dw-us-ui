import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledRadioGroup = styled.div`
	display: flex;
	border: none;
	padding: 0;
	margin: 0;
`;

/**
 * A uncontrolled custom visual radio group. This element should only contain RadioButton*
 * components that will be rendered with a group setting
 * @param {object} children - Determines which RadioButton components that you'd like to pass in and be part of this radio group
 * @param {string} groupName - Determines the id of the radio group and gives all the child radio buttons the same name as this ID to group them properly according to HTML5 standards
 * @param {string} className - Determines what the className of the radio group will be.
 * @param {function} register - Determines the register function from the useForm hook from react-hook-form to register the inputs of this RadioGroup component
 */
const RadioGroup = ({
	children,
	groupName,
	className,
	register,
	cypressClass,
	onChange,
	selectedValue,
	isControl,
}) => {
	const renderChildren = () => {
		return React.Children.map(children, (child) => {
			const childValue = child.props.value;

			return React.cloneElement(child, {
				name: groupName,
				register,
				onChange,
				isControl,
				isChecked: childValue === selectedValue,
			});
		});
	};

	return (
		<StyledRadioGroup
			id={groupName}
			className={className}
			data-cy={cypressClass}
		>
			{renderChildren()}
		</StyledRadioGroup>
	);
};

RadioGroup.propTypes = {
	/**
	 * The radio buttons passed in to be part of this radio group
	 */
	children: PropTypes.node.isRequired,
	/**
	 * This value gives all child radio buttons the same name as the ID of this group, linking them properly
	 */
	groupName: PropTypes.string.isRequired,
	/**
	 * This value gives a className that can be used for styling as part of the form
	 */
	className: PropTypes.string,
	/**
	 * A function that gets passed for the useForm hook from react-hook-form to register an input
	 */
	register: PropTypes.func.isRequired,
	/**
	 * Cypress name
	 */
	cypressClass: PropTypes.string,
	/**
	 * on change method
	 */
	onChange: PropTypes.func,

	/**
	 * the radio value that is selected
	 */
	selectedValue: PropTypes.string,
	/**
	 * Determines if group is a controlled group, not using any other library (i.e ReactHookForm)
	 */
	isControl: PropTypes.bool,
};

RadioGroup.defaultProps = {
	children: null,
	groupName: '',
	className: '',
	register: () => {},
	cypressClass: 'radioGroup',
	onChange: () => {},
	isControl: false,
};

export default RadioGroup;
