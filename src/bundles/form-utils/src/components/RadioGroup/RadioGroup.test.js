import { mount, shallow } from 'enzyme';

import { RadioButton } from '../RadioButton';
import RadioGroup from './RadioGroup';
import React from 'react';
import toJson from 'enzyme-to-json';

describe('Radio Group test', () => {
	it('Creates snapshot test for radio group not controlled ', () => {
		const wrapper = shallow(
			<RadioGroup groupName='testGroup'>
				<RadioButton value='1'></RadioButton>
				<RadioButton value='2'></RadioButton>
				<RadioButton value='3'></RadioButton>
			</RadioGroup>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot for controlled radio group', () => {
		const wrapper = shallow(
			<RadioGroup groupName='test-group' selectedValue='1' isControl>
				<RadioButton value='1'></RadioButton>
				<RadioButton value='2'></RadioButton>
				<RadioButton value='3'></RadioButton>
			</RadioGroup>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	// it('renders <RadioGroup/> component with children and selected value', () => {
	// 	const wrapper = mount(
	// 		<RadioGroup groupName='test-group' selectedValue='1'>
	// 			<RadioButton value='1'></RadioButton>
	// 			<RadioButton value='2'></RadioButton>
	// 			<RadioButton value='3'></RadioButton>
	// 		</RadioGroup>
	// 	);
	// 	const input = wrapper.find('input[type="radio"]');
	// 	expect(input).toHaveLength(3);
	// 	//checks that first radio button is checked and that the rest are not
	// 	expect(input.at(0).props().checked).toEqual(true);
	// 	expect(input.at(1).props().checked).toEqual(false);
	// 	expect(input.at(2).props().checked).toEqual(false);

	// 	expect(toJson(wrapper)).toMatchSnapshot();
	// });

	// it('Updates radio button checked property onChange', () => {
	// 	const initialValue = '1';
	// 	const wrapper = mount(
	// 		<RadioGroup
	// 			groupName='test-group'
	// 			selectedValue={initialValue}
	// 			onChange={onChange}
	// 		>
	// 			<RadioButton value='1'></RadioButton>
	// 			<RadioButton value='2'></RadioButton>
	// 			<RadioButton value='3'></RadioButton>
	// 		</RadioGroup>
	// 	);

	// 	const input = wrapper.find('input[type="radio"]');
	// 	const value1 = input.at(0);
	// 	const value2 = input.at(1);
	// 	const value3 = input.at(2);
	// 	expect(value1.props().checked).toEqual(true);
	// 	expect(value2.props().checked).toEqual(false);
	// 	expect(value3.props().checked).toEqual(false);

	// 	value2.simulate('change', wrapper.setProps({ selectedValue: '2' }));
	// 	expect(onChange).toHaveBeenCalled();
	// 	const inputUpdated = wrapper.find('input[type="radio"]');
	// 	const updatedValue1 = inputUpdated.at(0);
	// 	const updatedValue2 = inputUpdated.at(1);
	// 	const updatedValue3 = inputUpdated.at(2);
	// 	expect(updatedValue1.props().checked).toEqual(false);
	// 	expect(updatedValue2.props().checked).toEqual(true);
	// 	expect(updatedValue3.props().checked).toEqual(false);
	// });
});
