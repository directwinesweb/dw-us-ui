import CreditCardNumberInput from './CreditCardNumberInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Credit Card Number Input test', () => {
	it('Creates snapshot test for <CreditCardNumberInput />', () => {
		const wrapper = shallow(<CreditCardNumberInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
