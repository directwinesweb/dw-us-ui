import React, { useEffect } from 'react';

import { CheckKeyInput } from '@dw-us-ui/check-key-input';
import PropTypes from 'prop-types';
import { TextInput } from '../../TextInput';
import styled from '@emotion/styled';

const StyledTextInput = styled.div`
	position: relative;
	.card-type-icon {
		height: 25px;
		width: 40px;
		position: absolute;
		right: 0.5px;
		top: 22px;
		&.discover-icon {
			background: url('/images/us/common/recr/cc-icon.png') -101px -25px
				no-repeat;
		}
		&.americanexpress-icon {
			background: url('/images/us/common/recr/cc-icon.png') -150px -25px
				no-repeat;
		}
		&.mastercard-icon {
			background: url('/images/us/common/recr/cc-icon.png') -51px -25px
				no-repeat;
		}
		&.visa-icon {
			background: url('/images/us/common/recr/cc-icon.png') 1px -25px no-repeat;
		}
	}
`;

const CreditCardNumberInput = ({
	register,
	errors,
	setValue,
	cypressClass,
	checkValidCreditCard,
	cardType,
	onBlur,
	...props
}) => {
	const cardTypeImage = cardType.toLowerCase() || 'blank';

	useEffect(() => {
		setValue('cardType', cardType);
	}, [cardType, setValue]);

	return (
		<StyledTextInput>
			<TextInput
				type='text'
				placeholder='Credit Card Number'
				name='creditCardNumber'
				cypressClass={cypressClass}
				register={register({
					required: 'Please enter a valid credit card number.',
					validate: {
						isValidCreditCard: () => checkValidCreditCard(),
					},
				})}
				onKeyPress={CheckKeyInput}
				errors={errors}
				onBlur={onBlur}
				{...props}
			/>
			<span className={`card-type-icon ${cardTypeImage}-icon`}></span>
			{/* Hidden field to set data that is directly consumed by the API and used elsewhere in the application, middleware solution may be more desirable
			 https://www.codementor.io/@vkarpov/beginner-s-guide-to-redux-middleware-du107uyud */}
			<TextInput
				type='hidden'
				placeholder='Card Type'
				name='cardType'
				errors={errors}
				register={register}
				onBlur={onBlur}
			></TextInput>
		</StyledTextInput>
	);
};

CreditCardNumberInput.defaultProps = {
	register: () => {},
	errors: {},
	setValue: () => {},
	cypressClass: 'creditCardNumberInput',
	checkValidCreditCard: () => {},
	cardType: '',
	onBlur: () => {},
};
CreditCardNumberInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Set Value method from React Hook Form
	 */
	setValue: PropTypes.func,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
	/**
	 * method that validates credit card
	 */
	checkValidCreditCard: PropTypes.func,
	/**
	 * Card Type (VISA, AMERICAN EXPRESS, MASTERCARD, DISCOVER)
	 */
	cardType: PropTypes.string,
};

export default CreditCardNumberInput;
