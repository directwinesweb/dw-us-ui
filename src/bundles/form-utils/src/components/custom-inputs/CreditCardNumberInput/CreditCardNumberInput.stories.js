import CreditCardNumberInput from './CreditCardNumberInput';
import { GetCardInfo } from '@dw-us-ui/get-card-info';
import React from 'react';
import useCreditCardNumberInput from '../../../hooks/useCreditCardNumberInput';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Credit Card Number Input',
	parameters: {
		component: CreditCardNumberInput,
	},
};

export const Default = () => {
	const { register, errors, setValue, watch } = useForm({
		mode: 'onBlur',
	});
	const watchedCreditCardNumber = watch('creditCardNumber');
	const cardInfo = GetCardInfo(watchedCreditCardNumber);
	const { cardType, isValid } = cardInfo;
	useCreditCardNumberInput(watchedCreditCardNumber, cardType, setValue);
	const checkValidCreditCard = () => {
		const invalidCreditCardMessage =
			'Your credit card number is invalid, please use another credit card.';

		if (isValid) {
			return true;
		} else {
			return invalidCreditCardMessage;
		}
	};
	return (
		<form>
			<CreditCardNumberInput
				register={register}
				errors={errors}
				setValue={setValue}
				cardType={cardType}
				checkValidCreditCard={checkValidCreditCard}
			/>
		</form>
	);
};
