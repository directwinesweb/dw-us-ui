import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';
import styled from '@emotion/styled';
import useCvvInput from '../../../hooks/useCvvInput';

const StyledTextInput = styled(TextInput)`
	&::-webkit-inner-spin-button,
	&::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
	min-width: 70px;
`;

const CVVInput = ({
	register,
	errors,
	watch,
	setValue,
	maxLength,
	cypressClass,
	...props
}) => {
	const watchedCvv = watch('cvv');
	useCvvInput(watchedCvv, setValue);

	return (
		<StyledTextInput
			type='tel'
			placeholder='CVV'
			cypressClass={cypressClass}
			name='cvv'
			register={register({
				required: `Please enter your credit card's CVV.`,
				pattern: {
					value: /^[0-9]{3,4}$/,
					message: 'Please make sure that your CVV number is 3 or 4 digits',
				},
			})}
			errors={errors}
			{...props}
			maxLength={maxLength}
		/>
	);
};

CVVInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * Set Value method from React Hook Form
	 */
	setValue: PropTypes.func,
	/**
	 * Max length value
	 */
	maxLength: PropTypes.any,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
	/**
	 * Watch method from react hook form
	 */
	watch: PropTypes.func,
};

CVVInput.defaultProps = {
	register: () => {},
	setValue: () => {},
	watch: () => {},
	cypressClass: 'cvvInput',
	maxLength: 3,
	errors: {},
};
export default CVVInput;
