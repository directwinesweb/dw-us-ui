import CVVInput from './CVVInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('CVV Input test', () => {
	it('Creates snapshot test for <CVVInput />', () => {
		const wrapper = shallow(<CVVInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
