import CVVInput from './CVVInput';
import React from 'react';
import { select } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/CVV Input',
	parameters: {
		component: CVVInput,
	},
};

export const Default = () => {
	const { register, errors, setValue, watch } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<CVVInput
				register={register}
				errors={errors}
				watch={watch}
				setValue={setValue}
				maxLength={select('maxLength', { 3: 3, 4: 4 }, 4)}
			/>
		</form>
	);
};
