import PhoneNumberInput from './PhoneNumberInput';
import React from 'react';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Custom Inputs/Phone Number Input',
	parameters: {
		component: PhoneNumberInput,
	},
};

export const Default = () => {
	const { register, errors, watch, setValue } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<PhoneNumberInput
				register={register}
				errors={errors}
				watch={watch}
				setValue={setValue}
				name='phone'
			/>
		</form>
	);
};
