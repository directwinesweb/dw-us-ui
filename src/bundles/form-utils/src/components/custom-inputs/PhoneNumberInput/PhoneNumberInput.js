import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';
import usePhoneInput from '../../../hooks/usePhoneInput';

const PhoneNumberInput = ({
	register,
	errors,
	watch,
	setValue,
	name,
	cypressClass,
	onBlur,
	...props
}) => {
	// Phone Number Logic
	const watchedPhone = watch(name || 'phone');
	usePhoneInput(watchedPhone, setValue, name || 'phone');

	const onKeyPressHandler = (event) => {
		if (isNaN(event.key) && event.charCode !== 13) {
			event.preventDefault();
			return false;
		} else {
			return;
		}
	};

	return (
		<TextInput
			type='tel'
			placeholder='Phone'
			name={name || 'phone'}
			pattern='[0-9]{3}-[0-9]{3}-[0-9]{4}'
			register={register({
				required: 'Please enter your phone number.',
				pattern: {
					value: /[0-9]{3}-[0-9]{3}-[0-9]{4}/,
					message:
						'Please use this format for entering your phone number: XXX-XXX-XXXX.',
				},
			})}
			onKeyPress={onKeyPressHandler}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

PhoneNumberInput.propTypes = {
	/**
	 * Object containing the errors from this input field.
	 */
	errors: PropTypes.object,
	/**
	 *
	 * The password value to check against for validation when confirming password
	 */
	passwordInputValue: PropTypes.string,
	/**
	 * A function that gets passed for the useForm hook from react-hook-form to register an input
	 *
	 */
	register: PropTypes.func,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * the input's name
	 */
	name: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Set value method from react hook form
	 */
	setValue: PropTypes.func,
	/**
	 * Watch method from react hook form
	 */
	watch: PropTypes.func,
};

PhoneNumberInput.defaultProps = {
	errors: {},
	passwordInputValue: '',
	register: () => {},
	onBlur: () => {},
	cypressClass: 'PhoneNumberInput',
	watch: () => {},
	setValue: () => {},
	name: 'phone',
};

export default PhoneNumberInput;
