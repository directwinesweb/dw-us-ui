import PhoneNumberInput from './PhoneNumberInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Phone Number Input test', () => {
	it('Creates snapshot test for <PhoneNumberInput />', () => {
		const wrapper = shallow(<PhoneNumberInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
