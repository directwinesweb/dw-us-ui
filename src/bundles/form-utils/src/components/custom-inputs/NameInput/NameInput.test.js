import NameInput from './NameInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Name Input test', () => {
	it('Creates snapshot test for first name ', () => {
		const wrapper = shallow(<NameInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for last name', () => {
		const wrapper = shallow(
			<NameInput
				placeholder='Last Name'
				name='lastName'
				errorMessage='Please enter your last name.'
			/>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
