import NameInput from './NameInput';
import React from 'react';
import { text } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Name Input',
	parameters: {
		component: NameInput,
	},
};

export const FirstName = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<NameInput register={register} errors={errors} />
		</form>
	);
};

export const LastName = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<NameInput
				register={register}
				errors={errors}
				placeholder='Last Name'
				name='lastName'
				errorMessage='Please enter your last name.'
			/>
		</form>
	);
};
