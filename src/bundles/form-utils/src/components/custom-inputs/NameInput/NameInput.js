import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const NameInput = ({
	register,
	errors,
	onBlur,
	cypressClass,
	name,
	placeholder,
	errorMessage,
	...props
}) => {
	return (
		<TextInput
			type='text'
			placeholder={placeholder}
			name={name}
			register={register({
				required: errorMessage,
				pattern: name === "firstName" || name === "lastName" ?  {
					value: /^[^0-9]*$/,
					message: 'Please do not enter numeric values',
				} : null,
			})}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

NameInput.defaultProps = {
	register: () => {},
	onBlur: () => {},
	errors: {},
	name: 'firstName',
	placeholder: 'First Name',
	errorMessage: 'Please enter your first name.',
	cypressClass: 'nameInput',
};
NameInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Input name
	 */
	name: PropTypes.string,
	/**
	 * error message for the input
	 */
	errorMessage: PropTypes.string,
	/**
	 * Place holder value for the input
	 */
	placeholder: PropTypes.string,
};
export default NameInput;
