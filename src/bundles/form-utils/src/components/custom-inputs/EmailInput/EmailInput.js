import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';
import { emailRegex } from '@dw-us-ui/constants';

const EmailInput = ({
	register,
	errors,
	onBlur,
	inputLocation,
	name,
	placeholder,
	cypressClass,
	optional = false,
	...props
}) => {
	let errorMessage;

	if (name === 'giftRecipientEmail') {
		errorMessage = `Please provide your gift recipient's email address and/or a gift message.`;
	} else if (inputLocation === 'guestStart') {
		errorMessage = `Looks like we don't have an email address for you; please enter one now.`;
	} else {
		errorMessage = `Please enter your email address.`;
	}

	return (
		<TextInput
			type='email'
			placeholder={placeholder || 'Email'}
			name={name || 'email'}
			register={register({
				required: optional ? false : errorMessage,
				pattern: {
					value: emailRegex,
					message: `Sorry, we couldn't validate your email address; please re-enter.`,
				},
			})}
			errors={errors}
			onBlur={onBlur}
			onPaste={name === 'email' ? null : () => {}}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

EmailInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Where the input is being used
	 */
	inputLocation: PropTypes.string,
	/**
	 * The input's name
	 */
	name: PropTypes.string,
	/**
	 * Placeholder value for input
	 */
	placeholder: PropTypes.string,
};
EmailInput.defaultProps = {
	name: 'email',
	register: () => {},
	onBlur: () => {},
	errors: {},
	inputLocation: '',
	placeholder: 'Email',
	cypressClass: 'emailInput',
};
export default EmailInput;
