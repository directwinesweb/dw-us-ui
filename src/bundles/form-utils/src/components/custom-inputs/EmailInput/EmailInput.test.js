import EmailInput from './EmailInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Email Input test', () => {
	it('Creates snapshot test for <EmailInput />', () => {
		const wrapper = shallow(<EmailInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
