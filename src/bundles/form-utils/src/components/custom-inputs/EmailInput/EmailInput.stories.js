import EmailInput from './EmailInput';
import React from 'react';
import { text } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Email Input',
	parameters: {
		component: EmailInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<EmailInput
				register={register}
				errors={errors}
				placeholder={text('Placeholder', 'Email')}
				name={text('Name', 'email')}
				inputLocation={text('inputLocation', 'guestStart')}
			/>
		</form>
	);
};
