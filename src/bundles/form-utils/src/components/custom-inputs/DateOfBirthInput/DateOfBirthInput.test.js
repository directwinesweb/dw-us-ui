import DateOfBirthInput from './DateOfBirthInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Date of Birth Input test', () => {
	it('Creates snapshot test for <DateOfBirthInput />', () => {
		const wrapper = shallow(<DateOfBirthInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
