import { CheckKeyInput } from '@dw-us-ui/check-key-input';
import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';
import useDateOfBirthInput from '../../../hooks/useDateOfBirthInput';

const DateOfBirthInput = ({
	register,
	errors,
	watch,
	setValue,
	onBlur,
	tooltip,
	cypressClass,
	dobRef,
	...props
}) => {
	const watchedDate = watch('date');
	useDateOfBirthInput(watchedDate, setValue);

	const daysInMonth = (month, year) => {
		const m = parseInt(month);
		const y = parseInt(year);
		switch (m) {
			case 2:
				return (y % 4 === 0 && y % 100) || y % 400 === 0 ? 29 : 28;
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
			default:
				return 31;
		}
	};

	const checkAge = (date) => {
		let [m, d, y] = date.split('/');
		if (!(m > 0 && m <= 12 && d > 0 && d <= daysInMonth(m, y))) {
			return `Please enter a valid date of birth`;
		}
		let birthday = new Date(date);
		let ageDiff = Date.now() - birthday.getTime();
		let age = Math.abs(new Date(ageDiff).getUTCFullYear() - 1970);

		if (age < 21 || age > 120) {
			return `Sorry, we couldn't verify your age; please confirm your date of birth (MM/DD/YYYY). Note: You must be 21+ to order wine.`;
		}
		return true;
	};

	const tooltipObject = {
		content:
			'By entering your date of birth, you confirm that you are at least 21 years of age, and old enough to purchase wine.',
		position: 'bottom',
	};

	return (
		<TextInput
			type='text'
			placeholder='Date Of Birth (MM/DD/YYYY)'
			name='date'
			register={register({
				required: `Looks like we don't have your date of birth; please enter it now.`,
				pattern: {
					value:
						/^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.]\d\d\d\d$/i,
					message: 'Please follow the date format: MM/DD/YYYY.',
				},
				validate: {
					legalAge: (value) => checkAge(value),
				},
			})}
			onKeyPress={CheckKeyInput}
			errors={errors}
			onBlur={onBlur}
			tooltip={tooltip ? tooltipObject : null}
			cypressClass={cypressClass}
			inputRef={dobRef}
			{...props}
		/>
	);
};

DateOfBirthInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * Ref for DOB
	 */
	dobRef: PropTypes.any,
	/**
	 * Set Value method from React Hook Form
	 */
	setValue: PropTypes.func,
	/**
	 * Max length value
	 */
	maxLength: PropTypes.any,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Watch method from react hook form
	 */
	watch: PropTypes.func,

	/**
	 * Determines whether to use tooltip or not
	 */
	tooltip: PropTypes.bool,
};

DateOfBirthInput.defaultProps = {
	register: () => {},
	errors: {},
	setValue: () => {},
	cypressClass: 'dateOfBirthInput',
	onBlur: () => {},
	watch: () => {},
	tooltip: false,
};
export default DateOfBirthInput;
