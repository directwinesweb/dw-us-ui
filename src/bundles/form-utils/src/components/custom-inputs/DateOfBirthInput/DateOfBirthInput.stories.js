import DateOfBirthInput from './DateOfBirthInput';
import React from 'react';
import { boolean } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Date of Birth Input',
	parameters: {
		component: DateOfBirthInput,
	},
};

export const Default = () => {
	const { register, errors, setValue, watch } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<DateOfBirthInput
				register={register}
				errors={errors}
				watch={watch}
				setValue={setValue}
				tooltip={boolean('Tooltip', false)}
			/>
		</form>
	);
};
