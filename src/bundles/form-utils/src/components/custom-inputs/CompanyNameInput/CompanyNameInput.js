import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const CompanyNameInput = ({
	register,
	errors,
	onBlur,
	onChange,
	cypressClass,
	...props
}) => {
	return (
		<TextInput
			type='text'
			placeholder='Company Name (optional)'
			name='companyName'
			register={register}
			errors={errors}
			onBlur={onBlur}
			onChange={onChange}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

CompanyNameInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * on Change method
	 */
	onChange: PropTypes.func,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
};
CompanyNameInput.defaultProps = {
	register: () => {},
	errors: {},
	onBlur: () => {},
	onChange: () => {},
	cypressClass: 'CompanyNameInput',
};

export default CompanyNameInput;
