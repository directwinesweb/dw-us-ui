import CompanyNameInput from './CompanyNameInput';
import React from 'react';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Custom Inputs/Company Name Input',
	parameters: {
		component: CompanyNameInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});
	return (
		<form>
			<CompanyNameInput register={register} errors={errors} />
		</form>
	);
};
