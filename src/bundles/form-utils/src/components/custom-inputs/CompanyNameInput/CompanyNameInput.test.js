import CompanyNameInput from './CompanyNameInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Company Name Input test', () => {
	it('Creates snapshot test for <CompanyNameInput />', () => {
		const wrapper = shallow(<CompanyNameInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
