import EmailConfirmationInput from './EmailConfirmationInput';
import React from 'react';
import { text } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Email Confirmation Input',
	parameters: {
		component: EmailConfirmationInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<EmailConfirmationInput
				register={register}
				errors={errors}
				emailToCheck={text('Email to check', 'test@test1.com')}
			/>
		</form>
	);
};
