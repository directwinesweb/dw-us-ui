import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const EmailConfirmationInput = ({
	register,
	errors,
	emailToCheck,
	onBlur,
	cypressClass,
	...props
}) => {
	const checkEmail = (email) => {
		if (email === emailToCheck) {
			return true;
		} else {
			return 'Please enter the same Email again.';
		}
	};

	const onPaste = (event) => {
		event.preventDefault();
		return false;
	};
	return (
		<TextInput
			type='email'
			placeholder='Confirm Email'
			name='emailConfirmation'
			register={register({
				required: 'Please confirm your email address.',
				validate: { sameEmail: (value) => checkEmail(value) },
			})}
			errors={errors}
			onBlur={onBlur}
			onPaste={onPaste}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

EmailConfirmationInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Email to check for confirmation
	 */
	emailToCheck: PropTypes.string,
};
EmailConfirmationInput.defaultProps = {
	emailToCheck: '',
	register: () => {},
	onBlur: () => {},
	errors: {},
	cypressClass: 'emailConfirmationInput',
};
export default EmailConfirmationInput;
