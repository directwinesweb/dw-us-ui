import EmailConfirmationInput from './EmailConfirmationInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Email Confirmation Input test', () => {
	it('Creates snapshot test for <EmailConfirmationInput />', () => {
		const wrapper = shallow(<EmailConfirmationInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
