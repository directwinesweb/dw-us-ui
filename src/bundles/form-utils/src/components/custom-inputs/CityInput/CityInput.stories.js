import CityInput from './CityInput';
import React from 'react';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Custom Inputs/City Input',
	parameters: {
		component: CityInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});
	return (
		<form>
			<CityInput register={register} errors={errors} />
		</form>
	);
};
