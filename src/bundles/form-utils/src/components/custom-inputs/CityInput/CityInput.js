import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const CityInput = ({
	register,
	errors,
	onBlur,
	onChange,
	cypressClass,
	...props
}) => {
	return (
		<TextInput
			type='text'
			placeholder='City'
			name='city'
			register={register({ required: 'Missing City' })}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			onChange={onChange}
			{...props}
		/>
	);
};

CityInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * on Change method
	 */
	onChange: PropTypes.func,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
};
CityInput.defaultProps = {
	register: () => {},
	errors: {},
	onBlur: () => {},
	onChange: () => {},
	cypressClass: 'cityInput',
};

export default CityInput;
