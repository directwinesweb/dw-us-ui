import CityInput from './CityInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('City Input test', () => {
	it('Creates snapshot test for <CityInput />', () => {
		const wrapper = shallow(<CityInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
