import React from 'react';
import StateInput from './StateInput';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('State Input test', () => {
	it('Creates snapshot test for <StateInput />', () => {
		const wrapper = shallow(<StateInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
