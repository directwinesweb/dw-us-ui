import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const StateInput = ({
	register,
	errors,
	onBlur,
	name,
	cypressClass,
	onChange,
	...props
}) => {
	return (
		<TextInput
			type='text'
			placeholder='State'
			name={name || 'state'}
			register={register({ required: 'Missing State' })}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			onChange={onChange}
			{...props}
		/>
	);
};

StateInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * on Change method
	 */
	onChange: PropTypes.func,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
	/**
	 * Input's name
	 */
	name: PropTypes.string,
};
StateInput.defaultProps = {
	register: () => {},
	errors: {},
	onBlur: () => {},
	onChange: () => {},
	cypressClass: 'StateInput',
	name: 'state',
};

export default StateInput;
