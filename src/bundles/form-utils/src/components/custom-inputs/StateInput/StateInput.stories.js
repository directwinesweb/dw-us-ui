import React from 'react';
import StateInput from './StateInput';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Custom Inputs/State Input',
	parameters: {
		component: StateInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});
	return (
		<form>
			<StateInput register={register} errors={errors} />
		</form>
	);
};
