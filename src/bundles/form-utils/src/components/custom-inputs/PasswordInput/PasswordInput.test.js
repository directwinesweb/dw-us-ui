import PasswordInput from './PasswordInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Password Input test', () => {
	it('Creates snapshot test for <PasswordInput />', () => {
		const wrapper = shallow(<PasswordInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
