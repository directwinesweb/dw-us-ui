import PasswordInput from './PasswordInput';
import React from 'react';
import { boolean } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Password Input',
	parameters: {
		component: PasswordInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<PasswordInput
				register={register}
				errors={errors}
				newPassword={boolean('New password?', false)}
			/>
		</form>
	);
};
