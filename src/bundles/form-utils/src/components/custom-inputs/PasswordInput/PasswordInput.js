import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const PasswordInput = ({
	register,
	errors,
	onBlur,
	cypressClass,
	newPassword,
	...props
}) => {
	var registerObject = { required: 'Please enter your password.' };
	if (newPassword) {
		registerObject = {
			...registerObject,
			minLength: {
				value: 8,
				message: 'Please choose a password that is at least 8 characters long.',
			},
			pattern: {
				value:
					/^(?=.*[a-zA-Z].*)(?=.*\d)[a-zA-Z\d^!@#$%&*\_\-\+£^\(\)+=\.;:'"\{\}\[\]\\~/\`]{8,}$/,
				message:
					'Please make sure your password contains 1 number and 1 letter',
			},
		};
	}
	return (
		<TextInput
			type='password'
			placeholder='Password'
			name='password'
			register={register(registerObject)}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

PasswordInput.defaultProps = {
	register: () => {},
	onBlur: () => {},
	errors: {},
	cypressClass: 'passwordInput',
	newPassword: false,
};
PasswordInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Determines whether the input will be used for a new password or login
	 */
	newPassword: PropTypes.bool,
};
export default PasswordInput;
