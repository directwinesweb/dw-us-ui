import PasswordConfirmationInput from './PasswordConfirmationInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Password Confirmation Input test', () => {
	it('Creates snapshot test for <PasswordConfirmationInput />', () => {
		const wrapper = shallow(<PasswordConfirmationInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
