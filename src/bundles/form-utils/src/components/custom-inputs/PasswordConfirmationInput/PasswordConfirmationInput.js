import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';

const PasswordConfirmationInput = ({
	register,
	errors,
	passwordInputValue,
	onBlur = () => {},
	cypressClass,
	...props
}) => {
	const checkPassword = (password) => {
		if (password === passwordInputValue) {
			return true;
		} else {
			return 'Please enter the same Password again.';
		}
	};
	return (
		<TextInput
			type='password'
			placeholder='Confirm Password'
			name='passwordConfirmation'
			register={register({
				required: 'Please re-enter your password.',
				validate: { samePassword: (value) => checkPassword(value) },
			})}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};
PasswordConfirmationInput.propTypes = {
	/**
	 * Object containing the errors from this input field.
	 */
	errors: PropTypes.object,
	/**
	 *
	 * The password value to check against for validation when confirming password
	 */
	passwordInputValue: PropTypes.string,
	/**
	 * A function that gets passed for the useForm hook from react-hook-form to register an input
	 *
	 */
	register: PropTypes.func,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
};

PasswordConfirmationInput.defaultProps = {
	errors: {},
	passwordInputValue: '',
	register: () => {},
	onBlur: () => {},
	cypressClass: 'passwordConfirmationInput',
};
export default PasswordConfirmationInput;
