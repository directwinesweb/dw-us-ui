import PasswordConfirmationInput from './PasswordConfirmationInput';
import React from 'react';
import { text } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Password Confirmation Input',
	parameters: {
		component: PasswordConfirmationInput,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<PasswordConfirmationInput
				register={register}
				errors={errors}
				passwordInputValue={text('Password to validate', 'test1234')}
			/>
		</form>
	);
};
