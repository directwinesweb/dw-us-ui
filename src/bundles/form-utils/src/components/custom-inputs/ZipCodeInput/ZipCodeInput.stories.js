import React from 'react';
import ZipCodeInput from './ZipCodeInput';
import { boolean } from '@storybook/addon-knobs';
import useForm from 'react-hook-form';
export default {
	title: 'Form/Custom Inputs/Zip Code Input',
	parameters: {
		component: ZipCodeInput,
	},
};

export const Default = () => {
	const { register, errors, setValue, setError, clearError, watch } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<ZipCodeInput
				register={register}
				errors={errors}
				watch={watch}
				setError={setError}
				setValue={setValue}
				clearError={clearError}
				// validateAPI={boolean('Validate api', true)}
			/>
		</form>
	);
};
