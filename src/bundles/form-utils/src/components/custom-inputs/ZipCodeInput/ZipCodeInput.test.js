import React from 'react';
import ZipCodeInput from './ZipCodeInput';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Zip Code Input test', () => {
	it('Creates snapshot test for <ZipCodeInput />', () => {
		const wrapper = shallow(<ZipCodeInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
