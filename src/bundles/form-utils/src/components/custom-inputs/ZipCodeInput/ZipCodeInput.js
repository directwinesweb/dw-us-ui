import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';
import styled from '@emotion/styled';
import useZipCodeInput from '../../../hooks/useZipCodeInput';

const StyledTextInput = styled(TextInput)`
	&::-webkit-inner-spin-button,
	&::-webkit-outer-spin-button {
		-webkit-appearance: none;
		margin: 0;
	}
`;

const ZipCodeInput = ({
	register,
	errors,
	watch,
	setValue,
	setError,
	clearError,
	onBlur,
	cypressClass,
	placeholder,
	...props
}) => {
	// ZIP Code Logic
	const watchedZipCode = watch('zipCode');
	useZipCodeInput(watchedZipCode, setValue, setError, clearError);

	return (
		<StyledTextInput
			type='tel'
			placeholder={placeholder || 'ZIP'}
			name='zipCode'
			register={register({
				required: `Looks like we don't have your ZIP code; please enter it now.`,
				minLength: {
					value: 5,
					message: 'Please make sure that your zip code is 5 numbers',
				},
			})}
			maxLength='5'
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

ZipCodeInput.propTypes = {
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * react hook form method method
	 */
	clearError: PropTypes.func,
	/**
	 * react hook form method method
	 */
	setError: PropTypes.func,
	/**
	 * react hook form method method
	 */
	setValue: PropTypes.func,
	/**
	 * react hook form method method
	 */
	watch: PropTypes.func,
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
	// /**
	//  * determine whether to test if input value is a valid zip code
	//  */
	// validateAPI: PropTypes.bool,

	placeholder: PropTypes.string,
	/*
		can pass custom placehlder
	*/
};

ZipCodeInput.defaultProps = {
	register: () => {},
	onBlur: () => {},
	clearError: () => {},
	setError: () => {},
	setValue: () => {},
	watch: () => {},
	errors: {},
	cypressClass: 'ZipCodeInput',
};
export default ZipCodeInput;
