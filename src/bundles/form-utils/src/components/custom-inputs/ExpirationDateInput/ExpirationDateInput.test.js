import ExpirationDateInput from './ExpirationDateInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Expiration Date Input test', () => {
	it('Creates snapshot test for <ExpirationDateInput />', () => {
		const wrapper = shallow(<ExpirationDateInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
