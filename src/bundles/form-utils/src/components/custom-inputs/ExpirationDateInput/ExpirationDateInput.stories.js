import ExpirationDateInput from './ExpirationDateInput';
import React from 'react';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Custom Inputs/Expiration Date Input',
	parameters: {
		component: ExpirationDateInput,
	},
};

export const Default = () => {
	const { register, errors, setValue, watch } = useForm({
		mode: 'onBlur',
	});

	return (
		<form>
			<ExpirationDateInput
				register={register}
				errors={errors}
				watch={watch}
				setValue={setValue}
			/>
		</form>
	);
};
