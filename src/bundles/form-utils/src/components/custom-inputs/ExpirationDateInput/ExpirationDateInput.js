import React, { useEffect } from 'react';

import { CheckKeyInput } from '@dw-us-ui/check-key-input';
import PropTypes from 'prop-types';
import { TextInput } from '../../TextInput';
import styled from '@emotion/styled';
import useExpirationDateInput from '../../../hooks/useExpirationDateInput';

const StyledContainer = styled.div`
	min-width: 150px;
`;
const ExpirationDateInput = ({
	register,
	errors,
	watch,
	setValue,
	cypressClass,
	inputRef,
	onBlur,
	...props
}) => {
	const watchedDate = watch('expirationDate');
	useExpirationDateInput(watchedDate, setValue);

	const checkExpiredDate = (date) => {
		const expirationMonth = parseFloat(date.slice(0, 2));
		const currentMonth = new Date().getMonth() + 1;

		const expirationYear = parseFloat(`20${date.slice(-2)}`);
		const currentYear = new Date().getFullYear();

		const expiredCreditCardMessage =
			'Your credit card has expired; please update or choose another credit card.';

		if (expirationYear === currentYear) {
			if (expirationMonth < currentMonth) {
				return expiredCreditCardMessage;
			} else {
				return true;
			}
		} else if (expirationYear < currentYear) {
			return expiredCreditCardMessage;
		} else {
			return true;
		}
	};

	useEffect(() => {
		if (watchedDate) {
			setValue('expirationMonth', `${watchedDate.substring(0, 2)}`);
			setValue('expirationYear', `20${watchedDate.substring(3, 5)}`);
		}
	}, [watchedDate, setValue]);

	return (
		<StyledContainer>
			<TextInput
				{...props}
				type='text'
				placeholder='Exp. Date (MM/YY)'
				name='expirationDate'
				register={register({
					required: `Please enter your credit card's expiration date.`,
					pattern: {
						value: /^(0[1-9]|1[012])\/[0-9]{2}/,
						message: 'Please make sure to follow this date format MM/YY.',
					},
					validate: {
						isNotExpired: (value) => checkExpiredDate(value),
					},
				})}
				onKeyPress={CheckKeyInput}
				errors={errors}
				inputRef={inputRef}
				cypressClass={cypressClass}
				onBlur={onBlur}
			/>
			{/* Hidden fields mutate data at the field level to be directly accepted by the API, middleware solution may be more desirable
			 https://www.codementor.io/@vkarpov/beginner-s-guide-to-redux-middleware-du107uyud */}
			<TextInput
				type='hidden'
				placeholder='Exp. Month'
				name='expirationMonth'
				errors={errors}
				register={register}
				cypressClass={cypressClass}
				onBlur={onBlur}
				{...props}
			/>
			<TextInput
				type='hidden'
				placeholder='Exp. Year'
				register={register}
				name='expirationYear'
				errors={errors}
				cypressClass={cypressClass}
				onBlur={onBlur}
				{...props}
			/>
		</StyledContainer>
	);
};

ExpirationDateInput.propTypes = {
	/**
	 * Object containing the errors from this input field.
	 */
	errors: PropTypes.object,
	/**
	 * A function that gets passed for the useForm hook from react-hook-form to register an input
	 *
	 */
	register: PropTypes.func,
	/**
	 * cypress class data attribute
	 */
	cypressClass: PropTypes.string,

	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Set value method from react hook form
	 */
	setValue: PropTypes.func,
	/**
	 * Watch method from react hook form
	 */
	watch: PropTypes.func,

	/**
	 * ref for input
	 */
	inputRef: PropTypes.any,
};

ExpirationDateInput.defaultProps = {
	errors: {},
	register: () => {},
	onBlur: () => {},
	cypressClass: 'ExpirationDateInput',
	watch: () => {},
	setValue: () => {},
};
export default ExpirationDateInput;
