import AddressInput from './AddressInput';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Address Input test', () => {
	it('Creates snapshot test for <AddressInput />', () => {
		const wrapper = shallow(<AddressInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
