import { boolean, select } from '@storybook/addon-knobs';

import AddressInput from './AddressInput';
import React from 'react';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Custom Inputs/Address Input',
	parameters: {
		component: AddressInput,
	},
};

export const Default = () => (
	<AddressInput
		type={select('Type', { 1: 1, 2: 2 }, 1)}
		required={boolean('Required', true)}
	/>
);

export const InputValidation = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});
	return (
		<form>
			<AddressInput
				type={select('Type', { 1: 1, 2: 2 }, 1)}
				required={boolean('Required', true)}
				register={register}
				errors={errors}
			/>
		</form>
	);
};
