import PropTypes from 'prop-types';
import React from 'react';
import { TextInput } from '../../TextInput';
import { noPOBoxRegex } from '@dw-us-ui/constants';

const AddressInput = ({
	register,
	errors,
	onBlur,
	cypressClass,
	type,
	required,
	...props
}) => {
	let placeholderText = type == 1 ? 'Address' : 'Apt., Suite, etc. (optional)';
	let name = `address${type}`;

	let requiredMessage = required
		? 'Please enter your mailing address. Note: P.O. boxes not allowed.'
		: false;

	let patternObject = {
		value: noPOBoxRegex,
		message: 'PO Boxes are not allowed',
	};

	return (
		<TextInput
			placeholder={placeholderText}
			name={name}
			register={register({
				required: requiredMessage,
				pattern: patternObject,
			})}
			errors={errors}
			onBlur={onBlur}
			cypressClass={cypressClass}
			{...props}
		/>
	);
};

AddressInput.propTypes = {
	/**
	 * cypress class data attribute
	 */

	cypressClass: PropTypes.string,
	/**
	 * Error object
	 */
	errors: PropTypes.object,
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Determines whether the input is required or not, will throw validation
	 */
	required: PropTypes.bool,
	/**
	 * On blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * Type of address. Only accepts numbers 1 or 2
	 */
	type: PropTypes.oneOf([1, 2]).isRequired,
};

AddressInput.defaultProps = {
	cypressClass: 'addressInput',
	onBlur: () => {},
	register: () => {},
	errors: {},
	required: false,
	type: 1,
};
export default AddressInput;
