import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledSelect = styled.select`
	color: ${({ theme }) => theme.colors.body};
`;
const SelectInput = ({
	label,
	name,
	onChange,
	optionsList,
	value,
	defaultValue,
	cypressClass,
	className,
	...props
}) => {
	const renderOptions = () => {
		let options = [];

		optionsList.map((item, idx) => {
			options.push(
				<option
					key={idx}
					value={item.value}
					data-cy={`${cypressClass}-value-${item.value}`}
				>
					{item.text}
				</option>
			);
		});
		return options;
	};
	return value.length ? (
		<>
			{label.length ? <label>{label}</label> : null}
			<StyledSelect
				onChange={onChange}
				name={name}
				value={value}
				data-cy={cypressClass}
				className={className}
				{...props}
			>
				{renderOptions()}
			</StyledSelect>
		</>
	) : (
		<>
			{label.length ? <label>{label}</label> : null}
			<StyledSelect
				onChange={onChange}
				name={name}
				defaultValue={defaultValue}
				data-cy={cypressClass}
				className={className}
			>
				{renderOptions()}
			</StyledSelect>
		</>
	);
};

SelectInput.propTypes = {
	/**
	 * Class names to append to select container
	 */
	className: PropTypes.string,
	/**
	 * The value that gets displayed to the user next to the select input
	 */
	label: PropTypes.string,
	/**
	 * Name attribute of the input element.
	 */
	name: PropTypes.string,
	/**
	 * The function that will be sent the React event whenever the event is clicked and an item is selected.
	 */
	onChange: PropTypes.func,
	/**
	 *
	 *  A list of options with respective values.
	 */
	optionsList: PropTypes.arrayOf(
		PropTypes.exact({
			/**
			 * text to render in option
			 */
			text: PropTypes.string.isRequired,
			/**
			 * value of the option
			 */
			value: PropTypes.string.isRequired,
		})
	).isRequired,

	/**
	 * Value to give to select for automatic selection
	 */
	value: PropTypes.string,
	/**
	 * Cypress name
	 */
	cypressClass: PropTypes.string,
	/**
	 * Default value to be selected
	 */
	defaultValue: PropTypes.string,
};
SelectInput.defaultProps = {
	className: '',
	label: '',
	name: '',
	onChange: () => {},
	optionsList: [],
	value: '',
	defaultValue: '',
	cypressClass: 'selectInput',
};
export default SelectInput;
