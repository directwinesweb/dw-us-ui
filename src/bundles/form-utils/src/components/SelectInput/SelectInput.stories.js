import React from 'react';
import SelectInput from './SelectInput';
import { text } from '@storybook/addon-knobs';

export default {
	title: 'Form/Select Input',
	parameters: {
		component: SelectInput,
		componentSubtitle: 'Displays a branded radio select',
	},
};

export const Default = () => {
	const options = [
		{ text: 'option 1', value: '1' },
		{ text: 'option 2', value: '2' },
	];
	return (
		<SelectInput optionsList={options} label={text('Label', 'Select Label')} />
	);
};
