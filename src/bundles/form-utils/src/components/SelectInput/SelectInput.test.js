import React from 'react';
import SelectInput from './SelectInput';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Select Input test', () => {
	it('Creates snapshot test for Select Input ', () => {
		const wrapper = shallow(<SelectInput />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
