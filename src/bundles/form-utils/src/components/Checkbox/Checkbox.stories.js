import React, { useState } from 'react';
import { select, text } from '@storybook/addon-knobs';

import Checkbox from './Checkbox';
import useForm from 'react-hook-form';

export default {
	title: 'Form/Checkbox',
	parameters: {
		component: Checkbox,
	},
};

export const Default = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	const [state, setState] = useState({
		isChecked: false,
	});

	const props = {
		checked: state.isChecked,
		onClick: () => {
			setState((prevProps) => ({
				...prevProps,
				isChecked: !state.isChecked,
			}));
		},
	};
	return (
		<form>
			<Checkbox
				{...props}
				label={text('Label', 'Checkbox label')}
				register={register}
			/>
		</form>
	);
};

export const Tooltip = () => {
	const { register, errors } = useForm({
		mode: 'onBlur',
	});

	const [state, setState] = useState({
		isChecked: false,
	});

	const props = {
		checked: state.isChecked,
		onClick: () => {
			setState((prevProps) => ({
				...prevProps,
				isChecked: !state.isChecked,
			}));
		},
	};

	const positions = {
		top: 'top',
		left: 'left',
		right: 'right',
		bottom: 'bottom',
	};
	return (
		<form>
			<Checkbox
				{...props}
				label={text('Label', 'Checkbox label')}
				register={register}
				tooltip={{
					content: text('Tooltip content', 'I am tooltip'),
					position: select('Position', positions, 'bottom'),
				}}
			/>
		</form>
	);
};
