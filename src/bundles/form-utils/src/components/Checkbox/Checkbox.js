import PropTypes from 'prop-types';
import React from 'react';
import { Tooltip } from '@dw-us-ui/tooltip';
import styled from '@emotion/styled';

const CheckboxWrapper = styled.div`
	margin: 5px 0 5px 0;
	.container {
		display: flex;
		flex-flow: row-reverse nowrap;
		justify-content: center;
		align-items: center;
		padding-left: 30px;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		font-family: ${({ theme }) => theme.fonts.secondary};
		color: ${({ theme }) => theme.colors.body};
		p {
			margin: 0 0 0 0.5rem;
		}
	}

	/* Hide the browser's default checkbox */
	.container input {
		position: absolute;
		opacity: 0;
		cursor: pointer;
		height: 0;
		width: 0;
	}

	.checkmark-border {
		align-items: center;
		justify-content: center;
		display: flex;
		position: relative;
		cursor: pointer;
		height: ${({ theme }) => theme.checkbox.checkedBackgroundHeight};
		width: ${({ theme }) => theme.checkbox.checkedBackgroundWidth};
		border: ${({ theme }) => theme.checkbox.checkBorder};
		border-radius: ${({ theme }) => theme.checkbox.borderRadius};
	}

	/* When the checkbox is checked, add a blue background */
	.container input:checked ~ .checkmark-border .checkmark-background {
		align-items: center;
		justify-content: center;
		display: flex;
		position: relative;
		border-radius: ${({ theme }) => theme.checkbox.borderRadius};

		width: ${({ theme }) => theme.checkbox.checkedBackgroundWidth};
		height: ${({ theme }) => theme.checkbox.checkedBackgroundHeight};
		background-color: ${({ theme }) => theme.checkbox.checkedBackgroundColor};
	}

	/* Show the checkmark when checked */
	.container
		input:checked
		~ .checkmark-border
		.checkmark-background
		.checkmark-check {
		width: ${({ theme }) => theme.checkbox.checkMarkWidth};
		height: ${({ theme }) => theme.checkbox.checkMarkHeight};
		border: ${({ theme }) => theme.checkbox.checkMarkBorder};
		border-width: ${({ theme }) => theme.checkbox.checkMarkBorderWidth};
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		transform: rotate(45deg);
	}

	/* Hide the checkmark by default */
	.checkmark:after {
		content: '';
		display: none;
	}
`;

/**
 * A custom visual checkbox.  This element only contains the checkbox,
 * but is designed to still be accessible to screen readers.
 **/
const Checkbox = ({
	className,
	name,
	onBlur,
	onChange,
	label,
	tooltip,
	register,
	cypressClass,
	...props
}) => {
	const createMarkup = (input) => {
		return { __html: input };
	};
	return (
		<CheckboxWrapper className='align-self-start'>
			<label className='container' data-cy={cypressClass}>
				<p dangerouslySetInnerHTML={createMarkup(label)} />
				<input
					type='checkbox'
					name={name}
					className={className}
					onBlur={onBlur}
					onChange={onChange}
					ref={register}
					{...props}
				/>

				<span className='checkmark-border'>
					<span className='checkmark-background'>
						<span className='checkmark-check' />
					</span>
				</span>

				{tooltip ? (
					<div className='checkbox-tooltip ml-3'>
						<Tooltip
							tooltipContent={tooltip.content}
							tooltipPosition={tooltip.position}
						>
							<img
								src='/assets/icons/us/law/tooltip.svg'
								alt='tooltip'
								width='18px'
								height='18px'
							/>
						</Tooltip>
					</div>
				) : (
					''
				)}
			</label>
		</CheckboxWrapper>
	);
};

Checkbox.propTypes = {
	/**
	 * Name of the input
	 */
	name: PropTypes.string.isRequired,
	/**
	 * The value to be used if this checkbox is selected
	 */
	value: PropTypes.any,
	/**
	 * An optional boolean value indicating if the checkbox should be set.
	 * You should use this property if you are creating controlled components
	 */
	checked: PropTypes.bool,
	/**
	 * The function that will be sent the React event whenever the event is clicked.
	 * Useful when integrating this as a controlled element in a larger form (e.g. Formik)
	 */
	onChange: PropTypes.func,
	/**
	 * String value that defines the label
	 */
	label: PropTypes.string,
	/**
	 * The function that will be sent the React event whenever the event is blurred.
	 * Useful when integrating this as a controlled element in a larger form (e.g. Formik)
	 */
	cypressClass: PropTypes.string,
	/**
	 * Checkbox classname
	 */
	className: PropTypes.string,
	/**
	 * on blur method
	 */
	onBlur: PropTypes.func,
	/**
	 * react hook form register method
	 */
	register: PropTypes.func,
	/**
	 * Tooltip object
	 */
	tooltip: PropTypes.oneOfType([
		PropTypes.bool,
		PropTypes.shape({
			content: PropTypes.any,
			position: PropTypes.oneOf(['bottom', 'top', 'left', 'right']),
		}),
	]),
};

Checkbox.defaultProps = {
	name: 'checkbox',
	cypressClass: 'checkbx',
	className: '',
	onBlur: () => {},
	onChange: () => {},
	label: '',
	tooltip: false,
	register: () => {},
};

export default Checkbox;
