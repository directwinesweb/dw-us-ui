import Checkbox from './Checkbox';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('Checkbox test', () => {
	it('Creates snapshot test for Checkbox ', () => {
		const wrapper = shallow(<Checkbox />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
