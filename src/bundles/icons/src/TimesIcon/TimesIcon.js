import PropTypes from 'prop-types';
import React from 'react';

const TimesIcon = ({ brandTag, className, width, height, viewBox, type }) => {
	if (type === 'svg') {
		return (
			<svg
				className={className}
				focusable='false'
				x='0px'
				y='0px'
				viewBox={viewBox}
				width={width}
				height={height}
			>
				<path
					d='M9.8,8.1l5.2-5.2l1.1-1.1c0.2-0.2,0.2-0.4,0-0.6L15,0.1c-0.2-0.2-0.4-0.2-0.6,0L8.1,6.4L1.8,0.1C1.7,0,1.4,0,1.3,0.1
	L0.1,1.3C0,1.4,0,1.7,0.1,1.8l6.3,6.3l-6.3,6.3C0,14.6,0,14.8,0.1,15l1.1,1.1c0.2,0.2,0.4,0.2,0.6,0l6.3-6.3l5.2,5.2l1.1,1.1
	c0.2,0.2,0.4,0.2,0.6,0l1.1-1.1c0.2-0.2,0.2-0.4,0-0.6L9.8,8.1z'
				/>
			</svg>
		);
	}

	return (
		<img
			src={`/images/us/common/icons/${brandTag}/times_light_16x16.svg`}
			alt='times'
			height={height}
			width={width}
		/>
	);
};

TimesIcon.propTypes = {
	/**
	 * Brand tag for fetching icon path
	 */
	brandTag: PropTypes.string,
	/**
	 * Icon Class name
	 */
	className: PropTypes.string,
	/**
	 * width of icon
	 */
	width: PropTypes.string,
	/**
	 * height of icon
	 */
	height: PropTypes.string,
	/**
	 * Viewbox to be used with svg
	 */
	viewBox: PropTypes.string,
	/**
	 * Determines whether to return SVG or IMG tag for icon
	 */
	type: PropTypes.oneOf(['svg', 'img']),
};
TimesIcon.defaultProps = {
	brandTag: 'wsj',
	className: null,
	type: 'img',
	width: '16px',
	height: '16px',
	viewBox: '0 0 16.2 16.2',
};

export default TimesIcon;
