import React from 'react';
import TimesIcon from './TimesIcon';

export default {
	title: 'Icons/Times Icon',
	parameters: {
		component: TimesIcon,
	},
	argTypes: {
		type: { control: { type: 'radio' }, options: ['svg', 'img'] },
		brandTag: {
			control: { type: 'radio' },
			options: ['wsj', 'law', 'vir'],
		},
	},
};

export const Default = (args) => (
	<div style={{ textAlign: 'center' }}>
		<TimesIcon {...args} />
	</div>
);
