import React from 'react';
import TimesIcon from './TimesIcon';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('TimesIcon test', () => {
	it('Creates snapshot test for Times Icon ', () => {
		const wrapper = shallow(<TimesIcon />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot SVG for Times Icon  ', () => {
		const wrapper = shallow(<TimesIcon type='svg' />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
