import React from 'react';
import SearchIcon from './SearchIcon';

export default {
	title: 'Icons/Search Icon',
	parameters: {
		component: SearchIcon,
	},
	argTypes: {
		type: { control: { type: 'radio' }, options: ['svg', 'img'] },
		brandTag: {
			control: { type: 'radio' },
			options: ['wsj', 'law', 'vir'],
		},
	},
};

export const Default = (args) => (
	<div style={{ textAlign: 'center' }}>
		<SearchIcon {...args} />
	</div>
);
