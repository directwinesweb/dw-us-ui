import PropTypes from 'prop-types';
import React from 'react';

const SearchIcon = ({ brandTag, className, width, height, viewBox, type }) => {
	if (type === 'svg') {
		return (
			<svg
				className={className}
				focusable='false'
				x='0px'
				y='0px'
				viewBox={viewBox}
				width={width}
				height={height}
				style={{ enableBackground: 'new 0 0 16.9 19' }}
			>
				<path
					d='M16.7,15.7l-3.3-3.3c-0.1-0.1-0.4-0.2-0.6-0.2h-0.5c0.9-1.2,1.5-2.6,1.5-4.2c0-3.8-3.1-6.9-6.9-6.9S0,4.1,0,7.9
       s3.1,6.9,6.9,6.9c1.6,0,3.1-0.5,4.2-1.5v0.5c0,0.2,0.1,0.4,0.2,0.6l3.3,3.3c0.3,0.3,0.8,0.3,1.1,0l0.9-0.9C17,16.5,17,16,16.7,15.7z
        M6.9,12.1c-2.3,0-4.2-1.9-4.2-4.2c0-2.3,1.9-4.2,4.2-4.2c2.3,0,4.2,1.9,4.2,4.2C11.1,10.2,9.2,12.1,6.9,12.1z'
				/>
			</svg>
		);
	}

	return (
		<img
			src={`/images/us/common/icons/${brandTag}/search_solid_16x19.svg`}
			alt='search'
			width={width}
			height={height}
			className={className}
		/>
	);
};

SearchIcon.propTypes = {
	/**
	 * Brand tag for fetching icon path
	 */
	brandTag: PropTypes.string,
	/**
	 * Icon Class name
	 */
	className: PropTypes.string,
	/**
	 * width of icon
	 */
	width: PropTypes.string,
	/**
	 * height of icon
	 */
	height: PropTypes.string,
	/**
	 * Viewbox to be used with svg
	 */
	viewBox: PropTypes.string,
	/**
	 * Determines whether to return SVG or IMG tag for icon
	 */
	type: PropTypes.oneOf(['svg', 'img']),
};
SearchIcon.defaultProps = {
	brandTag: 'wsj',
	className: null,
	type: 'img',
	width: '20px',
	height: '25px',
	viewBox: '0 0 16.9 19',
};

export default SearchIcon;
