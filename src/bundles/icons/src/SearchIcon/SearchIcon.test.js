import React from 'react';
import SearchIcon from './SearchIcon';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('SearchIcon test', () => {
	it('Creates snapshot test for Search Icon ', () => {
		const wrapper = shallow(<SearchIcon />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot SVG for Search Icon  ', () => {
		const wrapper = shallow(<SearchIcon type='svg' />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
