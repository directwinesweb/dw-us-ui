import PrintIcon from './PrintIcon';
import React from 'react';

export default {
	title: 'Icons/PrintIcon',
	parameters: {
		component: PrintIcon,
	},
};

export const Default = () => (
	<div style={{ textAlign: 'center' }}>
		<PrintIcon />
	</div>
);
