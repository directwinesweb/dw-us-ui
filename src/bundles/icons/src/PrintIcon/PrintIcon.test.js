import PrintIcon from './PrintIcon';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('PrintIcon test', () => {
	it('Creates snapshot test for <PrintIcon />', () => {
		const wrapper = shallow(<PrintIcon></PrintIcon>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
