import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledPrintIconWrapper = styled.a`
	cursor: pointer;
`;

const PrintIcon = ({ printClassName }) => {
	const printPage = () => {
		window.print();
		return false;
	};
	return (
		<StyledPrintIconWrapper
			onClick={() => {
				printPage();
			}}
			className={'print-icon' || printClassName}
		>
			<img
				src='/images/us/common/icons/law/print-solid.svg'
				alt='print'
				height='15px'
				width='15px'
			/>
		</StyledPrintIconWrapper>
	);
};
PrintIcon.propTypes = {
	/**
	 * Class name for print icon
	 */
	printClassName: PropTypes.string,
};

PrintIcon.defaultProps = {};

export default PrintIcon;
