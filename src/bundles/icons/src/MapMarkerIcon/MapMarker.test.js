import MapMarkerIcon from './MapMarkerIcon';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('MapMarkerIcon test', () => {
	it('Creates snapshot test for Map Marker Icon ', () => {
		const wrapper = shallow(<MapMarkerIcon />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
