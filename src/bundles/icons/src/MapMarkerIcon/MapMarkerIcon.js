import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const MapMarkerIconSvg = styled.svg`
	.marker-path {
		fill: ${({ fill }) => (fill ? fill : '#fff')};
		stroke: ${({ strokeFill }) => (strokeFill ? strokeFill : '#000')};
		stroke-linecap: round;
		stroke-linejoin: round;
		stroke-miterlimit: 10;
		enable-background: new 0 0 26 36;
		stroke-width: 2px;
	}
`;

const MapMarkerIcon = ({
	fill,
	strokeFill,
	width,
	height,
	viewBox,
	className,
	id,
}) => {
	return (
		<MapMarkerIconSvg
			version='1.1'
			id={id}
			xmlns='http://www.w3.org/2000/svg'
			x='0px'
			y='0px'
			viewBox={viewBox}
			width={width}
			height={height}
			fill={fill}
			strokeFill={strokeFill}
			className={className}
		>
			<path
				className='marker-path'
				d='M13,1C8.1,1,0.8,3.9,0.8,13.5S13,35,13,35s12.2-12,12.2-21.5S17.9,1,13,1z'
			/>
		</MapMarkerIconSvg>
	);
};

MapMarkerIcon.propTypes = {
	id: PropTypes.string,
	/**
	 * Class name for the icon
	 */
	className: PropTypes.string,
	/**
	 * Color to fill
	 */
	fill: PropTypes.string,
	/**
	 * Stroke fill color
	 */
	strokeFill: PropTypes.string,
	/**
	 * Width of icon
	 */
	width: PropTypes.string,
	/**
	 * Height of icon
	 */
	height: PropTypes.string,
	/**svg viewbox */
	viewBox: PropTypes.string,
};
MapMarkerIcon.defaultProps = {
	id: null,
	className: '',
	width: '26px',
	height: '36px',
	strokeFill: '#000000',
	fill: '#FFF',
	viewBox: '0 0 26 36',
};

export default MapMarkerIcon;
