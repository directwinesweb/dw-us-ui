import MapMarkerIcon from './MapMarkerIcon';
import React from 'react';
import { text } from '@storybook/addon-knobs';

export default {
	title: 'Icons/Map Marker',
	parameters: {
		component: MapMarkerIcon,
	},
};

export const Default = () => (
	<div style={{ textAlign: 'center' }}>
		<MapMarkerIcon
			fill={text('Fill', '#fff')}
			strokeFill={text('Stroke fill', '#000')}
			className='story-map'
		/>
	</div>
);
