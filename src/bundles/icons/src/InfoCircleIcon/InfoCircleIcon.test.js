import InfoCircleIcon from './InfoCircleIcon';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('InfoCircleIcon test', () => {
	it('Creates snapshot test for Info Circle Icon ', () => {
		const wrapper = shallow(<InfoCircleIcon />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot SVG for Info Circle Icon  ', () => {
		const wrapper = shallow(<InfoCircleIcon type='svg' />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
