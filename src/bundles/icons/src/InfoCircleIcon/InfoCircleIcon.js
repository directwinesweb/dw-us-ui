import PropTypes from 'prop-types';
import React from 'react';
import styled from '@emotion/styled';

const StyledSVG = styled.svg`
	.info-light-path {
		fill: #333333;
	}
	.info-solid-path {
		fill: #a2a1a1;
	}
`;

const InfoCircleIcon = ({
	brandTag,
	className,
	width,
	height,
	viewBox,
	type,
	versionStyle,
}) => {
	let imagePath =
		versionStyle === 'light'
			? 'info_circle_light_16x16.svg'
			: 'info_circle_solid_4x12.svg';

	if (type === 'svg') {
		return (
			<StyledSVG
				className={className}
				focusable='false'
				x='0px'
				y='0px'
				viewBox={viewBox}
				width={width}
				height={height}
			>
				{versionStyle === 'light' ? (
					<path
						className='info-light-path'
						d='M8,1c3.8,0,7,3.1,7,7c0,3.8-3.1,7-7,7c-3.8,0-7-3.1-7-7C1,4.2,4.1,1,8,1 M8,0C3.6,0,0,3.6,0,8c0,4.4,3.6,8,8,8
	s8-3.6,8-8C16,3.6,12.4,0,8,0z M6.8,11.1h0.4V7.2H6.8c-0.2,0-0.4-0.2-0.4-0.4V6.6c0-0.2,0.2-0.4,0.4-0.4h1.5c0.2,0,0.4,0.2,0.4,0.4
	v4.5h0.4c0.2,0,0.4,0.2,0.4,0.4v0.3c0,0.2-0.2,0.4-0.4,0.4H6.8c-0.2,0-0.4-0.2-0.4-0.4v-0.3C6.5,11.3,6.6,11.1,6.8,11.1z M8,3.4
	c-0.6,0-1,0.5-1,1s0.5,1,1,1s1-0.5,1-1S8.6,3.4,8,3.4z'
					/>
				) : (
					<path
						className='info-solid-path'
						d='M2.2,3.8C1,3.8,0,4.8,0,6c0,1.2,1,2.1,2.2,2.1s2.2-1,2.2-2.1C4.3,4.8,3.3,3.8,2.2,3.8z M2.2,4.8
	c0.2,0,0.4,0.2,0.4,0.4c0,0.2-0.2,0.4-0.4,0.4S1.8,5.4,1.8,5.2C1.8,5,1.9,4.8,2.2,4.8z M2.6,7c0,0.1,0,0.1-0.1,0.1H1.8
	c-0.1,0-0.1,0-0.1-0.1V6.8c0-0.1,0-0.1,0.1-0.1h0.1V6.1H1.8c-0.1,0-0.1,0-0.1-0.1V5.8c0-0.1,0-0.1,0.1-0.1h0.6c0.1,0,0.1,0,0.1,0.1
	v0.9h0.1c0.1,0,0.1,0,0.1,0.1V7z'
					/>
				)}
			</StyledSVG>
		);
	}

	return (
		<img
			src={`/images/us/common/icons/${brandTag}/${imagePath}`}
			alt='info'
			height={height}
			width={width}
		/>
	);
};

InfoCircleIcon.propTypes = {
	/**
	 * Brand tag for fetching icon path
	 */
	brandTag: PropTypes.string,
	/**
	 * Icon Class name
	 */
	className: PropTypes.string,
	/**
	 * width of icon
	 */
	width: PropTypes.string,
	/**
	 * height of icon
	 */
	height: PropTypes.string,
	/**
	 * Viewbox to be used with svg
	 */
	viewBox: PropTypes.string,
	/**
	 * Determines whether to return SVG or IMG tag for icon
	 */
	type: PropTypes.oneOf(['svg', 'img']),
	/**
	 * Determines if icon is solid or light
	 */
	versionStyle: PropTypes.oneOf(['light', 'solid']),
};
InfoCircleIcon.defaultProps = {
	brandTag: 'wsj',
	className: null,
	type: 'img',
	width: '16px',
	height: '16px',
	viewBox: '0 0 16 16',
	versionStyle: 'light',
};

export default InfoCircleIcon;
