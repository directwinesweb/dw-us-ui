import InfoCircleIcon from './InfoCircleIcon';
import React from 'react';

export default {
	title: 'Icons/Info Circle Icon',
	parameters: {
		component: InfoCircleIcon,
	},
	argTypes: {
		type: { control: { type: 'radio' }, options: ['svg', 'img'] },
		brandTag: {
			control: { type: 'radio' },
			options: ['wsj', 'law', 'vir'],
		},
	},
};

export const Default = (args) => (
	<div style={{ textAlign: 'center' }}>
		<InfoCircleIcon {...args} />
	</div>
);
