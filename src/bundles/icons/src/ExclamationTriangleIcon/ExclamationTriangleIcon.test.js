import ExclamationTriangleIcon from './ExclamationTriangleIcon';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('ExclamationTriangleIcon test', () => {
	it('Creates snapshot test for ExclamationTriangle Icon ', () => {
		const wrapper = shallow(<ExclamationTriangleIcon />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot SVG for ExclamationTriangle Icon  ', () => {
		const wrapper = shallow(<ExclamationTriangleIcon type='svg' />);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
