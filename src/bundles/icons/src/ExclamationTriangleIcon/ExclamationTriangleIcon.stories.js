import ExclamationTriangleIcon from './ExclamationTriangleIcon';
import React from 'react';

export default {
	title: 'Icons/Exclamation Triangle Icon',
	parameters: {
		component: ExclamationTriangleIcon,
	},
	argTypes: {
		type: { control: { type: 'radio' }, options: ['svg', 'img'] },
		brandTag: {
			control: { type: 'radio' },
			options: ['wsj', 'law', 'vir'],
		},
	},
};

export const Default = (args) => (
	<div style={{ textAlign: 'center' }}>
		<ExclamationTriangleIcon {...args} />
	</div>
);
