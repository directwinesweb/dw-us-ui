import PropTypes from 'prop-types';
import React from 'react';

const ExclamationTriangleIcon = ({
	brandTag,
	className,
	width,
	height,
	viewBox,
	type,
}) => {
	if (type === 'svg') {
		return (
			<svg
				className={className}
				focusable='false'
				x='0px'
				y='0px'
				viewBox={viewBox}
				width={width}
				height={height}
				fill='#D1782A'
			>
				<path
					className='exclamation-triangle-path'
					d='M16,12.4c0.5,0.9-0.1,2-1.2,2H1.4c-1,0-1.7-1.1-1.2-2L6.9,0.7c0.5-0.9,1.8-0.9,2.3,0L16,12.4L16,12.4z M8.1,9.9
	c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S8.8,9.9,8.1,9.9z M6.9,5.3l0.2,3.8c0,0.2,0.2,0.3,0.3,0.3h1.4
	c0.2,0,0.3-0.1,0.3-0.3l0.2-3.8c0-0.2-0.1-0.4-0.3-0.4H7.2C7,4.9,6.9,5.1,6.9,5.3L6.9,5.3z'
				/>
			</svg>
		);
	}

	return (
		<img
			className={['exclamation-error', className].join(' ')}
			src={`/images/us/common/icons/${brandTag}/exclamation_triangle_solid_16x14.svg`}
			alt='exclamation-triangle'
			height={height}
			width={width}
		/>
	);
};

ExclamationTriangleIcon.propTypes = {
	/**
	 * Brand tag for fetching icon path
	 */
	brandTag: PropTypes.string,
	/**
	 * Icon Class name
	 */
	className: PropTypes.string,
	/**
	 * width of icon
	 */
	width: PropTypes.string,
	/**
	 * height of icon
	 */
	height: PropTypes.string,
	/**
	 * Viewbox to be used with svg
	 */
	viewBox: PropTypes.string,
	/**
	 * Determines whether to return SVG or IMG tag for icon
	 */
	type: PropTypes.oneOf(['svg', 'img']),
};
ExclamationTriangleIcon.defaultProps = {
	brandTag: 'wsj',
	className: null,
	type: 'img',
	width: '16px',
	height: '14px',
	viewBox: '0 0 16.2 14.4',
};

export default ExclamationTriangleIcon;
