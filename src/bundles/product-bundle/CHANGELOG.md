# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/product-bundle@1.0.2...@dw-us-ui/product-bundle@1.0.3) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/product-bundle






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/product-bundle@1.0.1...@dw-us-ui/product-bundle@1.0.2) (2021-07-02)


### Bug Fixes

* font change, texas winery change and skudata bug fix ([4c3b6a5](https://bitbucket.org/directwinesweb/dw-us-ui/commits/4c3b6a5ad624222d79abe00406038cd10234ac83))





## 1.0.1 (2021-06-30)


### Bug Fixes

* initial commit on pdp pages ([ffa85f1](https://bitbucket.org/directwinesweb/dw-us-ui/commits/ffa85f10e235e715c80b1e02fd38532d81cefe5a))
* minor fixes ([5917daa](https://bitbucket.org/directwinesweb/dw-us-ui/commits/5917daaafa114eeca50e1da680efcab775677e6b))
* optimized code and ui fixes ([069b043](https://bitbucket.org/directwinesweb/dw-us-ui/commits/069b043299c9a184246ceff62ea4121a4170c922))
* refactored components ([2d1125c](https://bitbucket.org/directwinesweb/dw-us-ui/commits/2d1125c03f06afaebc58b647213b4139a4fe1f42))
* removed unwanted component from bundle ([40460eb](https://bitbucket.org/directwinesweb/dw-us-ui/commits/40460eb77427e0fde1dc84bc1ee736eed17f0a9c))
