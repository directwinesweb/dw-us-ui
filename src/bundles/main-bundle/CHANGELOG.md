# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.1](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.1.0...@dw-us-ui/main-bundle@1.1.1) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/main-bundle






# [1.1.0](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.8...@dw-us-ui/main-bundle@1.1.0) (2021-07-02)


### Bug Fixes

* down merge master ([174f206](https://bitbucket.org/directwinesweb/dw-us-ui/commits/174f206b37d77cf305cac57abb6bfc780074f8f1))


### Features

* added useContentfulPreview and updated contentful logic and bundle exports ([7f793f9](https://bitbucket.org/directwinesweb/dw-us-ui/commits/7f793f9855bae0faecb188c02c3d2b342ec08f8c))
* added useProfile export ([fd2cb1f](https://bitbucket.org/directwinesweb/dw-us-ui/commits/fd2cb1f65d67d0710b20268760983b7e8be6e707))





## [1.0.8](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.7...@dw-us-ui/main-bundle@1.0.8) (2021-06-30)

**Note:** Version bump only for package @dw-us-ui/main-bundle






## [1.0.7](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.6...@dw-us-ui/main-bundle@1.0.7) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/main-bundle





## [1.0.6](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.4...@dw-us-ui/main-bundle@1.0.6) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/main-bundle





## [1.0.5](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.4...@dw-us-ui/main-bundle@1.0.5) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/main-bundle





## [1.0.4](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.3...@dw-us-ui/main-bundle@1.0.4) (2021-05-10)

**Note:** Version bump only for package @dw-us-ui/main-bundle





## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.2...@dw-us-ui/main-bundle@1.0.3) (2021-05-05)


### Bug Fixes

* added usePageLayer hook to page-layer-provider ([810a524](https://bitbucket.org/directwinesweb/dw-us-ui/commits/810a5244bf69ecdeeca4c3bbb694ca97bc85b134))





## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/main-bundle@1.0.1...@dw-us-ui/main-bundle@1.0.2) (2021-05-05)


### Bug Fixes

* added usePageLayer hook to page-layer-provider ([810a524](https://bitbucket.org/directwinesweb/dw-us-ui/commits/810a5244bf69ecdeeca4c3bbb694ca97bc85b134))





## 1.0.1 (2021-03-25)

**Note:** Version bump only for package @dw-us-ui/main-bundle
