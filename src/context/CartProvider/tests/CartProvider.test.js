import { CartProvider } from '../src/CartProvider';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('CartProvider test', () => {
	it('Creates snapshot test for <CartProvider />', () => {
		const wrapper = shallow(<CartProvider></CartProvider>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
