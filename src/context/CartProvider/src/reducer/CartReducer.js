/**
 * @todo Might need to add to show the modal after adding to cart
 */
const resetError = {
	hasError: false,
	message: false,
};
/**
 *
 * @param {*} state - state from the cart provider
 * @param {*} action - fire actions for cart provider
 */
export default (state, action) => {
	switch (action.type) {
		case 'FETCHING_CART':
			return {
				...state,
				isLoading: true,
				errorDetails: {
					...resetError,
				},
			};
		case 'FETCH_CART':
			return {
				...state,
				cartDetails: action.payload.cartDetails,
				isCartEmpty: action.payload.isCartEmpty,
				finalCartItems: action.payload.finalCartItems,
				isEVouchersOnly: action.payload.isEVouchersOnly,
				isUnlimitedInCart: action.payload.isUnlimitedInCart,
				isWineClubCases: action.payload.isWineClubCases,
				isLoading: false,
				errorDetails: {
					...resetError,
				},
			};
		case 'ADD_ITEM':
			return {
				...state,
				isLoading: true,
				errorDetails: {
					...resetError,
				},
			};
		case 'UPDATE_ITEM':
			return {
				...state,
				isLoading: true,
				errorDetails: {
					...resetError,
				},
			};
		case 'DELETE_ITEM':
			return {
				...state,
				isLoading: true,
				errorDetails: {
					...resetError,
				},
			};
		case 'CART_ERROR':
			return {
				...state,
				isLoading: false,
				errorDetails: {
					hasError: true,
					message: action.payload,
				},
			};
		case 'TRIGGER_MODAL':
			return {
				...state,
				isLoading: false,
				showCartModal: action.payload,
				errorDetails: {
					...resetError,
				},
			};
		default:
			return {
				...state,
			};
	}
};
