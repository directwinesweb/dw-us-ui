import CartItem from './CartItem';
import { DisplayMessage } from '@dw-us-ui/display-message';
import { EmotionBreakpoint } from '@dw-us-ui/emotion-breakpoint';
import { LoadingIcon } from '@dw-us-ui/loading-icon';
import { OrderSummary } from '@dw-us-ui/order-summary';
import React from 'react';
import styled from '@emotion/styled';
import useCart from '../hooks/useCart';

const StyledCartContents = styled.div`
	font-family: ${({ theme }) => theme.fonts.primary};
	color: ${({ theme }) => theme.colors.body};
	.cart-table {
		border: 1px solid ${({ theme }) => theme.colors.borderBottom};
		.cart-items {
			.cart-table-row:last-of-type {
				border-bottom: none;
			}
		}

		.cart-table-row {
			display: flex;
			align-items: center;
			justify-content: space-between;
			padding: 5px 8px;
			text-align: center;
			border-bottom: 1px solid #ddd;
			margin-bottom: 10px;
			.cart-flex-lg {
				flex-basis: 60%;
				text-align: left;
			}
			.cart-flex-md {
				flex-basis: 20%;
			}
			.cart-flex-sm {
				flex-basis: 20%;
			}
			.cart-flex-last {
				flex-basis: 10%;
			}
			.cart-header {
				font-weight: bold;
				font-family: ${({ theme }) => theme.fonts.subHeader};
				font-size: 14px;
				${EmotionBreakpoint('mobile')} {
					font-size: 12px;
					font-weight: normal;
				}
			}

			&.cart-item {
				padding-bottom: 10px;
				font-size: 14px;

				&.error {
					background: #f2dede;
					margin: 10px 0 0;
					color: #a94442;
				}
				.cart-item-info {
					text-align: left;
					line-height: 24px;

					h4 {
						margin: 0;
						font-weight: normal;
						font-size: 15px;
						${EmotionBreakpoint('mobile')} {
							line-height: 18px;
							font-size: 14px;
							padding-right: 5px;
						}
					}
					span {
						color: #666;
						font-size: 0.8em;
					}
				}
				.cart-pricing {
					display: flex;
					align-items: center;
					justify-content: center;
					flex-direction: column;
					line-height: 18px;
					.cart-price {
						margin-bottom: 5px;
						${EmotionBreakpoint('mobile')} {
							font-size: 12px;
						}
						@media screen and (max-width: 330px) {
							font-size: 0.8em;
						}
					}
					.reg-price {
						color: #616365;
						text-decoration: line-through;
					}
				}
			}
		}
		.input-qty {
			width: 50px;
			display: block;
			margin: 0 auto;
			height: 34px;
			padding: 6px 10px;
			font-size: 14px;
			${EmotionBreakpoint('mobile')} {
				font-size: 12px;
			}
			line-height: 1.428571429;
			color: #555;
			background-color: #fff;
			background-image: none;
			border: 1px solid #ccc;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			-webkit-transition: border-color ease-in-out 0.15s,
				box-shadow ease-in-out 0.15s;
			-o-transition: border-color ease-in-out 0.15s,
				box-shadow ease-in-out 0.15s;
			transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;

			&:disabled {
				background-color: #eee;
			}
		}
	}
	.cart-info {
		background: #fff;
		padding-top: 15px;
		padding-bottom: 15px;
		display: flex;
		align-items: center;
		justify-content: flex-end;
		.cart-order-summary {
			background: #fff;
			margin: 10px 0 10px 10px;
			padding: 20px 11px 20px 20px;
			flex-basis: 300px;

			${EmotionBreakpoint('mobile')} {
				flex-basis: 100%;
				margin: 10px 0 10px 0px;
				padding: 20px 0px 20px 0px;
			}
		}
	}

	.alert-danger {
		background: #f2dede;
		border-color: #ebccd1;
		color: #a94442;
		padding: 15px;
		margin-bottom: 20px;
		border: 1px solid transparent;
		border-radius: 4px;
	}
`;

/**
 * Cart Contents Component
 * Holds the contents of the modal
 * @todo - Add TextInput resuable input
 * @todo Finish Design - work with design team
 */
const CartContents = () => {
	const { cartState, cartActions } = useCart();
	/**
	 * @param {*} - event from input
	 * @param {object} - item - object with itemCode / quantity
	 * @param {number} - currentQty - current quantity
	 * if 0 - trigger delete action
	 * if > 1 - update item
	 */
	const update = (e, item) => {
		e.preventDefault();
		const val = e.target.value;
		const { quantity, itemCode } = item;
		if (quantity == val) {
			return;
		}
		if (val == 0 || !val.length) {
			cartActions.deleteItem(item.itemCode);
		} else {
			cartActions.updateItem({ itemCode, quantity: val });
		}
	};

	const remove = (e, itemCode) => {
		e.preventDefault();
		cartActions.deleteItem(itemCode);
	};

	const removeCommerceItem = (e, commerceItemId) => {
		e.preventDefault();
		cartActions.deleteCommerceItem(commerceItemId);
	};

	/**
	 * Error Message for empty cart
	 */
	if (
		cartState.isCartEmpty ||
		Object.entries(cartState.cartDetails).length === 0
	) {
		return (
			<DisplayMessage type='error'>
				Your shopping cart is currently empty
			</DisplayMessage>
		);
	}

	/**
	 * Loading Bar
	 * @todo - work with design team to design it
	 */
	if (cartState.isLoading) {
		return <LoadingIcon />;
	}

	/**
	 * Cart Contents Table
	 */
	return (
		<StyledCartContents>
			{
				cartState.errorDetails.hasError && 
					<div className='alert-danger'>
						{cartState.errorDetails.message.response.data.errorResponse.message}
					</div>
			}
			<div className='cart-table'>
				<div className='cart-table-header'>
					<div className='cart-table-row'>
						<span className='cart-header cart-flex-lg'>Item Description</span>
						<span className='cart-header cart-flex-sm'>Qty</span>
						<span className='cart-header cart-flex-sm'>Remove</span>
						<span className='cart-header cart-flex-md'>Price</span>
						<span className='cart-header cart-flex-last'>Subtotal</span>
					</div>
				</div>
				<div className='cart-items'>
					{cartState.finalCartItems.map((item, index) => (
						<CartItem
							item={item}
							key={index}
							remove={remove}
							removeCommerceItem={removeCommerceItem}
							update={update}
						/>
					))}
				</div>
			</div>
			<div className='cart-info'>
				<OrderSummary
					className='cart-order-summary'
					orderPriceInfo={cartState.cartDetails.orderPriceInfo}
					showViewCartLink={false}
				/>
			</div>
		</StyledCartContents>
	);
};

export default CartContents;
