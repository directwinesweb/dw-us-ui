import { BrandUtil } from '@dw-us-ui/brand-util';
import PropTypes from 'prop-types';
import React from 'react';

/**
 *
 * @param {object} item - Passes the formatted data
 * Maps out the cart item row
 */
const CartItem = ({ item, remove, removeCommerceItem, update }) => {
	const {
		name,
		itemCode,
		itemError,
		quantity,
		bottleText,
		priceDetails,
		subTotalDetails,
		disableQty,
		isServiceItem,
		isWinePlanItem,
		lineItemIdentification,
		isEVoucherItem = false,
	} = item;

	/**
	 *
	 * @param {*} event
	 * Make sure to you can only enter digits
	 */
	const checkDigit = (event) => {
		var numberRegex = new RegExp('^[0-9]+$');
		var key = event.key;

		if (!numberRegex.test(key)) {
			event.preventDefault();
		}

		return;
	};

	const brandTag = BrandUtil.content(BrandUtil.getBrand()).tag;

	return (
		<div className={`cart-table-row cart-item ${itemError ? ' error' : ''}`}>
			<div className='cart-item-info cart-flex-lg'>
				<h4>{name}</h4>
				<span>
					Item # {itemCode} {bottleText && !isWinePlanItem ? bottleText : null}
				</span>
			</div>
			<div className='cart-item-qty cart-flex-sm'>
				{disableQty ? (
					<input
						type='number'
						defaultValue={quantity}
						className='input-qty'
						disabled={disableQty}
					/>
				) : null}
				{isServiceItem || isWinePlanItem ? (
					<a
						href='#'
						className='cart-remove-link'
						onClick={(e) => {
							isEVoucherItem
								? removeCommerceItem(
										e,
										lineItemIdentification.commerceItemIdList[0]
								  )
								: remove(e, itemCode);
						}}
					>
						remove
					</a>
				) : null}
				{!disableQty && !isServiceItem ? (
					<>
						{!isEVoucherItem ? (
							<>
								<input
									type='tel'
									maxLength={2}
									defaultValue={quantity}
									className='input-qty'
									onBlur={(e) => update(e, { itemCode, quantity })}
									onKeyPress={(e) => checkDigit(e)}
								/>
								<a
									href='#'
									className='cart-update-link'
									onClick={(e) => e.preventDefault()}
								>
									update
								</a>
							</>
						) : (
							<>
								<input
									type='tel'
									maxLength={2}
									disabled='true'
									defaultValue={quantity}
									className='input-qty'
								/>
							</>
						)}
					</>
				) : null}
			</div>
			<div className='cart-item-remove cart-flex-sm'>
				<a
					href='#'
					className='cart-remove-link'
					onClick={(e) => {
						isEVoucherItem
							? removeCommerceItem(
									e,
									lineItemIdentification.commerceItemIdList[0]
							  )
							: remove(e, itemCode);
					}}
				>
					<img
						src={`/images/us/common/icons/${brandTag}/trash_alt_regular_11x14.svg`}
						alt='trash'
						height='18px'
						width='18px'
					/>
				</a>
			</div>
			<div className='cart-item-price cart-pricing cart-flex-md'>
				{priceDetails.listPrice ? (
					<label className='cart-price reg-price'>
						{priceDetails.listPrice}
					</label>
				) : null}
				<label className='cart-price sale-price'>
					{isEVoucherItem ? subTotalDetails.salePrice : priceDetails.salePrice}
				</label>
			</div>
			<div className='cart-item-subtotal cart-pricing cart-flex-last'>
				{subTotalDetails.listPrice && !isEVoucherItem ? (
					<label className='cart-price reg-price'>
						{subTotalDetails.listPrice}
					</label>
				) : null}
				<label className='cart-price sale-price'>
					{subTotalDetails.salePrice}
				</label>
			</div>
		</div>
	);
};

CartItem.propTypes = {
	/**
	 * Item object
	 */
	item: PropTypes.object,
	/**
	 * Method to remove item
	 */
	remove: PropTypes.func,
	/**
	 * Method to remove commerce Item
	 */
	removeCommerceItem: PropTypes.func,
	/**
	 * Method to update  Item
	 */
	update: PropTypes.func,
};

export default CartItem;
