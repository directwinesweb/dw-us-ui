import CartContents from './CartContents';
import { CartProvider } from '../CartProvider';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('CartProvider Cart Contents test', () => {
	it('Creates snapshot test for <CartContents />', () => {
		const wrapper = shallow(
			<CartProvider>
				<CartContents />
			</CartProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
