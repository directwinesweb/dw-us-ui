import CartContents from './CartContents';
import { CartProvider } from '../CartProvider';
import React from 'react';
export default {
	title: 'Organisms/Cart Contents',
	parameters: {
		component: CartContents,
	},
};

export const Default = () => (
	<CartProvider>
		<CartContents>The default is an error message</CartContents>
	</CartProvider>
);
