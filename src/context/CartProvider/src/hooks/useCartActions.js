import { FormatPrice } from '@dw-us-ui/format-price';
import axios from 'axios';
/**
 *
 * @todo Add GA Tracking Info from App - need to discuss with Tom
 * if we should mimic the Cake GA events with different categories
 * @todo Might need to add more logic from Cake but need to confirm first
 */
const CART_API = {
	fetch: {
		url: '/api/cart/list?validate=true',
	},
	add: {
		url: '/api/cart/itemcode/',
	},
	update: {
		url: '/api/cart/itemcode/',
		params: '?method=set',
	},
	commerceItemIdentifier: {
		url: '/api/cart/commerceitemidentifier/',
	},
};

/**
 *
 * @param {*} state - state from the cart provider
 * @param {*} dispatch - dispatch actions for cart provider
 */
export const useCartActions = (state, dispatch) => {
	/**
	 *
	 * @param {object} response - cart response
	 * Going through cart response and format it for the UI
	 * Most of the logic pulled from Cake Cart
	 */
	function formatData(response) {
		const { numItems, lineItems } = response;
		const isCartEmpty = numItems <= 0;
		const finalCartItems = [];

		lineItems.forEach((item) => {
			const {
				lineItemIdentification,
				product,
				quantity,
				itemPriceInfo,
				caseBottleCount,
				inventoryInfo,
				validationInformationDetails,
			} = item;
			const { name, productType } = product;
			const { itemCode, itemClassifications } = lineItemIdentification;
			//Set Bottle Text
			let bottleText = 'btl';
			if (caseBottleCount === 0) {
				bottleText = false;
			} else if (caseBottleCount > 1) {
				bottleText = 'btls';
			}
			//Free Gift Check
			const isFreeItem = itemClassifications.contextClassificationDetails.includes(
				'FREEGIFT_PROMOTION_ITEM'
			);
			//Is Wine Plan Item
			const isWinePlanItem = itemClassifications.contextClassificationDetails.includes(
				'WINEPLAN_RECRUITMENT_ITEM'
			);
			const isEVoucherItem = lineItemIdentification.evoucher || false;
			//Item Errors - Inventory / Compliance Checks
			let itemError = false;
			if (
				inventoryInfo &&
				inventoryInfo.summaryAvailabilityStatus === 'out_of_stock'
			) {
				itemError = true;
			} else if (
				validationInformationDetails &&
				validationInformationDetails.validationDetailsList
			) {
				validationInformationDetails.validationDetailsList.forEach(
					(validation) => {
						if (validation.type === 'COMPLIANCE') {
							itemError = true;
						}
					}
				);
			}
			//Item Pricing
			const { listPrice, salePrice, amount, pricingBreakdown } = itemPriceInfo;
			const listPriceVal = listPrice.toFixed(2) || false;
			const salePriceVal = salePrice.toFixed(2) || false;
			const amountVal = amount.toFixed(2) || false;
			let adjustmentAmount = 0;
			let adjustmentType;

			if (!isFreeItem) {
				const adjustmentArrayIndex = pricingBreakdown.length - 1;
				adjustmentType = pricingBreakdown[adjustmentArrayIndex].adjustmentType;
				const adjustmentQuant =
					pricingBreakdown[adjustmentArrayIndex].quantityAdjusted;
				const totalAdjustmentAmount = pricingBreakdown[
					adjustmentArrayIndex
				].adjustmentAmount.toFixed(2);
				adjustmentAmount = Math.abs(totalAdjustmentAmount) / adjustmentQuant;
			}
			//Adjustment Checks
			let adjustedPrice = adjustmentAmount.toFixed(2);
			let totalListPrice = listPriceVal;
			const listNotEqualAdjustCheck = listPriceVal !== adjustedPrice;
			const nonStandardPriceCheck =
				salePriceVal !== listPriceVal || adjustmentType === 'VPP Discount';
			const nonStandardPriceStatus =
				listNotEqualAdjustCheck && nonStandardPriceCheck;
			const adjustmentStatus =
				adjustmentType === 'VPP Discount' || adjustmentType === 'Item Discount';

			if (adjustmentStatus) {
				adjustedPrice = salePriceVal - adjustmentAmount;
			}

			if (quantity > 1) {
				totalListPrice = listPriceVal * quantity;
			}

			//New Cart Item
			let cartItem = {
				name,
				itemCode,
				quantity,
				bottleText: bottleText ? `- ${caseBottleCount} ${bottleText}` : false,
				isFreeItem,
				isEVoucherItem,
				isServiceItem: productType === 'service',
				isWinePlanItem,
				disableQty: isFreeItem || productType === 'service' || isWinePlanItem,
				itemError,
				lineItemIdentification,
				priceDetails: {
					listPrice: nonStandardPriceStatus ? FormatPrice(listPriceVal) : false,
					salePrice: FormatPrice(adjustedPrice),
				},
				subTotalDetails: {
					listPrice:
						totalListPrice != amountVal && !isFreeItem
							? FormatPrice(totalListPrice)
							: false,
					salePrice: FormatPrice(amountVal),
					rawSalePrice: amountVal,
				},
			};
			finalCartItems.push(cartItem);
		});

		//Check if Gift Cart Items Only
		const isEVouchersOnly = finalCartItems.length
			? finalCartItems.filter((item) => item.isEVoucherItem).length ===
			  finalCartItems.length
			: false;
		const isUnlimitedInCart = finalCartItems.filter(
			(item) => item.isServiceItem && item?.lineItemIdentification?.itemTypeTag === 'SUBSCRIPTION_RECRUITMENT_ITEM'
		);
		const isWineClubCases = finalCartItems.filter(
			(item) => item.isWinePlanItem
		);
		dispatch({
			type: 'FETCH_CART',
			payload: {
				cartDetails: response,
				isCartEmpty,
				isEVouchersOnly,
				isUnlimitedInCart,
				isWineClubCases,
				isLoading: false,
				finalCartItems,
			},
		});
	}
	/**
	 * Fetch Cart Info
	 * @todo Need to do winery direct checks
	 * @todo Might need loading indicator / bar
	 */
	function fetchCart() {
		dispatch({
			type: 'FETCHING_CART',
		});
		axios
			.get(CART_API.fetch.url)
			.then((cart) => {
				const { data } = cart;
				const { response } = data;
				//Format Cart Data
				if (response) {
					formatData(response);
				}
			})
			.catch((err) => {
				dispatch({ type: 'CART_ERROR', payload: err });
			});
	}

	/**
	 *
	 * @param {object} item - is an object with the keys (itemCode, quantity)
	 * Triggers the add to cart functionality
	 */
	function addItem(item) {
		const { itemCode, quantity } = item;
		dispatch({ type: 'ADD_ITEM' });
		axios
			.put(`${CART_API.add.url}${itemCode}/${quantity}`, {})
			.then((cart) => {
				const { data } = cart;
				const { response } = data;
				if (response) {
					fetchCart();
				}
			})
			.catch((err) => {
				dispatch({ type: 'CART_ERROR', payload: err });
			});
	}

	/**
	 *
	 * @param {number} itemCode
	 * Deletes the given itemCode from the cart
	 */
	function deleteItem(itemCode) {
		dispatch({ type: 'DELETE_ITEM' });
		axios
			.delete(`${CART_API.update.url}${itemCode}`)
			.then((cart) => {
				const { data } = cart;
				const { response } = data;
				if (response) {
					fetchCart();
				}
			})
			.catch((err) => {
				dispatch({ type: 'CART_ERROR', payload: err });
			});
	}

	/**
	 *
	 * @param {string} commerceItemId
	 * Deletes the given commerceItemId from the cart
	 */
	function deleteCommerceItem(commerceItemId) {
		dispatch({ type: 'DELETE_ITEM' });
		axios
			.delete(`${CART_API.commerceItemIdentifier.url}${commerceItemId}`)
			.then((cart) => {
				const { data } = cart;
				const { response } = data;
				if (response) {
					fetchCart();
				}
			})
			.catch((err) => {
				dispatch({ type: 'CART_ERROR', payload: err });
			});
	}

	/**
	 *
	 * @param {object} item - is an object with the keys (itemCode, quantity)
	 * Update the given items quantity
	 */
	function updateItem(item) {
		const { itemCode, quantity } = item;
		const { url, params } = CART_API.update;
		dispatch({ type: 'UPDATE_ITEM' });
		axios
			.put(`${url}${itemCode}/${quantity}${params}`, {})
			.then((cart) => {
				const { data } = cart;
				const { response } = data;
				if (response) {
					fetchCart();
				}
			})
			.catch((err) => {
				dispatch({ type: 'CART_ERROR', payload: err });
			});
	}

	/**
	 *
	 * @param {bool} trigger
	 * Will show or hide the cart modal
	 */
	function triggerCartModal(trigger) {
		dispatch({ type: 'TRIGGER_MODAL', payload: trigger });
	}

	/**
	 * Export out all the actions to be used in the app
	 */
	return {
		fetchCart,
		addItem,
		deleteItem,
		updateItem,
		triggerCartModal,
		deleteCommerceItem,
	};
};
