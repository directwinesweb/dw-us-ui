import { CartContext } from '../CartProvider';
import { useContext } from 'react';

/**
 * Use Cart Hook for using the cart context
 * If the app is not wrapped around a CartProvider it will throw an error
 */
const useCart = () => {
	const context = useContext(CartContext);

	if (context === undefined) {
		throw new Error('useCart requires a CartProvider in the upper scope.');
	}
	return context;
};

export default useCart;
