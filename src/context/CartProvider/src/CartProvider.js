import React, { createContext, useEffect, useReducer } from 'react';

import CartReducer from './reducer/CartReducer';
import PropTypes from 'prop-types';
import { useCartActions } from './hooks/useCartActions';

export const CartContext = createContext();

/**
 *
- * @todo - Add Loading bar logic to the actions / initial state
 */
export const CartProvider = ({ children }) => {
	const initialState = {
		showCartModal: false,
		cartDetails: {},
		finalCartItems: [],
		isCartEmpty: false,
		isLoading: false,
		isEVouchersOnly: '',
		isUnlimitedInCart: [],
		isWineClubCases: [],
		errorDetails: {
			hasError: false,
			message: false,
		},
	};

	/**
	 * Reducer / Actions Logic in their own files
	 * Then export the state, dispatch and actions to be used by
	 * useCart hook
	 */
	const [cartState, cartDispatch] = useReducer(CartReducer, initialState);
	const cartActions = useCartActions(cartState, cartDispatch);
	const value = { cartState, cartDispatch, cartActions };

	useEffect(() => {
		if (Object.entries(cartState.cartDetails).length === 0) {
			cartActions.fetchCart();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [cartState.cartDetails]);

	return <CartContext.Provider value={value}>{children}</CartContext.Provider>;
};

CartProvider.propTypes = {
	children: PropTypes.node,
};
