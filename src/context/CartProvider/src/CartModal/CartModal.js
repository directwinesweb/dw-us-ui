import { CartContents } from '../CartContents';
import { Modal } from '@dw-us-ui/modals';
import PropTypes from 'prop-types';
import React from 'react';
/**
 *
 * @param {bool} showModal - Indicates to show modal or not
 * @param {func} toggleModal - Function to show / hide modal
 */
const CartModal = ({ showModal, toggleModal }) => (
	<Modal
		showModal={showModal}
		toggleModal={toggleModal}
		modalHeader='Items in your cart'
	>
		<CartContents />
	</Modal>
);

CartModal.propTypes = {
	/**
	 * Indicates to show modal or not
	 */
	showModal: PropTypes.bool,
	/**
	 * Function to show / hide modal
	 */
	toggleModal: PropTypes.func,
};
CartModal.defaultProps = {
	showModal: false,
	toggleModal: () => {},
};
export default CartModal;
