import CartModal from './CartModal';
import { CartProvider } from '../CartProvider';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('CartProvider Cart Modal test', () => {
	it('Creates snapshot test for <CartModal />', () => {
		const wrapper = shallow(
			<CartProvider>
				<CartModal showModal={true} />
			</CartProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
