import CartModal from './CartModal';
import { CartProvider } from '../CartProvider';
import React from 'react';
import { boolean } from '@storybook/addon-knobs';

export default {
	title: 'Modals/Cart Modal',
	parameters: {
		component: CartModal,
		componentSubtitle: 'Displays Cart Modal component',
	},
};

export const Default = () => {
	return (
		<CartProvider>
			<CartModal showModal={boolean('Show modal', true)} />
		</CartProvider>
	);
};
