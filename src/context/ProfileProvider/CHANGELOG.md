# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/profile-provider@1.0.2...@dw-us-ui/profile-provider@1.0.3) (2021-07-08)


### Bug Fixes

* added loading ([c5078a8](https://bitbucket.org/directwinesweb/dw-us-ui/commits/c5078a8c1d22206a83859a996a3073ad6ed376ef))
* down merge master and resolved conflicts ([f5e8c8a](https://bitbucket.org/directwinesweb/dw-us-ui/commits/f5e8c8a98b3c39b6074c10aa0c0e871b9bc29039))






## [1.0.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/profile-provider@1.0.1...@dw-us-ui/profile-provider@1.0.2) (2021-07-02)


### Bug Fixes

* down merge master ([174f206](https://bitbucket.org/directwinesweb/dw-us-ui/commits/174f206b37d77cf305cac57abb6bfc780074f8f1))
* initialize profile provider with unidentified values ([a26f233](https://bitbucket.org/directwinesweb/dw-us-ui/commits/a26f23395892a9ebc28b60eb31885289e7f41b5c))





## 1.0.1 (2021-06-30)

**Note:** Version bump only for package @dw-us-ui/profile-provider
