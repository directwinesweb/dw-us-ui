import React, { createContext, useEffect, useState } from 'react';
import { dlUserUpdate } from 'dw-global/apps/dataLayer';
import PropTypes from 'prop-types';
import axios from 'axios';

export const ProfileContext = createContext();

export const ProfileProvider = ({
	children,
	loader,
	fetchWineSubscriptionsList,
}) => {
	const [userInformation, setUserInformation] = useState(null);
	const [visitorType, setVisitorType] = useState('Unidentified');
	const [profileId, setProfileId] = useState(null);
	const [isLoading, setIsLoading] = useState(true);
	const [winePlansListData, setWinePlansListData] = useState(null);

	useEffect(() => {
		axios
			.get('/api/user/details')
			.then((profileResponse) => {
				const profileData = profileResponse.data.response;
				setUserInformation(profileData);
				setVisitorType(mapVisitorType(profileData.securityStatus));
				dlUserUpdate(profileData);
				if (profileData.summaryDetails) {
					setProfileId(profileData.summaryDetails.profileId);
				}

				// Flag that will fetch use wine plans list
				if (fetchWineSubscriptionsList) {
					axios
						.get('/api/user/subscriptions/wineplans/list')
						.then((subscriptionRes) => {
							const wineplansResponseData = subscriptionRes.data.response;

							setWinePlansListData(wineplansResponseData);
							setIsLoading(false);
						})
						.catch((error) => {
							console.error(
								'Profile Provider - Wine Plans List Error: ',
								error
							);
							setIsLoading(false);
						});
				} else {
					setIsLoading(false);
				}
			})
			.catch((err) => {
				// if we have an error with user details we just set the context to unidentified
				console.error('Profile Provider - User Details Error: ', err);
				setVisitorType('Unidentified');
				setUserInformation({}); // we will need to provide more information regarding locationContext if necessary
				setIsLoading(false);
			});
	}, []);

	return (
		<ProfileContext.Provider
			value={{ userInformation, visitorType, profileId, winePlansListData }}
		>
			{userInformation && !isLoading ? children : loader ? loader : null}
		</ProfileContext.Provider>
	);
};

ProfileProvider.propTypes = {
	/**
	 * Child elements to display
	 */
	children: PropTypes.node,
	/**
	 * provide loader while fetching content
	 */
	loader: PropTypes.node,
	/**
	 * determines whether to fetch subscriptions list
	 */
	fetchWineSubscriptionsList: PropTypes.bool,
};

ProfileProvider.defaultProps = {
	children: null,
	loader: null,
	fetchWineSubscriptionsList: false,
};
// helper methods

/**
 *
 * @param {Number} securityStatus
 * sets visitor type based on security status value
 */
const mapVisitorType = (securityStatus = 0) => {
	let visitorType;
	switch (securityStatus) {
		case 5:
		case 4:
			visitorType = 'Hard Logged-in';
			break;
		case 3:
		case 2:
			visitorType = 'Soft Logged-in';
			break;
		default:
			visitorType = 'Unidentified';
	}
	return visitorType;
};
