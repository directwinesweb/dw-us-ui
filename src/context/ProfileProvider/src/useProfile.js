import { ProfileContext } from './ProfileProvider';
import { useContext } from 'react';

const useProfile = () => {
	const profile = useContext(ProfileContext);
	return profile;
};

export default useProfile;
