import { ProfileProvider } from '../src/ProfileProvider';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('ProfileProvider test', () => {
	it('Creates snapshot test for <ProfileProvider />', () => {
		const wrapper = shallow(<ProfileProvider></ProfileProvider>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
