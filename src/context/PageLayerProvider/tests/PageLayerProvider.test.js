import { PageLayerProvider } from '../src/PageLayerProvider';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('PageLayerProvider test', () => {
	it('Creates snapshot test for <PageLayerProvider />', () => {
		const wrapper = shallow(<PageLayerProvider></PageLayerProvider>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
