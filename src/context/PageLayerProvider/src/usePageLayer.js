import { PageLayerContext } from './PageLayerProvider';
import { useContext } from 'react';

const usePageLayer = () => {
	const pageLayer = useContext(PageLayerContext);

	return pageLayer;
};

export default usePageLayer;
