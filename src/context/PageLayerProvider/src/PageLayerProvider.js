import React, { createContext, useEffect, useState } from 'react';

import PropTypes from 'prop-types';
import axios from 'axios';

export const PageLayerContext = createContext();

/**
 *
 * @todo - Update Logic to pull a SIMPLER JSP and just stringify
 * EXTRA important, this broke, and not even sure what else broke on top of it!
 */
export const PageLayerProvider = ({ children }) => {
	const [pageLayer, setPageLayer] = useState(() => {
		return typeof pageLayer !== 'undefined' ? pageLayer[0] : false;
	});

	useEffect(() => {
		if (!pageLayer) {
			axios
				.get('/jsp/thirdparty/us/common/page_config_data_layer.jsp')
				.then((res) => {
					const data = res.data.replace(/^\s+|\s+$|\s+(?=\s)/g, '');
					// Format return data after the '<script></script>' tags
					let formattedResponse = data
						// Remove Script tags from response
						.split('<script>')[1]
						.split('</script>')[0]
						.replace(/^\s+|\s+$|\s+(?=\s)/g, '')
						.replace('window.dataLayer = window.dataLayer || [];', '')
						.replace('	var configLayer = ', '')
						.replace('window.dataLayer.push(configLayer[0]);', '')
						// Remove Whitespace
						.replace(/^\s+|\s+$|\s+(?=\s)/g, '')
						// Prepare response to be cast into a consumable object by removing inhibiting strings
						.replace('dataLayer = ', '')
						.replace('[{', '')
						.replace('}];', '')
						// Separate out key value pairs from formatted Response
						.split(',');
					let populatedObj = {};
					//
					// Iterate over the formatted response to populate new array with fresh object data
					//
					formattedResponse.forEach((el) => {
						let objectPair = el.split(':');
						let key = '';
						let value = '';
						// Edge case to catch elements not formatted in a uniform manner...
						// ... captures pairs that don't follow the ' : ', paring syntax
						// Create new key value pairs
						key = objectPair[0].trim().replace(/'/g, '');
						value = objectPair[1].trim().replace(/'/g, '');
						// If value is a boolean...
						if (value === 'true') {
							value = true;
						} else if (value === 'false') {
							value = false;
						}
						let checkVal = parseInt(value);
						// If value is a number, and not associated with the phone number or Brand ID
						if (
							!Number.isNaN(checkVal) &&
							key !== 'brandPhone' &&
							key !== 'bid'
						) {
							value = checkVal;
						}
						// Edge case to remove extra quotations at state code and promo code
						if (key === 'stateCode' || key === 'promoCode') {
							if (
								value.charAt(0) === `"` &&
								value.charAt(value.length - 1) === `"`
							) {
								value = value.substr(1, value.length - 2);
							}
						}
						populatedObj[key] = value;
					});
					// Return populated array
					setPageLayer(populatedObj);
				})
				.catch((err) => {
					if (err.response) {
						setPageLayer(false);
					}
				});
		}
	}, [pageLayer]);

	return (
		<PageLayerContext.Provider value={pageLayer}>
			{children}
		</PageLayerContext.Provider>
	);
};

PageLayerProvider.propTypes = {
	children: PropTypes.node,
};
