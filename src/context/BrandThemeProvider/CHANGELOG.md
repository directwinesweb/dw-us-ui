# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.11](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.10...@dw-us-ui/brand-theme-provider@1.1.11) (2021-07-08)


### Bug Fixes

* theme provider test fix ([7e2a12c](https://bitbucket.org/directwinesweb/dw-us-ui/commits/7e2a12cb219a3cf7a21ba1ce3a334777f45d688b))






## [1.1.10](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.9...@dw-us-ui/brand-theme-provider@1.1.10) (2021-07-02)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.9](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.8...@dw-us-ui/brand-theme-provider@1.1.9) (2021-06-30)


### Bug Fixes

* test case fixes ([e1d8d94](https://bitbucket.org/directwinesweb/dw-us-ui/commits/e1d8d9428644435578c7603dc2d431627f5f941a))






## [1.1.8](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.7...@dw-us-ui/brand-theme-provider@1.1.8) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.7](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.5...@dw-us-ui/brand-theme-provider@1.1.7) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.6](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.5...@dw-us-ui/brand-theme-provider@1.1.6) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.5](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.4...@dw-us-ui/brand-theme-provider@1.1.5) (2021-05-10)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.4](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.3...@dw-us-ui/brand-theme-provider@1.1.4) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.2...@dw-us-ui/brand-theme-provider@1.1.3) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.1...@dw-us-ui/brand-theme-provider@1.1.2) (2021-03-25)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





## [1.1.1](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/brand-theme-provider@1.1.0...@dw-us-ui/brand-theme-provider@1.1.1) (2021-03-09)

**Note:** Version bump only for package @dw-us-ui/brand-theme-provider





# 1.1.0 (2021-03-05)


### Features

* added brand theme provider ([245914d](https://bitbucket.org/directwinesweb/dw-us-ui/commits/245914d1e1ec80b84bad1c11e622a1fde75e2c43))
