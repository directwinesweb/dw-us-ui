import BrandThemeProvider from '../src/BrandThemeProvider';
import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

describe('BrandThemeProvider test', () => {
	it('Creates snapshot test for <BrandThemeProvider /> for law', () => {
		const wrapper = shallow(<BrandThemeProvider></BrandThemeProvider>);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <BrandThemeProvider /> for wsj', () => {
		const wrapper = shallow(
			<BrandThemeProvider brandTag='wsj'></BrandThemeProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <BrandThemeProvider /> for vir', () => {
		const wrapper = shallow(
			<BrandThemeProvider brandTag='vir'></BrandThemeProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <BrandThemeProvider /> for mcy', () => {
		const wrapper = shallow(
			<BrandThemeProvider brandTag='mcy'></BrandThemeProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
	it('Creates snapshot test for <BrandThemeProvider /> for npr', () => {
		const wrapper = shallow(
			<BrandThemeProvider brandTag='npr'></BrandThemeProvider>
		);
		expect(toJson(wrapper)).toMatchSnapshot();
	});
});
