import {
	lawTheme,
	mcyTheme,
	nprTheme,
	virTheme,
	wsjTheme,
} from '@dw-us-ui/themes';

import { BrandUtil } from '@dw-us-ui/brand-util';
import { Global } from '@emotion/core';
import PropTypes from 'prop-types';
import React from 'react';
import { ThemeProvider } from 'emotion-theming';
import { ThemeWrapper } from '@dw-us-ui/theme-wrapper';

const BrandThemeProvider = ({ children, brandTag }) => {
	// eslint-disable-next-line no-undef
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	brandTag = data.tag;

	let theme;

	switch (brandTag) {
		case 'wsj':
			theme = { data, ...wsjTheme };
			break;

		case 'vir':
			theme = { data, ...virTheme };
			break;

		case 'mcy':
			theme = { data, ...mcyTheme };
			break;

		case 'npr':
			theme = { data, ...nprTheme };
			break;

		default:
			theme = { data, ...lawTheme };
			break;
	}

	return (
		<ThemeProvider theme={theme}>
			<Global
				styles={() => ({
					body: {
						margin: 0,
					},
				})}
			/>
			<ThemeWrapper>{children}</ThemeWrapper>
		</ThemeProvider>
	);
};

BrandThemeProvider.propTypes = {
	/**
	 * Current brand tag
	 */
	brandTag: PropTypes.string,
	children: PropTypes.node,
};

BrandThemeProvider.defaultProps = {
	children: null,
};

export default BrandThemeProvider;
