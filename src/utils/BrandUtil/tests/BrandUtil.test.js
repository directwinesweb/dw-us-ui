import BrandUtil from '../src/BrandUtil';

const brandArray = [
	{ name: 'laithwaites', tag: 'law' },
	{ name: 'virgin', tag: 'vir' },
	{ name: 'wsjwine', tag: 'wsj' },
	{ name: 'macyswinecellar', tag: 'mcy' },
	{ name: 'nprwineclub', tag: 'npr' },
];

describe('Brand Util Tests', () => {
	brandArray.forEach((brand) => {
		let brandName = brand.name;
		let brandTag = brand.tag;

		it(`Gets Brand Name for ${brandTag} using getBrand function`, () => {
			let result = BrandUtil.getBrand(brandTag);
			expect(result).toEqual(brandName);
		});
		it(`Gets Brand Name for ${brandTag} using getBrandName function`, () => {
			let result = BrandUtil.getBrandName(brandTag);
			expect(result).toEqual(brandName);
		});
	});
});

it('Returns default brandName LAW for empty parameters', () => {
	let result = BrandUtil.getBrand();
	expect(result).toEqual('laithwaites');
	result = BrandUtil.getBrandName();
	expect(result).toEqual('laithwaites');
});

it('Returns default country US', () => {
	expect(BrandUtil.getCountry()).toEqual('us');
});

brandArray.forEach((brand) => {
	let brandName = brand.name;
	let brandTag = brand.tag;
	it(`Returns brand content for ${brandName}`, () => {
		let dataResult = BrandUtil.content(brandName);
		expect(dataResult.tag).toEqual(brandTag);
	});
});

it(`Returns empty data content for undefined param`, () => {
	let dataResult = BrandUtil.content();
	expect(dataResult).toEqual({});
});
it(`Returns empty data content for unmatched brand`, () => {
	let dataResult = BrandUtil.content('asd');
	expect(dataResult).toEqual({});
});
