// Required Libs

var BrandUtil = {
	getBrandName(brandTag) {
		switch (brandTag) {
			case 'law':
				return 'laithwaites';
			case 'wsj':
				return 'wsjwine';
			case 'vir':
				return 'virgin';
			case 'mcy':
				return 'macyswinecellar';
			case 'npr':
				return 'nprwineclub';
			default:
				return 'laithwaites';
		}
	},
	getBrand(brandTag = 'law') {
		var brandArr = [
			'laithwaites',
			'virgin',
			'wsjwine',
			'tcmwineclub',
			'natgeowine',
			'nprwineclub',
			'giltwine',
			'bhgwine',
			'macyswinecellar',
		];
		var brand = brandArr.filter((elem) => {
			return location.host.indexOf(elem) > 0;
		});
		return brand[0] || this.getBrandName(brandTag);
	},
	getCountry() {
		if (location.host.indexOf('co.uk') > 0) {
			return 'uk';
		} else if (location.host.indexOf('com.au') > 0) {
			return 'au';
		} else if (location.host.indexOf('co.nz') > 0) {
			return 'nz';
		} else {
			return 'us';
		}
	},
	content(brand) {
		var data = {};
		let phoneNumber = '1-800-649-4637';
		// Temporary fix as we build up brand switch, helps GTM for these brands
		// case return in switch will be the same except for the GTM container
		if (
			// brand === 'tcmwineclub' ||
			// brand === 'natgeowine' ||
			brand === 'giltwine' ||
			brand === 'bhgwine'
		) {
			brand = 'laithwaites';
		}

		switch (brand) {
			case 'wsjwine': {
				data = {
					tag: 'wsj',
					name: 'WSJ Wine from The Wall Street Journal',
					nameShort: 'WSJ Wine',
					brandKey: 'WSJ',
					url: 'https://www.wsjwine.com',
					urlShort: 'wsjwine.com',
					unlimited: 'Advantage',
					unlimitedFull: 'WSJ Wine Advantage',
					unlimitedBom: '00034SV',
					sunUnlimitedBom: '00034SV',
					phone: '1-877-975-9463',
					email: 'customerservice@wsjwine.com',
					serviceEmail: 'service@wsjwine.com',
					logo: '/images/us/en/brands/logo/wsj/wsj_rebrand_logo.svg',
					favIcon: '/images/us/en/brands/wsj/favicon_new.png',
					locale: 'en_US_WSJ',
					companyCode: 'DWI',
					caseImageUrl: '/images/us/en/product/',
					itemTrackingUrl:
						'https://wsj.narvar.com/tracking/wsjwine/fedex?tracking_numbers=',
					aboutUsLink: '/jsp/aboutus/us/common/aboutus.jsp',
					googleAnalytics: {
						merchantId: 'QespFNTg8vRK-wzG6i47jHKAl9IODmAnpSUFBSDWOAY',
						gtmId: 'GTM-8GMT',
					},
					POBox: '6821',
					footer: {
						header: 'The WSJ Wine Guarantee',
					},
					guarantee:
						'If ever a bottle fails to delight, you get your money back.',
				};
				break;
			}
			case 'laithwaites': {
				data = {
					tag: 'law',
					name: 'Laithwaites',
					nameShort: 'Laithwaites',
					brandKey: '4s',
					url: 'https://www.laithwaites.com',
					urlShort: 'laithwaites.com',
					unlimited: 'Unlimited',
					unlimitedFull: 'Laithwaites Unlimited',
					unlimitedBom: '00033SV',
					sunUnlimitedBom: '00033SV',
					phone: phoneNumber,
					email: 'customerservice@laithwaites.com',
					serviceEmail: 'service@laithwaites.com',
					logo: '/assets/logos/logotype-red/logotype-red.svg',
					favIcon: '/images/us/en/brands/lw/favicon_new.png',
					locale: 'en_US_4S',
					companyCode: 'DWI',
					cartImageUrl: '/images/us/law/cart-dark.svg',
					caseImageUrl: '/images/us/en/product/',
					itemTrackingUrl:
						'https://laithwaites.narvar.com/tracking/laithwaiteswine/fedex?tracking_numbers=',
					aboutUsLink:
						'/jsp/aboutus/us/common/aboutus.jsp?content=aboutus_tony',
					googleAnalytics: {
						merchantId: 'a6Vsyx0Ckg1Z06JUyaXhH79zQEUPCMQ8XpaLDhioVKY',
						gtmId: 'GTM-6V2M4',
					},
					POBox: '7274',
					footer: {
						header: 'The Laithwaites Guarantee',
					},
					guarantee: 'If you don’t enjoy a wine, you don’t pay for it.',
				};
				break;
			}
			case 'natgeowine': {
				data = {
					tag: 'law',
					name: 'Laithwaites',
					nameShort: 'Laithwaites',
					brandKey: '4s',
					url: 'https://www.laithwaites.com',
					urlShort: 'laithwaites.com',
					unlimited: 'Unlimited',
					unlimitedFull: 'Laithwaites Unlimited',
					unlimitedBom: '00033SV',
					sunUnlimitedBom: '00033SV',
					phone: phoneNumber,
					email: 'customerservice@laithwaites.com',
					serviceEmail: 'service@laithwaites.com',
					logo: '/assets/logos/logotype-red/logotype-red.svg',
					favIcon: '/images/us/en/brands/lw/favicon_new.png',
					locale: 'en_US_4S',
					companyCode: 'DWI',
					cartImageUrl: '/images/us/law/cart-dark.svg',
					caseImageUrl: '/images/us/en/product/',
					itemTrackingUrl:
						'https://laithwaites.narvar.com/tracking/laithwaiteswine/fedex?tracking_numbers=',
					aboutUsLink:
						'/jsp/aboutus/us/common/aboutus.jsp?content=aboutus_tony',
					googleAnalytics: {
						merchantId: 'a6Vsyx0Ckg1Z06JUyaXhH79zQEUPCMQ8XpaLDhioVKY',
						gtmId: 'GTM-TQZQ325',
					},
					POBox: '7274',
					footer: {
						header: 'The Laithwaites Guarantee',
					},
					guarantee: 'If you don’t enjoy a wine, you don’t pay for it.',
				};
				break;
			}
			case 'tcmwineclub': {
				data = {
					tag: 'law',
					name: 'Laithwaites',
					nameShort: 'Laithwaites',
					brandKey: '4s',
					url: 'https://www.laithwaites.com',
					urlShort: 'laithwaites.com',
					unlimited: 'Unlimited',
					unlimitedFull: 'Laithwaites Unlimited',
					unlimitedBom: '00033SV',
					sunUnlimitedBom: '00033SV',
					phone: phoneNumber,
					email: 'customerservice@laithwaites.com',
					serviceEmail: 'service@laithwaites.com',
					logo: '/assets/logos/logotype-red/logotype-red.svg',
					favIcon: '/images/us/en/brands/lw/favicon_new.png',
					locale: 'en_US_4S',
					companyCode: 'DWI',
					cartImageUrl: '/images/us/law/cart-dark.svg',
					caseImageUrl: '/images/us/en/product/',
					itemTrackingUrl:
						'https://laithwaites.narvar.com/tracking/laithwaiteswine/fedex?tracking_numbers=',
					aboutUsLink:
						'/jsp/aboutus/us/common/aboutus.jsp?content=aboutus_tony',
					googleAnalytics: {
						merchantId: 'a6Vsyx0Ckg1Z06JUyaXhH79zQEUPCMQ8XpaLDhioVKY',
						gtmId: 'GTM-KC8WRCC',
					},
					POBox: '7274',
					footer: {
						header: 'The Laithwaites Guarantee',
					},
					guarantee: 'If you don’t enjoy a wine, you don’t pay for it.',
				};
				break;
			}
			//@todo needs some update - based on logic
			case 'nprwineclub': {
				data = {
					tag: 'npr',
					name: 'National Public Radio',
					nameShort: 'NPR',
					brandKey: '4s',
					url: 'https://nprwineclub.org/',
					urlShort: 'laithwaites.com',
					unlimited: 'Unlimited',
					unlimitedFull: 'Laithwaites Unlimited',
					unlimitedBom: '00033SV',
					sunUnlimitedBom: '00033SV',
					phone: phoneNumber,
					email: 'customerservice@laithwaites.com',
					serviceEmail: 'service@laithwaites.com',
					logo: '/images/us/npr/common/npr_logo.png',
					favIcon: '//media.npr.org/templates/favicon/favicon-16x16.png',
					locale: 'en_US_4S',
					companyCode: 'DWI',
					cartImageUrl: '/images/us/law/cart-dark.svg',
					caseImageUrl: '/images/us/en/product/',
					itemTrackingUrl:
						'https://laithwaites.narvar.com/tracking/laithwaiteswine/fedex?tracking_numbers=',
					aboutUsLink: '/jsp/aboutus/us/common/aboutus.jsp?content=discover',
					googleAnalytics: {
						merchantId: '3K11Sa3VkosqIGg4JyeG04s1dnQSYigSkte7eGek0Fs',
						gtmId: 'GTM-MZCFV4B',
					},
					POBox: '7274',
					footer: {
						header: 'The NPR Guarantee',
					},
					guarantee:
						'If you don’t enjoy a wine, just let us know and we’ll arrange a refund.',
				};
				break;
			}

			case 'virgin': {
				data = {
					tag: 'vir',
					name: 'Virgin Wines US',
					nameShort: 'Virgin Wines',
					brandKey: 'Virgin',
					url: 'https://www.virginwines.com',
					urlShort: 'virginwines.com',
					unlimited: 'Unlimited',
					unlimitedFull: 'Virgin Wines Unlimited',
					unlimitedBom: '00035SV',
					sunUnlimitedBom: '00035SV',
					phone: '1-866-426-0336',
					email: 'service@virginwines.com',
					serviceEmail: 'service@virginwines.com',
					logo: '/images/us/common/logos/vir_logo.svg',
					favIcon: '/images/us/en/brands/virgin/favicon.ico',
					locale: 'en_US_Virgin',
					companyCode: 'DWI',
					caseImageUrl: '/images/us/en/brands/virgin/product/',
					itemTrackingUrl:
						'https://virgin.narvar.com/tracking/virginwines/fedex?tracking_numbers=',
					aboutUsLink: '/jsp/aboutus/us/common/aboutus.jsp',
					googleAnalytics: {
						merchantId: 'CrmdyV5fPesUqwDLj1Jw2po8YShf_P8k7Qge2fxuVBM',
						gtmId: 'GTM-B98LX',
					},
					POBox: '1269',
					footer: {
						header: 'The Virgin Wines Guarantee',
					},
					guarantee:
						'If you don’t like a wine, no problem! Every bottle is covered by our 100% money‐back guarantee.',
				};
				break;
			}
			case 'macyswinecellar': {
				data = {
					tag: 'mcy',
					name: 'Macys Wine Cellar US',
					nameShort: "Macy's Wine Cellar",
					brandKey: 'MACYS',
					url: 'https://www.macyswinecellar.com',
					urlShort: 'macyswinecellar.com',
					unlimited: 'Unlimited',
					unlimitedFull: "Macy's Wine Cellar Unlimited",
					unlimitedBom: '00036SV',
					sunUnlimitedBom: '00036SV',
					phone: '1-888-997-0319',
					email: 'customerservice@macyswinecellar.com',
					serviceEmail: 'service@macyswinecellar.com',
					logo: '/images/us/common/logos/mcy_logo.svg',
					favIcon: '/images/us/mcy/common/favicon.ico',
					locale: 'en_US_MACYS',
					companyCode: 'DWI',
					caseImageUrl: '/images/us/en/product/',
					itemTrackingUrl:
						'https://macyswinecellar.narvar.com/tracking/macyswinecellar/fedex?tracking_numbers=',
					aboutUsLink: '/jsp/aboutus/us/common/aboutus.jsp',
					googleAnalytics: {
						merchantId: 'mi1TXZBrCJbmIaSr2DvwS7Vd7DoQvANrP58RejouVok',
						gtmId: 'GTM-NW32KR9',
					},
					POBox: '6824',
					footer: {
						header: "The Macy's Wine Cellar Guarantee",
					},
					guarantee:
						'If you don’t enjoy a wine, just let us know and we’ll arrange a refund.',
				};
				break;
			}
			default:
				data = {};
		}

		return data;
	},
};
export default BrandUtil;
