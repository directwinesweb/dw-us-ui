import GetStateData from '../src/GetStateData';

describe('Get state data tests', () => {
	it('Returns infomation if name is the type', () => {
		let returned = GetStateData('Tennessee', 'name');
		let expected = 'Tennessee';
		expect(returned.retailer.currentState).toEqual(expected);
	});
	it('Returns infomation if id is the type', () => {
		let returned = GetStateData('1053', 'id');
		let expected = 'Alaska';
		expect(returned.stateName).toEqual(expected);
	});
	it('Returns infomation if code is the type', () => {
		let returned = GetStateData('KS', 'code');
		let expected = 'Kansas';
		expect(returned.stateName).toEqual(expected);
	});
	it('Returns false if both parameters are incorrect', () => {
		let returned = GetStateData('London', 'type');
		expect(returned).toEqual(false);
	});
});
