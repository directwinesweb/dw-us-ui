import { stateCodes } from '@dw-us-ui/constants';

/**
 * @param {string} - state - can be stateId, stateCode or stateName
 * @param {string} - type - can be name, id, code
 */
const GetStateData = (state, type) => {
	let value = false;

	if (type !== 'name') {
		for (const fullState in stateCodes) {
			if (type === 'id') {
				let currentId = stateCodes[fullState].stateId;
				if (currentId === state) {
					value = stateCodes[fullState];
				}
			} else if (type === 'code') {
				let currentStateCode = stateCodes[fullState].stateCode;
				if (currentStateCode === state) {
					value = stateCodes[fullState];
				}
			}
		}
	} else {
		value = stateCodes[state];
	}
	return value;
};

export default GetStateData;
