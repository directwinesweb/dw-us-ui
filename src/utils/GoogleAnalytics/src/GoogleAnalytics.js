import axios from 'axios';
/**
 * PageView - Set Page View
 * Event Tracking - Set all event tracking
 * These events are setup in GTM as well to pass the values to GA
 */
const GoogleAnalytics = {
	setPageView(path, title) {
		if (window && window.dataLayer) {
			return window.dataLayer.push({
				event: 'GAPageEvent',
				pagePath: path,
				pageTitle: title,
			});
		}
	},
	setEventTracking(category, action, label) {
		if (window && window.dataLayer) {
			return window.dataLayer.push({
				event: 'GAevent',
				eventCategory: category,
				eventAction: action,
				eventLabel: label,
			});
		}
	},
	setOrderDataLayer(orderDetails, brandData = {}) {
		// destructure order details object
		const { id, shippingDetails, paymentDetails } = orderDetails;
		const { tag, url } = brandData;
		// instantiate customer details
		let customerDetails = false;

		// if we have a shipping address
		if (
			(shippingDetails && shippingDetails.length) ||
			(paymentDetails && paymentDetails[0]?.billingAddress)
		) {
			// set the address, fallback to billingAddress
			customerDetails =
				shippingDetails[0]?.shippingAddress ||
				paymentDetails[0]?.billingAddress;
		}

		axios
			.get(`/jsp/thirdparty/us/common/json_order_data_layer.jsp?orderId=${id}`)
			.then((res) => {
				if (window && window.dataLayer) {
					let response = res.data;
					/* 
						if we have customer details, destructure newly created object
						, spread response from jsp, and insert values
						---
						Added this logic (below) to be able to send propsects vs. customers for new website reporting
						"productType = wineplanproduct" ==> "wine_club"
						"productType = wine" ==> "non_club"
						transactionAffiliation will be replaced with these values
					*/
					const { transactionProducts } = response;
					const isWinePlanCustomer = transactionProducts.some(
						(item) => item.productType == 'wineplanproduct'
					);
					const orderType = isWinePlanCustomer ? 'wine_club' : 'non_club';

					if (customerDetails) {
						response = {
							...response,
							customerDetails,
							transactionAffiliation: orderType,
						};
					}
					window.dataLayer.push(response);
				}
				if (window && window.attentive && tag === 'law') {
					let attentiveCartItems = [];
					orderDetails?.lineItems?.map((item) => {
						attentiveCartItems.push({
							productId: item.lineItemIdentification.itemCode,
							productVariantId: item.lineItemIdentification.productId,
							name: item.product.name,
							productImage: url + item.product.smallImage,
							category: item.product.productType,
							price: {
								value: item.itemPriceInfo.amount,
								currency: 'USD',
							},
							quantity: item.product.qty,
						});
					});

					window.attentive.analytics.purchase(
						{
							items: attentiveCartItems,
							order: {
								orderId: id
							},
							user: {
								phone: customerDetails?.contactInfo?.dayPhoneNumber
							}
						}
					);
				}
			})
			.catch((err) => {
				console.log(err);
			});
	},
	setCartDataLayer(data, brandTag = '', userPhoneNumber = '', url = '') {
		var response = data.response;
		var lineItems = response.lineItems;
		var numBottles = response.numBottles;
		var orderPriceInfo = response.orderPriceInfo;
		var newCartArray = [];
		var attentiveCartItems = [];
		lineItems.map((item) => {
			newCartArray.push({
				sku: item.lineItemIdentification.itemCode,
				name: item.product.name,
				description: item.product.webHeadline,
				category: item.product.productType,
				unitPrice: item.itemPriceInfo.amount / item.quantity,
				salePrice: item.itemPriceInfo.amount / item.quantity,
				quantity: item.quantity,
				totalPrice: item.itemPriceInfo.amount,
				productUrl: '/product/' + item.product.itemCode,
			});
			if(brandTag === 'law') {
				attentiveCartItems.push({
					productId: item.lineItemIdentification.itemCode,
					productVariantId: item.lineItemIdentification.productId,
					name: item.product.name,
					productImage: url + item.product.smallImage,
					category: item.product.productType,
					price: {
						value: item.itemPriceInfo.amount,
						currency: 'USD',
					},
					quantity: item.product.qty,
				});
			}
		});
		if (window && window.dataLayer) {
			window.dataLayer.push({
				event: 'cart',
				cartDetailUpdate: {
					numBottles: numBottles,
					id: response.id,
					products: newCartArray,
					orderPriceInfo: {
						currencyCode: orderPriceInfo.currencyCode,
						subtotal: orderPriceInfo.rawSubtotal,
						discountAmount: orderPriceInfo.discounted,
						taxAmount: orderPriceInfo.tax,
						grandTotal: orderPriceInfo.total,
						shippingAmount: orderPriceInfo.shipping,
					},
				},
			});
		}

		if (window && window.attentive && brandTag === 'law') {
			window.attentive.analytics.addToCart(
				{
					items: attentiveCartItems,
					cart: {
						cartId: response.id
					},
					user: {
						phone: userPhoneNumber
					}
				}
			);
		}
		return true;
	},
};

export default GoogleAnalytics;
