const TargetedUserType = (userType, visitorTypeDetailed) => {
    let isTargetedUser = false;

	if(userType === 'All Users') {
		isTargetedUser = true;
	} else if(userType === visitorTypeDetailed) {
		isTargetedUser = true;
	} else if(userType === 'Identified' && visitorTypeDetailed !== 'Unidentified') {
		isTargetedUser = true;
	}

	return isTargetedUser;
};
export default TargetedUserType;
