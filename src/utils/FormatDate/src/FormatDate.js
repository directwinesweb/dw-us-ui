import dateFormat from 'dateformat';

const FormatDate = (date = 0, pattern = 'd mmm, yyyy') => {
	if (typeof date === 'string') {
		if (date.includes('-')) {
			date = date.replace(/-/g, '/');
		}
	}
	const formattedDate = dateFormat(date, pattern);
	return formattedDate;
};

export default FormatDate;
