import FormatDate from '../src/FormatDate';

/**
 * @param {String || number} date
 * @param {string} pattern
 *
 * @return {string} Formatted Date
 */
describe('Format date tests', () => {
	it('returns the formatted date given a string', () => {
		let expectedValue = '1 Jan 2020';
		let returnedValue = FormatDate('01/01/2020', 'd mmm yyyy');
		expect(returnedValue).toEqual(expectedValue);
	});
	it('returns the formatted date given a timestamp', () => {
		let expectedValue = '2 Apr 2020';
		let returnedValue = FormatDate(1585834776077, 'd mmm yyyy');
		expect(returnedValue).toEqual(expectedValue);
	});
	it('returns the formatted date given "-"', () => {
		let expectedValue = '1 Jan 2020';
		let returnedValue = FormatDate('01-01-2020', 'd mmm yyyy');
		expect(returnedValue).toEqual(expectedValue);
	});
	it('returns the formatted date given "/"', () => {
		let expectedValue = '1 Jan 2020';
		let returnedValue = FormatDate('01/01/2020', 'd mmm yyyy');
		expect(returnedValue).toEqual(expectedValue);
	});
	it('returns the formatted date given "-" or "/"', () => {
		let expectedValue = '1 Jan 2020';
		let returnedValue = FormatDate('01-01/2020', 'd mmm yyyy');
		expect(returnedValue).toEqual(expectedValue);
	});
});
