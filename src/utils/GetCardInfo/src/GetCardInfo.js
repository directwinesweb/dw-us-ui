//Set CreditCard type
import valid from 'card-validator';

/**
 * Returns the card type and validity
 *
 * @param {string} cardNumber
 *
 * @return {{cardType: String, isValid: boolean}}
 */

const GetCardInfo = (cardNumber = '') => {
	let formattedNumber = cardNumber.replace(/\D/g, '');
	let cvvLength = 3;
	let cardValidation = valid.number(formattedNumber);
	const card = cardValidation.card;
	const isValid = cardValidation.isValid;

	// if we have access to the card type, return card type immediately
	let cardType = card ? card.type : '';

	if (card) {
		if (cardType === 'visa' || cardType === 'discover') {
			cardType = cardType.charAt(0).toUpperCase() + cardType.slice(1);
		}

		if (cardType === 'master-card' || cardType === 'mastercard') {
			cardType = 'MasterCard';
		}

		if (cardType === 'american-express') {
			cardType = 'AmericanExpress';
		}

		cvvLength = card.code.size;
	}

	return {
		cardType,
		cvvLength,
		isValid,
	};
};

export default GetCardInfo;
