import GetCardInfo from '../src/GetCardInfo';

const cardArray = [
	{ type: 'Visa', number: '4444333322221111' },
	{ type: 'AmericanExpress', number: '370000000000002' },
	{ type: 'MasterCard', number: '5105105105105100' },
	{ type: 'Discover', number: '6011498756978110' },
];

describe('Get card info tests', () => {
	it('Returns a valid card', () => {
		cardArray.forEach((card) => {
			let cardType = card.type;
			let returnedValue = GetCardInfo(card.number);
			expect(returnedValue.cardType).toEqual(cardType);
			expect(returnedValue.isValid).toEqual(true);
		});
	});

	it('Returns a invalid card', () => {
		let returnedValue = GetCardInfo('123456789010121');
		expect(returnedValue.cardType).toEqual('');
		expect(returnedValue.isValid).toEqual(false);
	});
});
