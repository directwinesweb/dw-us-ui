/**
 *
 * @param {number} number
 * Formats the price to have the dollar sign and
 * if the value is a whole number it adds the 2 decimal places
 */
const FormatPrice = (number) => {
	let formattedPrice = Math.abs(parseFloat(number)).toLocaleString(undefined, {
		minimumFractionDigits: 2,
		maximumFractionDigits: 2,
	});
	if (parseFloat(number) < 0) {
		formattedPrice = `- $${formattedPrice}`;
	} else {
		formattedPrice = `$${formattedPrice}`;
	}
	return formattedPrice;
};

export default FormatPrice;
