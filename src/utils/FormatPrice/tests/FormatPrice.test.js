import FormatPrice from '../src/FormatPrice';

describe('FormatPrice test', () => {
	it('returns the formatted price', () => {
		let value = Math.floor(Math.random() * 1000);
		let expectedValue = `$${value}.00`;
		let returnedValue = FormatPrice(value);
		expect(returnedValue).toEqual(expectedValue);
	});
});
