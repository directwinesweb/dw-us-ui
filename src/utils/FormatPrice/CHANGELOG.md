# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.10](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.9...@dw-us-ui/format-price@1.1.10) (2021-07-08)

**Note:** Version bump only for package @dw-us-ui/format-price






## [1.1.9](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.8...@dw-us-ui/format-price@1.1.9) (2021-07-02)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.8](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.7...@dw-us-ui/format-price@1.1.8) (2021-06-30)

**Note:** Version bump only for package @dw-us-ui/format-price






## [1.1.7](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.6...@dw-us-ui/format-price@1.1.7) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.6](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.4...@dw-us-ui/format-price@1.1.6) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.5](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.4...@dw-us-ui/format-price@1.1.5) (2021-05-11)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.4](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.3...@dw-us-ui/format-price@1.1.4) (2021-05-10)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.3](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.2...@dw-us-ui/format-price@1.1.3) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.2](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.1...@dw-us-ui/format-price@1.1.2) (2021-05-05)

**Note:** Version bump only for package @dw-us-ui/format-price





## [1.1.1](https://bitbucket.org/directwinesweb/dw-us-ui/compare/@dw-us-ui/format-price@1.1.0...@dw-us-ui/format-price@1.1.1) (2021-03-25)

**Note:** Version bump only for package @dw-us-ui/format-price





# 1.1.0 (2021-03-09)


### Features

* added format price ([ff0ae59](https://bitbucket.org/directwinesweb/dw-us-ui/commits/ff0ae599a99f55d20ec68aeb905929fa072a9730))
