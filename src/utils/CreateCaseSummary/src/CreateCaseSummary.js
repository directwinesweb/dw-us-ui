const CreateCaseSummary = (items) => {
    let whiteGrapeArr = [];
    let redGrapeArr = [];
    let countryArr = [];
    let roseGrapeArr = [];
    let caseColor = '';

    /*
        Get Data from Wines
        */
    items.forEach((obj) => {
        if (obj.product.productType === 'wine') {
            let color = obj.product.colourName;
            let grape = obj.product.grapeName;
            let country = obj.product.countryName;

            //Create Red Grape Array
            if (color === 'Red') {
                redGrapeArr.push(grape);
            }

            //Create White Grape Array
            if (color === 'White') {
                whiteGrapeArr.push(grape);
            }

            //Create White Grape Array
            if (color === 'Rose') {
                roseGrapeArr.push(grape);
            }

            //Create Country Array
            countryArr.push(country);
        }
    });

    let hasRedGrape = redGrapeArr.length ? true : false;
    let hasWhiteGrape = whiteGrapeArr.length ? true : false;
    let hasRoseGrape = roseGrapeArr.length ? true : false;

    //Set Case Color
    if (hasRedGrape && !hasWhiteGrape && !hasRoseGrape) {
        caseColor = 'All Red';
    } else if (hasWhiteGrape && !hasRedGrape && !hasRoseGrape) {
        caseColor = 'All White';
    } else if (hasRoseGrape) {
        if (!hasRedGrape && !hasWhiteGrape) {
            caseColor = 'All Rosé';
        } else if (hasRedGrape && !hasWhiteGrape) {
            caseColor = 'Red & Rosé';
        } else if (hasWhiteGrape && !hasRedGrape) {
            caseColor = 'White & Rosé';
        } else {
            caseColor = 'Red, White & Rosé';
        }
    } else {
        caseColor = 'Red & White';
    }

    //Create Final Array Objects
    let createObj = (arr) => {
        var newArr = [];

        arr.forEach((name) => {
            var qty = arr.filter((val) => {
                return val === name;
            }).length;

            newArr.push({
                name,
                qty
            });
        });

        //Get Unique Values
        newArr = newArr.filter((obj, pos, arr) => {
            return arr.map((item) => item.name).indexOf(obj.name) === pos;
        });

        return newArr;
    };

    whiteGrapeArr = createObj(whiteGrapeArr);
    redGrapeArr = createObj(redGrapeArr);
    countryArr = createObj(countryArr);
    roseGrapeArr = createObj(roseGrapeArr);

    //Return final arrary objects
    return {
        whiteGrapeArr,
        redGrapeArr,
        roseGrapeArr,
        countryArr,
        caseColor
    };
};

export default CreateCaseSummary;
