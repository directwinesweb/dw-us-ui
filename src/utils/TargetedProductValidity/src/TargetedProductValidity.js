const TargetedProductValidity = (startDate, endDate) => {
    let currentDate = new Date(),
		validity = false;

	startDate = new Date(startDate);
	endDate = new Date(endDate);

	if(currentDate >= startDate && currentDate <= endDate) {
		validity = true;
	}

	return validity;
};
export default TargetedProductValidity;
