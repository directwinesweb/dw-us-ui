import CheckProperties from '../src/CheckProperties';

describe('CheckProperties test', () => {
	it('returns false for empty object', () => {
		let obj = {};
		expect(CheckProperties(obj)).toBeFalsy();
	});

	it('returns false for missing properties', () => {
		let obj = {
			firstName: 'TestName',
			lastName: 'LastName',
			address1: 'test',
			city: 'test1',
			stateCode: '12345',
		};
		expect(CheckProperties(obj)).toBeFalsy();
	});
	it('returns false for null properties of a complete object', () => {
		let obj = {
			firstName: 'TestName',
			lastName: 'LastName',
			address1: 'test',
			city: 'test1',
			stateCode: '12345',
			zipCode: '',
			phoneNumber: null,
		};
		expect(CheckProperties(obj)).toBeFalsy();
	});
	it('returns true for nonempty properties of a complete object', () => {
		let obj = {
			firstName: 'TestName',
			lastName: 'LastName',
			address1: 'test',
			city: 'test1',
			stateCode: '12345',
			zipCode: '12345',
			phoneNumber: '123123123',
		};
		expect(CheckProperties(obj)).toBeTruthy();
	});
	it('returns true valid phone number keys', () => {
		let obj = {
			firstName: 'TestName',
			lastName: 'LastName',
			address1: 'test',
			city: 'test1',
			stateCode: '12345',
			zipCode: '12345',
			dayPhoneNumber: '123123123',
			phoneNumber: '123123123',
		};
		expect(CheckProperties(obj)).toBeTruthy();
	});
	it('returns true valid phone number key', () => {
		let obj = {
			firstName: 'TestName',
			lastName: 'LastName',
			address1: 'test',
			city: 'test1',
			stateCode: '12345',
			zipCode: '12345',
			dayPhoneNumber: '123123123',
		};
		expect(CheckProperties(obj)).toBeTruthy();
	});
});
