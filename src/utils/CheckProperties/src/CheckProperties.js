/**
 *
 * @param {Object} obj
 * @returns {Boolean} it is a valid object
 */
const CheckProperties = (obj = {}) => {
	if (Object.entries(obj).length === 0) {
		return false; // empty object would mean no required key is present
	}

	let required = [
		'firstName',
		'lastName',
		'address1',
		'city',
		'stateCode',
		'zipCode',
	];

	let phoneArray = ['phoneNumber', 'dayPhoneNumber', 'deliveryPhoneNumber'];

	const evaluateNull = (key) => {
		return (
			obj[key] === null || obj[key] === '' || typeof obj[key] === 'undefined'
		);
	};
	let missingRequiredKeys = required.filter(evaluateNull);
	let validPhoneKey = false;

	for (let key in obj) {
		if (phoneArray.includes(key)) {
			validPhoneKey = true;
		}
	}

	if (missingRequiredKeys.length === 0 && validPhoneKey) {
		// no key is missing. Valid object
		return true;
	} else {
		return false; // a key is missing in the object, therefore not a valid object
	}
};

export default CheckProperties;
