import ObscureCredit from '../src/ObscureCredit';

describe('Obscure Credit Test', () => {
	it('Returns an obscured version', () => {
		let expectedValue = '****1112';
		let returnedValue = ObscureCredit('123456789101112');
		expect(expectedValue).toEqual(returnedValue);
	});
});
