/**
 * Util that consumes a given credit card number and returns an obscured version
 * eg: ****1111
 * @param {string} creditCardNumber - As name implies
 */
const ObscureCredit = (creditCardNumber = '') => {
	return creditCardNumber
		.replace(/.(?=.{4})/g, '*')
		.substring(creditCardNumber.length - 8);
};
export default ObscureCredit;
