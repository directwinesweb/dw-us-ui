/**
 * @param {String} name The query key
 *
 * @returns {String} The query value if the key is in the url
 */

const GetParam = (name) => {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
		results = regex.exec(location.search);
	return results === null
		? ''
		: decodeURIComponent(results[1].replace(/\+/g, ' '));
};

export default GetParam;
