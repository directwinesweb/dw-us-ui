import GetParam from '../src/GetParam';

describe('Get Param tests', () => {
	const originalLocation = window.location;

	beforeEach(() => {
		delete window.location;
		window.location = {
			href: '',
		};

		const url = 'http://test.com/?abc=4';

		Object.defineProperty(window, 'location', {
			value: new URL(url),
		});
		window.location.href = url;
	});

	afterAll(() => {
		window.location.href = originalLocation;
	});

	it('returns the query value if the value is in the url', () => {
		let returned = GetParam('abc');
		expect(returned).toEqual('4');
	});
	it('returns an empty string if the value is not in the url', () => {
		let returned = GetParam('xyz');
		expect(returned).toEqual('');
	});
});
