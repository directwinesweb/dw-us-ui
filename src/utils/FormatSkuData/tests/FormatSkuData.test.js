import FormatSkuData from '../src/FormatSkuData';
import item from './item';

describe('FormatSkuData test', () => {
	it('Returns empty if no items', () => {
		let returned = FormatSkuData([false]);
		expect(returned).toEqual([]);
	});
	it('Returns items array with new item information added', () => {
		let returned = FormatSkuData([item[0]]);
		expect(returned).toEqual(item[0]);
		expect(returned.product.skus[1]).not.toBeNull();
	});
	it('Checks if B code has been added to skus array', () => {
		let returned = FormatSkuData([item[0]]);
		expect(returned.product.skus[1].isNotBCode).toEqual(true);
	});
	it('Checks if C code has been added to skus array', () => {
		let returned = FormatSkuData([item[0]]);
		expect(returned.product.skus[2].isNotCCode).toEqual(true);
	});
});
