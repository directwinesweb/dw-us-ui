/*
     Format Sku Data
     Set VPP for Single bottles
     Add Values for Mixed Case
 */
const FormatSkuData = (items) => {
	let itemsArray = [];

	items.forEach((item) => {
		if (!item) {
			return false;
		}
		let product = item.product;
		let vintage = product.vintage;
		let mixed = product.mixed || vintage === '1950';
		let skus = mixed ? product.skus[0] : product.skus;
		let subProductType = product.subProductType;
		let salePrice;
		let listPrice;
		let savings;

		if (mixed) {
			salePrice = skus.salePrice.toFixed(2);
			listPrice = skus.listPrice.toFixed(2);
			savings =
				listPrice === salePrice ? false : (listPrice - salePrice).toFixed(2);
			skus.savings = savings > 5 ? savings : false;
			product.isJCodeCan = skus.itemCode[0] === 'J' && product.bottleType?.toLowerCase() === 'can';
			product.isTCodeCan = skus.itemCode[0] === 'T' && product.bottleType?.toLowerCase() === 'can';
			skus.bottleText = skus.numberOfBottles === 1 ? 'bottle' : 'bottles';
			if (product.isJCodeCan || product.isTCodeCan) {
				skus.bottleText = skus.numberOfBottles === 1 ? 'pack' : 'packs';
			}
			if (subProductType === 'Z098') {
				if (skus.listPrice !== skus.salePrice) {
					skus.listPrice = parseFloat(salePrice);
					skus.savings = false;
				}
			}
		} else {
			skus.forEach((sku) => {
				let itemCode = sku.itemCode;
				let prodcutBottleType = product.bottleType;
				//Check if B / C Code Exists
				product.isBCode = itemCode[0] === 'B' || product?.isBCode ? true : false;
				product.isCCode = itemCode[0] === 'C' || product?.isCCode ? true : false;
				product.isTCode = itemCode[0] === 'T' || product?.isTCode ? true : false;
				product.isFourPackCan = prodcutBottleType?.toLowerCase() === 'can' && !product.itemCode.match('/^[a-z]+$/i');
				product.isPrepackItemCode = product.itemCode?.substr(-2) == 50 && !product.itemCode.match('/^[a-z]+$/i');
				//Check if VPP Enabled
				product.isVpp = sku.vppApplier ? true : product.isVpp;
				sku.bottleText = sku.numberOfBottles === 1 ? 'bottle' : 'bottles';
				sku.shortBtlText = sku.numberOfBottles === 1 ? 'btl.' : 'btls.';
				//Set Savings
				let savings =
					sku.listPrice === sku.salePrice
						? false
						: (sku.listPrice - sku.salePrice).toFixed(2);
				sku.savings = savings > 5 ? savings : false;
				let savingsPercent =
					sku.listPrice === sku.salePrice
						? false
						: ((skus[0].salePrice - sku.salePricePerBottle) /
								skus[0].salePrice) *
						  100;
				sku.savingsPercent =
					savingsPercent > 5 ? savingsPercent.toFixed(0) + '%' : false;
			});

			if(!product.isFourPackCan && !product.isPrepackItemCode) {
				/*
						If No B Code - Create One
					*/
				if (!item.product.isBCode) {
					let createBItem = JSON.parse(JSON.stringify(skus[0]));
					createBItem.numberOfBottles = 6;
					createBItem.bottleText = 'bottles';
					createBItem.shortBtlText = 'btls.';
					createBItem.listPrice = (createBItem.salePricePerBottle * 6).toFixed(2);
					createBItem.salePrice = (createBItem.salePricePerBottle * 6).toFixed(2);
					createBItem.isNotBCode = true;
					skus.push(createBItem);
				}
	
				/*
						If No C Code - Create One
					*/
				if (!item.product.isCCode) {
					let createCItem = JSON.parse(JSON.stringify(skus[0]));
					let VPP = (createCItem.vppPrice * 12).toFixed(2);
					let SPP = (createCItem.salePricePerBottle * 12).toFixed(2);
					let VPPSale = (SPP - VPP).toFixed(2);
	
					createCItem.numberOfBottles = 12;
					createCItem.bottleText = 'bottles';
					createCItem.shortBtlText = 'btls.';
					createCItem.salePricePerBottle = product.isVpp
						? createCItem.vppPrice
						: createCItem.salePricePerBottle;
					createCItem.listPrice = SPP;
					createCItem.salePrice = product.isVpp ? VPP : SPP;
					createCItem.savings = product.isVpp ? VPPSale : false;
					createCItem.isNotCCode = true;
	
					if (createCItem.savings < 5) {
						createCItem.savings = false;
					}
	
					skus.push(createCItem);
				}
			}
		}

		//Create new Array of the Items
		itemsArray.push(item);
	});
	/*
            Return the Array
        */
	return itemsArray.length === 1 ? itemsArray[0] : itemsArray;
};

export default FormatSkuData;
