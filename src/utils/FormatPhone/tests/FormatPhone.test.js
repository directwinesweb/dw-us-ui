import FormatPhone from '../src/FormatPhone';

describe('Format Phone tests', () => {
	it('Formats number including area code', () => {
		let expectedValue = '123-456-7890';
		let returnedValue = FormatPhone('1234567890');
		expect(returnedValue).toEqual(expectedValue);
	});
	it('Formats 3 digit number', () => {
		let expectedValue = '123-';
		let returnedValue = FormatPhone('123');
		expect(returnedValue).toEqual(expectedValue);
	});
	it('Formats 7 digit number', () => {
		let expectedValue = '1234567-';
		let returnedValue = FormatPhone('1234567');
		expect(returnedValue).toEqual(expectedValue);
	});
});
