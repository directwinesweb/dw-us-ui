/**
 *
 * @param {String} phoneInput
 * @returns formatted phone number
 */
const FormatPhone = (phoneInput = '') => {
	let updatedPhoneInput = phoneInput;
	if (updatedPhoneInput.length === 3 || updatedPhoneInput.length === 7) {
		updatedPhoneInput += '-';
	}
	if (updatedPhoneInput.length === 10 && !updatedPhoneInput.includes('-')) {
		let formatted = updatedPhoneInput.match(/^(\d{3})(\d{3})(\d{4})$/);
		updatedPhoneInput = formatted[1] + '-' + formatted[2] + '-' + formatted[3];
	}
	return updatedPhoneInput;
};

export default FormatPhone;
