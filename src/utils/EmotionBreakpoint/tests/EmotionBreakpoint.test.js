import EmotionBreakpoint from '../src/EmotionBreakpoint';
const emotionArray = [
	{ type: 'desktop', query: '@media (min-width: 992px)' },
	{ type: 'notDesktop', query: '@media (max-width: 991px)' },
	{ type: 'tablet', query: '@media (min-width: 768px) and (max-width: 991px)' },
	{ type: 'mobile', query: '@media (max-width: 767px)' },
	{ type: 'notMobile', query: '@media (min-width: 768px)' },
	{ type: 'default', query: '@media (min-width: 768px)' },
];
describe('EmotionBreakpoint test', () => {
	it('returns media query', () => {
		emotionArray.forEach((item) => {
			let expectedValue = item.query;
			let returnedValue = EmotionBreakpoint(item.type);
			expect(returnedValue).toEqual(expectedValue);
		});
	});
});
