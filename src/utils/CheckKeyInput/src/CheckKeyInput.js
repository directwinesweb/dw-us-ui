/**
 * Prevents user from entering non-numeric value
 * @param {object} event
 */

const CheckKeyInput = (event) => {
	if (isNaN(event.key)) {
		event.preventDefault();
		return false;
	} else {
		return;
	}
};
export default CheckKeyInput;
