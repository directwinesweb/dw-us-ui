import CheckKeyInput from '../src/CheckKeyInput';

describe('CheckKeyInput test', () => {
	const testfn = jest.fn();
	it('returns false for a invalid key', () => {
		const result = CheckKeyInput({ key: '123', preventDefault: testfn });
		expect().toBeFalsy();
	});
	it('returns undefined for a valid key', () => {
		const result = CheckKeyInput({ key: 5, preventDefault: testfn });
		expect().toEqual(undefined);
	});
});
