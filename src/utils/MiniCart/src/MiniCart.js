import { GoogleAnalytics } from '@dw-us-ui/google-analytics';
import axios from 'axios';

let { setCartDataLayer } = GoogleAnalytics;

const MiniCart = {
	GetCartDetails(setProductPricing) {
		let url = '/api/cart/list?validate=true';
		axios
			.get(url, {})
			.then((cart) => {
				let lineItems = cart.data.response.lineItems;

				lineItems.forEach((item) => {
					let lineItemCode = item.lineItemIdentification.itemCode;

					if (lineItemCode.search('UL') > -1) {
						setProductPricing((prevState) => {
							return {
								...prevState,
								unlimitedInCart: true,
							};
						});
					}
				});
			})
			.catch();
	},
	AddItemToCart(itemCode, qty, brandTag = '', userPhoneNumber = '', domainUrl, isPreSellItem) {
		updateAddToCartButtonCopy('Adding...');
		let url = '/api/cart/itemcode/';
		axios
			.put(url + itemCode + '/' + qty, {})
			.then((cart) => {
				let data = cart.data.response;
				let lineItems = cart.data.response.lineItems;
				let numItems = data.numItems;
				let total = data.orderPriceInfo.rawSubtotal;
				let itemPrice;

				//Update Cart Dom
				updateCartDom(numItems, total);
				// Update Cart Data Layer
				setCartDataLayer(cart.data, brandTag, userPhoneNumber, domainUrl);

				lineItems.forEach((item) => {
					let lineItemCode = item.lineItemIdentification.itemCode;

					if (itemCode === lineItemCode) {
						itemPrice = item.itemPriceInfo.salePrice.toString();
					}
				});

				miniCart.config.itemAdded = {
					itemCode: itemCode,
					qty: qty,
					price: itemPrice
				};
				miniCart.config.cartDetails = cart.data.response;

				addOnModal.isEligible(cart.data);
			})
			.catch((error) => {
				// Manual chat trigger
				// manuallyTriggerLiveChat();
				let response = error.response;
				let data;
				if (response) {
					data = response.data;
					let statusMessage = data.statusText || '';
					if (
						statusMessage.includes(
							'This item cannot be ordered as this customer is already on same wineclub.'
						) ||
						statusMessage.includes(
							'Sorry, you can only order one wine plan introductory case'
						)
					) {
						miniCart.processDefaultError('wineplan');
					} else {
						miniCart.processDefaultError();
					}
				} else {
					miniCart.processDefaultError();
				}
			})
			.finally(() => {
				updateAddToCartButtonCopy(isPreSellItem ? 'Reserve Today':'Add to Cart');
			});
	},
	BatchAddToCart(cartObject, setProductPricing, brandTag = '', userPhoneNumber = '', url = '', isPreSellItem) {
		let requestOpts = {
			method: 'POST',
			url: '/api/cart/itemcode/',
			data: JSON.stringify(cartObject),
			contentType: 'application/json',
			responseType: 'json',
			headers: {
				'Content-Type': 'application/json',
			},
		};

		let cartItems = cartObject.cartItems;
		let mainItem = cartItems[0].itemCode;
		let qty = cartItems[0].quantity;
		let unlimitedItem = cartItems[1].itemCode;

		updateAddToCartButtonCopy('Adding...');

		axios(requestOpts)
			.then((cart) => {
				let data = cart.data.response;
				let lineItems = cart.data.response.lineItems;
				let numItems = data.numItems;
				let total = data.orderPriceInfo.rawSubtotal;
				let itemPrice, unlimitedPrice;

				//Update Cart Dom
				updateCartDom(numItems, total);

				// Update Cart Data Layer
				setCartDataLayer(cart.data, brandTag, userPhoneNumber, url);
				
				lineItems.forEach((item) => {
					let lineItemCode = item.lineItemIdentification.itemCode;

					if (mainItem === lineItemCode) {
						itemPrice = item.itemPriceInfo.salePrice.toString();
					}

					if (unlimitedItem === lineItemCode) {
						unlimitedPrice = item.itemPriceInfo.salePrice.toString();
					}
				});

				let unlimitedText = document.createTextNode('(' + unlimitedPrice + ')');
				document
					.getElementsByClassName('added-unlimited-text')[0]
					.appendChild(unlimitedText);
				document
					.getElementsByClassName('added-unlimited-msg')[0]
					.classList.remove('is-hidden');

				miniCart.config.itemAdded = {
					itemCode: mainItem,
					qty: qty,
					price: itemPrice
				};
				miniCart.config.cartDetails = cart.data.response;

				addOnModal.isEligible(cart.data);
				setProductPricing((prevState) => {
					return {
						...prevState,
						unlimitedInCart: true,
						unlimitedCheckbox: false,
					};
				});
			})
			.catch((error) => {
				// manuallyTriggerLiveChat();
				miniCart.processDefaultError(
					error.statusMessage.includes(
						'This item cannot be ordered as this customer is already on same wineclub.'
					) ||
						error.statusMessage.includes(
							'Sorry, you can only order one wine plan introductory case'
						)
						? 'wineplan'
						: null
				);
			})
			.finally(() => {
				updateAddToCartButtonCopy(isPreSellItem ? 'Reserve Today' : 'Add to Cart');
			});
	},
};

export default MiniCart;

const updateAddToCartButtonCopy = (copy = 'Adding...') => {
	let btnProductCart = document.querySelectorAll('.btn-product-cart');
	btnProductCart.forEach((button) => {
		button.textContent = copy;
		button.disabled = copy === 'Adding...';
	});
};

const updateCartDom = (numItems, total) => {
	let qtyElements = document.querySelectorAll('.mobile-qty');
	Array.from(qtyElements).forEach((element) => {
		element.innerHTML = numItems;
	});

	let totalElements = document.querySelectorAll('.mini-cart-total');
	Array.from(totalElements).forEach((element) => {
		element.innerHTML = '$' + total;
	});
};
