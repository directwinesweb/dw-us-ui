import { GetParam } from '@dw-us-ui/get-param';
import { GoogleAnalytics } from '@dw-us-ui/google-analytics';

let { setEventTracking } = GoogleAnalytics;

/*
  Set Food Pairings
*/
const FoodPairingUtils = {
    SetFoodPairings(items, config) {
        /*
        Loop through Product Array
        */
        items.forEach((item) => {
            let product = item.product;
    
            /*
         Make sure the pairing is Not
         Set Yet
              */
            if (product.pairing === false) {
                /*
            Filter on Pairing Matches
            */
                product.pairing = config.filter((pairing) => {
                    let basedOn = pairing.basedOn;
                    let basedOnOne = basedOn[0];
                    let matchFieldOne = product[basedOnOne];
                    let pairingOne = pairing[basedOnOne];
                    /*
               Check if the Based On Field
               Only Needs to Match on One Field
               */
                    if (basedOn.length === 1) {
                        /*
                Match the Product Field w/
                Food Pairing Field
                Make sure the field is not based off StyleId
              */
                        if (matchFieldOne === pairingOne && basedOnOne !== 'styleId') {
                            let roseStyles = ['Rse1', 'Rse2', 'Rse3'];
    
                            /*
                 Rose Wine Styles Should Not Match
                 If there are Grape Matches
                    */
                            if (roseStyles.indexOf(product.styleId) === -1) {
                                return pairing;
                            }
                        } else if (basedOnOne === 'styleId') {
                            /*
                            Match the Product Field w/
                            Food Pairing Field
                            If the based on field is style, then Check
                            the styles listed against the wine style
                        */
                            let styleMatches = pairingOne;
                            if (styleMatches.indexOf(matchFieldOne) > -1) {
                                return pairing;
                            }
                        }
                    } else {
                        /*
                         Check if the Based On Field
                         Needs Match on 2 Fields
                    */
                        let basedOnTwo = basedOn[1];
                        let matchFieldTwo = product[basedOnTwo];
                        let pairingTwo = pairing[basedOnTwo];
                        /*
                Check if Match Field One to Pairing One
                And the first based on field is grapeName
                */
                        if (matchFieldOne === pairingOne && basedOnOne === 'grapeName') {
                            /*
                 Checks if the two based on Field
                 is Region Name
                    */
                            if (basedOnTwo === 'regionName') {
                                if (pairingTwo[0].includes('not')) {
                                    if (matchFieldTwo !== pairingTwo) {
                                        return pairing;
                                    }
                                } else {
                                    if (matchFieldTwo === pairingTwo[0]) {
                                        return pairing;
                                    }
                                }
                            } else if (basedOnTwo === 'styleId') {
                                /*
                                 Checks if the two based on Field
                                 is Style Id
                            */
                                let styleMatches = pairingTwo;
                                if (styleMatches.indexOf(matchFieldTwo) > -1) {
                                    return pairing;
                                }
                            } else if (basedOnTwo === 'alcoholPercent') {
                                /*
                                 Checks if the two based on Field
                                 is alcoholPercent
                            */
                                let alcoholMatches = pairingTwo[0].split('_');
                                let alcoholPercent = parseInt(alcoholMatches[1]);
    
                                if (alcoholMatches[0] === 'lte') {
                                    if (matchFieldTwo <= alcoholPercent) {
                                        return pairing;
                                    }
                                } else {
                                    if (matchFieldTwo > alcoholPercent) {
                                        return pairing;
                                    }
                                }
                            }
                        } else if (basedOnOne === 'styleId') {
                            /*
                            If the first based on field is styleId
                            Checks if the style id matches on the product
                            then checks if the region value matches the product
                        */
                            let styleMatches = pairingOne;
                            if (styleMatches.indexOf(matchFieldOne) > -1) {
                                if (basedOnTwo === 'regionName') {
                                    if (matchFieldTwo === pairingTwo) {
                                        return pairing;
                                    }
                                }
                            }
                        }
                    }
                });
            }
        });
    
        //Return the Array of Pairing Matches
        return items;
    },
    CreateFoodPairingsArray(items, category, action) {
        let foodPairingsArray = [];
        /*
          Pushes the Food Pairings to
          one final array
        */
        items.forEach((item, index) => {
            let product = item.product;
            /*
           For Debugging Purposes
           */
            if (GetParam('dw_foodPairings')) {
                console.log('=================');
                console.log(
                    product.name,
                    'styleId',
                    product.styleId,
                    'grapeName ' + product.grapeName,
                    'regionName ' + product.regionName,
                    'alcoholPercent ' + product.alcoholPercent
                );
                console.log(product.pairing, index + 1);
                console.log('=================');
            }
    
            if (product.pairing[0] === undefined) {
                setEventTracking({
                    category: category,
                    action: action,
                    label: 'Food_Pairings_Missing_' + product.grapeName
                });
            }
    
            foodPairingsArray.push(product.pairing[0]);
        });
    
        //Return Food Pairing Array
        return foodPairingsArray;
    },
    CreateFinalPairingsArray(items) {
        let finalPairingArray = [];
    
        items.forEach((obj) => {
            if (obj !== undefined) {
                var qty = items.filter((val) => {
                    if (val !== undefined) {
                        return val.label === obj.label;
                    }
                }).length;
    
                obj.qty = qty;
                finalPairingArray.push({obj});
            }
        });
    
        /*
        Grab Unique Food Pairings
        */
        finalPairingArray = finalPairingArray.map((item) => item.obj);
        let array = finalPairingArray.reduce((arr, arrObj) => {
            if (
                !arr.some((obj) => {
                    return obj.label === arrObj.label;
                })
            )
                arr.push(arrObj);
            return arr;
        }, []);
    
        finalPairingArray = array;
    
        /*
        Sort Array
      */
        finalPairingArray = finalPairingArray.sort((a, b) => b.qty - a.qty);
    
        //Return Final Pairing Array
        return finalPairingArray;
    }
}

export default FoodPairingUtils;
