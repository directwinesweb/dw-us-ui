/**
 * @param {String} errorKey - Key value for custom trackJS tracking
 * @param {String} errorMessage - Simple message for tracking
 *
 * Pushes errors to trackJS for manual tracking
 */

const HandleTrackJSError = ({ errorKey = 'API FAILURE', errorMessage }) => {
	if (window?.TrackJS) {
		window.TrackJS.track(`${errorKey}: ${errorMessage}.`);
	} else {
		console.warn('Track JS not in scope');
	}
};
export default HandleTrackJSError;
