import { BrandUtil } from '@dw-us-ui/brand-util';
// import LogoWineryDirect from '../assets/logo_winery_direct.png';
import { Button } from '@dw-us-ui/button';
import React from 'react';

// import LogoCA from '../assets/logo_CA.png';
// import LogoCO from '../assets/logo_CO.png';
import LogoIN from '../assets/logo_IN.png';
import LogoMA from '../assets/logo_MA.png';
import LogoMI from '../assets/logo_MI.png';
import LogoNJ from '../assets/logo_NJ.png';
import LogoNY from '../assets/logo_NY.png';
import LogoVA from '../assets/logo_VA.png';
import LogoWA from '../assets/logo_WA.png';

/**
 * Get Retailer Information
 * @param {string} - retailer
 * @param {string} - state
 */
const GetRetailer = (retailer, state = false) => {
	const LogoCA = '/images/us/common/Lionstone_West_Logo.svg';
	const LogoWineryDirect = '/images/us/common/Lionstone_Intl_Logo.svg';
	const brandTag =
		// eslint-disable-next-line no-undef
		typeof WEBPACK_BRAND_TAG !== 'undefined' ? WEBPACK_BRAND_TAG : 'law';
	const brand = BrandUtil.getBrand(brandTag);
	const data = BrandUtil.content(brand);
	const { name, phone } = data;
	let retailerName = retailer;
		
	let literLimitNumber = '108',
		literTimeLimitCopy = 'year',
		literLimit = false,
		recipient = 'address';
	let address = false,
		belowZipCityState = false,
		content = false,
		modalAction = false,
		CAProp65Lang = false,
		modalContent = false,
		footerContent = false,
		complianceRedirect = false,
		logo = false,
		customerServiceMailingAddress = false,
		SDContent = false,
		states = [],
		liquorCommission = 'Department of Alcoholic Beverage Control';
		
		

	// Liter Limit check
	switch (state) {
		case 'Alabama':
			literLimit = true;
			break;
		case 'Georgia':
			literLimit = true;
			break;
		case 'Hawaii':
			literLimit = true;
			literLimitNumber = '54';
			break;
		case 'Idaho':
			literLimit = true;
			literLimitNumber = '216';
			literTimeLimitCopy = 'year';
			recipient = 'person';
			break;
		case 'Kansas':
			literLimit = true;
			break;
		case 'Kentucky':
			literLimitNumber = '90';
			literTimeLimitCopy = 'month';
			recipient = 'person';
			literLimit = true;
			break;
		case 'Maine':
			literLimit = true;
			break;
		case 'Maryland':
			literLimit = true;
			literLimitNumber = '162';
			break;
		case 'Minnesota':
			recipient = 'person';
			literLimit = true;
			literLimitNumber = '18';
			break;
		case 'Montana':
			literLimit = true;
			literLimitNumber = '162';
			recipient = 'person';
			break;
		case 'New Hampshire':
			literLimit = true;
			break;
		case 'North Carolina':
			recipient = 'person';
			literLimit = true;
			literLimitNumber = '18';
			literTimeLimitCopy = 'month';
			break;
		case 'Oklahoma':
			literLimit = true;
			literLimitNumber = '54';
			break;
		case 'Pennsylvania':
			literLimitNumber = '324';
			literLimit = true;
			break;
		case 'South Carolina':
			recipient = 'person';
			literLimit = true;
			literLimitNumber = '18';
			literTimeLimitCopy = 'month';
			break;
		case 'South Dakota':
			literLimit = true;
			break;
		case 'Tennessee':
			literLimit = true;
			literLimitNumber = '9';
			literTimeLimitCopy = 'month to a maximum of 27 liters per person per year';
			recipient = 'person';
			break;
		case 'Vermont':
			literLimit = true;
			break;

		// May need below code for future reference - under review by Legal Department
		/* 
		case 'Virginia':
		 	literLimit = true;
		 	literLimitNumber = '18';
		 	literTimeLimitCopy = 'month';
		 	break;
		 case 'Wyoming':
		 	literLimit = true;
			break; 
		*/

		default:
			null;
	}

	
	const literLimitCopy = `<b>Good to know:</b> Each winery is licensed in ${state} to sell up to ${literLimitNumber} liters per ${recipient} 
	per ${literTimeLimitCopy}.`;


	switch (retailer) {
		case 'Wine Dock Co.':
			states = ['MI'];
			address = '24421 E. Jefferson, Saint Clair Shores, MI 48080';
			content = 'All orders are fulfilled by a Michigan licensed retailer';
			footerContent = `All orders are fulfilled by a Michigan licensed retailer<br />${address}<br />Who is ${retailer}?`;
			logo = LogoMI;
			complianceRedirect = `Hello there, and thanks for shopping with Wine Dock's ${name} site.<br /><br />We will now be redirecting you to Wine Dock Co., a Michigan licensed retailer that can legally accept, fulfill, and ship your order in the state of Michigan.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			liquorCommission = 'Liquor Control Commission';
			break;
		case 'Wine Direct':
			states = ['IN'];
			address = '4520 Lima Road, Fort Wayne, IN 46808';
			content =
				'All orders are accepted and fulfilled by a Indiana licensed retailer';
			footerContent = `All orders are accepted and fulfilled by a Indiana licensed retailer<br />${address}<br />Who is ${retailer}`;
			logo = LogoIN;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}<br /><br />We will now be redirecting you to ${retailer}, a Indiana licensed retailer who can legally accept, fulfill and ship your order in the state of Indiana.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			break;
		case 'The Wine Cellar Outlet':
			states = ['MA'];
			address = '565 Main Street, Walpole, MA 02081-3726';
			footerContent = `All orders are fulfilled by a Massachusetts licensed retailer<br />${address}<br />Who is ${retailer}?`;
			logo = LogoMA;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a Massachusetts licensed retailer who can legally accept, fulfill and ship your order in the state of Massachusetts.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			break;
		case 'The Wine Cellar at Rye Ridge':
			states = ['NY'];
			address = '122 S Ridge Street, Rye Brook, NY 10573';
			content =
				'All orders are accepted and fulfilled by a New York licensed retailer';
			logo = LogoNY;
			belowZipCityState = `Upon completion of this form, your order will be forwarded to ${retailer}, located in Rye Brook, NY for processing and shipping.`;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a New York licensed retailer who can legally accept, fulfill and ship your order in the state of New York.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			customerServiceMailingAddress = `${retailer}<br />Attn: Customer Service<br />PO Box 5160<br />Largo, FL 33779`;
			break;
		case 'The Wine Cellar at Red Bank':
			states = ['NJ'];
			address = '23 Monmouth St, Red Bank, NJ 07701';
			footerContent = `All orders are accepted and fulfilled by a New Jersey licensed retailer.<br /><br /><br />${address}<br />Who is ${retailer}?`;
			logo = LogoNJ;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a New Jersey licensed retailer who can legally accept, fulfill and ship your order in the state of New Jersey.<br /><br /><b>Good to know:</b> New Jersey law does not allow for free goods in conjunction with the sale of alcohol beverages. If your purchase today involves a free good, the cost of all advertised items in the offer are included in the advertised price. The invoice in your shipment will itemize the cost of each item included in your shipment. <br /><br />If you have any questions, please give us a call at ${phone}.`;
			break;
		case 'The Wine Cellar':
			states = ['VA', 'IL'];
			address = '1705 New Lenox Rd, Joliet, IL 60433';
			content = `All orders are accepted and fulfilled by a ${state} licensed retailer`;
			footerContent = `All orders are accepted and fulfilled by a ${state} licensed retailer<br />${address}<br />Who is ${retailer}?`;
			logo = LogoVA;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a ${state} licensed retailer who can legally accept, fulfill and ship your order in the state of ${state}.<br /><br />If you have any questions, please give us a call at ${phone}.`;

			if (state === 'Virginia') {
				belowZipCityState =
					'By completing and submitting this form, I confirm that I am placing an order for wine with Connoisseur Encounters Co., Inc. d/b/a The Wine Cellar.';
			}
			break;
		case 'The Wine Cellar at Wallingford':
			// states = ['CT'];
			address = '42 Capital Dr. Wallingford CT 06492';
			content = `All orders are accepted and fulfilled by a ${state} licensed retailer`;
			footerContent = `All orders are accepted and fulfilled by a ${state} licensed retailer<br />${address}<br />Who is ${retailer}?`;
			logo = LogoVA;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a ${state} licensed retailer who can legally accept, fulfill and ship your order in the state of ${state}.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			belowZipCityState =
				'Upon completion of this form, your order will be forwarded to The Wine Cellar at Wallingford, located in Wallingford, CT for processing and shipping.';
			break;
		case 'Multiple-TX':
			states = ['TX'];
			belowZipCityState =
				'If your order is accepted by one of the wineries below, they will then process and fulfill your wine order.<br />Homestead Winery @ Plano <br />Homestead Winery <br />Sunset Winery LLC <br />Salado Wine Seller <br />Brennan Vineyards <br />Bar Z Winery <br />Landon Winery <br />Paris Vineyards';
			break;
		case 'Lionstone West':
			states = ['NM', 'NV', 'CA', 'OR', 'DC'];
			address = '50 Technology Court, Napa, CA 94558';
			content = 'All orders are fulfilled by a California licensed retailer';
			footerContent = `All orders are fulfilled by a California licensed retailer<br />${address}<br />Who is ${retailer}?`;
			logo = LogoCA;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a California licensed retailer who can legally accept, fulfill and ship your order ${
				state === 'District Of Columbia' ? 'to the ' : 'in the state of '
			}${state}.<br /><br />If you have any questions, please give us a call at ${phone}.<br /><br />`;
			if (state === 'California') {
				CAProp65Lang = {
					warning:
						"<b>WARNING:</b> Drinking distilled spirits, beer, coolers, wine and other alcoholic beverages may increase cancer risk, and, during pregnancy, can cause birth defects. For more information go to <a href='https://www.P65Warnings.ca.gov/alcohol'>www.P65Warnings.ca.gov/alcohol.</a> <br /><br /><b>WARNING:</b> Consuming this product can expose you to chemicals including BPA, which is known to the State of California to cause birth defects or other reproductive harm. For more information go to <a href='https://www.P65Warnings.ca.gov/food'>www.P65Warnings.ca.gov/food.</a>",
					content:
						'<b>Good to know</b>: California law does not allow for free goods in conjunction with the sale of alcohol beverages. If your purchase today involves a free good, the cost of all advertised items in the offer are included in the advertised price. The invoice in your shipment will itemize the cost of each item included in your shipment. <br /><br />',
				};
				belowZipCityState = CAProp65Lang.content + CAProp65Lang.warning;
			}
			break;
		case 'Lionstone International':
			states = [
				'AL',
				'CO',
				'GA',
				'HI',
				'ID',
				'KS',
				'KY',
				'MD',
				'ME',
				'MN',
				'MT',
				'NC',
				'OK',
				'PA',
				'SC',
				'SD',
				'TN',
				'VT',
			];
			content = `All orders are accepted and fulfilled by ${retailer}, a licensed winery`;
			modalContent = `<p>
                In accordance with state Winery Direct regulations, all orders for wine must be placed with a licensed winery.
                ${name}, along with its marketing partners, has chosen ${retailerName}.
            </p>
            <p>
                ${retailerName} is a licensed winery who is able to accept, fulfill and ship your order on behalf of ${name} in your state. Should you have any questions regarding ${retailerName}, please contact customer
                service at ${phone}.
            </p>`;
			modalAction = (
				<Button
					onClick={() => {
						window.location.href = '/';
					}}
				>
					Continue Shopping
				</Button>
			);
			footerContent = `All orders are accepted and fulfilled by ${retailer}, a licensed winery<br />Who is ${retailer}?`;
			logo = LogoWineryDirect;
			literLimit = state === 'Colorado' ? false : true;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br /> We will now be redirecting you to one of Lionstone International’s licensed wineries that is able to accept, fulfill, and ship your order on ${name}’s behalf to the state of ${state}.${
				literLimit ? `<br /><br />${literLimitCopy}` : `<br /><br />`
			}${
				state === 'Colorado'
					? `<b>Good to know:</b> Colorado law does not allow for free goods in conjunction with the sale of alcohol beverages. If your purchase today involves a free good, the cost of all advertised items in the offer are included in the advertised price. The invoice in your shipment will itemize the cost of each item included in your shipment.`
					: ''
			}<br /><br />If you have any questions, please give us a call at ${phone}.`;
			if (state === 'Kentucky') {
				complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br /> We will now be redirecting you to one of Lionstone International’s licensed wineries that is able to accept, fulfill, and ship your order on ${name}’s behalf to the state of ${state}.
				<br /><br />${literLimitCopy}
				<br /><br />If you have any questions, please give us a call at ${phone}.`;
			}
			if (state === 'South Dakota') {
				SDContent = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to one of Lionstone International’s licensed wineries that can accept, fulfill, and ship your order on ${name}’s behalf to the state of South Dakota.<br /><br /><b>Good to know:</b> Each winery is licensed in South Dakota to sell up to 108 liters per address per year.<br /><br />South Dakota offers include no free or bonus items, and no vouchers are required.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			}
			if (state === 'Hawaii') {
				complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br /> We will now be redirecting you to one of Lionstone International’s licensed wineries that is able to accept, fulfill and ship your order to ${state} (Honolulu and Hawaii Counties only).${
					literLimit ? `<br /><br />${literLimitCopy}` : `<br /><br />`
				}<br /><br />If you have any questions, please give us a call at ${phone}.`;
			}
			if (state === 'Alabama') {
				belowZipCityState = `<b>Good to know</b>: The licensed retailer has collected the simplified sellers use tax on taxable transactions delivered into Alabama and the tax will be remitted on the customer's behalf to the Alabama Department of Revenue. Seller's program account number is SSU-R011166849.`;
			}
			if (state === 'Idaho') {
				complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br /> We will now be redirecting you to one of Lionstone International’s licensed wineries that is able to accept, fulfill, and ship your order 
				on 
				${name === "Laithwaites" ? `${name}’` : `${name}’s`} 
				behalf to the state of ${state}.  
				<br /><br />${literLimitCopy}
				<br /><br />If you have any questions, please give us a call at ${phone}.`;
			}
			break;
		case 'The Wine Cellar Outlet Issaquah':
			states = ['WA'];
			content =
				'All orders are accepted and fulfilled by a Washington licensed retailer';
			address = '710 NW Gilman Blvd., D-104, Issaquah, WA 98027';
			footerContent = `All orders are accepted and fulfilled by a Washington licensed retailer, ${address}<br />Who is ${retailer}?`;
			logo = LogoWA;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to ${retailer}, a Washington licensed retailer who can legally accept, fulfill and ship your order in the state of Washington.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			break;
		case 'The Wine Cellar Outlet Cincinnati':
			states = ['WY'];
			address = '700 W Pete Rose Way, Suite E1, Cincinnati, OH 45203';
			content = `All orders are accepted and fulfilled by a WY licensed retailer`;
			footerContent = `All orders are accepted and fulfilled by a WY licensed retailer<br />${address}<br />Who is The Wine Cellar Outlet?`;
			logo = LogoMA;
			complianceRedirect = `Hello there, and thanks for shopping with ${name}.<br /><br />We will now be redirecting you to The Wine Cellar Outlet, a WY licensed retailer who can legally accept, fulfill and ship your order in the state of WY.<br /><br />If you have any questions, please give us a call at ${phone}.`;
			break;
		default:
			return "Retailer doesn't exist";
	}

	if (state === 'Wyoming') {
		retailerName = 'The Wine Cellar Outlet';
		state = 'WY';
	}

	if (state !== 'Texas') {
		modalContent = `<p>
            In accordance with the ${state} ${liquorCommission}, all orders for wine must be placed with a licensed ${state} wine retailer. ${name}, along with its marketing partners, has chosen ${retailerName}.
        </p>
        <p>
            ${retailerName} is a ${state} licensed retailer who is able to accept, fulfill and ship your order for ${name} in the state of ${state}. Should you have any questions regarding ${retailerName}, please
            contact customer service at ${phone}.
        </p>`;
	}

	return {
		retailerName,
		states,
		currentState: state,
		address,
		content,
		logo,
		modalContent,
		modalAction,
		footerContent,
		belowZipCityState,
		CAProp65Lang,
		complianceRedirect,
		customerServiceMailingAddress,
		SDContent,
	};
};

export default GetRetailer;
