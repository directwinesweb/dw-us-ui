import GetRetailer from '../src/GetRetailer';

const infoArr = [
	{ state: 'Washington', retailer: 'The Wine Cellar Outlet Issaquah' },
	{ state: 'Virginia', retailer: 'The Wine Cellar' },
	{ state: 'Texas', retailer: 'Multiple-TX' },
	{ state: 'South Dakota', retailer: 'Lionstone International' },
	{ state: 'Massachusetts', retailer: 'The Wine Cellar Outlet' },
	{ state: 'Vermont', retailer: 'Lionstone International' },
	{ state: 'New York', retailer: 'The Wine Cellar at Rye Ridge' },
];

describe('Get Retailer tests', () => {
	it('Returns if retailer does not exits', () => {
		let expected = "Retailer doesn't exist";
		let returned = GetRetailer(null, 'Alaska');
		expect(expected).toEqual(returned);
	});
	it('Returns object with retailer info', () => {
		infoArr.forEach((item) => {
			let retailer = item.retailer;
			let returned = GetRetailer(retailer, item.state);
			expect(retailer).toEqual(returned.retailerName);
		});
	});
});
