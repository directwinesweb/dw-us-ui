import GetEnvironment from '../src/GetEnvironment';

const setLocation = (url) => {
	Object.defineProperty(window, 'location', {
		value: new URL(url),
	});
	window.location.href = url;
};

describe('Get Environment tests', () => {
	const originalLocation = window.location;

	afterAll(() => {
		window.location.href = originalLocation;
	});

	beforeEach(() => {
		delete window.location;
		window.location = {
			href: '',
		};
	});

	it('Should handle return the correct environment', () => {
		const urlArray = [
			'https://webdev005.wsjwine.com:9090/',
			'https://preview.laithwaiteswine.com/',
			'https://www.wsjwine.com/',
		];

		urlArray.forEach((url) => {
			setLocation(url);
			let returned = GetEnvironment();
			if (url.includes('www') || url.includes('preview')) {
				expect(returned).toBe('production');
			} else {
				expect(returned).toBe('staging');
			}
		});
	});
});
