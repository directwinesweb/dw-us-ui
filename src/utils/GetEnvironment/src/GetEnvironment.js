/**
 *
 * @returns {String} - production or staging
 */
const GetEnvironment = () => {
	const hostname = location.hostname.split('.');
	const prodArr = ['www', 'preview', 'softlaunch'];

	return prodArr.includes(hostname[0]) ? 'production' : 'staging';
};

export default GetEnvironment;
