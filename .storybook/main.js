module.exports = {
	stories: [
		'../src/**/*.stories.js',
		'../src/**/*.stories.mdx',
		'./src/**/*.stories.js',
		'./src/**/*.stories.mdx',
	],
	addons: [
		'@storybook/addon-actions',
		'@storybook/addon-knobs',
		'@storybook/addon-links',
		'@storybook/addon-viewport',
		'@storybook/addon-docs',
		'@react-theming/storybook-addon',
		'@storybook/addon-a11y',
		'@storybook/addon-controls',
	],
};
