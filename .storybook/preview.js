import {
	lawTheme,
	mcyTheme,
	nprTheme,
	virTheme,
	wsjTheme,
} from '../src/themes/src';

import { BrandUtil } from '../src/utils/BrandUtil/src';
import React from 'react';
import { ThemeProvider } from 'emotion-theming';
import { addDecorator } from '@storybook/react';
import { withThemes } from '@react-theming/storybook-addon';

// pass ThemeProvider and array of your themes to decorator

// .storybook/preview.js

const themes = [
	{
		data: BrandUtil.content(lawTheme.brand),
		...lawTheme,
		class: ['law'],
	},
	{
		data: BrandUtil.content(wsjTheme.brand),
		...wsjTheme,
	},
	{
		data: BrandUtil.content(virTheme.brand),
		...virTheme,
	},
	{
		data: BrandUtil.content(mcyTheme.brand),
		...mcyTheme,
	},
	{
		data: BrandUtil.content(nprTheme.brand),
		...nprTheme,
	},
];

addDecorator(withThemes(ThemeProvider, themes));

export const decorators = [
	(Story) => (
		<div style={{ margin: '0 auto', maxWidth: '900px' }}>
			<Story />
		</div>
	),
];

export const parameters = {
	options: {
		storySort: {
			order: [
				'Introduction',
				'Working in DW_US_UI',
				[
					'Introduction',
					'Library Architecture',
					'Developing a new component',
					'Testing Components',
					'Building Components',
					'Publishing Components',
					'Creating Bundles',
				],
				'For Developers',
				['Getting Started'],
			],
		},
	},
};
