const proxy = require('http-proxy-middleware');

module.exports = function expressMiddleware(router) {
	console.log('Building proxy...');
	router.use(
		['/api', '/jsp', '/images', '/assets'],
		proxy({
			target: 'https://www.wsjwine.com',
			changeOrigin: true,
		})
	);
};
