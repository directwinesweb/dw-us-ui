module.exports = {
	roots: ['<rootDir>/src', '<rootDir>/tests'],
	testEnvironment: 'jsdom',
	testRegex: 'src/.*\\.test.(js|jsx)$',
	setupFiles: ['<rootDir>/.jest/bootstrap.js', 'jest-prop-type-error'],
	moduleNameMapper: {
		'\\.(css|less|scss|sass)$': 'identity-obj-proxy',
		'\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
			'<rootDir>/.jest/fileMock.js',
		// '^react($|/.+)': '../node_modules/react$1',
	},
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
	testPathIgnorePatterns: [
		'<rootDir>/node_modules/',
		'<rootDir>/.docz/',
		'<rootDir>/build/',
		'<rootDir>/dist/',
	],
	collectCoverageFrom: [
		'**/src/**/*.{js,jsx}',
		'!**/src/**/*.stories.{js,jsx}',
		'!**/node_modules/**',
		'!**/coverage/**',
	],
	snapshotSerializers: ['enzyme-to-json/serializer', 'jest-emotion'],
	verbose: true,
};
