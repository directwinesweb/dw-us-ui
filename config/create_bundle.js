const fs = require('fs');
var inquirer = require('inquirer');
const libraryName = '@dw-us-ui';
const {
	CheckDirectoryExists,
	CreateWebpackDevConfig,
	CopyWriteFiles,
	CreatePackageJson,
} = require('./helper_methods');

const rootConfig = [
	{
		filename: '.npmignore',
		sourcePath: './config/files/.npmignore',
	},
	{
		filename: 'src/index.js',
		sourcePath: './config/files/bundle_index.js',
	},
];

inquirer
	.prompt([
		{
			name: 'packageName',
			message:
				'A package.json file will be created, please provide a package name for the bundle: ',
			validate: function (input) {
				const regex = /^[a-z]+(-[a-z]+)*$/;

				var done = this.async();

				// Do async stuff
				setTimeout(function () {
					if (!regex.test(input)) {
						// Pass the return value in the done callback
						done(
							'Package name must only include lowercase characters and words separated by dashes.'
						);
						return;
					}
					// Pass the return value in the done callback
					done(null, true);
				}, 100);
			},
		},
		{
			name: 'packageDescription',
			message: 'Package Bundle description?',
		},
	])
	.then((answers) => {
		const { packageName, packageDescription } = answers;
		const directory = './src/bundles/' + packageName;

		CheckDirectoryExists({ directory, createDirectory: true, bypass: false });
		// check that directory exists
		fs.mkdirSync(directory + '/src');

		// files

		CreateWebpackDevConfig(directory);
		CopyWriteFiles({ directory, rootConfig });

		CreatePackageJson({
			directory,
			packageName,
			packageDescription,
			hasTests: false,
			envName: '--env.bundle=true',
		});
	})
	.catch((error) => {
		if (error.isTtyError) {
			// Prompt couldn't be rendered in the current environment
		} else {
			// Something else went wrong
			console.error(error);
		}
	});
