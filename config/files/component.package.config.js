module.exports = {
	name: 'noname',
	description: 'no description',
	version: '1.0.0',
	license: 'MIT',
	author: 'DW US',
	main: 'dist/{name}.cjs.js',
	module: 'dist/{name}.esm.js',
	src: 'src/index.js',
	scripts: {
		build: 'builder',
		dev: 'builder-watch',
		'build-webpack':
			'npm run lint && cross-env webpack -p --config webpack.config.js --env.mode=prod --env.name=',
		watch:
			'cross-env webpack -w --config ./webpack.config.dev.js --env.mode=dev --env.name=',
		'dev-webpack': 'npm run watch && npm run start',
		lint: 'eslint "src/**/*.{js,jsx}"',
		format: 'prettier --write "src/**/*.{js,jsx}"',
		start:
			'cross-env webpack-dev-server -d --config ./webpack.config.dev.js --env.mode=dev --env.brandTag=law --env.name=',
		clean: 'rimraf dist && rimraf node_modules && rimraf package-lock.json',
		test: 'jest',
		'test:watch': 'jest --watch',
	},
	files: ['dist', 'package.json'],
	babel: {
		presets: ['@babel/preset-env', '@babel/preset-react'],
		env: {
			test: {
				plugins: ['require-context-hook'],
			},
		},
	},
	peerDependencies: {},
	devDependencies: {},
};
