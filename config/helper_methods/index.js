const fs = require('fs');
const libraryName = '@dw-us-ui';

/**
 *
 * @param {String} directory - directory to check
 * @param {Boolean} createDirectory - determine if directory should be created
 * @param {Boolean} bypass - bypass error if directory exists
 * checks if given directory exists
 * will create directory if it doesn't exist
 */
const CheckDirectoryExists = ({
	directory,
	createDirectory = false,
	bypass = false,
}) => {
	try {
		if (fs.existsSync(directory)) {
			if (!bypass) {
				throw 'Directory: ' + directory + ' already exists, please try again.';
			} else {
				console.log(
					'Directory: ' + directory + ' already exists. Continuing...'
				);
			}
		} else {
			console.log("Directory '" + directory + "' does not exist.");
			if (createDirectory) {
				fs.mkdirSync(directory);
				console.log('Directory created: ' + directory);
			}
		}
	} catch (e) {
		throw e;
	}
};

/**
 *
 * @param {String} directory - directory to component
 */
const GeneratePathToRoot = (directory) => {
	let pathLength = (directory.match(/\//g) || []).length;

	let path = '';

	for (let index = 0; index < pathLength; index++) {
		path = path + '../';
	}

	return path;
};

/**
 *
 * @param {String} packageName - package name
 * @param {String} directory - directory
 *
 * @returns {Object} { aliasName, aliasPath } ..alias path is relative to the ./webpack/aliases.config.js
 */
const CreateAlias = ({ packageName, directory }) => {
	const aliasName = libraryName + '/' + packageName;
	const aliasPath = '.' + directory + '/src/index';

	return { aliasName, aliasPath };
};

/**
 *
 * @param {String} directory - directory to component
 */
const CreateWebpackDevConfig = (directory) => {
	let webpackPath = directory + '/webpack.config.dev.js';

	let pathToRoot = GeneratePathToRoot(directory);

	let content = `module.exports = (env) => {
    const config = require('${pathToRoot}webpack/webpack.dev.base.config')(env);
    return config;
};`;

	fs.writeFile(webpackPath, content, (err) => {
		// throws an error, you could also catch it here
		if (err) throw err;

		// success case, the file was saved
		console.log(`${webpackPath} - created successfully`);
	});
};

const CopyWriteFiles = ({ rootConfig, directory }) => {
	// for each file config, copy and create new file for the package
	console.log('Creating files...');
	for (let index = 0; index < rootConfig.length; index++) {
		const fileData = rootConfig[index];
		const destinationPath = directory + '/' + fileData.filename;
		fs.copyFile(fileData.sourcePath, destinationPath, function (err) {
			if (err) throw 'Filename: ' + fileData.filename + ' Error: ' + err;
			console.log(fileData.filename + ' file created');
		});
	}
};

/**
 *
 * @returns name and version of builder package json
 */
const GetBuilder = () => {
	const builderPackage = require('../../src/ui-builder/package.json');
	const name = builderPackage.name;
	const version = `^${builderPackage.version}`;
	return { name, version };
};

/**
 *
 * @param {String} directory - component directory
 * @param {String} packageName - package name
 * @param {String} packageDescription - - package description
 * @param {Array} peerDepsArray - if this is a react component
 * @param {Boolean} hasTests - add test scripts
 * @param {String} envName - value to replace env.name
 *
 * generate package json, adds relative scripts, and react dependencies
 */
const CreatePackageJson = ({
	directory,
	packageName,
	packageDescription,
	peerDepsArray = [],
	hasTests,
	envName,
}) => {
	// Get base package json config
	let packageConfig = require('../files/component.package.config');

	// create library name alias
	const { aliasName } = CreateAlias({ packageName, directory });

	// update name and description
	packageConfig.name = aliasName;
	packageConfig.description = packageDescription;

	// generate path from current directory to root config file
	let pathToRoot = GeneratePathToRoot(directory);

	packageConfig.scripts['build-webpack'] = packageConfig.scripts[
		'build-webpack'
	].replace('webpack.config.js', pathToRoot + 'webpack.config.js');

	// add peer dependencies for react
	if (peerDepsArray.length) {
		for (let index = 0; index < peerDepsArray.length; index++) {
			const dep = peerDepsArray[index];

			packageConfig.peerDependencies[dep.name] = dep.version;
		}
	}

	if (!hasTests) {
		packageConfig.scripts.test = `echo \"Error: no test specified\"`;
		packageConfig.scripts['test:watch'] = `echo \"Error: no test specified\"`;
	}

	// Update main and module names

	packageConfig.main = packageConfig.main.replace('{name}', packageName);
	packageConfig.module = packageConfig.module.replace('{name}', packageName);

	// update name env
	packageConfig.scripts['build-webpack'] = packageConfig.scripts[
		'build-webpack'
	].replace('--env.name=', envName);
	packageConfig.scripts.watch = packageConfig.scripts.watch.replace(
		'--env.name=',
		envName
	);
	packageConfig.scripts.start = packageConfig.scripts.start.replace(
		'--env.name=',
		envName
	);

	// add builder
	const { name, version } = GetBuilder();

	packageConfig.devDependencies[name] = version;
	const packageJsonFilePath = directory + '/package.json';

	fs.writeFile(packageJsonFilePath, JSON.stringify(packageConfig), (err) => {
		// throws an error, you could also catch it here
		if (err) throw err;

		// success case, the file was saved
		console.log(`${packageJsonFilePath} - created successfully`);
	});
};

module.exports = {
	CheckDirectoryExists,
	CreateAlias,
	CopyWriteFiles,
	CreateWebpackDevConfig,
	GeneratePathToRoot,
	GetBuilder,
	CreatePackageJson,
};
