const fs = require('fs');
var inquirer = require('inquirer');
const {
	CheckDirectoryExists,
	GeneratePathToRoot,
	CreateWebpackDevConfig,
	CopyWriteFiles,
	CreatePackageJson,
} = require('./helper_methods');

const reactPeerDeps = [
	{
		name: 'react',
		version: '^16.13.1',
	},
	{
		name: 'react-dom',
		version: '^16.13.1',
	},
];
inquirer
	.prompt([
		/* Pass your questions in here */
		{
			name: 'componentName',
			message: 'What is the name of the component? ',
			validate: function (input) {
				const regex = /^[A-Za-z]+$/;

				var done = this.async();

				// Do async stuff
				setTimeout(function () {
					if (!regex.test(input)) {
						// Pass the return value in the done callback
						done('Component name must only include letters.');
						return;
					}
					// Pass the return value in the done callback
					done(null, true);
				}, 100);
			},
		},
		{
			name: 'componentDirectory',
			type: 'list',
			message: 'What type of component is it? ',
			choices: [
				{ name: 'atom', value: 'components/atoms/' },
				{ name: 'molecule', value: 'components/molecules/' },
				{ name: 'organism', value: 'components/organisms/' },
				{
					name: 'context',
					value: 'context/',
				},
				{ name: 'hook', value: 'hooks/' },
				{ name: 'util', value: 'utils/' },
			],
		},
		{
			name: 'hasStory',
			message: 'Add to storybook?',
			type: 'confirm',
		},
		{
			name: 'packageName',
			message:
				'A package.json file will be created, please provide a package name: ',
			validate: function (input) {
				const regex = /^[a-z]+(-[a-z]+)*$/;

				var done = this.async();

				// Do async stuff
				setTimeout(function () {
					if (!regex.test(input)) {
						// Pass the return value in the done callback
						done(
							'Package name must only include lowercase characters and words separated by dashes.'
						);
						return;
					}
					// Pass the return value in the done callback
					done(null, true);
				}, 100);
			},
		},
		{
			name: 'packageDescription',
			message: 'Package description?',
		},
	])
	.then((answers) => {
		const {
			componentDirectory,
			componentName,
			packageName,
			packageDescription,
			hasStory,
		} = answers;

		let isReact = true;
		let isHook = false;
		// some hooks may or may not use react.
		if (componentDirectory === 'hooks/' || componentDirectory === 'utils/') {
			isReact = false;

			if (componentDirectory === 'hooks/') {
				isHook = true;
			}
		}

		console.table(answers);
		console.log(`Building component: ${componentName}`);

		const rootPath = './src/' + componentDirectory;
		// check component directory exists
		CheckDirectoryExists({
			directory: rootPath,
			createDirectory: true,
			bypass: true,
		});
		const directory = rootPath + componentName;
		// check if directory already exists - error out if it does
		CheckDirectoryExists({
			directory,
			createDirectory: true,
			bypass: false,
		});
		// build directories and sub directories
		buildSubDirectories({ directory });
		// create root files
		writeRootFiles(directory);

		// add files
		addFiles({
			directory,
			componentName,
			isReact,
		});

		// create package json
		CreatePackageJson({
			directory,
			packageName,
			packageDescription,
			hasTests: !isHook,
			peerDepsArray: isReact ? reactPeerDeps : [],
			envName: `--env.name=${componentName}`,
		});

		//create webpack config
		CreateWebpackDevConfig(directory);

		if (hasStory) {
			addToStorybook({ directory, componentName, componentDirectory });
		}
	})
	.catch((error) => {
		if (error.isTtyError) {
			// Prompt couldn't be rendered in the current environment
		} else {
			// Something else went wrong
			console.error(error);
		}
	});

/**
 *
 * @param {String} directory - directory to use
 *
 *
 */
const buildSubDirectories = ({ directory }) => {
	// create sub directories
	fs.mkdirSync(directory + '/src');
	fs.mkdirSync(directory + '/tests');
	fs.mkdirSync(directory + '/.jest');
};

// write package root files

const rootConfig = [
	{
		filename: '.npmignore',
		sourcePath: './config/files/.npmignore',
	},
	{
		filename: '.jest/bootstrap.js',
		sourcePath: './.jest/bootstrap.js',
	},
];
const writeRootFiles = (directory) => {
	CopyWriteFiles({ rootConfig, directory });
	// add .jest.config.js

	let jestConfigPath = directory + '/jest.config.js';

	let pathToRoot = GeneratePathToRoot(directory);
	let content = `const base = require('${pathToRoot}.jest/jest.config.base.js');
	const pack = require('./package');
	
	module.exports = {
		...base,
		name: pack.name,
		displayName: pack.name,
	};
	`;

	fs.writeFile(jestConfigPath, content, (err) => {
		// throws an error, you could also catch it here
		if (err) throw err;

		// success case, the file was saved
		console.log(`${jestConfigPath} - created successfully`);
	});
};

/**
 *
 * @param {String} directory - component directory
 * @param {String} componentName - component name
 * @param {Boolean} isReact - if it is a react component
 */
const addFiles = ({ directory, componentName, isReact }) => {
	// add source files
	let indexPath = directory + '/src/index.js';
	let componentPath = directory + '/src/' + componentName + '.js';

	let regularFunction =
		`const ${componentName} = () => {};\n` + `export default ${componentName};`;

	let reactFunction =
		`\nconst ${componentName} = () => { return <div></div> };\n\n` +
		`${componentName}.propTypes= {};\n\n` +
		`${componentName}.defaultProps= {};\n\n` +
		`export default ${componentName};\n`;

	let reactImport = "import React from 'react';\n";

	let propTypeImport = "import PropTypes from 'prop-types';\n";

	let componentContent = regularFunction;

	if (isReact) {
		componentContent = reactImport + propTypeImport + reactFunction;
	}

	let indexContent = `export { default as ${componentName} } from './${componentName}';`;

	fs.writeFile(componentPath, componentContent, (err) => {
		// throws an error, you could also catch it here
		if (err) throw err;

		// success case, the file was saved
		console.log(`${componentPath} - created successfully`);
	});

	fs.writeFile(indexPath, indexContent, (err) => {
		// throws an error, you could also catch it here
		if (err) throw err;

		// success case, the file was saved
		console.log(`${indexPath} - created successfully`);
	});

	// add test files

	let testFilePath = directory + '/tests/' + componentName + '.test.js';
	let componentImportPath = `import ${componentName} from '../src/${componentName}';\n`;
	let enzyme = `import { shallow } from 'enzyme';\nimport toJson from 'enzyme-to-json';\n`;
	let testName = `${componentName} test`;
	let snapshotTest = `Creates snapshot test for <${componentName} />`;

	let describeContent = `\ndescribe('${testName}', () => {});`;
	let reactSnapshot = `\ndescribe('${testName}', () => {
        it('${snapshotTest}', () => {
            const wrapper = shallow(<${componentName}></${componentName}>);
            expect(toJson(wrapper)).toMatchSnapshot();
        });
    });`;

	let testContent = componentImportPath + describeContent;

	if (isReact) {
		testContent = componentImportPath + enzyme + reactImport + reactSnapshot;
	}

	fs.writeFile(testFilePath, testContent, (err) => {
		// throws an error, you could also catch it here
		if (err) throw err;

		// success case, the file was saved
		console.log(`${testFilePath} - created successfully`);
	});
};

/**
 *
 * @param {String} directory - directory to component
 */
const addToStorybook = ({ directory, componentName, componentDirectory }) => {
	let folder;
	switch (componentDirectory) {
		case 'components/atoms/':
			folder = 'Atoms';
			break;
		case 'components/molecules/':
			folder = 'Molecules';
			break;
		case 'components/organisms/':
			folder = 'Organisms';
			break;
		case 'utils/':
			folder = 'Utils';
			break;

		default:
			folder = null;
			break;
	}

	if (folder !== null) {
		console.log('Adding storybook file');

		fs.mkdirSync(directory + '/stories');
		const storyScript = `import React from 'react';
import ${componentName} from '../src/${componentName}';

export default {
	title: '${folder}/${componentName}',
	parameters: {
		component: ${componentName},
	},
}

export const Default = () => <${componentName} />;
`;
		const storyPath = `${directory}/stories/${componentName}.stories.js`;

		fs.writeFile(storyPath, storyScript, (err) => {
			// throws an error, you could also catch it here
			if (err) throw err;

			// success case, the file was saved
			console.log(`${storyPath} - created successfully`);
		});
	}
};
