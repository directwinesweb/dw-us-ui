Knowledge:
@lerna/bootstrap - https://github.com/lerna/lerna/tree/main/commands/bootstrap#readme
Link local packages together and install remaining package dependencies

https://devdojo.com/devdojo/what-is-a-symlink
https://medium.com/dailyjs/how-to-use-npm-link-7375b6219557
Aliases -
https://arunmichaeldsouza.com/blog/aliasing-module-paths-in-node-js
https://www.npmjs.com/package/link-module-alias

# To develop a new component:

1. `npm run add-component` - script will ask you for information regarding component and will build the component file structure.
2. Manually add the project alias (package.json -> name) to `/webpack/aliases.config.js` or the appropriate alias file `/webpack/aliases/`;
3. Manually add peer, dev, and dependencies to component.
4. If the component depends on another component: https://www.npmjs.com/package/@lerna/add
   - Install module-1 to module-2
     `lerna add module-1 --scope=module-2`
   - Install module-1 to module-2 in devDependencies
     `lerna add module-1 --scope=module-2 --dev`
   - Install module-1 to module-2 in peerDependencies
     `lerna add module-1 --scope=module-2 --peer`
5. If you would like to export the component as part of the `main-bundle`
   - run `lerna add @dw-us-ui/your-new-component --scope=@dw-us-ui/main-bundle`

# To develope using the playground:

Sometimes you will want to develop the component natively to dw-us-ui. A playground is the place to do that.

1. First make sure that the component alias was added to `./webpack/aliases.config.js`
2. Then add the component to `./playground/index.js`
3. run `npm run start-playground`

# To hot reload a new component across apps:

If a component depends on another component you can start the dev server on that component, essentially have a tree of dev servers running.

root `npm run dev -- --scope project-name`
example: `npm run dev -- --scope @dw-us-ui/button`

To Serve alongside the consumer repository:
`npm link` - this will create a local symlink for dw-us-ui
In consumer repository `npm link dw-us-ui`
_Make_ sure that the consumer repository is running dev server
in dw-us-ui run a scoped `npm run lerna-dev` to see the component hot serve.

After you finish dev work you will need To UNLINK
`npm unlink --no-save dw-us-ui` in consumer application
`npm unlink` in dw-us-ui

# To Build a scoped component

root : `npm run build:components -- --scope project-name`
example: `npm run build:components -- --scope @dw-us-ui/use-custom-theme`

Research:
https://github.com/facebook/react/issues/13991
https://webpack.js.org/configuration/externals/
https://github.com/lerna/lerna

List all component package names: https://github.com/lerna/lerna/tree/main/commands/list#readme
`lerna list`
Show dependency graph:
Will show components and their dependencies
`lerna ls --graph`

# Testing:

You can run tests across the whole dw-us-ui repository or just a single component using `scope`.

`npm run test` - jest will run all test files inside root ./src folder
`npm run test -- --scope project-name`

`npm run test:watch`- jest will run all watch test files inside root ./src folder (good for development) will update based on files you have updated.
`npm run test:watch -- --scope project-name`

# Versioning component:

First - commit changes with commit message pertaining to the files that were modified per component.
So if you modify files in the button component - commit those files and commit message will have fix or feat to match the files changed. The Changelog for the button component will include the commit message you specified.

https://github.com/lerna/lerna/tree/main/commands/version

`lerna version --conventional-commits`
