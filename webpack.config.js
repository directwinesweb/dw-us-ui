const webpack = require('webpack');

module.exports = (env) => {
	const config = require(`./webpack/webpack.config.${env.mode}.js`)(env);

	config.plugins.push(
		new webpack.DefinePlugin({
			WEBPACK_BRAND_TAG: JSON.stringify(env.brandTag),
		})
	);

	return config;
};
