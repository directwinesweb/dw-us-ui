import { BrandThemeProvider } from '@dw-us-ui/brand-theme-provider';
import { PageLayerProvider } from '@dw-us-ui/pagelayer-provider';
import { ProfileProvider } from '@dw-us-ui/profile-provider';
import Container from './container';
import React from 'react';
import ReactDOM from 'react-dom';
import { StateSelectorModal } from '../src/components/organisms/Modals/src';
const Playground = (props) => {
	return (
		// <BrandThemeProvider brandTag={'npr'}>
		// 	<CartProvider>
		// 		<Seo title='playground-tm' />
		// 		Welcome to the UI Playground!
		// 		<Container />
		// 	</CartProvider>
		// </BrandThemeProvider>
		<BrandThemeProvider
			brandTag={
				// eslint-disable-next-line no-undef
				WEBPACK_BRAND_TAG
			}
		>
			<PageLayerProvider>
				<ProfileProvider>
					<StateSelectorModal isOfferRedirector={true} />
				</ProfileProvider>
			</PageLayerProvider>
		</BrandThemeProvider>
	);
};

export default Playground;
ReactDOM.render(<Playground />, document.getElementById('dw-ui-playground'));
