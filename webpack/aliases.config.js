const path = require('path');
const { atomAliases } = require('./aliases/atoms');
const { hooksAliases } = require('./aliases/hooks');
const { moleculesAliases } = require('./aliases/molecules');
const { organismsAliases } = require('./aliases/organisms');
const { utilsAliases } = require('./aliases/utils');
const { bundleAliases } = require('./aliases/bundles');
// __dirname gives exact path where the file is, in this case: PROJECT/configs. It is needed to add ../ at the beginning of the aliases to solve correct path.

const aliases = {
	'@dw-us-ui/themes': path.resolve(__dirname, '../src/themes/src/index'),
	'@dw-us-ui/constants': path.resolve(__dirname, '../src/constants/src/index'),

	'@dw-us-ui/brand-theme-provider': path.resolve(
		__dirname,
		'../src/context/BrandThemeProvider/src/index'
	),
	'@dw-us-ui/cart-provider': path.resolve(
		__dirname,
		'../src/context/CartProvider/src/index'
	),
	'@dw-us-ui/pagelayer-provider': path.resolve(
		__dirname,
		'../src/context/PageLayerProvider/src/index'
	),
	'@dw-us-ui/profile-provider': path.resolve(
		__dirname,
		'../src/context/ProfileProvider/src/index'
	),

	...atomAliases,
	...bundleAliases,
	...hooksAliases,
	...moleculesAliases,
	...organismsAliases,
	...utilsAliases,
};

module.exports = { aliases };
