const devCookie = 'hEHHQ72a-YTiYtvRpH8U5Kgsw3_zPv3se4zlK6G6.node3';
const brandDomain = 'www.laithwaiteswine.com';
const HtmlWebpackPlugin = require('html-webpack-plugin');

const version = process.env.npm_package_version;
module.exports = ({ injectHotServe, mode }) => {
	const config = require('./webpack.config.base')({ mode });

	const useHotServeSettings = injectHotServe === 'true';
	/*
    	Dev Server
	*/

	const proxy = {
		'/api': {
			target: `https://${brandDomain}/`,
			secure: false,
			logLevel: 'debug',
			changeOrigin: true,
			headers: {
				host: brandDomain,
				Origin: `https://${brandDomain}`,
				Cookie: `JSESSIONID=${devCookie}`,
			},
		},
		'/jsp': {
			target: `https://${brandDomain}`,
			secure: false,
			logLevel: 'debug',
			changeOrigin: true,
			headers: {
				host: brandDomain,
				Origin: `https://${brandDomain}`,
				Cookie: `JSESSIONID=${devCookie}`,
			},
		},
		'/images': {
			target: `https://${brandDomain}/`,
			secure: false,
			logLevel: 'debug',
			changeOrigin: true,
		},
		'/json': {
			target: `https://${brandDomain}/`,
			secure: false,
			logLevel: 'debug',
			changeOrigin: true,
		},
	};
	const devOutput = {
		publicPath: 'http://localhost:5050/apps/us/customer_service/dist/',
		filename: `[name]._v${version}.min.js`,
		chunkFilename: `[name]._v${version}.min.js`,
		jsonpFunction: 'customerServiceApp',
	};

	/*
	Webpack-Dev-Server config
    	Dev Tool
	*/
	config.devtool = 'inline-source-map';
	config.mode = 'development';
	config.devServer = {
		port: 8084,
		contentBase: './dist',
		hot: true,
		disableHostCheck: false,
		open: !useHotServeSettings,
	};

	if (useHotServeSettings) {
		config.output = devOutput;
	} else {
		config.devServer.proxy = proxy;
	}

	config.devServer.publicPath = config.output.publicPath;

	config.plugins.push(
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: './playground/index.html',
		})
	);

	return config;
};
