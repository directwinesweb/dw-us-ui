const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const version = process.env.npm_package_version;
const { aliases } = require('./aliases.config');
require('@babel/polyfill');

module.exports = ({ mode }) => {
	const config = {
		entry: [
			'@babel/polyfill',
			'react-hot-loader/patch',
			'./playground/index.js',
		],
		output: {
			path: path.resolve(__dirname, 'dist/'),
			filename: `[name].bundle_v${version}.js`,
		},
		module: {
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude: /node_modules/,
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env', '@babel/react'],
					},
				},
				{
					exclude: /node_modules/,
					test: /\.(sa|sc|c)ss$/,
					use: [
						mode === 'dev' ? 'style-loader' : MiniCssExtractPlugin.loader,
						'css-loader',
						'sass-loader',
					],
				},
				{
					test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
					use: {
						loader: 'url-loader',
						options: {
							esModule: false,
							fallback: 'file-loader',
							outputPath: '../../assets/',
							publicPath: 'assets/',
						},
					},
				},
			],
		},
		resolve: {
			extensions: ['*', '.js', '.jsx'],
			alias: {
				'react-dom': '@hot-loader/react-dom',
				...aliases,
			},
			modules: ['node_modules'],
			symlinks: true,
		},
		plugins: [
			new CleanWebpackPlugin(),
			new MiniCssExtractPlugin({
				// Options similar to the same options in webpackOptions.output
				// both options are optional
				filename: `[name]._v${version}.css`,
				chunkFilename: `[name]._v${version}.css`,
			}),
		],
	};

	return config;
};
