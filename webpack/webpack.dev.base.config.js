const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack');
module.exports = (env) => {
	const libraryName = env.name ? env.name : '';

	const libraryTarget = env.name || env.bundle === 'true' ? 'umd' : 'commonjs2';

	const config = {
		entry: './src/index.js',
		output: {
			filename: 'index.js',
			library: '',
			libraryTarget: 'commonjs2',
		},
		externals: {
			react: 'commonjs react',
			'react-dom': 'commonjs react-dom',
			'@emotion/core': 'commonjs @emotion/core',
			'@emotion/styled': 'commonjs @emotion/styled',
			'emotion-theming': 'commonjs emotion-theming',
			contentful: 'commonjs contentful',
		},
		module: {
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude: /node_modules/,
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env', '@babel/react'],
					},
				},
				{
					exclude: /node_modules/,
					test: /\.(sa|sc|c)ss$/,
					use: ['style-loader', 'css-loader', 'sass-loader'],
				},
				{
					test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
					use: {
						loader: 'url-loader',
						options: {
							esModule: false,
							fallback: 'file-loader',
							outputPath: '../../assets/',
							publicPath: 'assets/',
						},
					},
				},
			],
		},
		plugins: [
			new CleanWebpackPlugin(),
			new webpack.DefinePlugin({
				WEBPACK_BRAND_TAG: JSON.stringify(env.brandTag),
			}),
		],
		devtool: 'inline-source-map',
		mode: 'development',
		devServer: {
			contentBase: './dist',
			hot: true,
			port: 8085,
		},
		resolve: {
			extensions: ['*', '.js', '.jsx'],
			alias: {
				'react-dom': '@hot-loader/react-dom',
				react: path.resolve('./node_modules/react'),
			},
			modules: ['node_modules'],
			symlinks: true,
		},
	};
	return config;
};
