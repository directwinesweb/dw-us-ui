const pkg = require('../package.json');
const main = pkg.main;
module.exports = ({ mode, name, bundle }) => {
	const config = require('./webpack.config.base')({ mode });

	const libraryName = name ? name : '';

	const libraryTarget = name || bundle === 'true' ? 'umd' : 'commonjs2';

	config.entry = './src/index.js';
	config.output = {
		filename: main,
		library: libraryName,
		libraryTarget: 'commonjs2',
	};
	config.mode = 'production';
	config.externals = {
		react: 'commonjs react',
		'react-dom': 'commonjs react-dom',
		'@emotion/core': 'commonjs @emotion/core',
		'@emotion/styled': 'commonjs @emotion/styled',
		'emotion-theming': 'commonjs emotion-theming',
		contentful: 'commonjs contentful',
	};
	if (name) {
		console.log('library name: ', config.output.library);
	}
	return config;
};
