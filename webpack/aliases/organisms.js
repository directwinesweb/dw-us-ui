const path = require('path');

const aliases = {
	'@dw-us-ui/estimated-delivery-date': path.resolve(
		__dirname,
		'../../src/components/organisms/EstimatedDeliveryDate/src/index'
	),
	'@dw-us-ui/modals': path.resolve(
		__dirname,
		'../../src/components/organisms/Modals/src/index'
	),
	'@dw-us-ui/order-summary': path.resolve(
		__dirname,
		'../../src/components/organisms/OrderSummary/src/index'
	),
	'@dw-us-ui/product-pricing': path.resolve(
		__dirname,
		'../../src/components/molecules/ProductPricing/src/index'
	),
	'@dw-us-ui/product-targeter': path.resolve(
		__dirname,
		'../../src/components/molecules/ProductTargeter/src/index'
	),
	'@dw-us-ui/seo': path.resolve(
		__dirname,
		'../../src/components/organisms/Seo/src/index'
	),
	'@dw-us-ui/step-wizard': path.resolve(
		__dirname,
		'../../src/components/organisms/StepWizard/src/index'
	),
	'@dw-us-ui/theme-wrapper': path.resolve(
		__dirname,
		'../../src/components/organisms/ThemeWrapper/src/index'
	),
	'@dw-us-ui/user-info-display-cards': path.resolve(
		__dirname,
		'../../src/components/organisms/UserInfoDisplayCards/src/index'
	),
};
module.exports = { organismsAliases: aliases };
