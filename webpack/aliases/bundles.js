const path = require('path');

const aliases = {
	'dw-us-ui/main-bundle': path.resolve(__dirname, '../src/bundles/main-bundle'),
	'@dw-us-ui/laithwaites-bundle': path.resolve(
		__dirname,
		'../../src/bundles/laithwaites-bundle'
	),
	'@dw-us-ui/hal-bundle': path.resolve(
		__dirname,
		'../../src/bundles/hal-bundle'
	),
	'@dw-us-ui/cms-content-bundle': path.resolve(
		__dirname,
		'../../src/bundles/cms-content-bundle'
	),
	'@dw-us-ui/icons': path.resolve(__dirname, '../../src/bundles/icons'),
	'@dw-us-ui/checkout-bundle': path.resolve(
		__dirname,
		'../../src/bundles/checkout-bundle'
	),
	'@dw-us-ui/customer-service-bundle': path.resolve(
		__dirname,
		'../../src/bundles/customer-service-bundle'
	),
	'@dw-us-ui/wsj-homepage-bundle': path.resolve(
		__dirname,
		'../../src/bundles/wsj-homepage-bundle'
	),
	'@dw-us-ui/product-bundle': path.resolve(
		__dirname,
		'../../src/bundles/product-bundle'
	),
	'@dw-us-ui/form-utils': path.resolve(
		__dirname,
		'../../src/bundles/form-utils'
	),
};
module.exports = { bundleAliases: aliases };
