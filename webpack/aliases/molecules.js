const path = require('path');

const aliases = {
	'@dw-us-ui/bottle-zoom': path.resolve(
		__dirname,
		'../../src/components/molecules/BottleZoom/src/index'
	),
	'@dw-us-ui/cart-icon': path.resolve(
		__dirname,
		'../../src/components/molecules/CartIcon/src/index'
	),
	'@dw-us-ui/case-rating': path.resolve(
		__dirname,
		'../../src/components/atoms/CaseRating/src/index'
	),
	'@dw-us-ui/description-tab': path.resolve(
		__dirname,
		'../../src/components/molecules/DescriptionTab/src/index'
	),
	'@dw-us-ui/flavor-profiles': path.resolve(
		__dirname,
		'../../src/components/molecules/FlavorProfiles/src/index'
	),
	'@dw-us-ui/food-pairings': path.resolve(
		__dirname,
		'../../src/components/molecules/FoodPairings/src/index'
	),
	'@dw-us-ui/image-zoom': path.resolve(
		__dirname,
		'../../src/components/molecules/ImageZoom/src/index'
	),
	'@dw-us-ui/live-chat': path.resolve(
		__dirname,
		'../../src/components/LiveChat/src/index'
	),
	'@dw-us-ui/product-details-display': path.resolve(
		__dirname,
		'../../src/components/molecules/ProductDetailsDisplay/src/index'
	),
	'@dw-us-ui/product-headline': path.resolve(
		__dirname,
		'../../src/components/molecules/ProductHeadline/src/index'
	),
	'@dw-us-ui/product-rating-stars': path.resolve(
		__dirname,
		'../../src/components/molecules/ProductRatingStars/src/index'
	),
	'@dw-us-ui/product-unlimited-upsell': path.resolve(
		__dirname,
		'../../src/components/molecules/ProductUnlimitedUpsell/src/index'
	),
	'@dw-us-ui/searchbar-input': path.resolve(
		__dirname,
		'../../src/components/molecules/SearchbarInput/src/index'
	),
	'@dw-us-ui/single-bottle-rating': path.resolve(
		__dirname,
		'../../src/components/molecules/SingleBottleRating/src/index'
	),
	'@dw-us-ui/tab': path.resolve(
		__dirname,
		'../../src/components/molecules/Tab/src/index'
	),
	'@dw-us-ui/tooltip': path.resolve(
		__dirname,
		'../../src/components/molecules/Tooltip/src/index'
	),
};
module.exports = { moleculesAliases: aliases };
