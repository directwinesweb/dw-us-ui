const path = require('path');

const aliases = {
	'@dw-us-ui/brand-util': path.resolve(
		__dirname,
		'../../src/utils/BrandUtil/src/index'
	),
	'@dw-us-ui/check-properties': path.resolve(
		__dirname,
		'../../src/utils/CheckProperties/src/index'
	),
	'@dw-us-ui/check-key-input': path.resolve(
		__dirname,
		'../../src/utils/CheckKeyInput/src/index'
	),
	'@dw-us-ui/create-case-summary': path.resolve(
		__dirname,
		'../../src/utils/CreateCaseSummary/src/index'
	),
	'@dw-us-ui/emotion-breakpoint': path.resolve(
		__dirname,
		'../../src/utils/EmotionBreakpoint/src/index'
	),
	'@dw-us-ui/food-pairing-utils': path.resolve(
		__dirname,
		'../../src/components/molecules/FoodPairingUtils/src/index'
	),
	'@dw-us-ui/format-date': path.resolve(
		__dirname,
		'../../src/utils/FormatDate/src/index'
	),
	'@dw-us-ui/format-phone': path.resolve(
		__dirname,
		'../../src/utils/FormatPhone/src/index'
	),
	'@dw-us-ui/format-price': path.resolve(
		__dirname,
		'../../src/utils/FormatPrice/src/index'
	),
	'@dw-us-ui/format-sku-data': path.resolve(
		__dirname,
		'../../src/utils/FormatSkuData/src/index'
	),
	'@dw-us-ui/get-card-info': path.resolve(
		__dirname,
		'../../src/utils/GetCardInfo/src/index'
	),
	'@dw-us-ui/get-environment': path.resolve(
		__dirname,
		'../../src/utils/GetEnvironment/src/index'
	),
	'@dw-us-ui/get-param': path.resolve(
		__dirname,
		'../../src/utils/GetParam/src/index'
	),
	'@dw-us-ui/get-retailer': path.resolve(
		__dirname,
		'../../src/utils/GetRetailer/src/index'
	),
	'@dw-us-ui/get-state-data': path.resolve(
		__dirname,
		'../../src/utils/GetStateData/src/index'
	),
	'@dw-us-ui/google-analytics': path.resolve(
		__dirname,
		'../../src/utils/GoogleAnalytics/src/index'
	),
	'@dw-us-ui/handle-track-js-error': path.resolve(
		__dirname,
		'../../src/utils/HandleTrackJSError/src/index'
	),
	'@dw-us-ui/mini-cart': path.resolve(
		__dirname,
		'../../src/components/molecules/MiniCart/src/index'
	),
	'@dw-us-ui/obscure-credit': path.resolve(
		__dirname,
		'../../src/utils/ObscureCredit/src/index'
	),
	'@dw-us-ui/targeted-product-validity': path.resolve(
		__dirname,
		'../../src/components/molecules/TargetedProductValidity/src/index'
	),
	'@dw-us-ui/targeted-user-type': path.resolve(
		__dirname,
		'../../src/components/molecules/TargetedUserType/src/index'
	),
	'@dw-us-ui/unilimited-config': path.resolve(
		__dirname,
		'../../src/components/molecules/UnlimitedConfig/src/index'
	),
};
module.exports = { utilsAliases: aliases };
