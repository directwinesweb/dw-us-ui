const path = require('path');

const aliases = {
	'@dw-us-ui/button': path.resolve(
		__dirname,
		'../../src/components/atoms/Button/src/index'
	),
	'@dw-us-ui/case-summary': path.resolve(
		__dirname,
		'../../src/components/atoms/CaseSummary/src/index'
	),
	'@dw-us-ui/compliance-alert-message': path.resolve(
		__dirname,
		'../../src/components/atoms/ComplianceAlertMessage/src/index'
	),
	'@dw-us-ui/details-tab': path.resolve(
		__dirname,
		'../../src/components/atoms/DetailsTab/src/index'
	),
	'@dw-us-ui/display-message': path.resolve(
		__dirname,
		'../../src/components/atoms/DisplayMessage/src/index'
	),
	'@dw-us-ui/food-pairing-error-message': path.resolve(
		__dirname,
		'../../src/components/atoms/FoodPairingErrorMessage/src/index'
	),
	'@dw-us-ui/loading-icon': path.resolve(
		__dirname,
		'../../src/components/atoms/LoadingIcon/src/index'
	),

	'@dw-us-ui/privacy-policy-content': path.resolve(
		__dirname,
		'../../src/components/atoms/PrivacyPolicyContent/src/index'
	),
	'@dw-us-ui/product-attributes': path.resolve(
		__dirname,
		'../../src/components/atoms/ProductAttributes/src/index'
	),
	'@dw-us-ui/product-description': path.resolve(
		__dirname,
		'../../src/components/atoms/ProductDescription/src/index'
	),
	'@dw-us-ui/review-stars': path.resolve(
		__dirname,
		'../../src/components/atoms/ReviewStars/src/index'
	),
	'@dw-us-ui/terms-conditions-content': path.resolve(
		__dirname,
		'../../src/components/atoms/TermsConditionsContent/src/index'
	),
	'@dw-us-ui/unlimited-terms': path.resolve(
		__dirname,
		'../../src/components/atoms/UnlimitedTerms/src/index'
	),
};
module.exports = { atomAliases: aliases };
