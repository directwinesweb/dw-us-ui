const path = require('path');

const aliases = {
	'@dw-us-ui/use-axios': path.resolve(
		__dirname,
		'../../src/hooks/useAxios/src/index'
	),
	'@dw-us-ui/use-case-data': path.resolve(
		__dirname,
		'../../src/hooks/useCaseData/src/index'
	),
	'@dw-us-ui/use-combined-refs': path.resolve(
		__dirname,
		'../../src/hooks/useCombinedRefs/src/index'
	),
	'@dw-us-ui/use-contentful': path.resolve(
		__dirname,
		'../../src/hooks/useContentful/src/index'
	),
	'@dw-us-ui/use-custom-theme': path.resolve(
		__dirname,
		'../../src/hooks/useCustomTheme/src/index'
	),
	'@dw-us-ui/use-email-validator': path.resolve(
		__dirname,
		'../../src/hooks/useEmailValidator/src/index'
	),
	'@dw-us-ui/use-event-listener': path.resolve(
		__dirname,
		'../../src/hooks/useEventListener/src/index'
	),
	'@dw-us-ui/use-focus': path.resolve(
		__dirname,
		'../../src/hooks/useFocus/src/index'
	),
	'@dw-us-ui/use-product-contentful': path.resolve(
		__dirname,
		'../../src/hooks/useProductContentful/src/index'
	),
	'@dw-us-ui/use-item-data': path.resolve(
		__dirname,
		'../../src/hooks/useItemData/src/index'
	),
	'@dw-us-ui/use-image-onload': path.resolve(
		__dirname,
		'../../src/hooks/useImageOnLoad/src/index'
	),
	'@dw-us-ui/use-lockbody-scroll': path.resolve(
		__dirname,
		'../../src/hooks/useLockBodyScroll/src/index'
	),
	'@dw-us-ui/use-login': path.resolve(
		__dirname,
		'../../src/hooks/useLogin/src/index'
	),
	'@dw-us-ui/use-is-mobile': path.resolve(
		__dirname,
		'../../src/hooks/useIsMobile/src/index'
	),
	'@dw-us-ui/use-onclick-outside': path.resolve(
		__dirname,
		'../../src/hooks/useOnClickOutside/src/index'
	),
	'@dw-us-ui/use-previous': path.resolve(
		__dirname,
		'../../src/hooks/usePrevious/src/index'
	),
	'@dw-us-ui/use-window-width': path.resolve(
		__dirname,
		'../../src/hooks/useWindowWidth/src/index'
	),
	'@dw-us-ui/use-terms-data': path.resolve(
		__dirname,
		'../../src/hooks/useTermsData/src/index'
	),
	'@dw-us-ui/use-state-profile': path.resolve(
		__dirname,
		'../../src/hooks/useStateProfile/src/index'
	),
	'@dw-us-ui/use-unlimited-info': path.resolve(
		__dirname,
		'../../src/hooks/useUnlimitedInfo/src/index'
	),
	'@dw-us-ui/use-zip-data': path.resolve(
		__dirname,
		'../../src/hooks/useZipData/src/index'
	),
};
module.exports = { hooksAliases: aliases };
